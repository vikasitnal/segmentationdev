var lodash = require('lodash');
var CryptoJS = require("crypto-js");
var config = require('config');
var utility = require('./../modules/all');
(function(adminConfigModel) {
	var Q = require('q');
	var self;
	adminConfigModel.init = function() {
		self = arguments[0];
		return this;
	}



	/*New codes*/

	adminConfigModel.getEmailField = function(id) {
		var deferred = Q.defer();

        var getObjects = 'SELECT * FROM source_field_mapping where Source_Field_Mapping_Id = "' + id + '"';
        self.db.query(getObjects, function(result,error) {
            deferred.resolve(result);
		})

		return deferred.promise;
	}




	adminConfigModel.addEmailMapping = function(instanceId, id, name) {
		var deferred = Q.defer();
		var getObjects = 'SELECT * FROM source_field_mapping where Source_Object_Id = "' + id + '" AND Source_Object_Name = "'+ name +'"';
		self.db.query(getObjects)
		.then(function(result){
			if(result[0].length == 0)
			{
				var getEmailtobeMapped = 'SELECT emailFieldLabel, emailFieldName FROM system_table';
					getEmailtobeMapped+= '  WHERE Source_Instance_Config_Id = "' + instanceId + '"';
					getEmailtobeMapped+= '  AND emailStatus = 1 AND Object_Name = "' + name + '"';
				return self.db.query(getEmailtobeMapped)
			}
			else
			{
				deferred.resolve(result);
			}
		})
		.then(function(result){
			if(result)
			{
				var queryInsert = 'INSERT INTO source_field_mapping ';
					queryInsert+= ' (Source_Object_Name, Source_Object_Id,Source_Field_Name,Source_Field_Id,';
					queryInsert+= ' Source_Field_Type, Target_Field_Name,Target_Field_Id,';
					queryInsert+= ' Target_Field_Type)';
					queryInsert+= '  VALUES("'+ name +'",'
					queryInsert+= ' "'+ id +'",';
					queryInsert+= ' "'+ result[0][0].emailFieldLabel +' ('+ result[0][0].emailFieldName +')",';
					queryInsert+= ' "'+ result[0][0].emailFieldName +'",';
					queryInsert+= ' "email","Email Address", "C_EmailAddress","text")';

				self.db.query(queryInsert, function(result,error) {
					deferred.resolve(result);
				})
			}else {
				deferred.resolve(result);
			}
		});
		return deferred.promise;
	}


	adminConfigModel.addMapping = function(input) {
		var deferred = Q.defer();

		var queryInsert = 'INSERT INTO source_field_mapping ';
			queryInsert+= ' (Source_Object_Name,Query_Relationship, Source_Object_Id,Source_Field_Name,Source_Field_Id,';
			queryInsert+= ' Source_Field_Type, Target_Field_Name,Target_Field_Id,';
			queryInsert+= ' Target_Field_Type)';
			queryInsert+= '  VALUES("'+ input.Source_Object_Name +'",'
			queryInsert+= ' "'+ input.objectParentRelation +'",';
			queryInsert+= ' "'+ input.Source_Object_Id +'",';
			queryInsert+= ' "'+ input.Source_Field_Name +' ('+ input.Source_Field_Id +')",';
			queryInsert+= ' "'+ input.Source_Field_Id +'",';
			queryInsert+= ' "'+ input.Source_Field_Type +'",';
			queryInsert+= ' "'+ input.Target_Field_Name +'",';
			queryInsert+= ' "'+ input.Target_Field_Id +'",';
			queryInsert+= ' "'+ input.Target_Field_Type +'"';
			queryInsert+= ' )';

        self.db.query(queryInsert, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}


	adminConfigModel.getParentObjectFields = function(input) {
		var deferred = Q.defer();
		var query = 'SELECT Object_Json as json FROM system_table where ';
			query+= ' Source_Instance_Config_Id='+input.instanceConfigId;
			query+= ' AND Object_Name="'+input.objectName+'"';
			query+= ' AND SiteId="'+input.siteId+'"';
		self.db.query(query,function(result,error) {
			deferred.resolve(result[0]);
		});

		return deferred.promise;
	}



    adminConfigModel.deleteInstance = function(id) {
    	 var deferred = Q.defer();
		var stmt = 'SELECT * FROM source_instance_config WHERE Source_Instance_Config_Id="' + id + '"';
		self.db.query(stmt)
			.then(function(result){
				var instance = result[0][0];
				if(instance.Status == 'Failed' || instance.Status == 'New'){
					deleteSalesforceInstance(instance);
				} else {
					deferred.reject(new Error("Instanse is "+ instance.Status + ", Cannot delete '" + instance.Description + "'"));
				}
			});


		function deleteSalesforceInstance(instance) {
				condition = ' WHERE Source_Instance_Config_Id=' + instance.Source_Instance_Config_Id;
				var query = 'DELETE FROM source_instance_config ' + condition;
				self.db.query(query)
				.then(function(result) {
					deferred.resolve({
						status:200
					});
				}, function(err){
					deferred.reject(err);
				});

		}

		return deferred.promise;
    }



   try{
 adminConfigModel.postInstance = function(data, siteId) {
    	 var deferred = Q.defer();
		var stmt = 'SELECT * FROM source_instance_config WHERE User_name="' + data.username + '"';
		stmt+= ' AND Source_System_Id=' + data.SourceSystemId  ;
		stmt+= ' AND Site_Id=' + siteId ;
		self.db.query(stmt)
			.then(function(result){
				var instances = result[0];
				if(instances.length > 1){
				 return deferred.reject(new Error("Username already exists"));
				} else if(instances.length == 1) {
		           if(data.Source_Instance_Config_Id){
	           			if(instances[0].Source_Instance_Config_Id == data.Source_Instance_Config_Id){
      			        	updateSalesforceInstance(data);
						} else {
		                   return deferred.reject(new Error("Username already exists"));
						}
			        } else {
			          return deferred.reject(new Error("Username already exists"));
			        }
				} else {
					 if(data.Source_Instance_Config_Id){
      			         updateSalesforceInstance(data);
			        } else {
			        	 insertSalesforceInstance(data);
			        }
				}
			});
		function insertSalesforceInstance(data) {
			var hashedpassword = utility.encrypt(data.password, config.eloquaConfig.AppId );

			var columns = [
				'User_name',
				'Password',
				'Security_token',
				'Source_System_Instance_Id',
				'Description',
				'Source_System_Id',
				'Status',
				'Last_Sync_Date_Time',
				'Data_Sync_Status',
				'Site_Id',
				'Email'
			].join(",");

			var info = [
				data.username,
				hashedpassword.toString(),
				data.token,
				data.instance_type,
				data.description,
				data.SourceSystemId,
				data.status,
				'-----',
				'Not Started',
				siteId,
				data.email
			]
			.map(function(item,index) {
				return '"'+item+'"';
			})
			.join(",");




			var query = 'INSERT INTO source_instance_config';
				query+= ' ('+columns+') VALUES ('+info+')';

			self.db.query(query)
			.then(function(result) {
				if(result[0].affectedRows){
					deferred.resolve({
						status:100
					});
				}
			});
		}


		function updateSalesforceInstance(data) {
			var columns = [
				'User_name',
				'Security_token',
				'Source_System_Instance_Id',
				'Description',
				'Source_System_Id',
				'Status',
				'Site_Id',
				'Email'
			];

			var info = [
				data.username,
				data.token,
				data.instance_type,
				data.description,
				data.SourceSystemId,
				data.status,
				siteId,
				data.email
			]
        if(data.password && data.password.length > 0){
	       var hashedpassword = utility.encrypt(data.password, config.eloquaConfig.AppId );
          columns.push('Password');
          info.push(hashedpassword.toString());
 		  	}
			if(columns.length == info.length){
				var length = columns.length;
				var changes= '';
				for (var i = 0; i < length; i++) {
					if(i!=length-1)
						changes += columns[i] + "=" + '"'+info[i]+'", ';
					else
						changes += columns[i] + "=" + '"'+info[i]+'" ';
				}


				condition = ' WHERE Source_Instance_Config_Id=' + data.Source_Instance_Config_Id;



				var query = 'UPDATE source_instance_config SET '+changes+condition;
				self.db.query(query)
				.then(function(result) {
					deferred.resolve({
						status:200
					});
				}, function(err){
					deferred.reject(err);
				});



			}else{
				deferred.reject({
					status:0,
					error:'Unmatched columns'
				})
			}




		}

		return deferred.promise;
    }

   } catch(err){
   	console.log(err);
   }






    adminConfigModel.activateInstance = function(id) {
    	 var deferred = Q.defer();
		condition = ' WHERE Source_Instance_Config_Id=' + id;
		var query = 'UPDATE source_instance_config SET Status = "Active"' + condition;
		self.db.query(query)
		.then(function(result) {
			deferred.resolve({
				status:200
			});
		}, function(err){
			deferred.reject(err);
		});
		return deferred.promise;
    }


    adminConfigModel.deactivateInstance = function(id) {
    	 var deferred = Q.defer();
		condition = ' WHERE Source_Instance_Config_Id=' + id;
		var query = 'UPDATE source_instance_config SET Status = "Inactive"' + condition;
		self.db.query(query)
		.then(function(result) {
			deferred.resolve({
				status:200
			});
		}, function(err){
			deferred.reject(err);
		});
		return deferred.promise;
    }

    adminConfigModel.campaignDependencyExistence = function(data) {
		var deferred = Q.defer();
    	var query = 'SELECT fi.Campaign_Id as campaignId,fi.Status as campaignStatus, fi.Campaign_Name as campaignName FROM segmentation.filters f ';
    		query+= ' JOIN segmentation.segments s ON s.Segment_Id = f.Segment_Id';
    		query+= ' JOIN segmentation.feeder_instance fi ON fi.Segment_Id = s.Segment_Id';
    		query+= ' WHERE f.Master_Object = '+data.Source_Primary_Object_Id+' GROUP BY fi.Feeder_Instance_Id';  //AND fi.Status = "ACTIVATED"


        self.db.query(query, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;

    }

    adminConfigModel.campaignDependencyExistenceForSourceInstance = function(data) {
		var deferred = Q.defer();
    	var query = 'SELECT fi.Campaign_Id as campaignId,fi.Status as campaignStatus, fi.Campaign_Name as campaignName FROM segmentation.segments s';
    		query+= ' JOIN segmentation.feeder_instance f ON f.Segment_Id = s.Segment_Id';
    		query+= ' JOIN segmentation.feeder_instance fi ON fi.Segment_Id = s.Segment_Id';
    		query+= ' WHERE s.Source_Instance_Config_Id = '+data.CategoryId+' GROUP BY fi.Feeder_Instance_Id';  //AND fi.Status = "ACTIVATED"


        self.db.query(query, function(result,error) {
            deferred.resolve(result);
		});


		return deferred.promise;

    }




	// getting external source
	adminConfigModel.getExternalSourceSystem = function() {
		var deferred = Q.defer();

        var SourceSystem = 'SELECT * from source_system';
        self.db.query(SourceSystem, function(result,error) {
            deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

	//getting external source instance
    adminConfigModel.getSourceSystemInstance = function() {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT * from source_system_instance';
        self.db.query(SourceSystemInstance, function(result,error) {
            deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

	//getting all instance info
	adminConfigModel.getInstanceInfo = function(siteId) {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT Source_Instance_Config_Id,Site_Id,Source_Type,Type,User_name,Description,Security_token,Status,Source_System_Id,Source_System_Instance_Id,Data_Sync_Status,Objects_Json,Last_Sync_Date_Time, Email from source_instance_config where Site_Id = "' + siteId + '"';
        self.db.query(SourceSystemInstance, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}


	//getting instance by id
	adminConfigModel.getInstanceInfoById = function(id, callback) {

        /*Sunil Code */
        var query = 'SELECT User_name as userName,Password as password,Security_token as securityToken';
        	query+= ' FROM segmentation.source_instance_config where Source_Instance_Config_Id = "' + id + '"';
		var deferred = Q.defer();

		self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	// Code done by RavikumarMG 9/06/2017
	//to add/edit instance

	//getting sfdc objects before expiry time
    adminConfigModel.getObjectsBeforeExpiryTime = function() {
		var deferred = Q.defer();

        var SourceSystemInstance = "SELECT expiry_time,json FROM endpoints Where Source = 'SFDC' AND Endpoint_name = 'objects'";
        self.db.query(SourceSystemInstance, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	//getting endpoint
	adminConfigModel.getEndpoint = function() {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT * FROM endpoints Where Source = "Eloqua" AND Endpoint_name = "login"';
        self.db.query(SourceSystemInstance, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}
	//save SObjects
	adminConfigModel.saveSObjects = function(id, siteId, data) {
		var deferred = Q.defer();
		var Last_Sync_Date_Time = Math.floor(Date.now()/1000)
		var deleteObjectsField = 'DELETE FROM system_table WHERE Source_Instance_Config_Id = "' + id + '" and SiteId="' + siteId + '"';
		self.db.query(deleteObjectsField)
		.then(function function_name(argument) {
			update();
		})

		function update() {
			var updateObjects = "UPDATE source_instance_config SET Data_Sync_Status= 'Sync On Progress', Objects_Json='" + data + "' ,Last_Sync_Date_Time='" + Last_Sync_Date_Time + " ' WHERE Source_Instance_Config_Id='" + id + "' and Site_Id='" + siteId + "'";
			self.db.query(updateObjects, function(result,error) {
	        	if(error){
	        		deferred.reject(error);
	        	} else {
	            	deferred.resolve(result);
	        	}
			})
		}


		return deferred.promise;
	}

	adminConfigModel.updateStatus = function(id, siteId) {
		var deferred = Q.defer();
		var updateStatus = "UPDATE source_instance_config SET Data_Sync_Status= 'Sync Complete' WHERE Source_Instance_Config_Id='" + id + "' and Site_Id='" + siteId + "'";
		self.db.query(updateStatus, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})


		return deferred.promise;
	}

	adminConfigModel.saveSObjectsFields = function(id, siteId, name, label, data, emailStatus, emailFieldName, emailFieldLabel) {
		var deferred = Q.defer();
		var data = data.replace("'", "\'");

		var insertField = "INSERT INTO system_table (SiteId, Source_Instance_Config_Id, Object_Name, Object_Label, Object_Json, emailStatus, emailFieldName, emailFieldLabel) VALUES('" + siteId + "', '" + id + "', '" + name + "', '" + label + "', " + data + ", '" + emailStatus + "', '" + emailFieldName + "', '" + emailFieldLabel + "')";

		self.db.query(insertField, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve([result,error,name, insertField]);
        	}
		})

		return deferred.promise;
	}
	//getting sfdc objects from local
	adminConfigModel.getObjectJson = function(instanceId, siteId) {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT Object_Name, Object_Label FROM system_table Where Source_Instance_Config_Id = "'+instanceId+'" AND SiteId = "'+siteId+'" AND emailStatus = 1';

		self.db.query(SourceSystemInstance, function(result,error) {

        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	adminConfigModel.getParentObjects = function(obj) {
		var deferred = Q.defer();
		var query = 'SELECT Object_Json as json FROM system_table where ';
		query    += 'Source_Instance_Config_Id='+obj.instanceConfigId+' and ';
		query    += 'SiteId='+obj.siteId+' and ';
		query    += 'Object_Name="'+obj.objectName+'";';


		self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}
	adminConfigModel.getObjectFieldJson = function(instanceId, siteId, object) {
		var deferred = Q.defer();
        var SourceSystemInstance = 'SELECT * FROM system_table Where Source_Instance_Config_Id = "'+instanceId+'" AND SiteId = '+siteId+' AND Object_Name = "'+ object +'"';

		self.db.query(SourceSystemInstance, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}
	//getting endpoint
	adminConfigModel.getSfdcEndpoint = function(source, Endpoint_name) {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT endpoint FROM endpoints Where Source = "'+source+'" AND Endpoint_name = "'+Endpoint_name+'"';
		self.db.query(SourceSystemInstance, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	//getting all instance configuration
	adminConfigModel.getConfiguration = function(getData) {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT * FROM source_instance_config where Site_Id = "' + getData.siteId + '" AND Source_Instance_Config_Id = "' + getData.Source_Instance_Config_Id + '"';

        self.db.query(SourceSystemInstance, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	adminConfigModel.getObjects = function(getData) {
		var deferred = Q.defer();

        var getObjects = 'SELECT * FROM source_primary_object where Source_Instance_Config_Id = "' + getData.Source_Instance_Config_Id + '"';
        self.db.query(getObjects, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	adminConfigModel.getMappingFields = function(SourceObjectId) {
		var deferred = Q.defer();

        var getObjects = 'SELECT * FROM source_field_mapping where Source_Object_Id = "' + SourceObjectId + '"';
        self.db.query(getObjects, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	//
	adminConfigModel.addSourcePrimaryObject = function(postData) {
		var deferred = Q.defer();

		var queryInsert = 'INSERT INTO source_primary_object (Source_Instance_Config_Id, Object_Name, Object_Id) VALUES("'+ postData.Source_Instance_Config_Id +'", "'+ postData.Object_Name +'", "'+ postData.Object_Id +'")';

        self.db.query(queryInsert, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}


	adminConfigModel.updateSFDCObject = function(object) {
		var deferred = Q.defer();
		var expiry_time = Math.floor(Date.now()/1000 + 3600);
		var updateUserInfo = "UPDATE endpoints SET json = '"+ object +"', expiry_time =  '"+ expiry_time +"' WHERE Source = 'SFDC' AND Endpoint_name='objects'";
		self.db.query(updateUserInfo, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}


    adminConfigModel.deleteOrg = function(postData) {
		var deferred = Q.defer();
        self.db.query('CALL delete_external_source('+postData.Source_Instance_Config_Id+')', function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	adminConfigModel.deleteSfdcOrg = function(postData) {


		var deferred = Q.defer();


        var deleteSfdcOrg = 'DELETE FROM source_primary_object WHERE Source_Primary_Object_Id = "' + postData.Source_Primary_Object_Id + '"';
		self.db.query(deleteSfdcOrg, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
        });
        
		var deleteSfdcMapping = 'DELETE FROM source_field_mapping WHERE Source_Object_Id = "' + postData.Source_Primary_Object_Id + '"';
        self.db.query(deleteSfdcMapping, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
        });

		return deferred.promise;
	}

	adminConfigModel.deleteSfdcMapping = function(postData) {
		var deferred = Q.defer();

		var deleteSfdcMapping = 'DELETE FROM source_field_mapping WHERE Source_Field_Mapping_Id = "' + postData.Source_Field_Mapping_Id + '"';
        self.db.query(deleteSfdcMapping, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	} else {
            	deferred.resolve(result);
        	}
		})

		return deferred.promise;
	}


})(module.exports)
