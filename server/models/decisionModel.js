(function(decisionModel) {

	var Q = require('q');
  	var _ = require('underscore');
	var self;

	decisionModel.init = function() {
		self = arguments[0];
		
        self.decisionObject = self.sqDbConfig.define('decision_object', {
            Source_Primary_Object_Id:{
                type:self.Sequelize.INTEGER,
                autoIncrement:true,
                primaryKey:true
            },
            instanceid: self.Sequelize.STRING(100),
            Source_Instance_Config_Id: self.Sequelize.INTEGER(45),
            Object_Name: self.Sequelize.STRING(50),
            Object_Id: self.Sequelize.STRING(100),
            import_priority: self.Sequelize.STRING(5000)
        }, {
            freezeTableName:true
        });

        self.sqDbConfig.sync()
        .then(function(result) {
        	// console.log(result);
        })
        .catch(function(error) {
            // console.log(error);
        });
        return this;
	}

    decisionModel.getSyncDetails = function(input) {
        var deferred = Q.defer();

        var query  = 'SELECT decision_status as status, decision_yes_count as yesCount, decision_no_count as noCount,';
            query += '  decision_errored_count as erroredCount, decision_start_time as startTime, decision_end_time as endTime';
            query += '  FROM decision_execution';
            query += '  WHERE instanceid = "'+input.instanceId+'"';

        // var query = 'SELECT * FROM decision_execution WHERE instanceid = "'+input.instanceId+'"';

            

        self.db.query(query)
        .then(function(result) {
            deferred.resolve(result);
        })
        .catch(function(error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    decisionModel.getAllEmailFields = function(input) {
        var deferred = Q.defer();
        
        var query  = 'SELECT emailFieldLabel, emailFieldName FROM system_table ST';
            query += '  WHERE ST.Source_Instance_Config_Id = "'+input.Source_Instance_Config_Id+'"';
            query += '  AND ST.emailStatus = 1 AND ST.Object_Name = "' + input.Object_Id + '"';
            

        self.db.query(query)
        .then(function(result) {
            deferred.resolve(result);
        })
        .catch(function(error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    decisionModel.saveImportPriority = function(obj) {
        var deferred = Q.defer();

        var updateAttr = {
            import_priority:JSON.stringify(obj.priority)
        }


        self.decisionObject.find({
            where: {
                instanceid: obj.instanceid
            }
        })
        .then(function (dsObjRes) {
            // console.log(dsObjRes);
            if (dsObjRes) {
                dsObjRes.updateAttributes(updateAttr)
                .then(function (res) {
                    deferred.resolve(res);
                })
                .catch(function(error) {
                    deferred.reject(error);
                })
            } else {
                deferred.reject({
                    error:"instance not found"
                })
            }
        })



        return deferred.promise;
    }

    decisionModel.getImportPriority = function(obj) {
        var deferred = Q.defer();

        var findObj = {
            where:{
                instanceid: obj.instanceid
            }
        };


        self.decisionObject.find(findObj)
        .then(function (result) {
            // console.log(result);
            deferred.resolve(result);
        })
        .catch(function(error) {
            deferred.reject(error);
        })



        return deferred.promise;
    }
	decisionModel.saveSFDCObject = function(obj) {
        var deferred = Q.defer();

        self.decisionObject.find({
            where: {
                instanceid: obj.instanceid
            }
        })
        .then(function (dsObjRes) {
            if (dsObjRes) {
                dsObjRes.updateAttributes(obj)
                .then(function (res) {
                    deferred.resolve(res);
                })
                .catch(function(error) {
                    deferred.reject(error);
                })
            } else {
                self.decisionObject.create(obj)
                .then(function(res) {
                    deferred.resolve(res);
                })
                .catch(function(error){
                    deferred.reject(error);
                })
            }
        })

        /*self.orm.decisionObject({
            where:input
        })
        .then(function(result) {
            deferred.resolve(result);
        });*/
        /*self.decisionObject.findOrCreate({
            where:{
                instanceid:obj.instanceid
            },
            defaults:obj
        })
        .then(function(result) {
            deferred.resolve(result);
        })
        .catch(function(error) {
            deferred.reject(error);
        });*/


        return deferred.promise;
    }

    decisionModel.updateInsertFeederInstances = function(input){

        var deferred = Q.defer();


        var queryCheck = "SELECT count(*) as cnt FROM feeder_instance WHERE Instance_Id = '"+input.instanceid+"'";
        self.db.query(queryCheck)
        .then(function(result) {
            if(parseInt(result[0][0].cnt) > 0){





                var queryStatement = "UPDATE feeder_instance";
                    queryStatement+= "  SET Segment_Id ='"+ input.segmentId + "',";
                    //queryStatement+= "  Repetation_Time ='"+ frequency + "',";
                    queryStatement+= "  Campaign_Id ='"+ input.campaignId + "',";
                    queryStatement+= "  Campaign_Name ='"+ input.campaignName.replace(new RegExp("'","g"),"''") + "',";
                    queryStatement+= "  Repetation_Time =' ',";
                    queryStatement+= "  decision_record_definition ='"+ input.recordDefinition + "'";
                    queryStatement+= "  WHERE Instance_Id='"+ input.instanceid +"'";

                self.db.query(queryStatement,function(result, error){
                    deferred.resolve(result);
                })

            } else {


                var queryStatement = "INSERT INTO feeder_instance (`Instance_Id`, `Site_Id`, `Campaign_Id`, "
                    queryStatement+= " `Campaign_Name`, `Segment_Id`, `Repetation_Time`, `Status`,`decision_record_definition`) VALUES ("
                    queryStatement+= " '"+input.instanceid+"',";
                    queryStatement+= " '"+input.siteid+"',";
                    queryStatement+= " '"+input.campaignId+"',";
                    queryStatement+= " '"+input.campaignName+"',";
                    queryStatement+= " '"+input.segmentId+"',";
                    queryStatement+= " ' ',";
                    queryStatement+= " 'DRAFT',";
                    queryStatement+= " '"+input.recordDefinition+"'";
                    queryStatement+= " )";


                self.db.query(queryStatement,function(result, error){
                    deferred.resolve(result);
                })
            }
        })
        return deferred.promise;
    }

    decisionModel.getDsSFDCObjects = function(input) {
        var deferred = Q.defer();


        var query  = 'SELECT Source_Instance_Config_Id, Object_Name as Object_Id, Object_Label as Object_Name FROM system_table';
            query += ' WHERE Source_Instance_Config_Id = "'+input.Source_Instance_Config_Id+'"';


            if(!input.sfCtrlStatus){
                query += ' AND emailStatus = "1"';
            }



        self.db.query(query, function(result,error) {


            if(error){
                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }

    
    decisionModel.getControllerStatus = function(instanceid) {
        var deferred = Q.defer();


        var query  = 'SELECT SIC.salesforce_controller_confirmation as sfCtrlStatus FROM feeder_instance FI, segments S, source_instance_config SIC';
            query += ' WHERE FI.Segment_Id = S.Segment_ID and FI.Instance_Id = "'+instanceid+'"';
            query += ' AND S.Source_Instance_Config_Id = SIC.Source_Instance_Config_Id;';


        self.db.query(query, function(result,error) {


            if(error){
                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }



	decisionModel.getDSSFDCFields = function(instanceid) {
		var deferred = Q.defer();


		var query  = 'SELECT  Source_Field_Id,Query_Relationship,Target_Field_Id as targetField FROM source_field_mapping_ds WHERE Decision_Instance_Id = ';
			query += '"'+instanceid+'"';



        self.db.query(query, function(result,error) {


        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}


    decisionModel.getAllElementsOperators = function() {
        
        var deferred = Q.defer();


        var query  = 'SELECT Field_Type_Name as fieldType,Operator_Name as operatorName,Operator_Translation as operatorTranslation';
            query += ' FROM field_operator FO,field_type FT,operator O';
            query += ' WHERE FO.Field_Type_Id = FT.Field_Type_Id AND O.Operator_Id = FO.Operator_Id';



        self.db.query(query, function(result,error) {
            if(error){
                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }

    decisionModel.getFeederInstanceData = function(input) {

        var deferred = Q.defer();


        var query  = 'SELECT Site_Id as siteId,status,Campaign_Id as campaignId,Campaign_Name as campaignName,Repetation_Time as repeatationTime,Segment_Id as segmentId, ';
            query += 'decision_record_definition as recordDefinition ';
            query += 'FROM segmentation.feeder_instance where Instance_Id = "'+input.instanceId+'";';




        self.db.query(query, function(result,error) {
            if(error){
                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }


})(module.exports);
