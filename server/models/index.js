(function(models) {
    var fs = require('fs');
    var modelsArray = [];

    // var reload = require('require-reload')(require);


    models.init = function (plugs,onModelsLoad){

        /*var plugs = {
            db:db
        }*/
        
        fs.readdir(__dirname, function(err, files) {
            if (err) throw err;
            files.forEach(function(file) {
                if(file.indexOf('index')<0){
                    if(file.substr(-3) == '.js') {
                        var modelInit = require('./' + file).init;
                        if(modelInit)
                            modelsArray[file.substr(0,file.indexOf('.js'))] = require('./' + file).init(plugs);
                        else
                            console.log("Please structure the model of "+file);
                    }
                }
            });
            onModelsLoad(modelsArray);
        });
    };

    models.reload = function (plugs,onModelsLoad){
        var reload = require('require-reload')(require);

        /*var plugs = {
            db:db
        }*/
        
        fs.readdir(__dirname, function(err, files) {
            if (err) throw err;
            files.forEach(function(file) {
                if(file.indexOf('index')<0){
                    if(file.substr(-3) == '.js') {
                        var modelInit = reload('./' + file).init;
                        if(modelInit)
                            modelsArray[file.substr(0,file.indexOf('.js'))] = reload('./' + file).init(plugs);
                        else
                            console.log("Please structure the model of "+file);
                    }
                }
            });
            onModelsLoad(modelsArray);
        });
    };
})(module.exports);
