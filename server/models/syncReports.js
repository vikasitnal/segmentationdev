(function(syncReports) {
	var Q = require('q');
	var self;
	syncReports.init = function() {
		self = arguments[0];
		return this;
	}


	syncReports.getExecutionNumbers = function(id) {
		var deferred = Q.defer();
		var query = 'SELECT SUM(created+updated+failed) AS totalImported, SUM(created) as totalCreated,';
			query+= ' SUM(updated) as totalUpdated, SUM(failed) as totalFailed, SUM(warning) as totalWarning FROM feeder_execution_data';
			query+= ' WHERE instance_id = "'+id+'"';


		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	syncReports.getExecutions = function(id) {
		var deferred = Q.defer();


		var query ='SELECT FE.create_timestamp as timestamp, SUM(FED.created+FED.updated) AS totalImported, SUM(FED.created) as totalCreated, ';
			query+='SUM(FED.updated) as totalUpdated, SUM(FED.failed) as totalFailed, SUM(FED.warning) as totalWarning ';
			query+='FROM feeder_execution FE,feeder_execution_data FED ';
			query+='WHERE FE.feeder_execution_id = FED.executionID ';
			query+='AND FED.instance_id = "'+id+'" ';
			query+='GROUP BY executionId ORDER BY executionId DESC LIMIT 150';


		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	syncReports.getPaginateImports = function(id,params) {
		var deferred = Q.defer();

		var query = 'SELECT timestamp as importedTime,created as created, updated as updated, failed as failed, warning as warning,';
			query+= ' (created+updated+failed) as syncCount, status as status';
			query+= ' FROM feeder_execution_data WHERE (status = "warning" OR  status = "success")';
			query+= ' AND instance_id = "'+id+'"';
			query+= ' ORDER BY feeder_execution_data_id DESC LIMIT '+params.limit+' OFFSET '+params.offset;

		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}


	syncReports.getPaginateErrors = function(id,params) {
		var deferred = Q.defer();

		var query = 'SELECT status as status,message as message, created_date as createdOn';
			query+= ' FROM feeder_execution_errors';
			query+= ' WHERE instance_id = "'+id+'"';
			query+= ' ORDER BY error_id DESC LIMIT '+params.limit+' OFFSET '+params.offset;

		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}





	syncReports.getImportsCount = function(id,params) {
		var deferred = Q.defer();

		var query = 'SELECT count(*) as totalImportsCount';
			query+= ' FROM feeder_execution_data WHERE (status = "warning" OR  status = "success")';
			query+= ' AND instance_id = "'+id+'"';

		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	syncReports.getErrorsCount = function(id,params) {
		var deferred = Q.defer();

		var query = 'SELECT count(*) as totalErrorsCount';
			query+= ' FROM feeder_execution_errors WHERE ';
			query+= ' instance_id = "'+id+'"';

		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}
	

	syncReports.getRecentImports = function(id) {
		var deferred = Q.defer();

		var query = 'SELECT timestamp as importedTime,created as created, updated as updated, failed as failed, warning as warning, uri as syncUri,';
			query+= ' (created+updated+failed) as syncCount, status as status';
			query+= ' FROM feeder_execution_data WHERE (status = "warning" OR  status = "success")';
			query+= ' AND instance_id = "'+id+'"';
			query+= ' ORDER BY feeder_execution_data_id DESC LIMIT 5';
			
		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	syncReports.getRecentActions = function(id) {
		var deferred = Q.defer();

		var query = 'SELECT timestamp as importedTime,created+updated+failed as syncCount, status as status';
			query+= ' FROM feeder_execution_data';
			query+= ' WHERE instance_id = "'+id+'"';
			query+= ' ORDER BY feeder_execution_data_id DESC LIMIT 5';

		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	syncReports.getRecentErrors = function(id) {
		var deferred = Q.defer();

		var query = 'SELECT status as status,message as message, created_date as createdOn';
			query+= ' FROM feeder_execution_errors';
			query+= ' WHERE instance_id = "'+id+'"';
			query+= ' ORDER BY error_id DESC LIMIT 5';

		self.db.query(query)
		.then(function(result,error) {
			deferred.resolve(result[0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

})(module.exports);