(function(instanceModel) {
	var Q = require('q');
	var self;
	instanceModel.init = function() {
		self = arguments[0];
		return this;
	}


	/* Initiate Start and End Update Token Times*/
	instanceModel.setTokenTimes = function(input) {
		var deferred = Q.defer();

        var query = "UPDATE instance_ SET ";
        	query+= "update_token_start_time='"+input.startTime+"',";
        	query+= "update_token_end_time='"+input.endTime+"' ";
        	query+= "WHERE Site_Id ='"+input.siteId+"'";

		self.db.query(query, function(result,error) {
			if(error){
				deferred.reject(error);
			}
			deferred.resolve(result);
		});
		return deferred.promise;
	}


	/* Get Start and End Update Token Times*/
	instanceModel.getTokenTimes = function(siteId) {
		var deferred = Q.defer();

        var query = " SELECT Token as token,Token_Expiry_Time as expiryTime, update_token_start_time as startTime, update_token_end_time as endTime ";
        	query+= " FROM instance_ WHERE Site_Id ='" + siteId +"'";



		self.db.query(query, function(result,error) {
			if(error){
				deferred.reject(error);
			}
			deferred.resolve(result,"status:true");
		});
		return deferred.promise;
	}


	/* Get Start and End Update Token Times*/
	instanceModel.getSFControllerStatus = function(input) {
		var deferred = Q.defer();

        var query = "SELECT salesforce_controller_confirmation FROM source_instance_config ";
        	query+= "WHERE Source_Instance_Config_Id ='"+input.instanceId+"' AND  ";
        	query+= "Site_Id ='"+input.siteId+"' ";

		self.db.query(query, function(result,error) {
			if(error){
				deferred.reject(error);
				return;
			}
			deferred.resolve(result);
		});
		return deferred.promise;
	}

	instanceModel.updateSFControllerStatus = function(input) {
		var deferred = Q.defer();

        var query = "UPDATE source_instance_config SET ";
        	query+= "salesforce_controller_confirmation='"+input.sfdcCtrlStatus+"' ";
        	query+= "WHERE Source_Instance_Config_Id ='"+input.instanceId+"' AND  ";
        	query+= "Site_Id ='"+input.siteId+"'";

		self.db.query(query, function(result,error) {
			if(error){
				deferred.reject(error);
				return;
			}
			deferred.resolve(result);
		});
		return deferred.promise;
	}



	instanceModel.setGetImportPriority = function(data) {
		var deferred = Q.defer();
		var toBeSavedData = {
			priority:data.priority
		};
		var toBeSavedDataString = JSON.stringify(toBeSavedData);

		var queryGet    = 'SELECT priority_value as priorityValue,importDefination FROM source_primary_object ';
			queryGet   += 'WHERE Source_Primary_Object_Id='+data.primaryObjectId+';';


		if(data.priority){
	        self.db.query(queryGet)
			.then(function(result) {
				var importDefination = JSON.parse(result[0][0].importDefination);

				importDefination.importPriorityUri = data.priority.uri;

				var queryUpdate    = 'UPDATE source_primary_object ';
					queryUpdate   += 'SET priority_value='+"'"+toBeSavedDataString+"' ";
					queryUpdate   += ' ,importDefination='+"'"+JSON.stringify(importDefination)+"' ";
					queryUpdate   += 'WHERE Source_Primary_Object_Id='+data.primaryObjectId+';';
				return self.db.query(queryUpdate);
			})
	        .then(function(result) {
	        	if(result.length){
					return self.db.query(queryGet)
	        	} else {
					deferred.resolve({
						status:0,
						message:'Not Updated'
					});
	        	}
	        })
	        .then(function(result) {
	        	if(result.length){
		        	deferred.resolve({
		        		status:1,
						message:'Updated and Got saved details',
		        		result:JSON.parse(result[0][0].priorityValue)
		        	})
	        	} else{
		        	deferred.resolve({
		        		status:2,
						message:'Updated and didnt Get saved details',
		        		result:null
		        	})
	        	}
	        });
		} else {
			self.db.query(queryGet)
	        .then(function(result) {
	        	if(result.length){
		        	deferred.resolve({
		        		status:3,
						message:'Got saved details',
		        		result:JSON.parse(result[0][0].priorityValue)
		        	})
	        	} else{
		        	deferred.resolve({
		        		status:4,
						message:'There are no saved details',
		        		result:null
		        	})
	        	}
	        });
		}

		return deferred.promise;
	}
	//rahul 02-06
	instanceModel.checkSiteId = function(getData) {
		var deferred = Q.defer();
        var query = "SELECT Site_Id FROM instance_ WHERE Site_Name ='" + getData.siteName +"'";
        self.db.query(query, function(result,error) {
			if(result && result.length)
            {
				var queryUpdate = 'UPDATE instance_ SET Site_Id = "'+ getData.siteId +'", User_Name =  "'+ getData.userName +'", User_Id = "'+ getData.UserId +'", Status = "'+ 1 +'" WHERE Site_Name = "'+ getData.siteName +'"';
                self.db.query(queryUpdate, function(result,error) {
                    deferred.resolve(result);
                })
            }
            else
            {
				var queryInsert = 'INSERT INTO instance_ (Site_Name, Site_Id, InstallId, User_Name, User_Id, Status) VALUES("'+ getData.siteName +'", "'+ getData.siteId +'","'+ getData.installId +'", "'+ getData.userName +'", "'+ getData.UserId +'","'+ 1 + '")';
                self.db.query(queryInsert, function(result,error) {
                    deferred.resolve(result);
                })
            }
		})
		return deferred.promise;
	}

	//rahul and sunil 06-06
	instanceModel.updateToken = function(result, tokenResult) {
		var deferred = Q.defer();
		var instanceInfo = JSON.parse(result);
		var query = "SELECT Site_Id FROM instance_ WHERE Site_Id ='" + instanceInfo.site.id +"'";
		self.db.query(query, function(result,error) {
           if(result && result.length)
            {
				var tokenExpiryTime = Math.floor(Date.now()/1000) + tokenResult.expires_in;
				var updateTokenBaseUrl = 'UPDATE instance_ SET Refresh_Token = "'+ tokenResult.refresh_token +'", Token =  "'+ tokenResult.access_token +'", Base_Url = "'+ instanceInfo.urls.base +'", Token_Expiry_Time = "'+ tokenExpiryTime +'" WHERE Site_Id = "'+ instanceInfo.site.id +'"';
				self.db.query(updateTokenBaseUrl, function(result,error) {
					deferred.resolve({status:true})
                })

				var updateUserInfo = 'UPDATE instance_ SET First_Name = "'+ instanceInfo.user.firstName +'", Last_Name =  "'+ instanceInfo.user.lastName +'", Email = "'+ instanceInfo.user.emailAddress +'" WHERE Site_Id = "'+ instanceInfo.site.id +'"';

				self.db.query(updateUserInfo, function(result,error) {
					deferred.resolve(result);
				})
            }
            else
            {
				deferred.resolve({status:false});
            }
		})
		return deferred.promise;
	}

	instanceModel.checkToken = function(result) {
		var deferred = Q.defer();

        var checkSiteId = "SELECT * FROM instance_ WHERE Site_Id ='" + result.siteId +"'";
		self.db.query(checkSiteId, function(result,error) {
			if(error){
				deferred.reject(error);
			}
			deferred.resolve(result);
		});
		return deferred.promise;
	}


	instanceModel.get_Source_Instance_Config_By_Id= function(id){
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM source_instance_config WHERE Source_Instance_Config_Id = " + id;
			self.db.query(queryStatement,function(result, error){
				if(error){
					deferred.reject(error);
				} else {
					var jsonresult = JSON.parse(JSON.stringify(result));
					var source_instance_config = jsonresult[0];
					deferred.resolve(source_instance_config);
				}
		})
		return deferred.promise;
	}

	instanceModel.updateExpiryTime = function(result, tokenResult) {
		var deferred = Q.defer();
		if(tokenResult)
			var tokenExpiryTime = Math.floor(Date.now()/1000)+ tokenResult.expires_in;
		else
			var tokenExpiryTime = Math.floor(Date.now()/1000);

		
		var updateTokenBaseUrl = 'UPDATE instance_ SET '
			updateTokenBaseUrl+= 'Refresh_Token = "'+tokenResult.refresh_token +'", ';
			updateTokenBaseUrl+= 'Token =  "'+ tokenResult.access_token +'",'
			updateTokenBaseUrl+= 'Token_Expiry_Time = "'+ tokenExpiryTime +'"'
			updateTokenBaseUrl+= ' WHERE Site_Id = "'+ result.siteId +'"';


		self.db.query(updateTokenBaseUrl, function(result,error) {
			deferred.resolve(result,"status:true");
		});

		return deferred.promise;
	}

	//rahul 04-06
    instanceModel.checkSiteName = function(result, tokenResult) {
		var deferred = Q.defer();

        var checkSiteName = "SELECT Site_Id FROM instance_ WHERE Site_Id ='" + result.site.id +"'";
		self.db.query(checkSiteName, function(result,error) {
            if(result.length)
            {
                var updateTokenBaseUrl = 'UPDATE instance_ SET Refresh_Token = "'+ tokenResult.refresh_token +'", Token =  "'+ tokenResult.access_token +'", Base_Url = "'+ result.urls.base +'", Token_Expiry_Time = "'+ tokenResult.expires_in +'" WHERE Site_Id = "'+ result.site.id +'"';
				self.db.query(updateTokenBaseUrl, function(result,error) {
                    deferred.resolve(result);
                })
            }
		})
		return deferred.promise;
	}

})(module.exports)
