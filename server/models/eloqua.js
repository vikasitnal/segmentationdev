(function(eloqua) {
	var Q = require('q');
    var request = require('request');
	var self;
	eloqua.init = function() {
		self = arguments[0];
		return this;
	}

    eloqua.getEndpoint = function() {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT * FROM endpoints Where Source = "Eloqua" AND Endpoint_name = "login"';
        self.db.query(SourceSystemInstance, function(result,error) {
            deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

	eloqua.getConfiguration = function(getData) {
		var deferred = Q.defer();

        var SourceSystemInstance = 'SELECT * FROM Source_Instance_Config where Site_Id = "' + getData.siteId + '" AND Source_Instance_Config_Id = "' + getData.Source_Instance_Config_Id + '"';
        self.db.query(SourceSystemInstance, function(result,error) {
            deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

    var siteName = 'tech'; //from session 
    var userName = 'username'; // from getConfiguration 
    var password = 'password'; // from getConfiguration 

    var Authorization = new Buffer(siteName + "\\" + userName + ":" + password).toString("base64");
    //url we are getting from getEndpoint function
    var url = 'https://login.eloqua.com/auth/oauth2/token/';

    var options = {  
        url : url,  
        headers : {
            'Content-type': 'application/json',
            "Authorization": "Basic " + Authorization
        } ,
        method:'GET'
    }
    request(options,function(result){
        var eloquadata = result;
    });

    // request(options, requestToken);
    // request(options,function(result){
    //     var requestToken = result;
    // });

    // var eloquadata = request(options2,resultantFunction);

})(module.exports)