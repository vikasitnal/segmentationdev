(function(licenseChecking_model) {
	var Q = require('q');
	var self;
	licenseChecking_model.init = function() {
		self = arguments[0];
		return this;
	}


    licenseChecking_model.checkValidity = function(data) {

        var deferred = Q.defer();
        var validity = false;

        var getEndDateQuery = 'SELECT end_date, type FROM `license` WHERE licenseId = (SELECT `license_id` FROM `instance_` WHERE Site_Id = "'+data.siteId+'")' ;
		self.db.query( getEndDateQuery , function(result, error) {
			if(error){
				deferred.reject(error);
			}
            var message;
			if((result && result.length !=0)){
                var license_type = result[0]['type'];
                
				if(new Date() <= new Date(result[0]['end_date'])){
					
                    validity = true;					
                }
				else
				{
					
					if(license_type == 'trial'){
						message = "Your trial period has expired";
					}else{
						message = "Your license has expired ";
					}
				}
                deferred.resolve([validity, message]);
            }else{
					
                deferred.resolve([validity, message]);
            }
        })
        return deferred.promise;
    }



})(module.exports)
