(function(feederModel) {
	var Q = require('q');
	var _ = require('underscore');
	var self;
	feederModel.init = function() {
		self = arguments[0];
		return this;
	}

	/*New Codes*/

	/*Only for Decision */

	feederModel.getDSSFDCFields = function(instanceid) {
		var deferred = Q.defer();


		var query  = 'SELECT  Source_Field_Id,Query_Relationship FROM source_field_mapping_ds WHERE Decision_Instance_Id = ';
			query += '"'+instanceid+'"';



        self.db.query(query, function(result,error) {


        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	feederModel.getAutoFieldMapping = function(instanceid) {
		var deferred = Q.defer();


		var query  = 'SELECT SFM.Source_Field_Id as sourceField, SFM.Source_Field_Name as sourceFieldName,';
			query += ' SFM.Query_Relationship as relName, SFM.Target_Field_Id as targetField ';
			query += ' FROM source_field_mapping SFM, feeder_instance FI, filters F WHERE ';
			query += ' FI.Instance_Id = "'+instanceid+'" AND F.Segment_Id = FI.Segment_Id AND ';
			query += ' SFM.Source_Object_Id = F.Master_Object GROUP BY SFM.Source_Field_Mapping_Id';



        self.db.query(query, function(result,error) {

        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	feederModel.getDecisionInstanceFieldMapping = function(instanceid) {
		var deferred = Q.defer();


		var query  = 'SELECT  Source_Field_Id as sourceField, Source_Field_Name as sourceFieldName, Query_Relationship as relName, Target_Field_Id as targetField ';
			query += ' FROM source_field_mapping_ds WHERE Decision_Instance_Id = ';
			query += '"'+instanceid+'"';


        self.db.query(query, function(result,error) {


        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	feederModel.saveDecisionExecution = function(id,instanceid,status,input) {





		var deferred = Q.defer();
		if(id){
			var columns = [
				'decision_status',
				'decision_end_time'				
			];

			var info = [
				status,
				Date.now()				
			];



			if(input && input.yesInfo){
				columns.push('decision_execution_yes_info');
				info.push(JSON.stringify(input.yesInfo).replace(new RegExp("'","g"),"''"));
			}

			if(input && input.noInfo){
				columns.push('decision_execution_no_info');
				info.push(JSON.stringify(input.noInfo).replace(new RegExp("'","g"),"''"));
			}

			if(input && input.errorInfo){
				columns.push('decision_error_info');
				info.push(JSON.stringify(input.errorInfo).replace(new RegExp("'","g"),"''"));
			}

			if(input && input.yesCount){
				columns.push('decision_yes_count');
				info.push(input.yesCount);
			}

			if(input && input.noCount){
				columns.push('decision_no_count');
				info.push(input.noCount);
			}

			if(input && input.erroredCount){
				columns.push('decision_errored_count');
				info.push(input.erroredCount);
			}

			if(input && input.syncDefinition){
				columns.push('sync_definition');
				info.push(JSON.stringify(input.syncDefinition).replace(new RegExp("'","g"),"''"));
			}



			if(columns.length == info.length){
				var length = columns.length;
				var changes= '';
				for (var i = 0; i < length; i++) {
					if(i!=length-1)
						changes += columns[i] + "=" + "'"+info[i]+"',";
					else
						changes += columns[i] + "=" + "'"+info[i]+"'";
				}


				condition = ' WHERE decision_execution_id = "' + id + '"';



				var query = 'UPDATE decision_execution SET '+changes+condition;


				self.db.query(query)
				.then(function(result,error) {
					if(error){
						console.log(error);
						deferred.reject(error);
						return;
					}

					deferred.resolve(result);
				});
			}else{
				deferred.resolve({
					status:0,
					error:'Unmatched columns'
				})
			}


		} else {

			var columns = [
				'instanceid',
				'decision_status',
				'decision_start_time'
			];

			var info = [
				instanceid,
				status,
				Date.now()+""
			];

			var query = "INSERT INTO decision_execution(" + columns.join(',') + ") VALUES (" + JSON.stringify(info).replace(/[\[\]]/g,'') + ")";




	        self.db.query(query, function(result,error) {
	        	if(error){
	        		deferred.reject(error);
	        	}else{
	            	deferred.resolve(result);
	        	}
			});

		}


		return deferred.promise;
	}


	feederModel.getDecisionUserData = function(siteId) {
		var deferred = Q.defer();


		var query  = 'SELECT Base_Url as baseUrl, token as accessToken FROM instance_ WHERE Site_Id = ';
			query += '"'+siteId+'"';



        self.db.query(query, function(result,error) {


        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	feederModel.getDecisionInstanceData = function(instanceid) {
		var deferred = Q.defer();


		var query  = 'SELECT s.Source_Instance_Config_Id as srcInsCfgId,s.Segment_ID as segmentID, s.Query as query ';
			query += ' FROM feeder_instance fi,segments s WHERE Instance_Id = ';
			query += '"'+instanceid+'" AND fi.Segment_Id = s.Segment_ID';


        self.db.query(query, function(result,error) {
        	
        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}







	/*Remaining*/
	feederModel.feederExecutionError = function(input) {
		var deferred = Q.defer();


		var query  = 'INSERT INTO feeder_execution_errors (instance_id, status, message,created_date)';
			query += '  VALUES ("'+input.instanceid
			query += '", "'+input.errStatus+'", ';
			query += "'"+input.errMessage.replace(new RegExp("'","g"),"''")+"',";
			query += '"'+Date.now()+'"';
			query += ')';


        self.db.query(query, function(result,error) {


        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		});

		return deferred.promise;
	}

	feederModel.getSegment = function(segmentID) {
        var deferred = Q.defer();
        var query = "SELECT Query as query FROM segments WHERE Segment_ID = "+segmentID;

        self.db.query(query, function(result,error) {
            if(error){

                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        })
        return deferred.promise;
	}

	feederModel.getFeederStatusCount = function(feederExId) {
        var deferred = Q.defer();
        var query = "SELECT (SELECT COUNT(*) FROM feeder_execution_data fed1 where fed1.status = 'success' AND fe.feeder_execution_id = fed1.executionId) AS success, ";
        	query+= " (SELECT COUNT(*) FROM feeder_execution_data fed1 WHERE fed1.status = 'warning'";
        	query+= "  AND fe.feeder_execution_id = fed1.executionId) AS warning FROM feeder_execution fe WHERE fe.feeder_execution_id = "+feederExId;

        self.db.query(query, function(result,error) {
            if(error){

                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        })
        return deferred.promise;
	}
	feederModel.deleteFeederInstance = function(instanceid,siteid){
        var deferred = Q.defer();
        var query = "UPDATE feeder_instance SET Status = 'DELETED' WHERE Instance_Id = '"+instanceid+"' AND Site_Id='"+siteid+"'";

        self.db.query(query, function(result,error) {
            if(error){
                deferred.reject(error);
            }else{
                deferred.resolve(result);
            }
        })
        return deferred.promise;
    }

	feederModel.getLastSyncDate = function(segmentId){
		var deferred = Q.defer();
		var query = "select fe.create_timestamp from filters fl";
		query += " join feeder_instance fi on fi.Segment_Id = fl.Segment_Id";
		query += " join feeder_execution fe on fe.instanceId = fi.Instance_Id";
		query += " where fl.Segment_Id = "+segmentId+" and fe.status = 'success'";
		query += " order by fe.create_timestamp desc limit 1";
        self.db.query(query, function(result,error) {
        	if(error){

        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	feederModel.getFeederExecutionsData = function(id){
		var deferred = Q.defer();
		var query = "SELECT feeder_execution_data_id as dataId, status,uri as url,io_meta_data as ioRecord FROM"
		query    += " feeder_execution_data";
		query    += " WHERE (status = 'pending' OR status = 'active') AND executionId = "+ id;


        self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	feederModel.updatePendingExecutionsData = function(feederInfo) {
		var deferred 	= Q.defer();

		// var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
		// 	queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ feederInfo.status +'", "'+ (Math.floor(Date.now()/1000)) +'")';

		var columns = [
			'status',
			'timestamp',
			'io_meta_data',
			'created',
			'updated',
			'failed',
			'warning'
		];

		var info = [
			feederInfo.status,
			Date.now(),
			feederInfo.ioRecord,
			feederInfo.created,
			feederInfo.updated,
			feederInfo.failed,
			feederInfo.warning
		]

		if(columns.length == info.length){
			var length = columns.length;
			var changes= '';
			for (var i = 0; i < length; i++) {
				if(i!=length-1)
					changes += columns[i] + "=" + "'"+info[i]+"',";
				else
					changes += columns[i] + "=" + "'"+info[i]+"'";
			}


			condition = ' WHERE feeder_execution_data_id="' + feederInfo.id + '"';



			var query = 'UPDATE feeder_execution_data SET '+changes+condition;



			self.db.query(query)
			.then(function(result,error) {

				deferred.resolve(result);
			});
		}else{
			deferred.resolve({
				status:0,
				error:'Unmatched columns'
			})
		}


		return deferred.promise;
	}


	feederModel.setSalesforceData = function(input){
		var deferred = Q.defer();


		var query  = 'INSERT INTO salesforce_data (feeder_execution_id, segment_id, salesforce_data)';
			query += '  VALUES ("'+input.feederExId+'", "'+input.segmentId+'", '+"'"+input.salesforceData.replace(new RegExp("'","g"),"''")+"'"+')';


        self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}
	feederModel.getContactColumns = function(primaryObjectId){
		var deferred = Q.defer();
		var query = "SELECT Source_Field_Id as sourceField, Source_Field_Name as sourceFieldName,"
		query    += " Query_Relationship as relName, Target_Field_Id as targetField FROM source_field_mapping";
		query    += " WHERE Source_Object_Id = "+ primaryObjectId;


        self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	/*feederModel.findSegmentFilters = function(segmentID) {


		var deferred = Q.defer();
		var query = "SELECT Filters_Id as id,Filter_Query as query, Master_Object as objectId, Segment_Id as SegmentId FROM filters"
		query    += " WHERE Segment_Id = "+ segmentID;


        self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}*/

	feederModel.findSegmentFilters = function(segmentID) {


		var deferred = Q.defer();
		var query = "Select f.Filters_Id as id,f.Filter_Query as query, f.Master_Object as objectId, f.Segment_Id as SegmentId,s.Query as segmentQuery ";
		query +="from filters f join segments s on s.Segment_ID = f.Segment_Id ";
		query    += " WHERE f.Segment_Id = "+ segmentID;


        self.db.query(query, function(result,error) {
        	if(error){
        		deferred.reject(error);
        	}else{
            	deferred.resolve(result);
        	}
		})
		return deferred.promise;
	}

	feederModel.delSegmentFilters = function(ids) {

		var deferred = Q.defer();
		if(ids.length){
			var query = "DELETE FROM filters WHERE Filters_Id in ("+ids+")";
	        self.db.query(query, function(result,error) {
	        	if(error){
	        		deferred.reject(error);
	        	}else{
	            	deferred.resolve(result);
	        	}
			})
		} else {
			deferred.resolve(ids);
		}
		return deferred.promise;
	}


	feederModel.saveActivationType = function(input) {
		var deferred 	= Q.defer();
		var tempStatus = input.status.toUpperCase();
		var query = "UPDATE feeder_instance SET";
			query+= " Status ='"+ tempStatus+ "',";
			query+= "Campaign_Id = '"+input.campaignId+"',";
			query+= "Campaign_Name = '"+input.campaignName+"'";
			query+= " WHERE Instance_Id = '"+input.instanceid+"'";



		self.db.query(query)
		.then(function(result) {
			deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.updateInsertFilters = function(editableIds,filtersWithQuery,segmentID) {

		var deferred = Q.defer();
		var asyncCounter = 0;
		var totalCalls = filtersWithQuery.length;
		var editsLength = editableIds.length;


		if(filtersWithQuery.length>editableIds.length){
			var insertingRemains = filtersWithQuery.slice(editableIds.length,filtersWithQuery.length);
		}else{
			var insertingRemains = [];
		}


		var results = [];

		var callCanBeDefer = function() {

			if(totalCalls == asyncCounter){
				deferred.resolve({
					status:1
				});
			}
		}


		if(editableIds.length){
			var incEdits = 0;
			editableIds.forEach(function(id,index) {
				var query = "UPDATE filters SET ";
					query+= "FilterName ='"+ filtersWithQuery[index].filterName + "', ";
					query+= "Filter_Query ="+'"'+filtersWithQuery[index].filterQuery+'"'+ ", ";
					query+= "Filter_Query_Prefix ="+'"'+filtersWithQuery[index].filterQueryPrefix+'"'+ ", ";
					query+= "Filter_Query_Suffix ="+'"'+filtersWithQuery[index].filterQuerySuffix+'"'+ ", ";
					query+= "Master_Object ='"+ filtersWithQuery[index].masterObjectId + "', ";
					query+= "Filter_JSON ="+'"'+ JSON.stringify(filtersWithQuery[index].filterJson).replace(/"/g, '\\"') + '"';
					query+= "WHERE Filters_Id ="+id;


				self.db.query(query, function(result,error) {

		        	if(error){
		        		deferred.reject(error);
		        	}else{
		        		asyncCounter++;
		        		results.push(result);

						incEdits++;

						if(editableIds.length == incEdits){
							callCanBeDefer();
						}
		        	}
				})
			});
		}


		if(insertingRemains.length){
			var incIns = 0;
			insertingRemains.forEach(function(item,index) {

				var query = 'INSERT INTO `filters` (`FilterName`, `Segment_Id`, `Filter_Query`, `Filter_Query_Prefix`,`Filter_Query_Suffix`,`Filter_JSON`, `Master_Object`, `Record_Count`) ';
					query+= 'VALUES (';
					query+= '"'+item.filterName+'",';
					query+= '"'+segmentID+'",';
					query+= '"'+item.filterQuery+'",';
					query+= '"'+item.filterQueryPrefix+'",';
					query+= '"'+item.filterQuerySuffix+'",';
					query+= '"'+JSON.stringify(item.filterJson).replace(/"/g, '\\"')+'",';
					query+= '"'+item.masterObjectId+'",';
					query+= '"'+0+'"';
					query+= ');';



				self.db.query(query, function(result,error) {

		        	if(error){
		        		deferred.reject(error);
		        	}else{
		        		results.push(result);
			        	asyncCounter++;
			        	incIns++;
						if(insertingRemains.length == incIns){
							callCanBeDefer();
						}
		        	}
				})
			})
		}



		return deferred.promise;
	}

	  feederModel.updateSegmentQueries = function(filtersWithQuery,segmentID){
        var deferred = Q.defer();
        var asyncCounter = 0;
        var queryPrefix = '';
        var querySuffix = '';

        var onLoopCompletion = function(queryPrefix,querySuffix){

        	if(filtersWithQuery.length){
        		querySuffix = '('+querySuffix+')';
        	}


            var query = 'UPDATE segments SET Query_Prefix = "'+queryPrefix+'",';
            query+= ' Query_Suffix = "'+querySuffix+'",';
            query+= ' Query = "'+queryPrefix+' '+querySuffix+'" WHERE Segment_ID='+segmentID;
            self.db.query(query)
            .then(function(result){
                deferred.resolve();
            })
        }

        filtersWithQuery.forEach(function(item,index){

            if(asyncCounter == 0)
            {
                queryPrefix = item.filterQueryPrefix;
                querySuffix = "("+item.filterQuerySuffix+")";
            }
            else
            {
                querySuffix += " OR ("+item.filterQuerySuffix+")";
            }
            asyncCounter++;
            if(asyncCounter == filtersWithQuery.length)
            {

                onLoopCompletion(queryPrefix,querySuffix);
            }

        });


        return deferred.promise;

    }

	feederModel.updateSegmentQueryPrefix = function(queryPrefix, querySuffix ,segmentID){
        var deferred = Q.defer();
         var query = 'UPDATE segments SET Query_Prefix = "'+queryPrefix+'",';
        query+= ' Query = "'+queryPrefix+' '+querySuffix+'" WHERE Segment_ID='+segmentID;
        self.db.query(query)
        .then(function(result){
            deferred.resolve(result);
        })
        .catch(function(error) {
        	deferred.reject(error);
        });


        return deferred.promise;

    }


    feederModel.updateFilterQueryPrefix = function(queryPrefix, querySuffix ,filterId){
        var deferred = Q.defer();
         var query = 'UPDATE filters SET Filter_Query_Prefix = "'+queryPrefix+'",';
        query+= ' Filter_Query = "'+queryPrefix+' '+querySuffix+'" WHERE Filters_Id='+filterId;
        self.db.query(query)
        .then(function(result){
            deferred.resolve();
        })


        return deferred.promise;

    }





	feederModel.getSfdcUserData = function(instanceConfigId) {
		var deferred 	= Q.defer();
		var query = 'SELECT User_name as userName,Password as password, Security_token as securityToken, Site_Id as Site_Id, salesforce_controller_confirmation as sfCtrlStatus ';
			query+= 'FROM source_instance_config WHERE ';
			query+= 'Source_Instance_Config_Id = '+instanceConfigId;


		self.db.query(query)
		.then(function(result) {
			deferred.resolve(result[0][0]);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	feederModel.updateSegmentCount = function(input) {
		var deferred 	= Q.defer();
		var query = 'UPDATE segments SET Record_Count='+input.count+' WHERE Segment_ID='+input.segmentID;
		self.db.query(query)
		.then(function(result) {
			deferred.resolve(result);
		});
		return deferred.promise;

	}

	feederModel.getOperators = function() {
		var deferred = Q.defer();
       	var getMappedFields = 'SELECT ft.Field_Type_Name, fo.Field_Operator_UI_ID as ui_id,';
       		getMappedFields+= ' o.Operator_Name, o.Operator_Label, o.Operator_Id, o.Operator_Message as msg,o.Operator_Not_Message as msgNot';
			getMappedFields+= ' FROM field_type as ft INNER JOIN field_operator as fo ON ft.Field_Type_Id = fo.Field_Type_Id';
			getMappedFields+= ' INNER JOIN operator as o ON o.Operator_Id = fo.Operator_Id';
		self.db.query(getMappedFields, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getExistingSegment = function(getData) {
		var deferred = Q.defer();

		var query = "SELECT s.Source_Instance_Config_Id AS instanceId, s.SegmentName AS segmentName, s.Segment_ID AS segmentId,";
 			query+= " s.Created_On AS createdOn ,s.created_by AS createdBy, s.Modified_At AS modifiedAt, s.modified_by AS modifiedBy FROM segments s";
  			query+= " JOIN source_instance_config sic ON sic.Source_Instance_Config_Id = s.Source_Instance_Config_Id";
 			query+= " WHERE sic.Status = 'Active' AND s.SiteId = '"+ (getData.siteId||getData.siteid) +"' ";
 			query+= " AND (s.segment_type = 'Program' OR s.segment_type = 'Campaign') ";




		self.db.query(query, function(result,error) {
			if(error){
				deferred.reject(error);
			}

			deferred.resolve(result);
		})
		return deferred.promise;
	}


	feederModel.get_mapped_fields = function(MasterObjectId){
		var deferred = Q.defer();
		var queryStatement = "SELECT Source_Field_Id,Query_Relationship FROM source_field_mapping ";
			queryStatement+= " WHERE Source_Object_Id ='"+ MasterObjectId + "'";

		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}


	feederModel.saveSegment = function(json, siteId,userName,type){
		var deferred = Q.defer();
		var Source_Instance_Config_Id = json.sourceInstanceConfigId;
		var segmentName = json.name;
		var stringJson = JSON.stringify(json).replace(/"/g, '\\"');



		var segmentId = json.id;
		var queryStatement = '';
		if(json.id){
			segmentId = json.id;
			queryStatement = "UPDATE segments ";
			queryStatement+= "SET SegmentName ='"+ segmentName +"',SegmentJSON ="+'"' +stringJson +'",';
			queryStatement+= "Modified_At = (select utc_timestamp()),";
			queryStatement+= "Source_Instance_Config_Id = '"+Source_Instance_Config_Id+"',";
			queryStatement+= "segment_type = '"+type+"',";
			queryStatement+= "modified_by = '"+userName+"'";
			queryStatement+= " WHERE Segment_ID ="+ segmentId;



			self.db.query(queryStatement,function(result, error){

				if(error){
					deferred.resolve(error);
					return;
				}
				deferred.resolve(segmentId);
			})
		}else{
			queryStatement = "INSERT INTO segments (Source_Instance_Config_Id,SiteId,segment_type,SegmentName,SegmentJSON,Created_On,created_by,Modified_At,modified_by)";
			queryStatement+= " VALUES('"+ Source_Instance_Config_Id +"','"+ siteId +"','"+ type +"','"+ segmentName +"',"+ '"'+stringJson+'"' +"";
			queryStatement+= ",(select utc_timestamp()),'"+userName+"' ,(select utc_timestamp()), '"+userName+"')";



			self.db.query(queryStatement,function(result, error){
				if(result)
					deferred.resolve(result.insertId);
				else
					deferred.resolve(0);
			});
		}

		return deferred.promise;
	}

	/*Metadata of execution of data Import*/
	feederModel.saveFeederExecution_backup = function(feederInfo) {
		var deferred 	= Q.defer();

		var queryCheck = "SELECT feeder_execution_id FROM feeder_execution WHERE instanceId = '"+feederInfo.instanceId+"' AND status = 'In Progress'";

		self.db.query(queryCheck)
		.then(function(result) {
			if(!(result[0]&&result[0].length))
			{
				var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
					queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ feederInfo.status +'", "'+ (Math.floor(Date.now()/1000)) +'")';

				self.db.query(queryInsert)
				.then(function(result) {
					deferred.resolve({
						insertId:result[0].insertId,
						status:'In Progress'
					});

				});

			}
			else
			{


				var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
					queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ 'Skipped' +'", "'+ (Math.floor(Date.now()/1000)) +'")';


				self.db.query(queryInsert)
				.then(function(result) {
					deferred.resolve({
						insertId:result[0].insertId,
						status:'Skipped'
					});
				});


			}
		});

		return deferred.promise;
	}

	feederModel.saveFeederExecution = function(feederInfo) {
		var deferred 	= Q.defer();
		var checkForInactiveInstance = "SELECT sic.Status FROM feeder_instance fe";
		checkForInactiveInstance += " JOIN segments s on s.Segment_ID = fe.Segment_Id";
		checkForInactiveInstance += " JOIN source_instance_config sic on sic.Source_Instance_Config_Id = s.Source_Instance_Config_Id";

		if(feederInfo.cron)
			checkForInactiveInstance += " WHERE fe.Instance_Id ='"+feederInfo.instanceId+"' and sic.status = 'Inactive'";
		else
			checkForInactiveInstance += " WHERE fe.Instance_Id ='"+feederInfo.instanceId+"' and (sic.status = 'Inactive' OR fe.Repetation_Time <> 'Run Once')";



		self.db.query(checkForInactiveInstance)
		.then(function(result1){
			
			if(result1[0]&&result1[0].length>0)
			{

				deferred.resolve({
					insertId:0,
					status:'Inactive'
				});
			}
			else
			{
				var queryCheck = "SELECT feeder_execution_id FROM feeder_execution WHERE instanceId = '"+feederInfo.instanceId+"' AND status like '%In Progress%'";
				self.db.query(queryCheck)
				.then(function(result) {
					if(!(result[0]&&result[0].length))
					{
						var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
							queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ feederInfo.status +'", "'+ (Math.floor(Date.now()/1000)) +'")';

						self.db.query(queryInsert)
						.then(function(result) {
							deferred.resolve({
								insertId:result[0].insertId,
								status:'In Progress'
							});

						});

					}
					else
					{


						var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
							queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ 'Skipped' +'", "'+ (Math.floor(Date.now()/1000)) +'")';


						self.db.query(queryInsert)
						.then(function(result) {
							deferred.resolve({
								insertId:result[0].insertId,
								status:'Skipped'
							});
						});


					}
				});
			}

		});



		return deferred.promise;
	}


	feederModel.saveFeederExecution_Old = function(feederInfo) {
		var deferred 	= Q.defer();

		var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
			queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ feederInfo.status +'", "'+ (Math.floor(Date.now()/1000)) +'")';

		self.db.query(queryInsert)
		.then(function(result) {
			deferred.resolve(result[0]);

		})
		return deferred.promise;
	}

	feederModel.updateFeederExecution = function(feederInfo) {
		var deferred 	= Q.defer();

		// var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
		// 	queryInsert	+= ' VALUES("'+ feederInfo.instanceId +'", "'+ feederInfo.status +'", "'+ (Math.floor(Date.now()/1000)) +'")';

		var columns = [
			'status',
			'completion_timestamp'
			
		];


		var info = [
			feederInfo.status,
			(Math.floor(Date.now()/1000))
		]


		if(feederInfo.length){
			columns.push('count');
			info.push(feederInfo.length);
		}

		if(feederInfo.query){
			columns.push('query');
			info.push(feederInfo.query);
		}


		if(columns.length == info.length){
			var length = columns.length;
			var changes= '';
			for (var i = 0; i < length; i++) {
				if(i!=length-1)
					changes += columns[i] + "=" + '"'+info[i]+'", ';
				else
					changes += columns[i] + "=" + '"'+info[i]+'" ';
			}


			condition = ' WHERE feeder_execution_id="' + feederInfo.id + '"';



			var query = 'UPDATE feeder_execution SET '+changes+condition;

			self.db.query(query)
			.then(function(result) {
				deferred.resolve({
					status:200
				});
			});
		}else{
			deferred.resolve({
				status:0,
				error:'Unmatched columns'
			})
		}



		/*self.db.query(queryInsert)
		.then(function(result) {
			deferred.resolve(result[0]);

		})*/
		return deferred.promise;
	}



	feederModel.insertExecutionStatus = function(instanceId, status, oauth_timestamp) {
		var deferred 	= Q.defer();


		var getRecord = 'SELECT * FROM feeder_execution Where instanceId = "'+ instanceId +'" ';
		self.db.query(getRecord)
		.then(function(result) {

			if(!(result[0]&&result[0].length)){
				var queryInsert	 = 'INSERT INTO feeder_execution (instanceId, status, create_timestamp)';
					queryInsert	+= ' VALUES("'+ instanceId +'", "'+ status +'", "'+ (Date.now()/1000) +'")';

				return self.db.query(queryInsert);
			} else {

                var timestampFromUrl = new Date(Date.now());
                var timestampFromDb = new Date(result[0][0].create_timestamp * 1000);
                var diffTime = (timestampFromUrl - timestampFromDb)/1000;

				var queryUpdate = 'UPDATE feeder_execution';

				if(diffTime > 5){
					queryUpdate    += ' SET status="'+status+'",';
					queryUpdate    += ' create_timestamp="'+(Date.now()/1000)+'"';
				} else {
					queryUpdate    += ' SET status="'+status+'"';
				}


				queryUpdate    += ' WHERE feeder_execution_id='+result[0][0].feeder_execution_id+';';

				return self.db.query(queryUpdate);
			}
		})
		.then(function(result) {

			return self.db.query(getRecord);
		})
		.then(function(result) {
			deferred.resolve(result[0]);
		})
		return deferred.promise;
	}

	feederModel.updateExecutionDataImportAndURI = function(input) {

		var deferred = Q.defer();




		var queryInsert = "INSERT INTO feeder_execution_data (importDefination, instance_id, status, uri,";
			queryInsert+= " timestamp, io_meta_data, executionId,created,updated,failed,warning)";
			queryInsert+= " VALUES('"+ input.importDefination +"', '"
			queryInsert+= input.instanceId +"', '";
			// queryInsert+= input.chunkedData.replace(new RegExp("'","g"),"''") +"', '";
			queryInsert+= input.status +"', '";
			queryInsert+= input.uri +"', '";
			queryInsert+= input.timestamp +"', '";
			queryInsert+= input.ioRecord +"', '";
			queryInsert+= input.feederExecutionId +"', '";
			queryInsert+= input.created +"', '";
			queryInsert+= input.updated +"', '";
			queryInsert+= input.failed +"', '";
			queryInsert+= input.warning +"')";


		self.db.query(queryInsert)
		.then(function(result,error) {
			if(error){
				console.log(error);
			}

			deferred.resolve(result);
		})



		return deferred.promise;

	}

	feederModel.updateInsertFeederInstances = function(siteid,instanceid, segmentId, frequency,campaignId,campaignName){

		var deferred = Q.defer();
		campaignName = (campaignName)?campaignName : '';
		campaignId = (campaignId)?campaignId : 0;


		var queryCheck = "SELECT count(*) as cnt FROM feeder_instance WHERE Instance_Id = '"+instanceid+"'";
		self.db.query(queryCheck)
		.then(function(result) {


			try{

				if(parseInt(result[0][0].cnt) > 0){


					var queryStatement = "UPDATE feeder_instance";
						queryStatement+= "  SET Segment_Id ='"+ segmentId + "',";
					    //queryStatement+= "  Repetation_Time ='"+ frequency + "',";
						queryStatement+= "  Campaign_Id ='"+ campaignId + "',";
						queryStatement+= "  Campaign_Name ='"+ campaignName.replace(new RegExp("'","g"),"''") + "',";
						queryStatement+= "  Repetation_Time ='"+ frequency + "'";
						queryStatement+= "  WHERE Instance_Id='"+ instanceid +"'";


					self.db.query(queryStatement,function(result, error){
						deferred.resolve(result);
					})

				} else {

					var queryStatement = "INSERT INTO feeder_instance (`Instance_Id`, `Site_Id`, `Campaign_Id`, "
						queryStatement+= " `Campaign_Name`, `Segment_Id`, `Repetation_Time`, `Status`) VALUES ("
						queryStatement+= " '"+instanceid+"',";
						queryStatement+= " '"+siteid+"',";
						queryStatement+= " '"+campaignId+"',";
						queryStatement+= " '"+campaignName.replace(new RegExp("'","g"),"''")+"',";
						queryStatement+= " '"+segmentId+"',";
						queryStatement+= " '"+frequency+"',";
						queryStatement+= " 'DRAFT'";
						queryStatement+= " )";


					self.db.query(queryStatement,function(result, error){
						deferred.resolve(result);
					})
				}
			}catch(error){
				console.log(error);
			}
		})
		return deferred.promise;
	}

	feederModel.getFeederForRunOnce = function(repetationTime, instanceid) {
		var deferred = Q.defer();
        var getExistingSegment = 'SELECT fi.Instance_Id, fi.Segment_Id, fi.Campaign_Id, s.SiteId, s.Source_Instance_Config_Id, f.Filter_Query, f.Master_Object ';
        	getExistingSegment+= ' FROM segments s';
        	getExistingSegment+= ' INNER JOIN filters f';
        	getExistingSegment+= ' ON s.Segment_ID = f.Segment_Id';
        	getExistingSegment+= ' INNER JOIN feeder_instance fi';
        	getExistingSegment+= ' ON fi.Segment_Id = s.Segment_ID';
        	getExistingSegment+= ' WHERE fi.Repetation_Time = "'+ repetationTime +'"';
        	getExistingSegment+= ' AND fi.Instance_Id="'+ instanceid +'"';

		self.db.query(getExistingSegment, function(result,error) {
			deferred.resolve(result);
		})
		return deferred.promise;
	}


	feederModel.getCurrentInstanceData = function(siteId,instanceId) {
		var deferred = Q.defer();

		var queryStatement = "SELECT I.Token_Expiry_Time as tokenExpTime, I.Token as token, I.Refresh_Token as refreshToken," ;
			queryStatement+= " I.Base_Url as baseUrl," ;
			queryStatement+= " FI.Repetation_Time as frequency, FI.Segment_Id as segmentId," ;
			queryStatement+= " SIC.User_name as SFDC_userName,SIC.Password as SFDC_password, SIC.Security_token as SFDC_securityToken, SIC.Source_Instance_Config_Id as SICID" ;
			queryStatement+= " FROM instance_ I,feeder_instance FI, source_instance_config SIC,segments S" ;
			queryStatement+= " WHERE I.Site_Id = FI.Site_Id" ;
			queryStatement+= " AND SIC.Site_Id = FI.Site_Id" ;
			queryStatement+= " AND S.Segment_ID = FI.Segment_Id" ;
			queryStatement+= " AND SIC.Source_Instance_Config_Id = S.Source_Instance_Config_Id" ;
			queryStatement+= " AND S.Segment_ID = FI.Segment_Id" ;
			queryStatement+= " AND FI.Site_id = '"+siteId+"' " ;
			queryStatement+= " AND FI.Instance_Id = '"+instanceId+"';" ;


		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}

	feederModel.getImportDefination = function(objectId) {
		var deferred = Q.defer();

		var queryStatement = "SELECT importDefination FROM source_primary_object";
			queryStatement+= " WHERE Source_Primary_Object_Id = " + objectId;

		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;

	}

	feederModel.getFieldMappings = function(objectId) {
		var deferred = Q.defer();

		var queryStatement = "SELECT Source_Field_Id AS sourceField, Target_Field_Id as targetField";
			queryStatement+= "  FROM source_field_mapping WHERE Source_Object_Id = " + objectId;

		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;

	}

	feederModel.getTokenExpTime = function(siteId) {
		var deferred = Q.defer();

		var queryStatement = "SELECT Token_Expiry_Time as tokenExpTime,Refresh_Token as refreshToken";
			queryStatement+= " FROM instance_";
			queryStatement+= " WHERE Site_Id = '" + siteId + "'";

		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		});

		return deferred.promise;

	}

	feederModel.getFeederWithFreq = function(frequency) {
		var deferred = Q.defer();

		var queryStatement = "SELECT FI.Instance_Id as instanceid,FI.Site_Id as siteId, FI.Status as eventType, FI.Repetation_Time as timeStatus";
			queryStatement+= " FROM feeder_instance FI";
			queryStatement+= " WHERE";
			queryStatement+= "  FI.Repetation_Time = '" + frequency + "'";
			queryStatement+= " and FI.Status = 'ACTIVATED'";

		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		});

		return deferred.promise;
	}


	feederModel.getSegmentID = function(instanceid) {
		var deferred = Q.defer();
		var queryStatement = "SELECT FI.Segment_ID,FI.status as status, SIC.Status as instanceStatus ";
			queryStatement+= " FROM feeder_instance as FI, segments as S,source_instance_config SIC";
			queryStatement+= "  WHERE S.Segment_ID = FI.Segment_Id ";
			queryStatement+= "  AND SIC.Source_Instance_Config_Id = S.Source_Instance_Config_Id ";
			queryStatement+= "  AND FI.Instance_Id = '"+instanceid+"' ";


		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		});

		return deferred.promise;
	}




	feederModel.getSegmentJSON = function(segmentId,appType){

		var deferred = Q.defer();
		var queryStatement = "SELECT SegmentJSON FROM segments WHERE Segment_ID ='"+ segmentId + "'";


		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}

	feederModel.get_segment_id = function(instanceid){
		var deferred = Q.defer();

		var queryStatement = 'SELECT FI.Segment_Id as Segment_Id, FE.status as status';
			queryStatement+= ' FROM feeder_execution FE,feeder_instance FI';
			queryStatement+= ' WHERE FE.instanceId = FI.Instance_Id ';
			queryStatement+= ' and FI.Instance_Id = "'+instanceid+'"';





		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}






	/*Old Codes*/

	feederModel.createFeederService = function(getData) {
		var getInstance = 'SELECT * FROM feeder_instance Where Site_Id = "'+ getData.siteId +'" AND Instance_Id = "'+ getData.instanceid +'"';
		self.db.query(getInstance, function(result,error) {
			if(!result.length)
			{
				var queryInsert = 'INSERT INTO feeder_instance (Site_Id, Instance_Id, Status) VALUES("'+ getData.siteId +'", "'+ getData.instanceid +'", "DRAFT")';
				self.db.query(queryInsert);
            }
		})
	}

	feederModel.updateFeederInstance = function(getData, status) {
		var queryUpdate = 'UPDATE feeder_instance SET Campaign_Id= "'+ getData.assetid +'",  Campaign_Name= "'+ getData.assetName +'", Status = "'+ status +'" Where Site_Id = "'+ getData.siteId +'" AND Instance_Id = "'+ getData.instanceid +'"';
		self.db.query(queryUpdate);

	}

	feederModel.updateExecutionStatusWithCompleted = function(instanceId, status, oauth_timestamp) {
		var queryUpdate = 'UPDATE feeder_execution SET status= "'+ status +'" Where instanceId = "'+ instanceId +'" AND status = "Active"';
		self.db.query(queryUpdate);
	}

	// feederModel.updateRecordsCount = function(Filters_Id, totalSize, Segment_ID, Instance_Id) {
	feederModel.updateRecordsCount = function(Filters_Id, totalSize) {
		var deferred = Q.defer();
		var queryUpdate = 'UPDATE filters SET Record_Count= "'+ totalSize +'" Where Filters_Id = "'+ Filters_Id +'"';
		self.db.query(queryUpdate, function(result, error){
			deferred.resolve(result,"status:true");
		});
		return deferred.promise;
	}

	feederModel.updateRecordCountSegment = function(segmentId, totalSegmentContacts) {
		var deferred = Q.defer();
		var queryUpdate = 'UPDATE segments SET Record_Count= "'+ totalSegmentContacts +'" Where Segment_ID = "'+ segmentId +'"';
		self.db.query(queryUpdate, function(result, error){
			deferred.resolve(result,"status:true");
		});
		return deferred.promise;
	}

	/*feederModel.updateExecutionDataImportAndURI = function(importDefination, instanceIdfordb, chunkData, status, uri, timestamp, executionId) {
		var queryInsert = "INSERT INTO feeder_execution_data (importDefination, instance_id, feeder_execution_data, status, uri, timestamp, executionId) VALUES('"+ importDefination +"', '"+ instanceIdfordb +"', '"+ chunkData +"', '"+ status +"', '"+ uri +"', '"+ timestamp +"', '"+ executionId +"')";
		self.db.query(queryInsert);

	}*/
	feederModel.insertSfdcData = function(Filters_Id, Segment_Id, Instance_Id, records, newestTimestamp, executionId) {
		var queryInsert = "INSERT INTO sfdc_Data (filter_id, segment_id, instance_id, sfdc_data, newest_timestamp, execution_id) VALUES('"+ Filters_Id +"', '"+ Segment_Id +"', '"+ Instance_Id +"', '"+ records +"', '"+ newestTimestamp +"', '"+ executionId +"')";
		self.db.query(queryInsert);

	}

	feederModel.updateExecutionImportAndURI = function(importDefination, uri, executionId) {
		var queryInsert = "Update feeder_execution SET importDefination = '"+ importDefination +"', uri = '"+ uri +"' Where feeder_execution_id ='"+ executionId +"'";
		self.db.query(queryInsert);

	}

	feederModel.getFeederStatus = function(getData) {
		var deferred = Q.defer();
		var getUpdate = 'Select Status from feeder_instance Where Instance_Id = "'+ getData.instanceid +'"';
		self.db.query(getUpdate, function(result,error) {
			deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

	feederModel.getExecutionStatus = function(getData, Status) {
		var deferred = Q.defer();
		var getUpdate = 'SELECT * FROM feeder_execution Where instanceId = "'+ getData.instanceid +'" AND status = "' + Status + '"';
		self.db.query(getUpdate, function(result,error) {
			deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

	feederModel.getSfdcDataTimeStamp = function(Filters_Id, Segment_Id, Instance_Id) {
		var deferred = Q.defer();
		var getUpdate = 'SELECT *, MAX(newest_timestamp) as mxts FROM sfdc_Data Where instance_id = "'+ Instance_Id +'" AND filter_id = "' + Filters_Id + '" ORDER BY newest_timestamp LIMIT 1';
		self.db.query(getUpdate, function(result,error) {
			deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}
	feederModel.getExecutionStatusWithSkipped = function(getData, Status) {
		var deferred = Q.defer();
		// var getUpdate = 'SELECT *, MAX(create_timestamp) as mxts FROM feeder_execution Where instanceId = "'+ getData.instanceid +'" AND status = "' + Status + '" ORDER BY create_timestamp LIMIT 1';
		// var getUpdate = 'SELECT * FROM feeder_execution fex INNER JOIN feeder_execution_data fexd ON fex.feeder_execution_id = fexd.executionId Where fex.instanceId = "'+ getData.instanceid +'" AND fex.status = "' + Status + '"';
		var getUpdate = 'SELECT * FROM feeder_execution Where instanceId = "'+ getData.instanceid +'" AND status = "' + Status + '"';
		self.db.query(getUpdate, function(result,error) {
			deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}


	feederModel.getFeederAccToExecutionTime = function(repetationTime, status) {
		var deferred = Q.defer();
        var getExistingSegment = 'SELECT fi.Instance_Id, fi.Segment_Id, fi.Campaign_Id, s.SiteId, s.Source_Instance_Config_Id, f.Filter_Query, f.Master_Object FROM segments s INNER JOIN filters f ON s.Segment_ID = f.Segment_Id INNER JOIN feeder_instance fi ON fi.Segment_Id = s.Segment_ID Where fi.Repetation_Time = "'+ repetationTime +'" AND fi.Status="'+ status +'"';
		self.db.query(getExistingSegment, function(result,error) {
			deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}



	feederModel.getInstance = function(getData) {
		var deferred = Q.defer();
        var getInstance = 'SELECT * FROM source_instance_config Where Site_Id = "'+ getData.siteId +'"';
		self.db.query(getInstance, function(result,error) {
			deferred.resolve(result,"status:true");
		})
		return deferred.promise;
	}

	feederModel.getActiveInstance = function(getData) {
		var deferred = Q.defer();
		var getActiveInstance = 'SELECT Source_Instance_Config_Id, Description, Status, salesforce_controller_confirmation as sfCtrlStatus FROM source_instance_config Where Site_Id = "'+ (getData.siteId||getData.siteid) +'" AND Status NOT IN ("Failed", "New")';
		// console.log(getActiveInstance);
        self.db.query(getActiveInstance, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getSfdcSavedData = function(getData) {
		var deferred = Q.defer();
		var getSfdcSavedData = 'SELECT * FROM soql_query Where Site_Id = "'+ (getData.siteId||getData.siteid) +'" AND Instance_id = "'+ getData.instanceid +'"';
        self.db.query(getSfdcSavedData, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.selectedSfdcObjects = function(getData) {
		var deferred = Q.defer();
		var getSelectedSfdcObjectsChild = 'SELECT * FROM source_primary_object Where Source_Instance_Config_Id ="'+ getData.Source_Instance_Config_Id +'"';

        self.db.query(getSelectedSfdcObjectsChild, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getMappedFields = function(Source_Object_Id) {
		var deferred = Q.defer();
       	var getMappedFields = 'SELECT * FROM source_primary_object Where Source_Primary_Object_Id = "'+ Source_Object_Id +'"';
		self.db.query(getMappedFields, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getMappedFieldForImportDefination = function(getData) {
		var deferred = Q.defer();
       	var getMappedFields = 'SELECT DISTINCT sfm.Source_Field_Name, sfm.Source_Field_Id, sfm.Target_Field_Name, sfm.Target_Field_Id FROM feeder_instance fi INNER JOIN filters f ON fi.Segment_Id = f.Segment_Id INNER JOIN source_field_mapping sfm ON sfm.Source_Object_Id = f.Master_Object Where fi.Instance_Id = "' + getData.instanceid + '"';
		self.db.query(getMappedFields, function(result,error) {
			deferred.resolve(result);
		})
		return deferred.promise;
	}


	feederModel.fieldOperatorUI = function() {
		var deferred = Q.defer();
       	var field_operator_ui = 'SELECT * from field_operator_ui';
		self.db.query(field_operator_ui, function(result,error) {
            deferred.resolve(result);
		});
		return deferred.promise;
	}

	feederModel.getHtmlContent = function(getData) {
		var deferred = Q.defer();
		// var getHtmlContent = 'Select * from feeder_instance fi INNER JOIN segments s ON fi.Segment_Id = s.Segment_ID INNER JOIN filters f ON fi.Segment_Id = f.Segment_Id INNER JOIN source_field_mapping sfm ON sfm.Source_Object_Id = f.Master_Object Where fi.Instance_Id = "'+ getData.instanceid +'"';
       	var getHtmlContent = 'SELECT * FROM soql_query Where Site_Id == "'+ getData.site_id +'" AND Object_Id = "'+ getData.Source_Primary_Object_Id +'" AND Instance_id = "'+ getData.Instance_id +'"';
        self.db.query(getHtmlContent, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getSourceInstanceData = function(getData) {
		var deferred = Q.defer();
       	var getHtmlContent = 'Select * from source_instance_config sf INNER JOIN soql_query sp ON sp.Source_Instance_Config_Id = sf.Source_Instance_Config_Id Where sp.Instance_Id = "'+ getData.instanceid +'"';
        self.db.query(getHtmlContent, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getHtmlSoql = function(getData) {
		var deferred = Q.defer();
        var htmlSoql = 'SELECT * FROM soql_query sf INNER JOIN source_instance_config sp on sf.Source_Instance_Config_Id = sp.Source_Instance_Config_Id ';
        	htmlSoql+= 'INNER JOIN source_primary_object so on sf.Source_Instance_Config_Id = so.Source_Instance_Config_Id Where sf.Site_id ="'+ (getData.siteId||getData.siteid) +'" AND sf.Instance_id ="'+ getData.instanceid +'"';
        self.db.query(htmlSoql, function(result,error) {
            deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getSrcInsId = function(getData) {
		var deferred = Q.defer();

       	var getHtmlContent = 'Select fi.Instance_Id, fi.Status, fi.Segment_Id, fi.Campaign_Id, s.SiteId, s.Source_Instance_Config_Id, f.Filter_Query, f.Filters_Id, f.Master_Object  from feeder_instance fi INNER JOIN segments s ON fi.Segment_Id = s.Segment_ID INNER JOIN filters f ON fi.Segment_Id = f.Segment_Id Where fi.Instance_Id = "'+ getData.instanceid +'"';

		self.db.query(getHtmlContent, function(result, error) {

            deferred.resolve(result,"status:true")
		})
		return deferred.promise;
	}



	//to update importDefination
	feederModel.updateImportDefination = function(masterObjectId, importDefination) {
		var deferred = Q.defer();


		var stringImportDef = JSON.stringify(importDefination);
		var queryUpdate = "UPDATE source_primary_object SET importDefination= '"+ stringImportDef +"' Where Source_Primary_Object_Id = '"+ masterObjectId +"'";


		self.db.query(queryUpdate)
		.then(function (res) {
			deferred.resolve(res);
		})

    	return deferred.promise;

	}

	feederModel.get_operators_all = function(){
		var deferred = Q.defer();

		var queryStatement = "SELECT field_operator.Field_Type_Id,field_type.Field_Type_Name,field_operator.Operator_Id, operator.Operator_Name, operator.Operator_Translation from field_operator inner join operator on field_operator.Operator_Id=operator.Operator_Id inner join field_type on field_operator.Field_Type_Id=field_type.Field_Type_Id";

        self.db.query(queryStatement, function(result, error){
            deferred.resolve(result);
        })
    	return deferred.promise;
	}

	feederModel.get_json = function(segmentId){
		var deferred = Q.defer();
		var queryStatement = "SELECT SegmentJSON FROM segments WHERE Segment_ID ='"+ segmentId + "'";

		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}

	feederModel.save_query = function(segment_id, filter_data){
		var deferred = Q.defer();
		var filtrJson = filter_data.filterJson;
		var filter_query = '"'+filter_data.filterQuery+'"';
		if(filtrJson.id){
			var queryStatement1 = "UPDATE filters SET Filter_JSON ='"+ JSON.stringify(filter_data.filterJson) +"', ";
				queryStatement1+= "Filter_Query ="+ filter_query +" WHERE Filters_Id="+ filtrJson.id +"";

			self.db.query(queryStatement1,function(result, error){
				if(error){
					deferred.resolve(error);
				}else{
					deferred.resolve([result, filtrJson.id] );
				}
			})
		}else{
			var queryStatement2 = "INSERT INTO filters (FilterName,Segment_Id,Filter_Query,Filter_JSON,Master_Object)";
				queryStatement2 += " VALUES('"+ filter_data.filterName +"', '" + segment_id +"', ";
				queryStatement2 += '"'+ filter_data.filterQuery +'"'+", '"+ JSON.stringify(filter_data.filterJson) +"', '"+ filter_data.masterObjectId +"')";

			self.db.query(queryStatement2,function(result, error){
				if(error){
					deferred.resolve(error);
				}else{
					deferred.resolve(result);
				}
			})
		}
		return deferred.promise;
	}

	// obtains json from the table segments for provided segmentId
	feederModel.get_master_object_id = function(MasterObjectName, sourceInstanceConfigId){
		var deferred = Q.defer();
		var queryStatement = "SELECT Source_Primary_Object_Id FROM source_primary_object ";
			queryStatement+= "WHERE Object_Id ='"+ MasterObjectName + "' AND Source_Instance_Config_Id='"+ sourceInstanceConfigId + "'";

		self.db.query(queryStatement,function(result, error){

			deferred.resolve(result);
		})

		return deferred.promise;
	}





	feederModel.get_new_filter_json = function(newFilterId){
		var deferred = Q.defer();
		var queryStatement = "SELECT Filter_JSON FROM filters WHERE Filters_Id ='"+ newFilterId + "'";
		self.db.query(queryStatement,function(result, error){
			deferred.resolve([result, newFilterId]);
		})

		return deferred.promise;
	}

	feederModel.update_filter_json = function(filterJson, filter_Id){
		var deferred = Q.defer();
		var queryStatement = "UPDATE filters SET Filter_JSON ='"+ JSON.stringify(filterJson) + "' WHERE Filters_Id='"+ filter_Id +"'";
		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}

	feederModel.get_Object_json = function(si_config_id, reqObject){// obtains a segment id from the table feeder_instance for provided instanceid
		var deferred = Q.defer();
		var queryStatement = "SELECT Object_Json FROM system_table WHERE Source_Instance_Config_Id ='"+ si_config_id + "' AND Object_Name ='"+ reqObject +"'";
		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}

	feederModel.give_parent_objects = function(objectJson, si_config_id){
		var deferred = Q.defer();
		var fields = objectJson.fields;
		var parentArray = [];
		var j = 0;
		for(var i=0; i<fields.length; i++){
			if(fields[i].type == "reference" && fields[i].relationshipName != "MasterRecord"){
				var referenceTo = fields[i].referenceTo;

				if(referenceTo.length == 1 && fields[i].relationshipName){
					for(var k=0; k<referenceTo.length; k++){
						parentArray[j] = {
							"Object_Name":fields[i].name,
							"Object_Label":fields[i].label,
							"referenceTo":referenceTo[k],
							"type":fields[i].type,
							"Source_Instance_Config_Id":si_config_id,
							"relationshipName":fields[i].relationshipName
						};
						j++;
					}
				}
			}
		}

		deferred.resolve(parentArray);
		return deferred.promise;
	}



	feederModel.get_filter_jsons = function(segmentId){// obtains a segment id from the table feeder_instance for provided instanceid
		var deferred = Q.defer();
		var queryStatement = "SELECT Filter_JSON FROM filters WHERE Segment_ID ='"+ segmentId + "'";
		self.db.query(queryStatement,function(result, error){
			// //console.log("SegmentJSON ", result);
			deferred.resolve(result);
		})

		return deferred.promise;
	}


	feederModel.get_filter_By_Object = function(objectId){// obtains a segment id from the table feeder_instance for provided instanceid
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		console.log(objectId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM filters WHERE Master_Object ='"+ objectId + "'";
		self.db.query(queryStatement,function(result, error){
			// //console.log("SegmentJSON ", result);
			deferred.resolve(result);
		})

		return deferred.promise;
	}

	feederModel.get_Segments_By_Id = function(ids){
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM segments WHERE Segment_ID IN (" + ids.join() +  ")";
		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.get_Segments_By_Source_Instance_Config_Id = function(id){
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM segments WHERE Source_Instance_Config_Id ="+ id;
		self.db.query(queryStatement,function(result, error){
			if(error){
				deferred.reject(error);
			}
			deferred.resolve(result);
		})
		return deferred.promise;
	}


    feederModel.get_Feeder_Instances_By_Segment_Id = function(ids){
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM feeder_instance WHERE Segment_Id IN (" + ids.join() +  ")";
		self.db.query(queryStatement,function(result, error){
			// //console.log("SegmentJSON ", result);
			deferred.resolve(result);
		})
		return deferred.promise;
	}


	feederModel.get_Source_Instance_Config_By_Segment_Id = function(id){
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM segments WHERE Segment_ID = " + id;
		self.db.query(queryStatement,function(result, error){
			// //console.log("SegmentJSON ", result);
			var jsonresult = JSON.parse(JSON.stringify(result));
			var segment = jsonresult[0];
			var queryStatement = "SELECT * FROM source_instance_config WHERE Source_Instance_Config_Id = " + segment.Source_Instance_Config_Id;
			self.db.query(queryStatement,function(result, error){
				// //console.log("SegmentJSON ", result);
				var jsonresult = JSON.parse(JSON.stringify(result));
				var source_instance_config = jsonresult[0];
				deferred.resolve(source_instance_config);
			})
		})
		return deferred.promise;
	}


	feederModel.get_Source_Instance_Config_Status_By_Id= function(id){
				var deferred = Q.defer();
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var queryStatement = "SELECT * FROM source_instance_config WHERE Source_Instance_Config_Id = " + id;
			self.db.query(queryStatement,function(result, error){
				// //console.log("SegmentJSON ", result);
				if(error){
					deferred.reject(error);
				} else {
					var jsonresult = JSON.parse(JSON.stringify(result));
					var source_instance_config = jsonresult[0];
					deferred.resolve({status: source_instance_config.Data_Sync_Status});
				}
		})
		return deferred.promise;
	}



	feederModel.get_Instance_By_Id = function(id){
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM instance_ WHERE Instance_Id = " + id;
		self.db.query(queryStatement,function(result, error){
			// //console.log("SegmentJSON ", result);
	            var jsonresult = JSON.parse(JSON.stringify(result));
				var instance = jsonresult[0];
				deferred.resolve(instance);
		})
		return deferred.promise;
	}


	feederModel.get_Segment_By_Id = function(id){
		//console.log("inside get_filter_jsons, segmentId ", segmentId);
		var deferred = Q.defer();
		var queryStatement = "SELECT * FROM segments WHERE Segment_ID = " + id;
		self.db.query(queryStatement,function(result, error){
			// //console.log("SegmentJSON ", result);
	            var jsonresult = JSON.parse(JSON.stringify(result));
				var segment = jsonresult[0];
				deferred.resolve(segment);
		})
		return deferred.promise;
	}



	feederModel.update_segment_json = function(segmentId, segJson){// obtains a segment id from the table feeder_instance for provided instanceid
		var deferred = Q.defer();
		var queryStatement = "UPDATE segments SET SegmentJSON ='"+ JSON.stringify(segJson) +"' WHERE Segment_ID ='"+ segmentId + "'";
		self.db.query(queryStatement,function(result, error){
			deferred.resolve(result);
		})

		return deferred.promise;
	}


	feederModel.getObjectByName = function(name) {
		var deferred = Q.defer();
		var get = 'Select * from system_table Where Object_Name = "'+ name +'" and Source_Instance_Config_Id = 2';
		self.db.query(get, function(result,error) {
			if(error){
				console.log(error)
			}
			deferred.resolve(result);
		})
		return deferred.promise;
	}

	feederModel.getOperatorsByType = function(type) {
		var deferred = Q.defer();
		var get = 'Select Field_Type_Id from field_type Where Field_Type_Name = "'+ type +'"';
		self.db.query(get, function(result,error) {
			if(result[0] && result[0].Field_Type_Id){
				get = 'Select Operator_Id from field_operator Where Field_Type_Id = "'+ result[0].Field_Type_Id +'"';
				self.db.query(get, function(result,error) {
					if(result.length > 0){
						var ids = _.pluck(result, "Operator_Id");
						get = 'Select * from operator Where Operator_Id in ('+ ids.join(',') +')';
						self.db.query(get, function(result,error) {
							deferred.resolve(result);
						})
					} else {
						deferred.resolve([]);
					}
				})
			} else {
				deferred.resolve([]);
			}
		})
		return deferred.promise;
	}


})(module.exports)
