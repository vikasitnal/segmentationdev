(function(notificationModel) {
	var Q = require('q');
	var self;
	notificationModel.init = function() {
		self = arguments[0];
		return this;
	}
    var feederModel         = require('./feederModel');
    var emailService = require('./../modules/email');



	notificationModel.error = function(err, segmentId, instanceId){
	   feederModel.get_Segment_By_Id(segmentId).then(function(segment){
          feederModel.get_Source_Instance_Config_By_Segment_Id(segmentId).then(function(source_instance_config){
            feederModel.get_Instance_By_Id(instanceId).then(function(instance){
        	     var obj = {
        	     	user: instance,
        	     	segment: segment,
        	     	error: err
        	     };
        	     emailService.send(source_instance_config.Email, obj, 'feedererror', segment.SiteId, instanceId, instance.First_Name + " "+ instance.Last_Name);        		
    	    });
    	  }); 
    	});
	}

	notificationModel.createNotification = function(data) {
		var deferred = Q.defer();
		var now = new Date()
	    var queryInsert = "INSERT INTO notification (`User_name`, `Site_Id`, `Instance_Id`, `To`, `Status`, `Created`, `Type`) VALUES (" + "'" + data.User_name +  "'," +  "'" + data.Site_Id +  "'," + "'" + data.Instance_Id +  "'," +  "'" + data.To +  "'," +  "'" + data.Status +  "'," +  "'" + now.toISOString().slice(0, 19).replace('T', ' ') +  "'," +  "'" + data.Type +  "'" + ")";
        self.db.query(queryInsert, function(result,error) {
        	if(error){
         		deferred.reject(error);
        	} else {
            	deferred.resolve(result,"status:true");
        	}
		})
		return deferred.promise;
	}

	notificationModel.getNotifications = function(query) {
		var deferred = Q.defer();
        var keys = Object.keys(query);
        var query = "SELECT * FROM notification";
        if(query.Site_Id){
          query += "WHERE Site_Id = '" +  query.Site_Id + "'";
        }
        if(query.Instance_Id){
          query += "AND Instance_Id = '" +  query.Instance_Id + "'";
        }
        self.db.query(query, function(result,error) {
             deferred.resolve(result);
		})
		return deferred.promise;
	}
	

})(module.exports)