(function(exampleModel) {
	var Q = require('q');
	var self;
	exampleModel.init = function() {
		self = arguments[0];
		return this;
	}


	exampleModel.getInstances = function() {
		var deferred = Q.defer();
		self.db.query('SELECT * FROM instance_')
		.then(function(result) {
			deferred.resolve([result]);
		});



		return deferred.promise;
	}

	exampleModel.showTables = function(data) {
		var deferred = Q.defer();
		self.db.query('show tables',function(result,error) {
			deferred.resolve([data,result]);
		})
		return deferred.promise;
	}

	


})(module.exports)