// Coded by RavikumarMG on 7/Jun/2017
(function(eloqua_model) {
    var Q = require('q');
    var self;
    eloqua_model.init = function() {
        self = arguments[0];
        return this;
    }

	eloqua_model.getEloquaObject = function(userData, SourceInstanceConfigId){
        var deferred = Q.defer();
		var stmt 	= 'SELECT * FROM instance_ WHERE Site_Id="'+userData.siteId+'"';
        var stmt2 	= 'SELECT * FROM endpoints WHERE Source="Eloqua" and Endpoint_Name = "contact"';
        var stmt3 	= 'SELECT * FROM source_instance_config WHERE Site_Id="' + userData.siteId + '" and Source_Instance_Config_Id="' + SourceInstanceConfigId + '"';
        var queryResult;
        var BaseUrl;
        var endpoint;
        var InstanceConfig;
        var url;
        self.db.query(stmt)
        .then(function(result, error){
            BaseUrl = result[0][0]['Base_Url'];
			return self.db.query(stmt2);
        })
		.then(function(result2, error2){	
			endpoint = result2[0][0]['Endpoint'];
			return self.db.query(stmt3);
        })
		.then(function(result3, error3){
            InstanceConfig = result3[0][0];
            var InstanceConfigUNameLength = Object.keys(InstanceConfig.User_name).length;
			
            if(InstanceConfigUNameLength){
                var url = BaseUrl + endpoint;
				//console.log(url)
                var siteName = userData.siteName;
            }
            deferred.resolve([url]);
        });
        return deferred.promise;
    }

})(module.exports)
