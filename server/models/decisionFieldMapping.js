var lodash = require('lodash');
var CryptoJS = require("crypto-js");
var config = require('config');
var utility = require('./../modules/all');
(function(decisionFieldMapping) {
    var Q = require('q');
    var self;
    decisionFieldMapping.init = function() {
        self = arguments[0];
        self.orm = self.sqDbConfig.define('source_field_mapping_ds', {
            id:{
                type:self.Sequelize.INTEGER,
                autoIncrement:true,
                primaryKey:true
            },
            Source_Field_Id: self.Sequelize.STRING,
            Target_Field_Id: self.Sequelize.STRING,
            Source_Object_Id: self.Sequelize.STRING(255),
            Decision_Instance_Id: self.Sequelize.STRING(255),
            Source_Field_Name: self.Sequelize.STRING,
            Target_Field_Name: self.Sequelize.STRING,
            Source_Field_Type: self.Sequelize.STRING,
            Target_Field_Type: self.Sequelize.STRING,
            Source_Object_Name: self.Sequelize.STRING,
            Query_Relationship: self.Sequelize.STRING
        }, {});


        self.decisionExecution = self.sqDbConfig.define('decision_execution', {
            decision_execution_id:{
                type:self.Sequelize.INTEGER,
                autoIncrement:true,
                primaryKey:true
            },
            instanceid: self.Sequelize.STRING(45),
            decision_status: self.Sequelize.STRING(45),
            decision_yes_count: self.Sequelize.INTEGER(11),
            decision_no_count: self.Sequelize.INTEGER(11),
            decision_errored_count: self.Sequelize.INTEGER(11),
            decision_execution_yes_info: self.Sequelize.STRING(99999999),
            decision_execution_no_info: self.Sequelize.STRING(99999999),
            decision_error_info: self.Sequelize.STRING(99999999),
            decision_start_time: self.Sequelize.STRING(45),
            decision_end_time: self.Sequelize.STRING(45),
            sync_definition: self.Sequelize.STRING(99999999)
        }, {
            freezeTableName:true
        });

        self.sqDbConfig.sync()
        .then(function(result) {

        });

        return this;
    }



    decisionFieldMapping.getDecisionFieldMaps = function(input,sfCtrlStatus,forDeleting) {
        var deferred = Q.defer();

        var query  = 'SELECT emailFieldLabel, emailFieldName FROM system_table ST,source_primary_object SPO';
            query += '  WHERE ST.Source_Instance_Config_Id = SPO.Source_Instance_Config_Id';
            query += '  AND ST.emailStatus = 1 AND ST.Object_Name = "' + input.Source_Object_Id + '"';


        /*if(!sfCtrlStatus && !forDeleting){

            self.db.query(query)
            .then(function(result) {


                var findObj = {
                    Source_Field_Id: result[0][0].emailFieldName,
                    Target_Field_Id: 'C_EmailAddress',
                    Source_Object_Id: input.Source_Object_Id,
                    Decision_Instance_Id: input.Decision_Instance_Id,
                    Source_Field_Name: result[0][0].emailFieldLabel,
                    Target_Field_Name: 'Email Address',
                    Source_Field_Type: 'email',
                    Target_Field_Type: 'text',
                    Source_Object_Name: input.Source_Object_Id,
                    Query_Relationship: input.Source_Object_Id
                }

                return self.orm.findOrCreate({
                    where:findObj,
                    defaults:findObj
                });
            })
            .then(function(result) {
                if(result){
                    getAll();
                }

            })
            .catch(function(error) {
                console.log(error);
                console.log("error");
            })
        } else {
            getAll();
        }*/
        getAll();

        function getAll() {
            self.orm.findAll({
                where: input,
                attributes:[
                    'id',
                    'Source_Field_Id',
                    'Target_Field_Id',
                    'Source_Object_Id',
                    'Source_Field_Name',
                    'Target_Field_Name',
                    'Source_Object_Name',
                    'Query_Relationship'
                ]
            })
            .then(function(result) {
                deferred.resolve(result);
            })
            .catch(function(error) {
                deferred.reject(error);
            });
        }



        return deferred.promise;
    }


    decisionFieldMapping.createDecisionField = function(input) {
        var deferred = Q.defer();

        var query  = 'SELECT emailFieldLabel, emailFieldName FROM system_table ST,source_primary_object SPO';
            query += '  WHERE ST.Source_Instance_Config_Id = SPO.Source_Instance_Config_Id';
            query += '  AND ST.emailStatus = 1 AND ST.Object_Name = SPO.Object_Id AND SPO.Source_Primary_Object_Id = "' + input.Source_Object_Id + '"';


        /*self.db.query(query)
        .then(function(result) {


            var findObj = {
                Source_Field_Id: result[0][0].emailFieldName,
                Target_Field_Id: 'C_EmailAddress',
                Source_Object_Id: input.Source_Object_Id,
                Decision_Instance_Id: input.Decision_Instance_Id,
                Source_Field_Name: result[0][0].emailFieldLabel,
                Target_Field_Name: 'Email Address',
                Source_Field_Type: 'email',
                Target_Field_Type: 'text',
                Source_Object_Name: input.Source_Object_Name,
                Query_Relationship: input.Query_Relationship
            }

            return self.orm.findOrCreate({
                where:findObj,
                defaults:findObj
            });
        })        
        .then(function(result){
            return self.orm.create(input);
        })*/
        self.orm.create(input)
        .then(function(result) {
            deferred.resolve(result);
        })
        .catch(function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

    decisionFieldMapping.deleteDsFieldMapping = function(input) {
        var deferred = Q.defer();


        self.orm.destroy({
            where:input
        })
        .then(function(result) {
            deferred.resolve(result);
        });

        return deferred.promise;
    }


    decisionFieldMapping.deleteAll = function(instanceid) {
        var deferred = Q.defer();
        self.orm.destroy({
            where:{
                Decision_Instance_Id:instanceid
            }
        })
        .then(function(result) {
            deferred.resolve(result);
        });
        return deferred.promise;
    }



})(module.exports)