(function(acl) {
    var Q = require('q');
    var self;
    acl.init = function() {
        self = arguments[0];
        return this;
    }


    acl.findById = function(id) {
        var deferred = Q.defer();
        self.db.query('SELECT * FROM access_control WHERE UserId=' + id)
            .then(function(result) {
                deferred.resolve(result);
            });
        return deferred.promise;
    }


    acl.get = function(siteId, page) {
        var from = (page - 1) * 10;
        var to = from + 10;
        var deferred = Q.defer();

        self.db.query('SELECT * FROM access_control WHERE siteId = ' + siteId + ' LIMIT ' + from + ',' + to)
            .then(function(result) {
                deferred.resolve(result);
            });
        return deferred.promise;
    }


    acl.getUserIds = function(siteId) {
        var deferred = Q.defer();
        self.db.query('SELECT UserId FROM access_control WHERE siteId = ' + siteId)
            .then(function(result) {
                deferred.resolve(result);
            });
        return deferred.promise;
    }


    acl.create = function(payload) {
        var deferred = Q.defer();
        self.db.query('SELECT * FROM access_control WHERE UserId=' + payload.UserId+' AND siteId = ' + payload.SiteId)
            .then(function(result) {
                if (result[0].length > 0) {
                    deferred.reject(new Error("user already has access"));
                } else {
                    var queryInsert = 'INSERT INTO access_control ';
                    queryInsert += ' (UserId, Name,Email,SiteId,Created';
                    queryInsert += ' )';
                    queryInsert += '  VALUES("' + payload.UserId + '",'
                    queryInsert += ' "' + payload.Name + '",';
                    queryInsert += ' "' + payload.Email + '",';
                    queryInsert += ' "' + payload.SiteId + '",';
                    queryInsert += ' "' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '"';
                    queryInsert += ')';
                    self.db.query(queryInsert)
                        .then(function(result, error) {
                            if (error) {
                                deferred.reject(error);
                            }
                            self.db.query('SELECT * FROM access_control  WHERE siteId = ' + payload.SiteId)
                                .then(function(result) {
                                    deferred.resolve(result);
                                });
                        });
                }
            });
        return deferred.promise;
    }


    acl.remove = function(query) {
        var deferred = Q.defer();
        self.db.query('DELETE FROM access_control WHERE UserId=' + query.id)
            .then(function(result) {
                deferred.resolve([result]);
            });
        return deferred.promise;
    }




})(module.exports)