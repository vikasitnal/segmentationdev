/*Node Js In built*/
var fs = require('fs');
var path = require('path');
var env = process.env.NODE_ENV;
/*For Developent Purpose*/
var watch = require('node-watch');

/*Node Js Custom Installed*/
var express = require('express');
var https 	= require('https');
var app = express();


var bodyParser = require('body-parser');
var methodOverride = require('method-override')
var multer = require('multer');
var upload = multer();
var session = require('express-session');
var cookieParser = require('cookie-parser');
var config = require('config'); // gets default json

// var port = process.env.PORT || 8081;

app.use(express.static('client'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(methodOverride())
app.use(upload.array());
app.use(cookieParser());
app.use(session({
    secret: "Segmentation"
}));

var controllers = require("./controllers");
var models = require("./models");
var db = require('./db/dbConnection');
require('./modules/all');
require('./modules/softto');
var eloquaCall = require('./modules/eloquaCallBasic');
var sfdcCall = require('./modules/sfdcCallBasic');
var updateExpiryTime = require('./modules/updateExpiryTime');
var getSfdcObject = require('./modules/getSfdcObject');
var notificationURL = require('./modules/notificationURL');
var buildSOQL = require('./modules/buildSOQL');
var importDefination = require('./modules/importDefination');
var emailService = require('./modules/email');

var controllerObj = {};
const Sequelize = require('sequelize');
const sqDbConfig = new Sequelize(config.dbConfig.database, config.dbConfig.user, config.dbConfig.password, {
    host: config.dbConfig.host,
    dialect: 'mysql',
    logging:false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    operatorsAliases: false
});
// var decisionFieldMapping = require('./../models/source_field_mapping_ds')(sequelize, Sequelize);
db.init(config);

//Setup the models
models.init({
	db:db,
	Sequelize:Sequelize,
	sqDbConfig:sqDbConfig
}, function(modelsArray) {
    // modelsArray.decisionFieldMapping = decisionFieldMapping;
    // Setup the routes
    controllers.init(app, modelsArray, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination, controllerObj, emailService);
});

var i = 0;
watch('./server', {
    recursive: true
}, function(evt, name) {
    i++;
    if (i == 3) {
        console.log("``````````````````````````````````")
        console.log("``````````````````````````````````")
        console.log("``````````````````````````````````")
        console.log("``````````````````````````````````")
        console.log("`````````Refreshed````````````````")
        console.log("``````````````````````````````````")
        console.log("``````````````````````````````````")
        console.log("``````````````````````````````````")
        console.log("``````````````````````````````````")
        var reload = require('require-reload')(require);
        reload('./modules/all');
        var models = reload("./models");
        var eloquaCall = reload('./modules/eloquaCallBasic');
        var sfdcCall = reload('./modules/sfdcCallBasic');
        var updateExpiryTime = reload('./modules/updateExpiryTime');
        var getSfdcObject = reload('./modules/getSfdcObject');
        var notificationURL = reload('./modules/notificationURL');
        var buildSOQL = reload('./modules/buildSOQL');
        var importDefination = reload('./modules/importDefination');
        var emailService = require('./modules/email');
        reload('./modules/parameterisedSoql');


        controllers = reload("./controllers");
        models.reload({
        	db:db,
        	Sequelize:Sequelize,
        	sqDbConfig:sqDbConfig
        }, function(modelsArray) {
            // modelsArray.decisionFieldMapping = decisionFieldMapping;
            // Setup the routes
            controllers.reload(app, modelsArray, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination, controllerObj, emailService);
        });
        i = 0;
    }

});


/*app.get('/code-refresh',function(req,res) {

	var reload = require('require-reload')(require);
	var eloquaCall 			= reload('./modules/eloquaCallBasic');
	var sfdcCall 			= reload('./modules/sfdcCallBasic');
	var updateExpiryTime 	= reload('./modules/updateExpiryTime');
	var getSfdcObject 		= reload('./modules/getSfdcObject');
	var notificationURL 	= reload('./modules/notificationURL');
	var buildSOQL 			= reload('./modules/buildSOQL');
	var importDefination 	= reload('./modules/importDefination');


	controllers = reload("./controllers");
	models.reload(db,function(modelsArray) {
		// Setup the routes
		controllers.reload(app, modelsArray, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj);
	});
	// controllers.reload(app, modelsArray, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj);
	res.send("Code Refreshed!...");
})*/

/** For debugging
 *
 *
 *88888888888888888*****/
/*var reload = require('require-reload')(require);
var n = 0;

function removeMiddlewares(route, i, routes) {
    switch (route.handle.name) {
        case 'getInstances':
            routes.splice(i, 1);
    }
    if (route.route)
        route.route.stack.forEach(removeMiddlewares);
}*/

/*app.get('/*',function(req,res,next) {
	try{
		var routes = app._router.stack;

		if(n%5==0){
			// routes.forEach(removeMiddlewares);
		}
		n++;


		controllers = reload("./controllers");
		models = reload("./models");
		db = reload('./db/dbConnection');
		eloquaCall = reload('./modules/eloquaCallBasic');

		db.init(config);


		//Setup the models
		models.init(db,function(modelsArray) {

			// Setup the routes
			controllers.init(app,modelsArray,eloquaCall);
		});

		// res.send("this text");

		next();

	} catch(error){
		console.log(error);
	}
});*/

/*88888888888888888888*/
/*88888888888888888888*/
/*88888888888888888888*/
/*88888888888888888888*/
/*88888888888888888888*/
var options = {
    key: fs.readFileSync(__dirname + '/PortQiiPrivate.key'),
    ca: fs.readFileSync(__dirname + '/cert.pem'),
    cert: fs.readFileSync(__dirname + '/STAR_portqii_com.crt')
};
// console.log(options);

if (env == 'production') {
    /*Running Server*/
    var server = https.createServer(options, app);

    server.on('listening', function() {
        console.log('ok, server is running');
    });

    server.listen(config.get('port'), function() {
        console.log(server.address());
        console.log('Started!');
    });

} else {
    app.listen(config.get('port'), function() {
        console.log("Example app listening at http://%s:%s", config.get('host'), config.get('port'));
    });
}
module.exports = app;