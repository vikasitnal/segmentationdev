

(function(db) {
	var mysql = require('mysql');
	var Q = require('q');

	var con,config,pool;

	// console.log("config ", config);// undefined
	// var pool  = mysql.createPool({
	// 	"host":			"ec2-54-202-113-3.us-west-2.compute.amazonaws.com",
	// 	"user":			"root",
	// 	"password":		"p0rtq1!@Aug",
	// 	"database":		"segmentation"
	// });
	db.init = function(configParam) {
		config = configParam;
		pool = mysql.createPool({
			"host": config.dbConfig.host,
			"user": config.dbConfig.user,
			"password": config.dbConfig.password,
			"database": config.dbConfig.database
		});
		
	}

	db.query = function(queryStatement,resultantFunction,largeConnectionStatus) {
		var deferred = Q.defer();

		pool.getConnection(function(err, con){

			if(err){
				deferred.reject(err);
				return;
			}

			if(typeof(queryStatement) == 'string' && typeof(resultantFunction) == 'function'){
				con.query(queryStatement, function (err, result) {
					if(err){
						console.log("-------------------")
						console.log("-------------------")
						console.log("-------------------")
						console.error(new Date()+': Sql',queryStatement);
						console.error(new Date()+': Error in database',err);
						console.log("-------------------")
						console.log("-------------------")
						console.log("-------------------")
						deferred.reject(err);
						return;
					}
					resultantFunction(result,err,con);
					con.release();
				});
			}else if(typeof(queryStatement) == 'string'){
				con.query(queryStatement, function (err, result1) {
					if(err){
						console.log("-------------------")
						console.log("-------------------")
						console.log("-------------------")
						console.error(new Date()+': Sql',queryStatement);
						console.error(new Date()+': Error in database',err);
						console.log("-------------------")
						console.log("-------------------")
						console.log("-------------------")
						deferred.reject(err);
						return;
					}
					deferred.resolve([result1,err]);
					con.release();

				});
			}
		});
		//
		// var options = {
		    // host: config.dbConfig.host,
		    // user: config.dbConfig.user,
		    // password: config.dbConfig.password,
		    // database: config.dbConfig.database
		// };
		// con = mysql.createConnection(options);

		// con.connect(function(err) {
			// if(err!=null){
				// if(typeof(resultantFunction) == 'function'){
					// console.log(err);
					// resultantFunction({errorStatus:1,error:err},err,con);
				// }
				// else
					// console.err('Expecting string and function in 1st and 2nd argument respectively')
			// }else{

			// }

		// });

		// if(typeof(queryStatement) == 'string' && typeof(resultantFunction) == 'function'){
					// console.log("hello");
					// con.query(queryStatement, function (err, result) {
						// console.log(queryStatement);
						// console.log(result);
						// resultantFunction(result,err,con);
						// con.end();
					// });
				// }else if(typeof(queryStatement) == 'string'){
					// con.query(queryStatement, function (err, result) {
						// deferred.resolve([result,err]);
						// con.end();
					// });
				// }


		return deferred.promise;
	}
})(module.exports);
