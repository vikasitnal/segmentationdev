(function(notificaionController) {
	notificaionController.init = function(app, model) {

		//--passing data to ui (external source system, source instances,  all saved instance)
		var getNotifications = function(req,res) {
		   model.notificationModel.getNotifications(req.query).then(function(result){
		   	  res.status(200).send(result);
		   });
		}

		app.get('/notifications', function(req, res) {
			getNotifications(req,res);
		});


		var createNotification = function(req,res) {
		   model.notificationModel.createNotification(req.body).then(function(result){
		   	  res.status(200).send(result);
		   });
		}

		app.post('/notifications', function(req, res) {
			createNotification(req,res);
		});

	
	}
})(module.exports)
