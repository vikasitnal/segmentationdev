(function(aclController) {
	aclController.init = function(app,model) {


		aclController.get = function(siteId, query, callback) {
			model.accessControlModel.get(siteId, query.page)
				.then(function(result) {
					callback(null, result)
				}, callback);
		}

		app.get('/acl',function(req, res, next){
			aclController.get(req.session.appInfo.siteId || req.session.appInfo.siteid, req.query, function(err, result){
				if(err){
					next(err);
				} else {
					res.status(200).send(result);
				}
			});
		});



		aclController.getuserids = function(siteId, callback) {
			model.accessControlModel.getUserIds(siteId)
				.then(function(result) {
					callback(null, result);
				}, callback);
		}

		app.get('/acl/userids',function(req, res, next){
			aclController.getuserids(req.session.appInfo.siteId || req.session.appInfo.siteid, function(err, result){
				if(err){
					next(err);
				} else {
					res.status(200).send(result);
				}
			});
		});

    aclController.post = function(payload, callback) {
      model.accessControlModel.create(payload)
        .then(function(result) {
					callback(null, result)
        }, callback);
    }

    app.post('/acl',function(req, res, next){
			req.body.SiteId = req.session.appInfo.siteId || req.session.appInfo.siteid;
			aclController.post(req.body, function(err, result){
				if(err){
					next(err);
				} else {
					res.status(200).send(result);
				}
			});
		});


    aclController._delete = function(query, callback) {
      model.accessControlModel.remove(query)
        .then(function(result) {
					callback(null, result)
        }, callback);
    }

    app.delete('/acl/:id',function(req, res, next){
			aclController._delete(req.params, function(err, result){
				if(err){
					next(err);
				} else {
					res.status(200).send(result);
				}
			});
		});

	}

})(module.exports);
