 "use strict";

(function(appConfigController) {
	appConfigController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime,
	 getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj) {
		var request = require('request-promise');
		// we just need to test --DONE--
		// rahul and sunil 04-06-17
		//-- after installing eloqua going to call this function.
		//-- using to save data in local system.
		controllerObj.getLoginDetails = function(req,res, next) {
			var loginInfo = req.session;
			model.licenseChecking_model.checkValidity({ siteId: loginInfo.appInfo.siteId || loginInfo.appInfo.siteid}).then(function(result){
				loginInfo.license = result[0];
				res.send(loginInfo);
			}, function(err){
                 next(err);
			});
		}
		app.get('/instance/getLoginDetails',function(req,res) {
			controllerObj.getLoginDetails(req,res);
		})

		// this is first step when you going to click on admin panel

		controllerObj.instanceEnableUrl = function(req,res) {
			//--getting data from config file
			var eloquaConfig 	= config.get('eloquaConfig');
			var getData = req.query;


			model.instanceModel.checkSiteId(getData)
			.then(function(data) {
				var eloquaConfig 	= config.get('eloquaConfig');
				var callBackUrl 	= getData.CallbackUrl;
				var info 			= callBackUrl + '|' + getData.siteName + '|' + getData.siteId;
				var clientId 		= eloquaConfig.AppId;
				var redirectUri 	= eloquaConfig.redirectUri;
				//rediercting for OAUTH to get token
				res.redirect('https://login.eloqua.com/auth/oauth2/authorize?response_type=code&client_id=' + clientId + '&redirect_uri=' + redirectUri + '&scope=full&state=' + info +', method="auto",code=302');
			});
		}


		app.post('/instance/enableUrl', function(req, res) {
			controllerObj.instanceEnableUrl.apply(null,arguments);
		});


		//-- rediercting from eloqua
		app.get('/instance/saveToken', function(req, res) {
			var getData 		= req.query;
			//exploding info to get callBackUrl
			var callBackUrl 	= getData.state.substr(0, getData.state.indexOf("|"));
			var eloquaConfig 	= config.get('eloquaConfig');

			var tokenResult;
			//to get access_token and refresh_token
			//--POST call
			eloquaCall.post({
				host 	 : eloquaConfig.TokenUrl,
				username : eloquaConfig.AppId,
				password : eloquaConfig.ClientSecret,
				body: {
					'grant_type' 	: 	'authorization_code',
					'code'			: 	getData.code,
					'redirect_uri'	:	eloquaConfig.redirectUri
				}
			})
            .then(function(response) {
				tokenResult = response && response.result;
				var token 	= tokenResult.access_token;
				// to update token and userInfo
				if(token.length > 0){
					return eloquaCall.get({
						host	: 'https://login.eloqua.com/id',
						oauth	: true,
						token	: token
					})
				}
				else{
					res.send('Token Not Available');
				}
            })
			.then(function(response){

				var result = response && response.result;

				model.instanceModel.updateToken(result, tokenResult)
				.then(function(result) {
					if(result.status = 'true')
					{
						res.redirect(callBackUrl);
					}
					else
					{
						var resultPost = {
							'success' : false,
							'token' : result
						}
						res.send(resultPost);
					}
				})

			})
		});


		controllerObj.instanceViewConfig = function(req,res) {
			var getParams = req.query;


      		softTo.oAuthSigning(req, config).then(function(result){
    			model.instanceModel.checkToken(getParams)
    			.then(function(result) {


    				if(result)
    				{

    					var sess 	= req.session;
    					sess.status = "";
    					var val 	= {
    						siteId		: req.query.siteId,
    						siteName	: req.query.siteName,
    						userName	: req.query.userName,
    						showMsgFor	: '',
    					};
    					var login_data, validity, check_trial;

    					model.settings_model.login(val)
    					.then(function(paramData) {

    						login_data = paramData;
    						return model.licenseChecking_model.checkValidity(val);
    					})
    					.then(function(paramData1) {

    						validity = paramData1[0];
    						if (login_data.length != 0) {
    							if (validity) {


    								var session_content = {
    									siteid		: login_data[0]['Site_Id'],
    									site_name	: login_data[0]['Site_Name'],
    									user_name	: login_data[0]['User_Name'],
    									licenseId	: login_data[0]['license_id'],
    									instanceid  : (sess.appInfo)?sess.appInfo.instanceid:null
    								};




    								sess.appInfo 	= session_content;
    								sess.status 	= 'logged_in';
    							} else {
    								res.redirect('/#/licenseExpired');
    							}
    							if (sess.status == 'logged_in') {




    								var session_data = sess.appInfo;
    								if (session_data.siteid) {
    									if (session_data.site_name != '' && session_data.user_name != '') {

    										res.redirect('/#/home');
    									}
    								}
    								else
    								{
    									res.redirect('/#/sessionExpired');
    								}
    							}
    						} else {
	    						res.send("Please try to re-install the App.");

    						}
    					});
    				}
    			});
			}, function(err){
				res.send(err);
			});


		}

		// coded by RavikumarMG 30/05/2017
        app.get('/instance/viewConfig', function(req, res) {
        	controllerObj.instanceViewConfig(req,res);
        });

		app.get('/testing',function(req,res) {
			console.log("HEllo");
			res.redirect('/#/home');
			// res.send("Hello");
		})

	}
})(module.exports)
