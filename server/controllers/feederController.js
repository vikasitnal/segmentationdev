(function(feederController) {
	feederController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj) {
		var request = require('request-promise');
		var Q 		= require('q');
		var _ 		= require('lodash');
        var sf = require('jsforce');
				var async = require('async');
        const { spawn } = require('child_process');
		var fs = require('fs');
		// var free = spawn('free', ['-m']);




		function logMemory() {
			// console.log(process);
			/*var data = JSON.stringify()

				var cstmzdData = '\n\n' + Date.now() ;
					cstmzdData+= '\n' + new Date();
					cstmzdData+= '\n' + '-------------';
					cstmzdData+= '\n' + '-------------';
					cstmzdData+= '\n' + `${data}`;*/
			/*var free = spawn('free', ['-m']);
			free.stdout.on('data', (data) => {
				var cstmzdData = '\n\n' + Date.now() ;
					cstmzdData+= '\n' + new Date();
					cstmzdData+= '\n' + '-------------'i
					cstmzdData+= '\n' + '-------------';
					cstmzdData+= '\n' + `${data}`;

			 	fs.appendFile('Memory.txt', cstmzdData, function (err) {
					if (err) throw err;
				});
			});

			free.stderr.on('data', (data) => {
				var cstmzdData = '\n\n' + Date.now() ;
					cstmzdData+= '\n' + new Date();
					cstmzdData+= '\n' + '-------------';
					cstmzdData+= '\n' + '-------------';
					cstmzdData+= '\n' + `${data}`;

			 	fs.appendFile('Memory.txt', cstmzdData, function (err) {
					if (err) throw err;
				});
			});

			free.on('close', (code) => {
			  // console.log(`child process exited with code ${code}`);
			});*/
		}


		/*Services*/
		var updateToken = function(siteId,deferred) {
			if(!deferred)
				var deferred = Q.defer();

			model.feederModel.getTokenExpTime(siteId)
			.then(function(result) {


				data = result[0];

				if ( Date.now() + 18e5 > new Date(parseInt(data.tokenExpTime)*1e3) ) {
					updateExpiryTime.get({
				        body: {
				        	siteId:siteId
				        },
				        token: data.refreshToken
				    })
				    .then(function() {
				    	updateToken(siteId,deferred);
				    })
				    .catch(function(result) {
				    	console.log("***************")
				    	console.log("***************")
				    	console.log(result);
				    	console.log("***************")
				    	console.log("***************")
				    });
				} else {
					deferred.resolve(true)
				}
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});
			return deferred.promise;

		}

		/*Services end*/

		/*All apis*/

		/*Decision specific*/

		/*app.get('/feederService/onFeederDecisionConfig',function() {
			controllerObj.onFeederDecisionConfig.apply(null,arguments);
		});


		app.post('/feederService/onFeederDecisionCreate',function() {
			controllerObj.onFeederDecisionCreate.apply(null,arguments);
		});

		app.post('/feederService/onFeederDecisionNotify',function() {
			controllerObj.onFeederDecisionNotify.apply(null,arguments);
		});

		app.post('/feederService/onDecisionSave',function() {
			controllerObj.onDecisionSave.apply(null,arguments);
		});*/


		/*Feeder*/

		app.post('/feederService/getParentObjects', function(req, resp){
			controllerObj.getParentObjects(req,resp);
		});

		//working
		app.post('/feederService/create', function(req, res) {
			var getData = req.query;
			model.feederModel.createFeederService(getData)
			res.status(200).send('{"requiresConfiguration":true}');
			controllerObj.thisFunction(req,res);
		});

		app.post('/feederService/copy', function(req, res) {
			controllerObj.feederCopy(req,res);
		});


		app.get('/feederService/configure', function(req, res) {
			controllerObj.feederConfigure(req,res);
		});

		 app.delete('/feederService/delete',function(req,resp){
            controllerObj.deleteFeederService(req,resp);

        });


		app.post('/feederService/getContactsCount', function(req, res){
			controllerObj.getContactsCount(req,res);
		});


		app.post('/feederService/getContactsRecords', function(req, res){
			controllerObj.getContactsRecords(req,res);
		});


		app.post('/feederService/getContactsColumns', function(req, res){
			controllerObj.getContactsColumns(req,res);
		});



		app.post('/feederService/parentFields', function(req, res) {
			controllerObj.feederServiceParentFields(req,res);
		});


		app.post('/feederService/mappedFields', function(req, res) {
			controllerObj.feederServiceMappedFields(req, res);
		});


		app.post('/feederService/saveSegmentJson', function(req, res){
			controllerObj.saveSegmentJson(req,res);
		})


		app.post('/feederService/notification', function(req, res) {
			controllerObj.feederServiceNotification(req,res);
		});

		app.get('/feederService/getSegment', function(req, res){
			controllerObj.feederServiceGetSegment(req,res);
		});


		app.post('/feederService/getExistingSegment', function(req, res){
			controllerObj.getFeederExistingSegment(req,res);
		});


	    app.get('/feederService/checkSync', function(req, res){
			controllerObj.checkSync(req,res);
		});


		app.post('/feederService/validateSOQL', function(req, res){
			controllerObj.validateSOQL(req,res);
		});


		app.post('/feederService/getExistingSegments', function(req, res){
			controllerObj.getExistingSegments(req,res);
		});


		app.get('/feederService/getExternalSys', function(req, res) {
			controllerObj.getExternalSys(req,res);
		});

		app.get('/feederService/getunittestingdata', function(req, res) {
			controllerObj.getunittestingdata(req,res);
		});


		app.post('/feederService/getSOQLObjectFields', function(req, res) {
			controllerObj.getSOQLObjectFields(req,res);
		});




		/*Feeder*/

		controllerObj.getSOQLObjectFields = function(req,res) {
			var appInfo = req.session.appInfo;
			if(req.body.Source_Instance_Config_Id && req.body.Object_Id && (appInfo.siteId||appInfo.siteid)){

				model.adminConfigModel.getObjectFieldJson(req.body.Source_Instance_Config_Id, appInfo.siteId||appInfo.siteid , req.body.Object_Id)
				.then(function(result) {
					try{
						var fields 	= JSON.parse(result[0].Object_Json).fields;
						var allFields = [];
						fields.forEach(function(item) {
							allFields.push({
								'label':item.label,
								'name' : item.name,
								'type' : item.type
							});
						});
						res.send(allFields);
						
					}catch(error){
						console.log(error);
					}
				});
			} else {
				res.status(401).send("Not logged in from application");
			}
		}

		controllerObj.getExternalSys = function(req,res) {
			var getData = req.session.appInfo;

			getData.instanceid = req.query.key;



			var noOfCallsDone = 0;
			var resultantData = {};
			var completeAsync = function(){
				if(noOfCallsDone==3){
					res.send(resultantData)
				}
			}
			//--getting all active Instance
			model.feederModel.getActiveInstance(getData)
			.then(function(getActiveInstance) {
				resultantData.getActiveInstance = getActiveInstance;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			})
			//--getting all saved mapping
			model.feederModel.getSfdcSavedData(getData)
			.then(function(getSfdcSavedData) {
				resultantData.getSfdcSavedData = getSfdcSavedData;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			})

			model.feederModel.getHtmlSoql(getData)
			.then(function(getHtmlSoql) {
				resultantData.getHtmlSoql = getHtmlSoql;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			})
		}

		var unittestingvalues = {
	     string: {
				 value: "a"
			 },
			 url: {
				value: "a"
			},
			 textarea: {
				 value: "a"
			 },
			 email: {
				 value: "a"
			 },
			 phone: {
				 value: "90"
			 },
			 number: {
				 value: 1,
				 start: 1,
				 end: 100
			 },
			 double: {
				 value: 1.11,
				 start: 1.11,
				 end: 10.45
			 },
			 date: {
				 value: "2017-08-31T18:30:00.000Z",
				 start: "2017-08-31T18:30:00.000Z",
				 end: "2017-09-31T18:30:00.000Z"
			 },
			 datetime: {
				 value: "2017-08-31T18:30:00.000Z",
				 start: "2017-08-31T18:30:00.000Z",
				 end: "2017-09-31T18:30:00.000Z"
			 },
			 currency: {
				 value: "IN"
			 }
		}
    // unit testing data
		try {
			controllerObj.getunittestingdata = function(req, res){
				var conditions = [];
				model.feederModel.getObjectByName(req.query.name).then(function(objectraw){
					var object = objectraw[0];
					  var objectjson = JSON.parse(object.Object_Json);
						 async.forEach(objectjson.fields, function(field, callback){
							 if(field.name && field.type){
								 model.feederModel.getOperatorsByType(field.type).then(function(operators){
									 async.forEach(JSON.parse(JSON.stringify(operators)), function(operator, callbacktwo){
									   try {

											 if(operator.Operator_Name == 'between' || operator.Operator_Name == 'notbetween' || operator.Operator_Name == 'dynamicWithin' ){
												 if(unittestingvalues[field.type]){
													 var json = {"json":{"name": "unit testing","setFrequency":"Run Once","sourceInstanceConfigId":object.Source_Instance_Config_Id,"createdAt":1523437075,"updatedAt":1523437075,"elements":[{"MasterObject": object.Object_Name ,"filter":{"name": field.type + operator.Operator_Label,"type":"Filter","elements":[{"type":"element","elementType":1,"fieldName":field.name,"operator":null,"condition":{"type":field.type,"operator":operator.Operator_Name,"start": unittestingvalues[field.type].start,"end": unittestingvalues[field.type].end  }}]}}],"id":null}}
													 conditions.push({type: field.name + " " + operator.Operator_Label, json: json});
												 }
											 } else {
												 if(unittestingvalues[field.type]){
													 var json = {"json":{"name": "unit testing","setFrequency":"Run Once","sourceInstanceConfigId":object.Source_Instance_Config_Id,"createdAt":1523437075,"updatedAt":1523437075,"elements":[{"MasterObject": object.Object_Name ,"filter":{"name": field.type + operator.Operator_Label,"type":"Filter","elements":[{"type":"element","elementType":1,"fieldName":field.name,"operator":null,"condition":{"type":field.type,"operator":operator.Operator_Name,"value": unittestingvalues[field.type].value }}]}}],"id":null}}
													 conditions.push({type: field.name + " " + operator.Operator_Label, json: json});
												 }else {
													 if(field.type == 'picklist'){
														 var json = {"json":{"name": "unit testing","setFrequency":"Run Once","sourceInstanceConfigId":object.Source_Instance_Config_Id,"createdAt":1523437075,"updatedAt":1523437075,"elements":[{"MasterObject": object.Object_Name ,"filter":{"name": field.type + operator.Operator_Label,"type":"Filter","elements":[{"type":"element","elementType":1,"fieldName":field.name,"operator":null,"condition":{"type":field.type,"operator":operator.Operator_Name,"value": field.picklistValues[0].value }}]}}],"id":null}}
														 conditions.push({type: field.name + " " + operator.Operator_Label, json: json});
													 }
												 }
											 }
									   } catch (e) {
									   	console.log(e);
									   }
										 callbacktwo();
									}, function(err){
										if(err){
											callback(err);
										} else {
											callback();
										}
									})
								 }, function(err){
									 callback(err);
								 });
							 } else {
								 callback();
							 }
						 }, function(err){
							 if(err){
								 res.status(500).send(err);
							 } else {
								 res.status(200).send(conditions)
							 }
						 })
				}, function(err){
					res.status(500).send(err);
				})
			}
		} catch (e) {
			console.log(e);
		} finally {

		}

		/*All apis end*/


		/* Modified New*/

		//working
		controllerObj.validateSOQL = function(req, res) {
		    var data = req.body;

		    var userInfo;


		    model.adminConfigModel.getInstanceInfoById(data.selExtSys.Source_Instance_Config_Id)
		        .then(function(result) {

		            userInfo = result[0];
		            salesforceLogin(userInfo);

		        });


		    function salesforceLogin(userInfo) {
		        if (userInfo && userInfo.userName && userInfo.password && userInfo.securityToken) {
		            var conn = new sf.Connection({

		            });

		            var password = all.decrypt(userInfo.password, config.eloquaConfig.AppId);
		            conn.login(userInfo.userName, password + userInfo.securityToken, function(err, userInfo) {
		                if (err) {
		                    res.send({
		                        result: false,
		                        msg: err.toString()
		                    });
		                } else {

		                    data.query = data.query.replace(/\(sinceLastSync\)/g, JSON.stringify(new Date()).replace(/\"/g,''));
		                    data.query = data.query.replace(/\(sinceLastSync\{date\}\)/g, JSON.stringify(new Date()).replace(/\"/g,'').split('T')[0]);

		                    var query = 'SELECT count() FROM ' + data.selObject.Object_Id + ' WHERE ' + data.query;


		                    conn.query(query, function(err, result) {
		                        if (err) {
		                            res.status(200).send({
		                                result: false,
		                                msg: err.toString()
		                            });
		                        } else {
		                            if (result.done) {
		                                res.status(200).send({
		                                    result: true
		                                });
		                            }
		                        }
		                    });
		                }
		            });
		        }
		    }

		}

		controllerObj.feederCopy = function(req,res) {
			console.log(req.query);
			res.send("save me");
		}

		controllerObj.thisFunction = function(req,res) {
			console.log("Pileup",req.query);
		}



		controllerObj.feederConfigure = function(req,res) {
			
			
			var getData = req.query;

			model.accessControlModel.findById(getData.userId || getData.userid).then(function(acl){
				
				if(acl[0] && acl[0][0] && acl[0][0].UserId == getData.userId || getData.userid){
					var tokenResult;
					var sessionData 	= req.session;
					var redirectUrl = 'https://apps.portqii.com/edidev/#/feeder?key='+req.query.instanceid;
					



					var appInfo 	= {
						siteId		: req.query.siteId,
						siteName	: req.query.siteName,
						instanceid	: req.query.instanceid,
						userName	: req.query.userName,
						campaignId  : req.query.assetid,
						campaignName: req.query.assetName,
						assetType   : req.query.assetType,
					};

					
					if(req.query && req.query.assetName){

						sessionData.appInfo = appInfo;

						
						req.session.appConfig = {
							type:appInfo.assetType
						}

						

						if(getData.assetid && getData.assetName){
							var status = 'DRAFT';
						
							//model.feederModel.updateAssetId(getData, status)
						}

						
						model.instanceModel.checkToken(getData)
						.then(function(result){
								
							tokenResult = result;
							return model.feederModel.getInstance(getData);
						})
						.then(function(result) {
							if(result) {
								
								var url = tokenResult[0].Base_Url+'/api/cloud/1.0/feeders/instances/' + getData.instanceid;
								var data = { requiresConfiguration : true };
								
								return eloquaCall.put({
									host	: url,
									oauth	: true,
									body	: data,
									json    : true,
									token	: tokenResult[0].Token,
									siteId  : getData.siteId
								})
							}else{
							}
						})
						.then(function(response){
							
							var result = response && response.result;

						})
						.catch(function(error){
							if(error.statusCode != 412){
								res.send(error);
							}
							else{
								res.redirect(redirectUrl);
							}
							// console.error(new Date()+': Error in Feeder controller ->',error);
						})
						.finally(function() {
							console.log(redirectUrl);
							res.redirect(redirectUrl);
						});


					} else {
						var CampaignSaveHTML= '';

						CampaignSaveHTML+= '<html>';
						CampaignSaveHTML+= '<body class="background-white">';
						CampaignSaveHTML+= '	<div>Please save the Campaign before opening the External Data Intelligence Feeder';
						CampaignSaveHTML+= '</body>';
						CampaignSaveHTML+= '<style>';
						CampaignSaveHTML+= '	.background-white{';
						CampaignSaveHTML+= '		background:white;';
						CampaignSaveHTML+= '		font-size:18px;';
						CampaignSaveHTML+= '		font-weight:bold;';
						CampaignSaveHTML+= '		text-align:center;';
						CampaignSaveHTML+= '	}';
						CampaignSaveHTML+= '	div{';
						CampaignSaveHTML+= '		width:80%;';
						CampaignSaveHTML+= '		margin:auto;';
						CampaignSaveHTML+= '	}';
						CampaignSaveHTML+= '</style>';
						CampaignSaveHTML+= '</html>';


						res.send(CampaignSaveHTML);
					}

				} else {
					res.redirect('/#/accessDenied');
				}



			})
		}


		controllerObj.getExistingSegments = function(req,res) {
			var getData = req.session.appInfo;
			model.feederModel.getExistingSegment(getData)
			.then(function(result){
				res.send(result);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		controllerObj.getContactsCount = function(req,res) {

			if(!req.body.segmentID){
				res.send({
					count:0,
					queryIssues:[],
					counts:[],
					updatedOn:new Date()
				});
			}

			/*Initializing Variables*/
			var userInfo={},filters = [],countResult;
			model.feederModel.getSfdcUserData(req.body.instanceConfigId)
			.then(function(result) {
				userInfo = result;
				return model.feederModel.findSegmentFilters(req.body.segmentID);
			})
			.then(function(result) {
				filters = result;
				return getSfdcObject.getQueriesCount(userInfo,filters,req.body.segmentID);
			})
			.then(function(result) {
				countResult = result;
				return model.feederModel.updateSegmentCount({
					segmentID:req.body.segmentID,
					count:countResult.totalCount
				});
			})
			.then(function(result) {
				res.send({
					count:countResult.totalCount,
					queryIssues:countResult.issues,
					counts:countResult.counts,
					segmentIssue:countResult.segmentIssue,
					updatedOn:new Date()
				});

				validateDraftInEloqua();
			})
			.catch(function(error) {
				res.send({
					segmentIssue:[error],
					updatedOn:new Date()
				});
			});

			function validateDraftInEloqua() {
				if(countResult.segmentIssue){
					if(countResult.segmentIssue.length){
						validDraft(true);
					} else {
						validDraft(false);
					}
				} else {
					validDraft(false);
				}
			}

			function validDraft(status) {


				var appInfo = req.session.appInfo;
				model.instanceModel.checkToken({
					siteId:appInfo.siteid||appInfo.siteId
				})
				.then(function(result){
					tokenResult = result;
					var url = tokenResult[0].Base_Url+'/api/cloud/1.0/feeders/instances/' + appInfo.instanceid;
					var data = { requiresConfiguration : status };

					console.log(data);





					return eloquaCall.put({
						host	: url,
						oauth	: true,
						body	: data,
						json    : true,
						token	: tokenResult[0].Token,
						siteId  : appInfo.siteid||appInfo.siteId
					});

				})
				.then(function(response) {
					var result = response && response.result;
					console.log(result);
				})
				.catch(function(error) {
					console.log(error);
				})
			}
		}

		controllerObj.getContactsRecords = function(req,res) {

			/*Initializing Variables*/
			var userInfo={},
				filters = [],
				fields = [],
				countResult;
			model.feederModel.getSfdcUserData(req.body.instanceConfigId)
			.then(function(result) {
				userInfo = result;
				return model.feederModel.findSegmentFilters(req.body.segmentID);
			})
			.then(function(result) {
				filters = result;

				return model.feederModel.getSegment(req.body.segmentID)
			})
			.then(function(result) {

				segmentInfo = {
            		query:result[0].query,
            		id:req.body.segmentID
            	}

				return model.feederModel.getContactColumns(req.body.primaryObjectId)
			})
			.then(function(result) {
				fields = result;

				return getSfdcObject.getQueriesRecords2(userInfo,filters,segmentInfo,fields);
			})
			.then(function(result) {


				res.send({
					records:result,
					updatedOn:new Date()
				});
			});
		}

		controllerObj.getContactsColumns = function(req,res) {
			model.feederModel.getContactColumns(req.body.primaryObjectId)
			.then(function(result) {
				res.send(result);
			})
		}









		/*Old*/
		//for new
		//
		app.post('/feederService/selectedSFDCObjects', function(req, res) {

			var getData = {
				Source_Instance_Config_Id : req.body.Source_Instance_Config_Id
			};

			model.feederModel.selectedSfdcObjects(getData)
			.then(function(data) {
				res.send(data);
			});
		});

		controllerObj.feederServiceParentFields = function(req,res) {
			var session = req.session;
			var siteId = session.appInfo.siteId||session.appInfo.siteid;
			var noOfCallsDone = 0;
			var resultantData = {};
			var referenceTo = req.body.referenceTo;
			var completeAsync = function(){
				if(noOfCallsDone==3){
					res.send(resultantData)
				}
			}

			model.feederModel.getOperators()
			.then(function(data) {
				resultantData.operators = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});

			model.feederModel.fieldOperatorUI()
			.then(function(data) {

				resultantData.fieldOperatorUI = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});

			model.adminConfigModel.getObjectFieldJson(req.body.Source_Instance_Config_Id, siteId, referenceTo)
			.then(function(data) {
				var childObjects 	= [];
				var parentObjects 	= [];
				var allFields 		= [];
				var picklistValuesArray = [];
				var objectsList		= [];
				var objects 		= JSON.parse(data[0].Object_Json)
				var temp 			= objects.childRelationships
				var fields 			= objects.fields;
				if(temp)
				{
					for(var i = 0; i <= temp.length-1; i++)
					{
						var fieldValue 		= temp[i].field
						var fieldId 		= temp[i].childSObject
						childObjects.push({"childValue": fieldValue, "childId" : fieldId})
					}
				}
				if(fields)
				{

					for(var x = 0; x <= fields.length-1; x++)
					{
						if(!(fields[x].type == 'textarea' || fields[x].type == 'address')){
							allFields.push({'label':fields[x].label, 'name' : fields[x].name, 'type' : fields[x].type});
						}

						var q = fields[x].referenceTo;
						if(q.length)
						{
							var fieldLabel 	= fields[x].label;
							var fieldName	= fields[x].name;
							parentObjects.push({'parentValue':fieldLabel, 'parentId' : fieldName})
						}
						var picklistValues = fields[x].picklistValues;
						if(picklistValues.length){
							var picklistName	= fields[x].name;
							var picklistdata	= fields[x].picklistValues;
							picklistValuesArray.push({'name':picklistName, 'picklist' : picklistdata})
						}
					}

				}
				objectsList.push({'childObjects':childObjects, 'parentObjects' : parentObjects, 'allFields' : allFields, 'picklistValuesArray' : picklistValuesArray})
				resultantData.objectsList = objectsList;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});
		}

		controllerObj.feederServiceMappedFields = function(req,res) {
			var noOfCallsDone = 0;
			var resultantData = {};
			var completeAsync = function(){
				if(noOfCallsDone==4){
					res.send(resultantData);
				}
			}
			var siteId;

			if(req.session.appInfo.siteId){
				siteId = req.session.appInfo.siteId;
			}else{
				siteId = req.session.appInfo.siteid;
			}

			model.feederModel.getMappedFields(req.body.Source_Primary_Object_Id, req.body.Object_Name)
			.then(function(data) {
				resultantData.mappedFields = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});

			model.feederModel.getOperators()
			.then(function(data) {
				resultantData.operators = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});

			model.feederModel.fieldOperatorUI()
			.then(function(data) {
				resultantData.fieldOperatorUI = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});

			model.adminConfigModel.getObjectFieldJson(req.body.Source_Instance_Config_Id, siteId, req.body.Object_Id)
			.then(function(data) {
				var childObjects 	= [];
				var parentObjects 	= [];
				var allFields 		= [];
				var picklistValuesArray = [];
				var objectsList		= [];

				if(data.length){

					var objects 		= JSON.parse(data[0].Object_Json)
					var temp 			= objects.childRelationships
					var fields 			= objects.fields;

					if(temp)
					{
						for(var i = 0; i <= temp.length-1; i++)
						{
							var fieldValue 		= temp[i].field
							var fieldId 		= temp[i].childSObject
							childObjects.push({"childValue": fieldValue, "childId" : fieldId})
						}
					}
					if(fields)
					{
						for(var x = 0; x <= fields.length-1; x++)
						{
							if(fields[x].type == 'id' || fields[x].type == 'reference'){

							}
							else{
								allFields.push({'label':fields[x].label, 'name' : fields[x].name, 'type' : fields[x].type})
							}
							var q = fields[x].referenceTo;
							if(q.length)
							{
								var fieldLabel 	= fields[x].label;
								var fieldName	= fields[x].name;
								parentObjects.push({'parentValue':fieldLabel, 'parentId' : fieldName})
							}
							var picklistValues = fields[x].picklistValues;
							if(picklistValues.length)
							{
								var picklistName	= fields[x].name;
								var picklistdata	= fields[x].picklistValues;
								picklistValuesArray.push({'name':picklistName, 'picklist' : picklistdata})
							}
						}
					}
					objectsList.push({'childObjects':childObjects, 'parentObjects' : parentObjects, 'allFields' : allFields, 'picklistValuesArray' : picklistValuesArray})
					resultantData.objectsList = objectsList;
					noOfCallsDone++;
					completeAsync();
				} else {
					objectsList.push({
						'childObjects':[],
						'parentObjects' : [],
						'allFields' : [],
						'picklistValuesArray' : []
					});
					resultantData.objectsList = objectsList;
					noOfCallsDone++;
					completeAsync();
				}
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});
		}

		controllerObj.deleteFeederService = function(req,res){
			model.feederModel.deleteFeederInstance(req.query.instanceid,req.query.siteId);
			res.status(200).send("OK");
		}

		controllerObj.feederServiceGetSegment = function(req,res) {

			var sess = req.session.appInfo;
			/*var appInfo 	= {
				siteId		: (sess.siteId||sess.siteid),
				siteName	: sess.siteName,
				instanceid	: sess.instanceid
			};*/
			var segmentId,status,instanceStatus;
			if(req.session.appConfig)
				var appType = req.session.appConfig.type;





			model.feederModel.getSegmentID(sess.instanceid)
			.then(function(result){

				if(result && result[0]){
					segmentId = result[0].Segment_ID;
					instanceStatus = result[0].instanceStatus;

					if(result[0].status)
						status    = result[0].status.toUpperCase();
				}

				if(segmentId && segmentId > 0){
					return model.feederModel.getSegmentJSON(result[0].Segment_ID,appType)
				}else{
					status = 'DRAFT';

					res.send({
						status:0,
						segmentStatus:'DRAFT'
					});
				}
			})
			.then(function(result){

				if(result.length){

					if(instanceStatus == 'Inactive')
						status = 'INACTIVE';

					res.send({
						status:1,
						SegmentId:segmentId ,
						segmentStatus:status||'DRAFT' ,
						SegmentJSON:result[0].SegmentJSON
					});
				} else {
					res.send({
						status:0,
						segmentStatus:'DRAFT'
					});
				}



			});
		}

		controllerObj.saveSegmentJson = function(req,res) {




			var session = req.session;
			var siteId;
			var instanceid;
			var userName;
			var campaignId;
			var campaignName;

			if(session.appInfo){
				siteId = session.appInfo.siteId;
				instanceid = req.query.key;
				userName = session.appInfo.userName;
				campaignId = session.appInfo.campaignId;
				campaignName = session.appInfo.campaignName;

			}




			var postData 		= req.body;
			var segmentID;
			var resultJson 		= '';
			var segmentJson 	= postData.json;
			var segJson;
			var filJson;
			var segElements;
			var segFilters;
			var filJsons;
			var elems;


			/*Initiating Variables*/
			var segmentFiltersIds = [],
				editableFiltersIds = [],
				filtersWithQuery = [];


			// savingSegment();
			model.feederModel.getSegmentID(instanceid)
			.then(function(result) {
				if(result[0] && result[0].instanceStatus == 'Inactive'){
					res.send({
						status:0,
						message:"INACTIVE DISABLED",
						segmentId:0
					});
				} else {
					savingSegment();
				}
			})
			.catch(function(error) {
				res.send(error);
			});


			function savingSegment() {


				model.feederModel.saveSegment(segmentJson, siteId, userName, session.appConfig.type)
				.then(function(result){

					console.log("1")
					
					var statements = [];

					segmentID = result;
					return model.feederModel.findSegmentFilters(segmentID);

				})
				.then(function(result){


					console.log("2")

					segmentFiltersIds = result.map(function(item) {
						return item.id;
					});


					return	buildSOQL.build(segmentID);
				})
				.then(function(result) {



					console.log("3")


					filtersWithQuery = result;


					/*************************/
					var newCountFilters = result.length;
					var deletableFiltersIds = [];

					deletableFiltersIds = segmentFiltersIds.splice(newCountFilters);
					editableFiltersIds = segmentFiltersIds.splice(0,newCountFilters);

					/*************************/

					return model.feederModel.delSegmentFilters(deletableFiltersIds.join(','));
				})
				.then(function(result){
					console.log("4")
                	return model.feederModel.updateSegmentQueries(filtersWithQuery,segmentID);
	           	 })
				.then(function(result) {
					console.log("5")

					return model.feederModel.updateInsertFilters(editableFiltersIds,filtersWithQuery,segmentID);
				})
				.then(function(result) {
					console.log("6")

					return model.feederModel.updateInsertFeederInstances(siteId,instanceid,segmentID,segmentJson.setFrequency,campaignId,campaignName);
				})
				.then(function(result) {
					console.log("7")

					res.send({status:1, segmentId:segmentID});
				})
				.catch(function(error) {
					res.send(error);
				});
			}
		}

		/*var events = require('events');
		var eventEmitter = new events.EventEmitter();

		eventEmitter.on('checkTokenAcrossSiteId')*/
		var multiSiteIdConf = {};
		multiSiteIdConf.groupSiteId = function(queryParams) {
			if(!this['siteId'+queryParams.siteId]){
				this['siteId'+queryParams.siteId] = [];
			}
			var notExists = !this['siteId'+queryParams.siteId].includes(queryParams.instanceid);

			if(notExists)
				this['siteId'+queryParams.siteId].push(queryParams.instanceid);


			return this;
		}

		multiSiteIdConf.checkMultipleExists = function(queryParams) {
			return this['siteId'+queryParams.siteId].length>1;
		}
		multiSiteIdConf.destroyMultipleExists = function(queryParams) {
			if(queryParams && queryParams.siteId)
				this['siteId'+queryParams.siteId] = null;
			else{
				/*for(key in this){
					if(key.indexOf('siteId')>-1){
						this[key] = null;
					}
				}*/
			}
		}


		var x1a = 0;

		controllerObj.feederServiceNotification = function(req, response) {


			if (global.gc) {
				global.gc();
			} else {
				console.log('No GC hook! Start your program as `node --expose-gc file.js`.');
			}




		    var queryParams = req.query;

		    /*{
		    	instanceid: '8db1192d-c087-4e5c-bc32-1e8ccc409b98',
		    	assetid: '976',
		    	siteId: '250973722',
		    	siteName: 'TechnologyPartnerPORTQIIPTELTD',
		    	assetName: 'Experiment 002',
		    	eventType: 'Draft',
		    	oauth_consumer_key: 'f7b33a3e-b64d-4d14-969d-20be1620217c',
		    	oauth_nonce: '1252645828',
		    	oauth_signature_method: 'HMAC-SHA1',
		    	oauth_timestamp: '1515479173',
		    	oauth_version: '1.0',
		    	oauth_signature: 'Y2xR5m+1mOeC2s20yUFs5atwamU='
		    }*/









		    /*Conditional Steps*/
		    if (response){
				response.status(204).send("");
		    }



		    var currentInstanceData, // 1.User data (e.g. sfdc data, segmentid,token,frequency)
		        currentExecutionData, // 9.Feeder execution data
		        /*{
		        	tokenExpTime: '1513949498',
		        	token: 'MjUwOTczNzIyOjFwdXVnWFhNdzhrQ2h6THd3amtUTXZORTlYR2c2fnhxa2RIWk5vbFdVRlM5TVBOSktTaHlFQ29vOUlaRWFCTHlUd2xsWGRBczJjYWdpQUwzckh0NjBwSWRid1lER3F2LVZuVEg=',
		        	refreshToken: 'MjUwOTczNzIyOjFYflNWRjZzLXUwQ1huWGk2LUFQMXhzMkpnWG5sRjMyVFI2eWlOVm1nRnF1Sm1icncxcTloN1hxdGloZzFZbX4wWEwxdTdkYXZ+WTBPMnJOcUhCTTgwMkwxYnRWY3JxYW55NHU=',
		        	baseUrl: 'https://secure.p02.eloqua.com',
		        	frequency: 'Run Once',
		        	segmentId: 5,
		        	SFDC_userName: 'sunil.choudhary@crmsci.com',
		        	SFDC_password: 'manoj&123',
		        	SFDC_securityToken: 'eN1yNgElDChsBDhPFp2JW5z2'
		        }*/
		        importDefination, // 2.import defination to put into eloqua
		        filters = [], // 3.Filters in segment for feeder has been used
		        fieldMappings, // 4.Field mapping from salesforce to eloqua
		        currentFeederObject, // 5.Object Id to get import defination
		        importEndPoint, // 6.Importing eloqua end point
		        segmentInfo = {}, // 7.Soql Query and segmentID to process
		        isExecutionComplete = false,
		        eloquaErrorStatus = false,
		        moveSecondStage = true,
		        importEndJSON, // 8.Importing eloqua end point resultant JSON
		        dataLength = 0;



		    var setFeederStatus = function() {


		        switch (queryParams.eventType.toLowerCase()) {
		            case 'activated':
		                startExecutionSave('In Progress');
		                saveActivationDetails('ACTIVATED');
		                break;
		            case 'draft':
		                saveActivationDetails('DRAFT');
		                break;
		            case 'completed':
		                saveActivationDetails('COMPLETED');
		                break;
		            default:
		                break;
		        }


		    }


		    setFeederStatus();


		    function saveActivationDetails(actType) {
		        model.feederModel.saveActivationType({
		                status: actType,
		                instanceid: queryParams.instanceid,
		                campaignId: queryParams.assetid,
		                campaignName: queryParams.assetName
		            })
		            .then(function() {

		            })
		    }

		    var executionSaveObj = {};
		    function startExecutionSave(status,query,length) {
		        if (status == 'In Progress') {
		            model.feederModel.saveFeederExecution({
		                    instanceId: queryParams.instanceid,
		                    status: status,
		                    cron:(queryParams.cron || false)
		                })
		                .then(function(result) {
		                    currentExecutionData = {
		                        feederExecutionID: result.insertId
		                    }

		                    if (result.status == 'In Progress') {
		                        checkRunOnce();
		                    }

		                });
		        } else {
		        	if(length){
		        		length = dataLength = length+dataLength;
		        	}


		            model.feederModel.updateFeederExecution({
		                    id: currentExecutionData.feederExecutionID,
		                    status: status,
		                    length: length,
		                    query:query
		                })
		                .then(function(result) {
		                	if(status == 'In Progress - Stage 5 completed'){
		                		/*console.log('ses',x1a);
		                		try{
		                			executionSaveObj.callback();
		                		}catch(error){
		                			console.log(error);
		                		}*/
		                	}
		                });
		        }

		    }

		    function feederExecutionError(input) {

				model.feederModel.feederExecutionError({
					instanceid:queryParams.instanceid,
					errStatus:input.errStatus,
					errMessage:input.errMessage
				});
		    }




		    /* Details on Current Instance*/
		    var setCurrentInstanceData = function(deferred) {
		        if (!deferred)
		            var deferred = Q.defer();

		        model.feederModel.getCurrentInstanceData(queryParams.siteId, queryParams.instanceid)
		            .then(function(result) {

		                currentInstanceData = result[0]; // #1
		                deferred.resolve(true);

		            });
		        return deferred.promise;

		    }

		    var checkRunOnce = function() {
		        setCurrentInstanceData()
		            .then(function() {
		                if (currentInstanceData.frequency === 'Run Once') {
		                    // initialization();
		                    checkTokenStartAndEnd();
		                } else {
		                    // initialization();
		                    checkTokenStartAndEnd();
		                }
		            });
		    }


		    function checkTokenStartAndEnd() {
		    	model.instanceModel.getTokenTimes(queryParams.siteId)
		    	.then(function(result){
		    		if(result && result.length){
		    			if(result[0].startTime === null){
		    				updateTokenStartAndEnd(result[0]);
		    			} else {
		    				initialization();
		    			}
		    		}
		    	});
		    }

		    function updateTokenStartAndEnd(input) {
		    	var expiryTimeMilliSecs = parseInt(input.expiryTime)*1000;
		    	model.instanceModel.setTokenTimes({
		    		/*set start time 2 hours before the expiry time.*/
		    		startTime:(expiryTimeMilliSecs-72e5),
		    		/*setting end time as last expected update time*/
		    		endTime:(expiryTimeMilliSecs-288e5),
		    		siteId:queryParams.siteId
		    	})
		    	.then(function(result) {
		    		if(result){
		    			initialization();
		    		}
		    	})
		    }




		    /*token times should be single variable throughout chunks within single activation*/
		    var tokenTimes;

		    var initialization = function() {

			    	model.instanceModel.getTokenTimes(queryParams.siteId)
			    	.then(function(result) {
			    		tokenTimes = result[0];
			    		return model.feederModel.findSegmentFilters(currentInstanceData.segmentId)
			    	})		        
		            .then(function(result) {

		                filters = result; // #3
		                currentFeederObject = result[0].objectId; // #6

		                return model.feederModel.getSegment(currentInstanceData.segmentId)
		            })
		            .then(function(result) {

		            	segmentInfo = {
		            		query:result[0].query,
		            		id:currentInstanceData.segmentId
		            	}


		                return model.feederModel.getImportDefination(currentFeederObject);

		            })
		            .then(function(result) {

		                importDefination = JSON.parse(result[0].importDefination); // #2

		                return model.feederModel.getContactColumns(currentFeederObject);
		            })
		            .then(function(result) {


		                fieldMappings = result; // #5

		                var source = 'Eloqua';
		                var Endpoint_name = 'Imports';
		                return model.adminConfigModel.getSfdcEndpoint(source, Endpoint_name)
		            })
		            .then(function(result) {

		                importEndPoint = currentInstanceData.baseUrl + result[0].endpoint; //# 6

		                var recordsExistStatus = false;
		                getSfdcObject.getQueriesRecords2({
			                    userName: currentInstanceData.SFDC_userName,
			                    password: currentInstanceData.SFDC_password,
			                    securityToken: currentInstanceData.SFDC_securityToken
			                }, filters,
			                segmentInfo,
			                fieldMappings,
			                currentExecutionData.feederExecutionID,
			                function(records) {
			                	if (!records.length && recordsExistStatus) {
				                    startExecutionSave("No Data");
				                    return;
				                } else {
				                	recordsExistStatus = true;
				                }


				                if(!eloquaErrorStatus)
			                		simultaneousEloqImporting(records);
			                },
			                function() {
			                	isExecutionComplete = true;
			                }, 
			                function(err){
			                	feederExecutionError({
									errStatus:"SOQL Error",
									errMessage:err.toString()
									.replace(/(?:\r\n|\r|\n)/g, ' ')
									.replace('MALFORMED_QUERY: ',' ')
									.replace(/[\s-]+/g,' ')
			                	});
				                startExecutionSave("SOQL Error");
			                   // model.notificationModel.error(err, currentInstanceData.segmentId, queryParams.instanceid);
			                },
			                function(query) {
				                startExecutionSave("Query Found",query);
			                });
		            })

		    }

		    var	firstStageComplete = false;
				secondStageComplete = false;
				thirdStageComplete = false;
				fourthStageComplete = false;


		    function simultaneousEloqImporting(records) {

		    	setFeederStatus  = null;
				saveActivationDetails = null;
				setCurrentInstanceData = null;
				checkTokenStartAndEnd = null;
				updateTokenStartAndEnd = null;
				initialization = null;
				filters = null;
				segmentInfo = null;


		    	var eloquaImportingData = [],
			        chunkedImportingData = [],
			        urlMappedRecords = [],
			        ioRecords = [],
			        propSync = {},
			        recordsProcessing = {};

			    checkToken();




			    function checkToken() {

			    	multiSiteIdConf.groupSiteId(queryParams);


			    	multiSiteIdConf.checkMultipleExists(queryParams);

		    		tokenTimes.startTime = parseInt(tokenTimes.startTime);
		    		tokenTimes.endTime = parseInt(tokenTimes.endTime);


		    		if(tokenTimes.startTime <= Date.now() && !multiSiteIdConf.checkMultipleExists(queryParams)){
		    			/* First update database for the next starting time because of asynchronous */

		    			tokenTimes.startTime = (Date.now()+216e5);
		    			tokenTimes.endTime = (Date.now()+216e5);

		    			model.instanceModel.setTokenTimes({
		    				/*Set token time to next*/
				    		startTime:tokenTimes.startTime,
				    		endTime:tokenTimes.endTime,
				    		siteId:queryParams.siteId
				    	})
				    	.then(function(result) {
				    		return eloquaCall.forceUpdateToken(queryParams.siteId)
				    	})
				    	.then(function(result) {


				    		currentInstanceData.token = result.access_token;


		    				tokenTimes.endTime = (Date.now()+216e5);

				    		model.instanceModel.setTokenTimes({
			    				/*Set token time to next*/
					    		startTime:tokenTimes.startTime,
					    		endTime:tokenTimes.endTime,
					    		siteId:queryParams.siteId
					    	})
				    	})
				    	.then(function() {
		    				forNextAsynOperations();
				    	})

		    		} else {
				    	var timeVal = setTimeout(function() {
			    			forNextAsynOperations();
			    			clearTimeout(timeVal);
			    			timeVal = null;
				    	}, 3000);
		    		}

			    }

			    function forNextAsynOperations() {
		    		model.instanceModel.getTokenTimes(queryParams.siteId)
		    		.then(function(result) {
		    			tokenTimes = result[0];
		    			currentInstanceData.token = tokenTimes.token;

		    			if(tokenTimes.startTime === tokenTimes.endTime && tokenTimes.startTime <= Date.now()){
		    				forNextAsynOperations();
		    			}
		    			else{
		    				prepareChunkImportData();
		    			}
		    		});
			    }




			    function prepareChunkImportData() {

			        /*records.forEach(function(record) {
			            var recordEntity = {};
			            fieldMappings.forEach(function(fieldMap) {
			                recordEntity[fieldMap.targetField] = record[fieldMap.sourceField] || '';
			            })
			            eloquaImportingData.push(recordEntity);
			        });*/




			        // chunkedImportingData = [eloquaImportingData.slice()];
			        chunkedImportingData = [records.slice()];



			        eloquaImportingData = null;
			        records = null;
			        eloquaImport();
			    }

			    function eloquaImport() {


			        importDefination.syncActions[0].destination = '{{FeederInstance(' + queryParams.instanceid.replace(/-/g, "") + ')}}';


			        var throughImportCount = 0;

			        /*Multiple import defination*/
			        chunkedImportingData.forEach(function(record) {

			            var ioRecord = {};

			            ioRecord.Step1 = {
			                name: "Import Defination",
			                input: {
			                    host: importEndPoint,
			                    oauth: true,
			                    body: importDefination,
			                    token: 'token'
			                }
			            }


			            if(!moveSecondStage)
			            	return;


			            eloquaCall.post({
			                    host: importEndPoint,
			                    oauth: true,
			                    body: importDefination,
			                    token: currentInstanceData.token,
			                    siteId:queryParams.siteId
			                })
			                .then(function(response) {
			                	if(response.token){
			                		currentInstanceData.token = response.token;
			                	}
								var result = response && response.result;
			                	if(result){

				                    importEndJSON = result;
				                    /* Posing error certain cases*/
				                    /* TypeError: Cannot read property 'uri' of undefined */
				                    var url = currentInstanceData.baseUrl + '/api/bulk/2.0' + importEndJSON.uri + '/data';

				                    ioRecord.Step1.output = result;

					                startExecutionSave("In Progress - Stage 1 completed");

				                    ioRecord.Step2 = {
				                        name: "Transmit data to  staging area",
				                        input: {
				                            host: url,
				                            oauth: true,
				                            body: 'feeder_execution_data',
				                            token: 'token'
				                        }
				                    }

				                    urlMappedRecords.push({
				                        uri: importEndJSON.uri,
				                        instanceid: queryParams.instanceid,
				                        data: record,
				                        ioRecord: ioRecord
				                    });


				                    return eloquaCall.post({
				                        host: url,
				                        oauth: true,
				                        body: record,
				                        token: currentInstanceData.token,
			                    		siteId:queryParams.siteId
				                    });
			                	} else {
			                		moveSecondStage = false;


			                		feederExecutionError({
										errStatus:"Eloqua Error",
										errMessage: "Import End JSON undefined"
				                	});
					                startExecutionSave("Eloqua Error");
			                	}


			                },function importDefinationError(error) {
			                	console.log('---------');
			                	console.log('---------');
			                	console.log('-----importDefinationError----');
			                	console.log(error);
			                	console.log('---------');
			                	console.log('---------');
			                	console.log('---------');
			                })
			                .then(function(response) {
			                	if(response.token){
			                		currentInstanceData.token = response.token;
			                	}

								var result = response && response.result;


			                	if(moveSecondStage){
				                    throughImportCount++;
					                startExecutionSave("In Progress - Stage 2 completed",undefined, record.length);
				                    ioRecord.Step2.output = result;

				                    if (throughImportCount === chunkedImportingData.length) {
				                        syncEloquaData();
				                        chunkedImportingData = null;
				                        if(isExecutionComplete){
				                        	firstStageComplete = true;
				                        }
				                    }
			                	}
			                })
			                .catch(eloquaError);
			        });
			    }


			    function syncEloquaData() {

			        var throughSyncCount = 0;
			        var throughSyncCountCatch = 0;

			        var syncUrl = currentInstanceData.baseUrl + '/api/bulk/2.0/syncs';

			        /*Multiple Import Defination*/


			        urlMappedRecords.forEach(function(item) {

			            item.ioRecord.step3 = {
			                name: 'Transmit data from staging area to eloqua',
			                input: {
			                    host: syncUrl,
			                    oauth: true,
			                    body: {
			                        syncedInstanceUri: item.uri
			                    },
			                    token: 'token'
			                }
			            }



			            eloquaCall.post({
			                    host: syncUrl,
			                    oauth: true,
			                    body: {
			                        syncedInstanceUri: item.uri
			                    },
			                    token: currentInstanceData.token,
			                    siteId:queryParams.siteId
			                })
			                .then(function(response) {
			                	if(response.token){
			                		currentInstanceData.token = response.token;
			                	}

								var result = response && response.result;
					            startExecutionSave("In Progress - Stage 3 completed");

			                    item.ioRecord.step3.output = result;
			                    throughSyncCount++;

			                    item.syncStatusUri = result.uri;

			                    if (throughSyncCount === urlMappedRecords.length) {
			                    	var timeVal = setTimeout(function() {
			                        	checkSyncStatus();
				                        if(firstStageComplete){
				                        	secondStageComplete = true;
				                        }
				                        clearTimeout(timeVal);
				                        timeVal = null;
			                    	}, 2000);
			                    	/*change it to 2000 */
			                    }
			                })
			                .catch(eloquaError);
			        })

			    }


			    function checkSyncStatus() {

		            var throughCheckSyncCount = 0,
		                newPendingRecords = [];

		            if(!currentInstanceData){
		            	return;
		            }

		            var syncStatusUrl = currentInstanceData.baseUrl + '/api/bulk/2.0';



		            /*Multiple Import Defination*/

		            urlMappedRecords.forEach(function(item) {

		                if (!item.ioRecord.step4) {
		                    item.ioRecord.step4 = {
		                        name: 'Verify Sync',
		                        input: [{
		                            host: syncStatusUrl + item.syncStatusUri,
		                            oauth: true,
		                            token: 'token'
		                        }]
		                    }
		                } else {
		                    item.ioRecord.step4.input.push({
		                        host: syncStatusUrl + item.syncStatusUri,
		                        oauth: true,
		                        token: 'token'
		                    })
		                }




		                eloquaCall.get({
		                        host: syncStatusUrl + item.syncStatusUri,
		                        oauth: true,
		                        token: currentInstanceData.token,
			                    siteId:queryParams.siteId
		                    })
		                    .then(function(response) {
			                	if(response.token){
			                		currentInstanceData.token = response.token;
			                	}

								var result = response && response.result;
					            startExecutionSave("In Progress - Stage 4 completed");

		                        var syncStatusProp = JSON.parse(result);
		                        if (!item.ioRecord.step4.output)
		                            item.ioRecord.step4.output = [syncStatusProp];
		                        else
		                            item.ioRecord.step4.output.push(syncStatusProp);

		                        item.status = syncStatusProp.status;

		                        throughCheckSyncCount++;
		                        if (throughCheckSyncCount === urlMappedRecords.length) {
		                            verifySyncLogs();

			                        if(secondStageComplete){
			                        	thirdStageComplete = true;
			                        }
		                        }

		                    })
		                    .catch(function(res) {

			                	feederExecutionError({
									errStatus:"Eloqua Error",
									errMessage:res.toString()
									.replace(/(?:\r\n|\r|\n)/g, ' ')
									.replace('MALFORMED_QUERY: ',' ')
									.replace(/[\s-]+/g,' ')
			                	});
				                startExecutionSave("Eloqua Error");
		                    })
		            });
			    }

			    function verifySyncLogs() {

			        var throughCheckSyncCount = 0,
			            newPendingRecords = [];

			        var syncStatusUrl = currentInstanceData.baseUrl + '/api/bulk/2.0';

			        /*Multiple Import Defination*/
			        urlMappedRecords.forEach(function(item) {



			            item.ioRecord.step5 = {
			                name: 'Verify Sync Logs',
			                input: {
			                    host: syncStatusUrl + item.syncStatusUri + '/logs',
			                    oauth: true,
			                    token: 'token'
			                }
			            }

			            eloquaCall.get({
			                    host: syncStatusUrl + item.syncStatusUri + '/logs',
			                    oauth: true,
			                    token: currentInstanceData.token,
			                    siteId:queryParams.siteId
			                })
			                .then(function(response) {
			                	if(response.token){
			                		currentInstanceData.token = response.token;
			                	}
								var result = response && response.result;

			                    var syncStatusLogsProp = JSON.parse(result);
			                    x1a++;
					            // startExecutionSave("In Progress - Stage 5 completed");
					            // startExecutionSave("Stage 5 completed");

					            

			                    item.ioRecord.step5.output = [syncStatusLogsProp];

			                    recordsProcessing =  updateRecordProcessing(syncStatusLogsProp.items);

			                    throughCheckSyncCount++;
			                    if (throughCheckSyncCount === urlMappedRecords.length) {

									onCompletionCheckingSync();
			                    	/*try{
				                    	executionSaveObj.callback = function() {
							            }
			                    	} catch(error){
			                    		console.log(error);
			                    	}*/


			                        if(thirdStageComplete){
			                        	fourthStageComplete = true;
			                        }
			                    }

			                })
			                .catch(eloquaError);
			        })
			    }

			    function onCompletionCheckingSync() {

			        var throughDBfilledSync = 0;



			        urlMappedRecords.forEach(function(item) {
			            model.feederModel.updateExecutionDataImportAndURI({
			                    importDefination: JSON.stringify(importDefination),
			                    instanceId: item.instanceid,
			                    status: item.status,
			                    uri: item.syncStatusUri,
			                    timestamp: Date.now(),
			                    feederExecutionId: currentExecutionData.feederExecutionID,
			                    ioRecord: JSON.stringify(item.ioRecord),
			                    created : recordsProcessing.created,
			                    updated : recordsProcessing.updated,
								failed : recordsProcessing.failed,
								warning : recordsProcessing.warning
			                })
			                .then(function(result) {

			                	// console.log('oCC',x1a);

			                    throughDBfilledSync++;
			                    if (throughDBfilledSync === urlMappedRecords.length) {
			                    	/*Experiment */
			                    	urlMappedRecords = null;
			                        pendingExecutionsData();
			                    }
			                })
			                .catch(function(res) {
			                    urlMappedRecords = null;
			                	/*feederExecutionError({
									errStatus:"Eloqua Error",
									errMessage:res.toString()
									.replace(/(?:\r\n|\r|\n)/g, ' ')
									.replace('MALFORMED_QUERY: ',' ')
									.replace(/[\s-]+/g,' ')
			                	});
				                startExecutionSave("Eloqua Error");*/
			                });
			        });
			    }

			    function pendingExecutionsData() {

			    	/*On Experimental Basis*/
			       	if(onCompletionCheckingSync){
                		checkToken	= null;
						forNextAsynOperations	= null;
						prepareChunkImportData	= null;
						eloquaImport	= null;
						syncEloquaData	= null;
						checkSyncStatus	= null;
						verifySyncLogs	= null;
						onCompletionCheckingSync	= null;
			       	}


			        var feederExecutionStatus = '';
			        var timeVal = setTimeout(function() {
			        	if(currentExecutionData && currentExecutionData.feederExecutionID){
				            model.feederModel.getFeederExecutionsData(currentExecutionData.feederExecutionID)
				                .then(function(result) {
				                    var syncStatusUrl = currentInstanceData.baseUrl + '/api/bulk/2.0';
				                    var throughStatus = 0,
				                        throughPendingStatus = 0,
				                        pendingStatus = 0;

				                    if(result.length){

				                    	var executionsDataLength = result.length;
					                    result.forEach(function(item) {
					                        throughStatus++;
					                        var ioRecord = JSON.parse(item.ioRecord);
					                        var status;

					                        eloquaCall.get({
				                                    host: syncStatusUrl + item.url,
				                                    oauth: true,
				                                    token: currentInstanceData.token,
					                    			siteId:queryParams.siteId
				                                })
				                                .then(function(response) {

				                                	try{

									                	if(response.token){
									                		currentInstanceData.token = response.token;
									                	}
														var result = response && response.result;

					                                    var syncStatusProp = JSON.parse(result);
					                                    console.log(syncStatusProp);

					                                    status = syncStatusProp.status;
					                                    ioRecord.step4.output.push(syncStatusProp);

					                                	return eloquaCall.get({
						                                    host: syncStatusUrl + item.url + "/logs",
						                                    oauth: true,
						                                    token: currentInstanceData.token,
						                    				siteId:queryParams.siteId
						                                })
				                                	}catch(error){
				                                		console.log(error);
				                                	}

				                                })
				                                .then(function(response) {
				                                	try{

									                	if(response.token){
									                		currentInstanceData.token = response.token;
									                	}

														var result = response && response.result;

					                                    var syncStatusProp = JSON.parse(result);
					                                    ioRecord.step5.output.push(syncStatusProp);
						                    			recordsProcessing =  updateRecordProcessing(syncStatusProp.items);


					                                    updatePendingExecutionsData(item.dataId, status, JSON.stringify(ioRecord));


					                                    throughPendingStatus++;


					                                    if(throughPendingStatus === executionsDataLength){

					                                    	pendingExecutionsData();
					                                    }

					            						clearTimeout(timeVal);
					            						timeVal = null;
				                                	} catch(error){
				                                		console.log(error);
				                                	}


				                                })
								                .catch(eloquaError);
					                    });
				                    } else {



				                    	if(fourthStageComplete){


			                        		finishExecutionsData();


					                    	if (global.gc) {
												global.gc();
											} else {
												console.log('No GC hook! Start your program as `node --expose-gc file.js`.');
											}
				                    	} else {
				                    		// console.log("Expectations go berserk...");
				                    	}


	            						clearTimeout(timeVal);
	            						timeVal = null;

				                    }
				                });

			        	} else {
			        		clearTimeout(timeVal);
			        		timeVal = null;
			        	}


			        }, 3000);
			    }

			    function updatePendingExecutionsData(id, status, ioRecord) {
			        model.feederModel.updatePendingExecutionsData({
			                status: status,
			                ioRecord: ioRecord,
			                id: id,
			                created:recordsProcessing.created,
			                updated:recordsProcessing.updated,
			                failed:recordsProcessing.failed,
			                warning:recordsProcessing.warning
			            })
			            .then(function(result) {
			            	// console.log("^^^^^^^^^",result);
			            });
			    }


			    function finishExecutionsData() {
			    	if(isExecutionComplete){
				    	model.feederModel.getFeederStatusCount(currentExecutionData.feederExecutionID)
	                		.then(function(result) {
	                			if(result.warning){
	                				startExecutionSave("warning");
	                			} else {
	                				startExecutionSave("success");
	                			}

	                			completeFinishExecution();
	                		})
	                		.catch(function(error) {

	                		});
			    	}


                	/*console.log("before",process.memoryUsage().rss);
                	heapdump.heapDump(function() {
                		console.log("after",process.memoryUsage().rss);
                	})*/
			    }


			    function completeFinishExecution() {

			    	var timeVal = setTimeout(function() {


				    	multiSiteIdConf.destroyMultipleExists(queryParams);
				        urlMappedRecords = null;
				        ioRecords = null;
				        propSync = null;
				        recordsProcessing = null;
						currentExecutionData = null; /* Need to see this */
						importDefination = null; /* Need to see this */
		        		currentInstanceData = null; /* Need to see this */
		        		queryParams = null;
						eloquaErrorStatus = null;
						moveSecondStage = null;
						firstStageComplete = null;
						secondStageComplete = null;
						thirdStageComplete = null;
						fourthStageComplete = null;



						currentFeederObject = null;
						importEndPoint = null;
						importEndJSON = null;
						fieldMappings = null;

						if (global.gc) {
							global.gc();
						} else {
							console.log('No GC hook! Start your program as `node --expose-gc file.js`.');
						}


			    		clearTimeout(timeVal);
			    		timeVal = null;
			    	}, 120000);

			    }


			    function updateRecordProcessing(items) {
	                var created = _.find(items,{statusCode:'ELQ-00004'});
	                var updated = _.find(items,{statusCode:'ELQ-00022'});
	                var failed = _.find(items,{statusCode:'ELQ-00060'});
	                var warning = _.find(items,{statusCode:'ELQ-00144'});

                    var recordsProcessing = {
                    	created:(created && created.count)||0,
                    	updated:(updated && updated.count)||0,
                    	failed:(failed && failed.count)||0,
                    	warning:(warning && warning.count)||0
                    }

                    return recordsProcessing;
			    }



			    function eloquaError(err) {
			        moveSecondStage = false;
			        if(feederExecutionError)
	                	feederExecutionError({
							errStatus:"Eloqua Error",
							errMessage:err.toString()
							.replace(/(?:\r\n|\r|\n)/g, ' ')
							.replace(/[\s-]+/g,' ')
	                	});
	                if(startExecutionSave)
	                	startExecutionSave("Eloqua Error");

					startExecutionSave = null;
					feederExecutionError = null;
			    }

		    }
		}





		/*New Cron Jobs*/
		// var $Cron = require('cron');
		if(!controllerObj.timet)
			var timet = controllerObj.timet = {};
		else{
			var timet = controllerObj.timet;
			timet.crons.forEach(function(item){
				clearInterval(item);
			});
		}

		timet.crons = [];




		timet.dataSyncService = function(time) {
			model.feederModel.getFeederWithFreq(time)
			.then(function(result) {



				result.forEach(function(item) {
					item.cron = true;
					controllerObj.feederServiceNotification({
						query:item

					});
				});
			});
		}


		/*var every5min = setInterval(function(argument) {
			timet.dataSyncService.apply(null,[
				'30 minutes'
			])
		}, 10*1000);
		timet.crons.push(every5min);*/

			/*timet.dataSyncService.apply(null,[
				'30 minutes'
			]);*/

		/*var every5min = setInterval(function(argument) {
			timet.dataSyncService.apply(null,[
				'30 minutes'
			])
		}, 5000);
		timet.crons.push(every5min);*/

		/*var every30min = setInterval(function(argument) {
			timet.dataSyncService.apply(null,[
				'30 minutes'
			])
		}, 18e5);
		timet.crons.push(every30min);

		var every1Hour = setInterval(function(argument) {
			timet.dataSyncService.apply(null,[
				'1 Hour'
			])
		}, 36e5);
		timet.crons.push(every1Hour);


		var every1Day = setInterval(function(argument) {
			timet.dataSyncService.apply(null,[
				'1 Day'
			])
		}, 24*36e5);
		timet.crons.push(every1Day);*/
		var x =0 ;

		var cron = setInterval(function (argument) {
			var now = new Date();
			var hours = now.getHours();
			var minutes = now.getMinutes();
			var day = now.getDay();

				
			if(minutes == 15 || minutes == 45){
				timet.dataSyncService.apply(null,[
					'15 minutes'
				]);
			}
			if(minutes == 0){
				timet.dataSyncService.apply(null,[
					'1 Hour'
				]);

				timet.dataSyncService.apply(null,[
					'15 minutes'
				]);

				timet.dataSyncService.apply(null,[
					'30 minutes'
				]);
			} else if (minutes == 30) {

				timet.dataSyncService.apply(null,[
					'15 minutes'
				]);

				timet.dataSyncService.apply(null,[
					'30 minutes'
				]);


			}
			if(hours == 7 && minutes == 0){
				timet.dataSyncService.apply(null,[
					'1 Day'
				])
			}
			if(day == 1 && hours == 7 && minutes == 0){
				timet.dataSyncService.apply(null,[
					'1 Week'
				])
			}
		}, 1000 * 60);
		timet.crons.push(cron);


		/*var every1Week = setInterval(function(argument) {
			timet.dataSyncService.apply(null,[
				'1 Week'
			])
		}, 24*7*36e5);
		timet.crons.push(every1Week);*/



		controllerObj.getFeederExistingSegment = function(req,res) {
			model.feederModel.get_json(req.body.segmentID)
			.then(function(result){
				res.send({
					status:1,
					segmentJson:result[0].SegmentJSON
				});
			});
		}


		controllerObj.checkSync = function(req,res) {
			model.feederModel.get_Source_Instance_Config_Status_By_Id(req.query.id)
			.then(function(result){
				res.send(result);
			});
		}

		controllerObj.getParentObjects = function(req,resp) {
			var sess = req.session;
			var appInfo = sess.appInfo;

			var siteId	= appInfo.siteId||appInfo.siteid;
			if(!siteId){
				resp.send({status:5});
				return;
			}
			var siteName	= appInfo.siteName;
			var instanceid	= appInfo.instanceid;
			var parentsArray = [];
			var reqObject = '';

			if( req.body.referenceTo){
				reqObject = req.body.referenceTo;
			}else{
				reqObject = req.body.Object_Name;
			}
			var si_config_id = req.body.Source_Instance_Config_Id;


			try{
				model.feederModel.get_Object_json(si_config_id, reqObject)
				.then(function(result){
					if(result.length > 0){

						var objectJson = result[0].Object_Json;
						objectJson = JSON.parse(objectJson);

						model.feederModel.give_parent_objects(objectJson, si_config_id)
						.then(function(result){

							parentsArray = result;
							resp.send(parentsArray);
						})
					}else{
						resp.send(parentsArray);
					}
				})
			}
			catch(error){

			}
		}

	}
})(module.exports)
