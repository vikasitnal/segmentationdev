(function(feederController) {
	feederController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj) {
		var request = require('request-promise');
    	var parameterisedSoql = require('../modules/parameterisedSoql');
		var Q 		= require('q');
		var _ 		= require('lodash');
        var sf = require('jsforce');
		var async = require('async');
        const { spawn } = require('child_process');
		var fs = require('fs');
		var sm = {},ms = {};
		// var free = spawn('free', ['-m']);

		var configUrl = './config/';
		function getDsElqOperators() {
        	var deferred = Q.defer();
			fs.readFile(configUrl+'ds_compare_eloqua_operators.json', function(err, data) {
				if(err){
					deferred.reject(err);
					return;
				}
				deferred.resolve(JSON.parse(data.toString()));
			});        	

        	return deferred.promise;
		}



		/*Services end*/

		/*All apis*/
		/*Decision UI Specific*/
		app.post('/decisionService/objectFields',function() {
			controllerObj.dsObjectFields.apply(null,arguments);
		});

		app.post('/decisionService/getEloquaRelatedData',function() {
			controllerObj.dsGetEloquaRelatedData.apply(null,arguments);
		});


		app.post('/decisionService/getSFDCObjects', function(req, res) {
			controllerObj.getDsSFDCObjects(req,res);
		});

		/*Decision import Specific*/

		app.get('/decisionService/onFeederDecisionConfig',function() {
			controllerObj.onFeederDecisionConfig.apply(null,arguments);
		});


		app.post('/decisionService/onFeederDecisionCreate',function() {
			controllerObj.onFeederDecisionCreate.apply(null,arguments);
		});

		app.post('/decisionService/onFeederDecisionNotify',function() {
			controllerObj.onFeederDecisionNotifyChoose.apply(null,arguments);
		});

		app.post('/decisionService/onDecisionSave',function() {
			controllerObj.onDecisionSave.apply(null,arguments);
		});

		app.get('/decisionService/getSyncDetails',function() {
			controllerObj.getSyncDetails.apply(null,arguments);
		})


		app.post('/decisionService/saveImportPriority',function() {
			controllerObj.saveDSImportPriority.apply(null,arguments);
		})


		app.post('/decisionService/getImportPriority',function() {
			controllerObj.getDSImportPriority.apply(null,arguments);
		})



		/*All functionalities*/



		/*Sync Details Decision Service*/
		controllerObj.getSyncDetails = function(req,res) {
			
			var appInfo = req.session.appInfo;

			if(appInfo && (appInfo.siteId||appInfo.siteid)){
				model.decisionModel.getSyncDetails({
					instanceId:appInfo.instanceid
				})
				.then(function(result) {
					res.send({
						result:result[0]
					});
				})
				.catch(function(error) {
					console.log(error);
				})
			} else {
				res.send({
					error:"Authentication error or session failure."
				});
			}
		}

		/*Decision UI Specific*/
		controllerObj.saveDSImportPriority = function(req,res) {
			model.decisionModel.saveImportPriority(req.body)
			.then(function(res) {
				res.send(res);
			})
			.catch(function(error) {
				res.send(error);
			})
		}

		controllerObj.getDSImportPriority = function(req,res) {
			model.decisionModel.getImportPriority(req.body)
			.then(function(result) {
				res.send(result);

			})
			.catch(function(error) {
				// res.send(error);
			});

		}

		controllerObj.dsObjectFields = function(req,res) {
			var noOfCallsDone = 0;
			var resultantData = {};
			var totalNoOfCalls = 4;


			var completeAsync = function(){
				if(noOfCallsDone==totalNoOfCalls){
					res.send(resultantData);
				}
			}
			var siteId;

			if(req.session.appInfo.siteId){
				siteId = req.session.appInfo.siteId;
			}else{
				siteId = req.session.appInfo.siteid;
			}

			
			var queryObject = req.body;
			queryObject.instanceid = req.session.appInfo.instanceid;

			if(!queryObject.sfCtrlStatus){
				totalNoOfCalls = 5;
				model.decisionModel.getAllEmailFields(queryObject)
				.then(function(result) {
					resultantData.EmailFields = result[0];
					noOfCallsDone++;
					completeAsync();
				})
				.catch(function(error){
					console.log(error);
				})
			}



			model.decisionModel.saveSFDCObject(queryObject)
			.then(function(result) {
				resultantData.selSFDCObject = result;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				noOfCallsDone++;
				completeAsync();
			})



			model.feederModel.getOperators()
			.then(function(data) {
				resultantData.operators = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Decision controller ->',error);
			});

			model.feederModel.fieldOperatorUI()
			.then(function(data) {
				resultantData.fieldOperatorUI = data;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});

			model.adminConfigModel.getObjectFieldJson(req.body.Source_Instance_Config_Id, siteId, req.body.Object_Id)
			.then(function(data) {
				var childObjects 	= [];
				var parentObjects 	= [];
				var allFields 		= [];
				var picklistValuesArray = [];
				var objectsList		= [];

				if(data.length){

					var objects 		= JSON.parse(data[0].Object_Json);
					var temp 			= objects.childRelationships;
					var fields 			= objects.fields;

					if(temp)
					{
						for(var i = 0; i <= temp.length-1; i++)
						{
							var fieldValue 		= temp[i].field;
							var fieldId 		= temp[i].childSObject;
							childObjects.push({"childValue": fieldValue, "childId" : fieldId})
						}
					}
					if(fields)
					{
						for(var x = 0; x <= fields.length-1; x++)
						{
							if(fields[x].type == 'id' || fields[x].type == 'reference'){

							}
							else{
								allFields.push({'label':fields[x].label, 'name' : fields[x].name, 'type' : fields[x].type})
							}
							var q = fields[x].referenceTo;
							if(q.length)
							{
								var fieldLabel 	= fields[x].label;
								var fieldName	= fields[x].name;
								parentObjects.push({'parentValue':fieldLabel, 'parentId' : fieldName})
							}
							var picklistValues = fields[x].picklistValues;
							if(picklistValues.length)
							{
								var picklistName	= fields[x].name;
								var picklistdata	= fields[x].picklistValues;
								picklistValuesArray.push({'name':picklistName, 'picklist' : picklistdata})
							}
						}
					}
					objectsList.push({'childObjects':childObjects, 'parentObjects' : parentObjects, 'allFields' : allFields, 'picklistValuesArray' : picklistValuesArray})
					resultantData.objectsList = objectsList;
					noOfCallsDone++;
					completeAsync();
				} else {
					objectsList.push({
						'childObjects':[],
						'parentObjects' : [],
						'allFields' : [],
						'picklistValuesArray' : []
					});
					resultantData.objectsList = objectsList;
					noOfCallsDone++;
					completeAsync();
				}
			})
			.catch(function(error) {
				console.error(new Date()+': Error in Feeder controller ->',error);
			});
		}


		controllerObj.getDsSFDCObjects = function(req,res) {
			model.decisionModel.getDsSFDCObjects(req.body)
			.then(function(data) {
				res.send(data);
				sm.data = data;
				ms.data = sm;
			});
		}

		controllerObj.dsGetEloquaRelatedData = function(req,res) {

			var appInfo = req.session.appInfo;
			var host;
			var eloquaFields,dsElqOperators;


			model.eloqua_model.getEloquaObject({
				siteId:appInfo.siteId || appInfo.siteid
			}, req.body.Source_Instance_Config_Id)
			.then(function(result) {
				host = result[0];
				return model.instanceModel.checkToken({
					siteId:appInfo.siteId || appInfo.siteid
				})
			})
			.then(function(result) {

				return eloquaCall.get({
					host:	host,
					oauth: 	true,
					token: 	result[0].Token,
					siteId: appInfo.siteId || appInfo.siteid
				})
			})
			.then(function(response) {
				var result = response && response.result;
				eloquaFields = JSON.parse(result).elements;

				
				return getDsElqOperators()
			})
			.then(function(result) {

				dsElqOperators = result;


				res.send({
					eloquaFields:eloquaFields,
					eloquaComparingOperators:dsElqOperators.operators
				});
			})
			.catch(function(error) {
				console.log(error);
				// res.send(error);
			})

			
		}


		/*Decision import Specific*/
		controllerObj.onFeederDecisionConfig = function(req,res) {

			var getData = req.query;

			model.accessControlModel.findById(getData.userId).then(function(acl){

				if(acl[0] && acl[0][0] && acl[0][0].UserId == getData.userId){
					var redirectUrl = '/#/decisionService?key='+req.query.instanceid;

					var tokenResult;
					var sessionData 	= req.session;
					var appInfo 	= {
						siteId		: req.query.siteId,
						siteName	: req.query.siteName,
						instanceid	: req.query.instanceid,
						userName	: req.query.userName,
						campaignId  : req.query.assetid,
						campaignName: req.query.assetName
					};
					sessionData.appInfo = appInfo;
					req.session.appConfig = {
						type:'decisionservice'
					}

					var sourceInstanceData;



					if(getData.AssetId && getData.AssetName){
						var status = 'DRAFT';
						try{
							model.feederModel.updateAssetId(getData, status)
							.then(function(result) {
								console.log("9",result);
							});


						}catch(error){

							console.log(error);
						}
					}

					model.instanceModel.checkToken(getData)
					.then(function(result){
						tokenResult = result;

						return model.feederModel.getInstance(getData);

					})
					.then(function(result) {

						sourceInstanceData = result;



						var info = {
							host	: tokenResult[0].Base_Url+'/api/REST/1.0/assets/campaign/'+appInfo.campaignId+'?depth=minimal',
							oauth	: true,
							token	: tokenResult[0].Token,
							siteId  : getData.siteId,
							json 	: true
						}


						return eloquaCall.get(info)

					})
					.then(function(response) {
						var result = response && response.result;

						result = JSON.parse(result);

						var status = result.currentStatus;


						if(status.toUpperCase() == 'ACTIVE'){
							status = 'ACTIVATED'
						}


				        model.feederModel.saveActivationType({
				                status: status,
				                instanceid: appInfo.instanceid,
				                campaignId: appInfo.campaignId,
				                campaignName: appInfo.campaignName
				            })
				            .then(function(result) {
				            })
				            .catch(function(error) {
				            })



						return model.decisionModel.getFeederInstanceData({
							instanceId:appInfo.instanceid
						});

					})
					.then(function(result) {

						var instanceInfo = result && result[0];



						var segmentId = (instanceInfo)?instanceInfo.segmentId:0;

						if(sourceInstanceData) {

							var url = tokenResult[0].Base_Url+'/api/cloud/1.0/decisions/instances/' + getData.instanceid;

							var data = {
							    "recordDefinition": {
          							"ContactID" : "{{Contact.Id}}",
									"EmailAddress" : "{{Contact.Field(C_EmailAddress)}}"
						    	},
						    	"requiresConfiguration":!+segmentId
							};


							if(instanceInfo && instanceInfo.recordDefinition){
								data.recordDefinition = JSON.parse(instanceInfo.recordDefinition);
							}



							var info = {
								host	: url,
								oauth	: true,
								body	: data,
								token	: tokenResult[0].Token,
								siteId  : getData.siteId,
								json 	: true
							}

							return eloquaCall.put(info)

						}else{
						}
					})
					.then(function(response){

						var result = response && response.result;
					})
					.catch(function(error){

						if(error.statusCode != 412)
							res.send(error);
						else{
							res.redirect(redirectUrl);
						}
					})
					.finally(function() {
						res.redirect(redirectUrl);
					});

				} else {
					res.redirect('/#/accessDenied');
				}
			})

		}

		controllerObj.onFeederDecisionCreate = function(req,res) {
			res.status(200).send({
			    "recordDefinition": {
          			"ContactID" : "{{Contact.Id}}",
					"C_EmailAddress" : "{{Contact.Field(C_EmailAddress)}}"
		       	},
		       	"requiresConfiguration":true
			});
		}
		


		function checkGreaterOrEquals(a,b){
			var comparisonArray = [];

			comparisonArray = comparisonArray.concat([a,b]);
			var compSortArr = _.orderBy(comparisonArray);
			var throughIndex = _.lastIndexOf(compSortArr, a);

			if(throughIndex == 0){
				return false;
			} else {
				return true;
			}
		}


	
		function evaluateModifiedStatus(leftArray,rightSortedArray,rightSearchField) {

			var leftSortedArray = _.orderBy(leftArray,['EmailAddress']);
			rightSortedArray = _.orderBy(rightSortedArray,[rightSearchField]);

			var yesArray = [], noArray = [], toBeSearchedAgain = [];

			searchIndex = 0,stopIndex = 0;



			// console.log(leftSortedArray);

			rightSortedArray.every(function(item,index) {

				if(leftSortedArray[searchIndex]){

					if(!item[rightSearchField]){
						return true;
					}
					/*Equation*/
					do{
						var cond = checkGreaterOrEquals(leftSortedArray[searchIndex].EmailAddress,item[rightSearchField]);

						if(!cond){
							noArray.push(leftSortedArray[searchIndex]);
							/*console.log("part",item,searchIndex);
							noArray.push(item);*/
							searchIndex++;
						}

						if(leftSortedArray[searchIndex] && leftSortedArray[searchIndex].EmailAddress == item[rightSearchField]){
							yesArray.push(item);

							/*if(noArray[noArray.length-1] && noArray[noArray.length-1].EmailAddress == item[rightSearchField]){
								noArray.pop();
							}*/


							searchIndex++;
						} else {

						}

					} while(!cond && leftSortedArray[searchIndex]);
					/*Equation end*/

					/*if(leftSortedArray[searchIndex] && leftSortedArray[searchIndex].EmailAddress == item){
						yesArray.push(item);
						searchIndex++;
					} else {
						// console.log(item);
					}*/

					return true;
				} else {
					// console.log("********   *************  ***********",index);
					return false;
				}
			});

			toBeSearchedAgain = toBeSearchedAgain.concat( leftSortedArray.splice(searchIndex,leftSortedArray.length) );

			return {
				yesArray:yesArray,
				noArray:noArray,
				toBeSearchedAgain:toBeSearchedAgain
			}
		}


		function evaluateStatus(leftArray,rightSortedArray) {

			var leftSortedArray = _.orderBy(leftArray,['EmailAddress']);

			var yesArray = [], noArray = [], toBeSearchedAgain = [];

			searchIndex = 0,stopIndex = 0;


			rightSortedArray.every(function(item,index) {

				if(leftSortedArray[searchIndex]){
					/*Equation*/
					do{
						var cond = checkGreaterOrEquals(leftSortedArray[searchIndex].EmailAddress,item);

						if(!cond){
							noArray.push(leftSortedArray[searchIndex]);
							searchIndex++;
						}


						if(leftSortedArray[searchIndex] && leftSortedArray[searchIndex].EmailAddress == item){
							yesArray.push(leftSortedArray[searchIndex]);

							if(noArray[noArray.length-1] && noArray[noArray.length-1].EmailAddress == item){
								noArray.pop();
							}


							searchIndex++;
						} else {

						}

					} while(!cond && leftSortedArray[searchIndex]);
					/*Equation end*/

					/*if(leftSortedArray[searchIndex] && leftSortedArray[searchIndex].EmailAddress == item){
						yesArray.push(item);
						searchIndex++;
					} else {
						// console.log(item);
					}*/

					return true;
				} else {
					// console.log("********   *************  ***********",index);
					return false;
				}
			});
			toBeSearchedAgain = toBeSearchedAgain.concat( leftSortedArray.splice(searchIndex,leftSortedArray.length) );

			return {
				yesArray:yesArray,
				noArray:noArray,
				toBeSearchedAgain:toBeSearchedAgain
			}
		}



		function setRange(limit,offset) {
			return " LIMIT "+limit+" OFFSET "+offset;
		}


		/*Not Usable*/

		controllerObj.onFeederDecisionNotify = function(req,res) {

			if(res)
				res.status(200).send('');

			var eloquaData = req.body;

			/*var fs = require('fs');

			fs.appendFile('eloquaData.txt', JSON.stringify(eloquaData), function (err) {
			  if (err) throw err;
			  console.log('Saved!');
			});

			console.log(req.url);*/








			var instanceData;
			var queryParams = req.query;
			var soqlQuery,userInfo,srcInsCfgId;
			var userInfo;
			var decisionExecInfo = {},fieldMappingData = [],cstFieldMappingData = [] ;
			var utilisedFieldMappingData = [];



			/*Regarding query*/
			var queryObj;
			var width = 2000;
			var end = width;
			var limit = width;
			var offset = 0;


			/*Arraysets*/
			var totalYes = [],
				totalNo = [],
				yesToSend = [],
				noToSend = [];

			var variantRecordSet = {};


			/*Sync Definition*/
			const rawSyncDefinition = {
				"name" : "External Data Intelligence",
				"updateRule" : "always",
				"fields" : {
					"C_EmailAddress" : "{{Contact.Field(C_EmailAddress)}}"
				},
				"syncActions" : [
					{
						"destination" : "{{DecisionInstance("+queryParams.instanceid.replace(/-/g, "")+")}}",
						"action" : "setStatus",
						"status" : "error"
					}
				],
				"identifierFieldName" : "C_EmailAddress",
				// "importPriorityUri" : "C_EmailAddress"
			};


			function startExecution(id,status,input){

				model.feederModel.saveDecisionExecution(id,queryParams.instanceid,status,input)
					.then(function(result) {

						switch(status){
							case 'In Progress':
								if(result.insertId){
									decisionExecInfo.id = result.insertId;
									initializeData();
								}
								break;
							default:
								break;
						}
					})
					.catch(function(err) {
						console.log(err);
					});
			}

			startExecution(undefined,"In Progress");



			function initializeData() {
				model.feederModel.getDecisionUserData(queryParams.siteId)
				.then(function(result) {
					instanceData = result[0];


					return model.decisionModel.getImportPriority({
						instanceid:queryParams.instanceid
					});
				})
				.then(function(result) {

					if(result && result.import_priority){
						var importPriority = JSON.parse(result.import_priority);
						rawSyncDefinition.importPriorityUri = importPriority.uri;

						startExecution(decisionExecInfo.id,"Update Definition",{
							syncDefinition:rawSyncDefinition
						});

					}

					return model.feederModel.getDecisionInstanceFieldMapping(queryParams.instanceid);
				})
				.then(function(result) {

					cstFieldMappingData = result;
					return model.feederModel.getDecisionInstanceData(queryParams.instanceid);
				})
				.then(function(result) {

					soqlQuery = result[0].query;
					srcInsCfgId = result[0].srcInsCfgId;

					return model.feederModel.getSfdcUserData(srcInsCfgId);
				})
				.then(function(result) {
					userInfo = result;


					queryObj = getSfdcObject.decisionModifiedQueryItems(soqlQuery);

					recursiveFind();

				})
				.catch(function(error) {
					startExecution(decisionExecInfo.id,"Database Error");
				});
			}


			function recursiveFind() {

				if(cstFieldMappingData && cstFieldMappingData.length){
					utilisedFieldMappingData = cstFieldMappingData;
				} else {
					utilisedFieldMappingData = fieldMappingData;
				}


				getSfdcObject.getDecisionRecords(
					userInfo,
					queryObj.query,
					queryObj.field,
					utilisedFieldMappingData,
					{
						sendResult:function(arr,modifiedData,status,callback) {
							if(arr  && eloquaData.items && eloquaData.items.length){
								if(utilisedFieldMappingData && utilisedFieldMappingData.length){

									if(variantRecordSet && variantRecordSet.toBeSearchedAgain){
										variantRecordSet = evaluateModifiedStatus(variantRecordSet.toBeSearchedAgain,modifiedData,'C_EmailAddress');
									}
									else{
										variantRecordSet = evaluateModifiedStatus(eloquaData.items,modifiedData,'C_EmailAddress');
									}

								} else {

									if(variantRecordSet && variantRecordSet.toBeSearchedAgain){
										variantRecordSet = evaluateStatus(variantRecordSet.toBeSearchedAgain,arr);
									}
									else{
										variantRecordSet = evaluateStatus(eloquaData.items,arr);
									}
								}


								totalYes = totalYes.concat(variantRecordSet.yesArray);
								totalNo = totalNo.concat(variantRecordSet.noArray);




								if(variantRecordSet.toBeSearchedAgain && variantRecordSet.toBeSearchedAgain.length && !status){
									callback();
								} else {

									if(utilisedFieldMappingData && utilisedFieldMappingData.length){
										yesToSend = totalYes;
									} else {
										totalYes.forEach(function(item,index) {
											var keySend = 'C_EmailAddress';
											var keyRec = 'EmailAddress';
											datum[keySend] = item[keyRec];
											yesToSend.push(datum);
										});
									}


									totalNo.forEach(function(item,index) {
										var keySend = 'C_EmailAddress';
										var keyRec = 'EmailAddress';
										var datum = {};
										datum[keySend] = item[keyRec];
										noToSend.push(datum);
									});

									variantRecordSet.toBeSearchedAgain.forEach(function(item,index) {
										var keySend = 'C_EmailAddress';
										var keyRec = 'EmailAddress';
										var datum = {};
										datum[keySend] = item[keyRec];
										noToSend.push(datum);
									});



									setStatus('yes',yesToSend);
									setStatus('no',noToSend);
								}
							}
						},
						onError:function(error) {

							startExecution(decisionExecInfo.id,"SOQL Error");
						}
					}
				)
			}

			function setStatus(status,data) {
				if(status == 'yes'){
					startExecution(decisionExecInfo.id,"In Progress",{
						yesCount:data.length
					});
				}  else if(status == 'no') {
					startExecution(decisionExecInfo.id,"In Progress",{
						noCount:data.length
					});
				}
				
				if(data && data.length){
					var statusUrl = instanceData.baseUrl+'/api/bulk/2.0/contacts/imports';


					if(status === 'yes'){
						var fields =  Object.assign({}, rawSyncDefinition.fields);
						var syncDefinition = Object.assign({}, rawSyncDefinition);

						utilisedFieldMappingData.forEach(function(item) {
							fields[item.targetField] = "{{Contact.Field("+item.targetField+")}}"
						});

						syncDefinition.fields = fields;
						syncDefinition.syncActions[0].status = status;

					} else {

						var syncDefinition = Object.assign({}, rawSyncDefinition);
						syncDefinition.syncActions[0].status = status;
					}

					// console.log(syncDefinition);



					eloquaCall.post({
	                    host: statusUrl,
	                    oauth: true,
	                    body: syncDefinition,
	                    token: instanceData.accessToken,
	                    siteId:queryParams.siteId
	                })
	                .then(function(response) {
	                	var result = response && response.result;
	                	setStatusData(result.uri,data,status);
	                })
	                .catch(function(error) {
						startExecution(decisionExecInfo.id,"Set Status Error",{
							errorInfo:{
								status:error.statusCode,
								statusMessage:error.message
							}
						});

	                })
				} else {
					startExecution(decisionExecInfo.id,"No Data",{
						yesInfo:"No Data"
					});
				}

			}

			function setStatusData(uri,data,status) {

				var statusDataUrl = instanceData.baseUrl+'/api/bulk/2.0'+uri+"/data";


				eloquaCall.post({
                    host: statusDataUrl,
                    oauth: true,
                    body: data,
                    token: instanceData.accessToken,
                    siteId:queryParams.siteId
                })
                .then(function(response) {
                	var result = response && response.result;

                	if(result === undefined){
                		synchronizeData(uri,status);
                	} else {
                		console.log("not withstanding..",result);
                	}
                })
                .catch(function(error) {

					startExecution(decisionExecInfo.id,"Set Status Data Error");

                })
			}

			function synchronizeData(uri,status) {

				var synchronizeUrl = instanceData.baseUrl+'/api/bulk/2.0/syncs';


				eloquaCall.post({
                    host: synchronizeUrl,
                    oauth: true,
                    body: {
					    "syncedInstanceURI":uri
					},
                    token: instanceData.accessToken,
                    siteId:queryParams.siteId
                })
                .then(function(response) {
                	var result = response && response.result;

                	if(status == 'yes'){
						startExecution(decisionExecInfo.id,result.status,{
							yesInfo:result
						});
                	} else {
						startExecution(decisionExecInfo.id,result.status,{
							noInfo:result
						});
                	}

                	var timeVal = setTimeout(function() {

                		checkSyncStatus(result,status);
                		clearTimeout(timeVal);
                		timeVal = null;

                	}, 3000);
                })
                .catch(function(error) {

					startExecution(decisionExecInfo.id,"Set Status Data Error");
                });
			}

			function checkSyncStatus(result,status) {
				var syncInfo = result;
				if(syncInfo.status === 'pending'){
					var timeVal = setTimeout(function() {
						var syncStatusUrl = instanceData.baseUrl+'/api/bulk/2.0'+syncInfo.uri;


						eloquaCall.get({
		                    host: syncStatusUrl,
		                    oauth: true,
		                    token: instanceData.accessToken,
		                    siteId:queryParams.siteId
		                })
		                .then(function(response) {
		                	var result =  response && response.result;

		                	var result = JSON.parse(result);

							checkSyncStatus(result,status);
							clearTimeout(timeVal);
							timeVal = null;
		                })
		                .catch(function(error) {
							startExecution(decisionExecInfo.id,"Check Sync Status Error");
		                });

					}, 3000);
				} else {
                	if(status == 'yes'){
						startExecution(decisionExecInfo.id,syncInfo.status,{
							yesInfo:syncInfo
						});
                	} else {

						startExecution(decisionExecInfo.id,syncInfo.status,{
							noInfo:syncInfo
						});
                	}
				}
			}

		}
		/*Not Usable End*/


		controllerObj.onDecisionSave = function(req,res) {
			var session = req.session;
			var siteId;
			var instanceid;
			var userName;
			var campaignId;
			var campaignName;


			if(session.appInfo){
				siteId = session.appInfo.siteId||session.appInfo.siteid;
				instanceid = req.query.key;
				userName = session.appInfo.userName;
				campaignId = session.appInfo.campaignId;
				campaignName = session.appInfo.campaignName;

			}


			var postData 		= req.body;
			var segmentID;
			var resultJson 		= '';
			var segmentJson 	= postData.json;
			var segJson;
			var filJson;
			var segElements;
			var segFilters;
			var filJsons;
			var elems;
			/*Intance related Infos*/
			var baseUrl,token;

			/*Initiating Variables*/
			var segmentFiltersIds = [],
				editableFiltersIds = [],
				filtersWithQuery = [],
				deletableFiltersIds = [],
				newCountFilters  = 0,
				elqFields = [];


			model.feederModel.getSegmentID(instanceid)
			.then(function(result) {
				if(result[0] && result[0].instanceStatus == 'Inactive'){
					res.send({
						status:0,
						message:"INACTIVE DISABLED",
						segmentId:0
					});
				} else {
					startBuild();
				}
			});


			function startBuild() {

				var data = {
				    "recordDefinition": {
						"ContactID" : "{{Contact.Id}}",
						"EmailAddress" : "{{Contact.Field(C_EmailAddress)}}"
			       	},
			       	"requiresConfiguration":false
				};

				segmentJson.elements.forEach(function(filterItem) {

					var elqCmpItems = _.filter(filterItem.filter.elements,{type:'elqcmp'});

					elqCmpItems.forEach(function(queryItem) {
						data.recordDefinition[queryItem.condition.elqField] = "{{Contact.Field("+queryItem.condition.elqField+")}}";
					});
				});




				model.instanceModel.checkToken({
					siteId:siteId
				})
				.then(function(result) {
					baseUrl = result[0].Base_Url;
					token = result[0].Token;

					return model.feederModel.saveSegment(segmentJson, siteId, userName, session.appConfig.type)




				})
				.then(function(result) {
					segmentID = result;



					return model.decisionModel.getDSSFDCFields(instanceid);
				})
				.then(function(result) {


					elqFields = result.map(function(item) {
						return item.targetField;
					})

					var fields = result.map(function(item) {
	                    var field;
	                    if(item.Query_Relationship !== null && item.Query_Relationship.indexOf('.') > 0){
	                        field = item.Query_Relationship.replace(/\w{1,}./,'')+ "." + item.Source_Field_Id;
	                    } else {
	                        field = item.Source_Field_Id;
	                    }

	                    return field;

	                });



					elqFields.forEach(function(item) {
						data.recordDefinition[item] = "{{Contact.Field("+item+")}}";
					});

					

					return parameterisedSoql.build(segmentJson,fields);
				})
				.then(function(result) {

					filtersWithQuery = result;
					newCountFilters = result.length;

					return model.feederModel.findSegmentFilters(segmentID);
				})
				.then(function(result) {




					segmentFiltersIds = result.map(function(item) {
						return item.id;
					});

					deletableFiltersIds = segmentFiltersIds.splice(newCountFilters);
					editableFiltersIds = segmentFiltersIds.splice(0,newCountFilters);



					return model.feederModel.delSegmentFilters(deletableFiltersIds.join(','));
				})

				.then(function(result){



                	return model.feederModel.updateSegmentQueries(filtersWithQuery,segmentID);
	           	})
				.then(function(result) {

					return model.feederModel.updateInsertFilters(editableFiltersIds,filtersWithQuery,segmentID);
				})
				.then(function(result) {


					return model.decisionModel.updateInsertFeederInstances({
						siteid:siteId,
						instanceid:instanceid,
						segmentId:segmentID,
						frequency:segmentJson.setFrequency,
						campaignId:campaignId,
						campaignName:campaignName,
						recordDefinition:JSON.stringify(data.recordDefinition).replace(new RegExp("'","g"),"''")
					});



				})
				.then(function(result) {


					var info = {
						host	: baseUrl+'/api/cloud/1.0/decisions/instances/'+instanceid,
						oauth	: true,
						body	: data,
						token	: token,
						siteId  : siteId,
						json 	: true
					}
					


					return eloquaCall.put(info);
				})
				.then(function(response){
					var result = response && response.result;



					res.send({
						status:1,
						segmentId:segmentID
					});

				})
				.catch(function(error){
					console.log(error);
				})

			}

		}


		controllerObj.onFeederDecisionNotifyChoose = function(req,res) {
			var queryParams = req.query;
			model.decisionModel.getControllerStatus(queryParams.instanceid)
			.then(function(result) {
				if(result[0].sfCtrlStatus == 'true'){
					controllerObj.onFeederDecisionNotifyWithController(req,res)
				} else {
					controllerObj.onFeederDecisionNotify(req,res);
				}
			})
			.catch(function(error) {
				console.log(error);
			});
		}

		controllerObj.onFeederDecisionNotifyWithController = function(req,res) {

			// console.log("coming...");


			var eloquaData = req.body;

			/*console.log(req.url);


			var fs = require('fs');

			fs.appendFile('eloquaData.json', JSON.stringify(eloquaData), function (err) {
				if (err) throw err;
				console.log('Saved!');
			});*/
			
			

			var instanceData;
			var queryParams = req.query;
			var soqlQuery,userInfo,srcInsCfgId;
			var decisionExecInfo = {},cstFieldMappingData = [] ;


			/*Array sets*/
			var yesArray = [],noArray = [],erroredArray = [];

			/*Sync Definition*/
			const rawSyncDefinition = {
				"name" : "External Data Intelligence",
				"updateRule" : "always",
				"fields" : {
					"C_EmailAddress" : "{{Contact.Field(C_EmailAddress)}}"
				},
				"syncActions" : [
					{
						"destination" : "{{DecisionInstance("+queryParams.instanceid.replace(/-/g, "")+")}}",
						"action" : "setStatus",
						"status" : "error"
					}
				],
				"identifierFieldName" : "C_EmailAddress"
				// "importPriorityUri" : "C_EmailAddress"
			};




			if(res)
				res.status(200).send('');




			function startExecution(id,status,input){


				model.feederModel.saveDecisionExecution(id,queryParams.instanceid,status,input)
					.then(function(result) {

						switch(status){
							case 'In Progress':
								if(result.insertId){
									decisionExecInfo.id = result.insertId;

									initializeData();
								}
								break;
							default:
								break;
						}
					})
					.catch(function(err) {
						console.log(err);
					});
			}

			startExecution(undefined,"In Progress");




			function initializeData() {



				model.feederModel.getDecisionUserData(queryParams.siteId)
				.then(function(result) {


					instanceData = result[0];

					return model.decisionModel.getImportPriority({
						instanceid:queryParams.instanceid
					});
				})
				.then(function(result) {

					if(result && result.import_priority){
						var importPriority = JSON.parse(result.import_priority);
						rawSyncDefinition.importPriorityUri = importPriority.uri

					}

					return model.feederModel.getDecisionInstanceFieldMapping(queryParams.instanceid);
				})
				.then(function(result) {

					cstFieldMappingData = result;
					return model.feederModel.getDecisionInstanceData(queryParams.instanceid);
				})
				.then(function(result) {


					soqlQuery = result[0].query;
					srcInsCfgId = result[0].srcInsCfgId;

					return model.feederModel.getSfdcUserData(srcInsCfgId);
				})
				.then(function(result) {
					userInfo = result;

					var data = eloquaData.items.slice();
					if(data.length>=90){
						var dataSet = _.chunk(data,50);
					} else {
						var dataSet = _.chunk(data,data.length);
					}



					var withoutFieldsStatus = false;
					if(soqlQuery.indexOf('count()') > -1){
						withoutFieldsStatus = true;
					} 
					


					dataSet.forEach(function(eachDataArr) {

						chunkControlledQuery({
							query:soqlQuery,
							data:eachDataArr,
							withoutFieldsStatus:withoutFieldsStatus
						});

					})


					function chunkControlledQuery(sfdcCourierJSON) {
						getSfdcObject.getDecisionRecordsThroughController(userInfo,sfdcCourierJSON)
						.then(function(result) {
							try{



								result.forEach(function(item) {
									var cstItem = {};

									cstItem.C_EmailAddress = item.EmailAddress;

									if(item.resultSize==1){
										if(cstFieldMappingData && cstFieldMappingData.length){
											cstFieldMappingData.forEach(function(keyObj) {
												cstItem[keyObj.targetField] = item.result[keyObj.sourceField];
											});
										}
										yesArray.push(cstItem);
									} else if(item.resultSize == 0) {
										noArray.push(cstItem);
									} else {
										erroredArray.push(cstItem);
									}
								});
							} catch(error){
								console.log('y',error);
							}




							


							setStatus('yes',yesArray);
							setStatus('no',noArray);
							setStatus('errored',erroredArray);
						})
						.catch(function(error) {
							console.log('_______________')
							console.log('_______________')
							console.log('_______________')
							console.log(error);
							console.log('_______________')
							console.log('_______________')

							startExecution(decisionExecInfo.id,"Controller Soql Error");


						});
					}
				})
				.catch(function(error) {
					startExecution(decisionExecInfo.id,"Database Error");
				});
			}

			function setStatus(status,data) {
				if(status == 'yes'){
					startExecution(decisionExecInfo.id,"In Progress",{
						yesCount:data.length
					});
				}  else if(status == 'no') {
					startExecution(decisionExecInfo.id,"In Progress",{
						noCount:data.length
					});
				} else {
					startExecution(decisionExecInfo.id,"In Progress",{
						erroredCount:data.length
					});
				}


				if(data && data.length){

					var statusUrl = instanceData.baseUrl+'/api/bulk/2.0/contacts/imports';


					if(status === 'yes'){
						var fields =  Object.assign({}, rawSyncDefinition.fields);
						var syncDefinition = Object.assign({}, rawSyncDefinition);

						cstFieldMappingData.forEach(function(item) {
							fields[item.targetField] = "{{Contact.Field("+item.targetField+")}}";
						});

						syncDefinition.fields = fields;
						syncDefinition.syncActions[0].status = status;

					} else {

						var syncDefinition = Object.assign({}, rawSyncDefinition);
						syncDefinition.syncActions[0].status = status;
					}



					eloquaCall.post({
	                    host: statusUrl,
	                    oauth: true,
	                    body: syncDefinition,
	                    token: instanceData.accessToken,
	                    siteId:queryParams.siteId
	                })
	                .then(function(response) {
	                	var result = response && response.result;




	                	setStatusData(result.uri,data,status);
	                })
	                .catch(function(error) {
						startExecution(decisionExecInfo.id,"Set Status Error",{
							errorInfo:{
								status:error.statusCode,
								statusMessage:error.message
							}
						});

	                })
				} else {
					startExecution(decisionExecInfo.id,"No Data",{
						yesInfo:"No Data"
					});
				}
			}

			function setStatusData(uri,data,status) {

				var statusDataUrl = instanceData.baseUrl+'/api/bulk/2.0'+uri+"/data";


				eloquaCall.post({
                    host: statusDataUrl,
                    oauth: true,
                    body: data,
                    token: instanceData.accessToken,
                    siteId:queryParams.siteId
                })
                .then(function(response) {
                	var result = response && response.result;


                	if(result === undefined){
                		synchronizeData(uri,status);
                	} else {
                		console.log("not withstanding..",result);
                	}
                })
                .catch(function(error) {

					startExecution(decisionExecInfo.id,"Set Status Data Error");

                })
			}

			function synchronizeData(uri,status) {
				var synchronizeUrl = instanceData.baseUrl+'/api/bulk/2.0/syncs';



				eloquaCall.post({
                    host: synchronizeUrl,
                    oauth: true,
                    body: {
					    "syncedInstanceURI":uri
					},
                    token: instanceData.accessToken,
                    siteId:queryParams.siteId
                })
                .then(function(response) {
                	var result = response && response.result;


                	if(status == 'yes'){
						startExecution(decisionExecInfo.id,result.status,{
							yesInfo:result
						});
                	} else if(status == 'no'){
						startExecution(decisionExecInfo.id,result.status,{
							noInfo:result
						});
                	} else {
						startExecution(decisionExecInfo.id,result.status,{
							errorInfo:result
						});
                	}

                	var timeVal = setTimeout(function() {

                		checkSyncStatus(result,status);
                		clearTimeout(timeVal);
                		timeVal = null;

                	}, 3000);
                })
                .catch(function(error) {

					startExecution(decisionExecInfo.id,"Set Status Data Error");
                });
			}

			function checkSyncStatus(result,status) {
				var syncInfo = result;
				if(syncInfo.status === 'pending'){
					var timeVal = setTimeout(function() {
						var syncStatusUrl = instanceData.baseUrl+'/api/bulk/2.0'+syncInfo.uri;


						eloquaCall.get({
		                    host: syncStatusUrl,
		                    oauth: true,
		                    token: instanceData.accessToken,
		                    siteId:queryParams.siteId
		                })
		                .then(function(response) {
		                	var result =  response && response.result;

		                	var result = JSON.parse(result);

		                	checkSyncStatus(result,status);
							clearTimeout(timeVal);
							timeVal = null;
		                })
		                .catch(function(error) {
							startExecution(decisionExecInfo.id,"Check Sync Status Error");
		                });

					}, 3000);
				} else {
                	if(status == 'yes'){
						startExecution(decisionExecInfo.id,syncInfo.status,{
							yesInfo:syncInfo
						});
                	} else if(status == 'no') {

						startExecution(decisionExecInfo.id,syncInfo.status,{
							noInfo:syncInfo
						});
                	} else {

						startExecution(decisionExecInfo.id,syncInfo.status,{
							errorInfo:syncInfo
						});
                	}
				}
			}
		}


	}
})(module.exports);
