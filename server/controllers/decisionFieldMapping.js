(function(decisionFieldMappingController) {
    decisionFieldMappingController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime,
        getSfdcObject, notificationURL, buildSOQL, importDefination, controllerObj) {


        controllerObj.get = function(req, callback) {
            var sess = req.session.appInfo;

            var input = {
                Decision_Instance_Id: sess.instanceid,
                Source_Object_Id:req.body.Source_Object_Id
            };

            var eloquaObjects, selected_SFDC_Objects, mappedFields, SFDCOjects, host, allFilters;

            var userData = {
                siteId: sess.siteid || sess.siteId,
                siteName: sess.site_name,
                status: "logged_in",
            }
            

            if (userData.status == "logged_in") {
                model.eloqua_model.getEloquaObject(userData, req.body.Source_Instance_Config_Id)
                    .then(function(result) {
                        host = result[0];
                        getParams = userData;
                        return model.instanceModel.checkToken(getParams);
                    })
                    .then(function(result) {
                        if (result) {
                            return eloquaCall.get({
                                host: host,
                                oauth: true,
                                token: result[0].Token
                            })
                        }
                    }).then(function(response) {
                        var result = response && response.result;
                        eloquaObjects = result;
                        return model.adminConfigModel.getObjectFieldJson(req.body.Source_Instance_Config_Id, userData.siteId, req.body.Source_Object_Id)
                    })
                    .then(function(result) {
                        SFDCOjects = JSON.parse(result[0].Object_Json).fields;

                        return model.decisionFieldMapping.getDecisionFieldMaps(input,req.body.sfCtrlStatus,req.body.forDeleting);
                    })
                    .then(function(result) {
                        allFilters = result;
                        var data = {
                            EloquaObjects: JSON.parse(eloquaObjects),
                            SelectedSFDCObjects: selected_SFDC_Objects,
                            MappedFields: allFilters,
                            SFDCOjects: SFDCOjects
                        };
                        callback(null, data);
                    }).catch(callback)
            }
        }

        app.post('/decision/getdecisionFieldMapping', function(req, res, next) {
            controllerObj.get(req, function(err, result) {
                if (err) {
                    next(err);
                } else {

                    res.status(200).send(result);
                }
            });
        });


        controllerObj.post = function(req, callback) {


            var input = {
                Source_Field_Id:req.body.Source_Field_Id,
                Target_Field_Id:req.body.Target_Field_Id,
                Source_Object_Id:req.body.Source_Object_Id,
                Decision_Instance_Id:req.session.appInfo.instanceid,
                Source_Field_Name:req.body.Source_Field_Name,
                Target_Field_Name:req.body.Target_Field_Name,
                Source_Field_Type:req.body.Source_Field_Type,
                Target_Field_Type:req.body.Target_Field_Type,
                Source_Object_Name:req.body.Source_Object_Name,
                Query_Relationship:req.body.Query_Relationship
            }



            model.decisionFieldMapping.createDecisionField(input)
            .then(function(result) {
                return model.decisionFieldMapping.getDecisionFieldMaps({
                    Decision_Instance_Id:req.session.appInfo.instanceid,
                    Source_Object_Id:req.body.Source_Object_Id,
                })
            })
            .then(function(result) {
                callback(null, result);
            })
            .catch(callback);
        }

        app.post('/decision/decisionFieldMapping', function(req, res, next) {
            controllerObj.post(req, function(err, result) {
                if (err) {
                    next(err);
                } else {
                    res.status(200).send(result);
                }
            });
        });

        controllerObj.deleteAlldecisionFieldMapping = function(req,callback) {
            model.decisionFieldMapping.deleteAll(req.session.appInfo.instanceid)
            .then(function(result) {
                if(result){
                    callback(null, {
                        status:true,
                        message:"Previously mapped fields deleted successfully."
                    });
                } else {
                    callback(null, {
                        status:false,
                        message:"There wasn't any mapped field."
                    });
                }
            })
            .catch(callback);
        } 

        app.post('/decision/deleteAlldecisionFieldMapping', function(req, res, next) {
            controllerObj.deleteAlldecisionFieldMapping(req, function(err, result) {
                if (err) {
                    next(err);
                } else {
                    res.status(200).send(result);
                }
            });
        });


        /*controllerObj.update = function(id, payload, callback) {
            decisionFieldMapping.update(payload, {
                where: {
                    id: id
                }
            }).then(function(result) {
                callback(null, result);
            }, callback);
        }

        app.put('/decision/decisionFieldMapping', function(req, res, next) {
            controllerObj.update(req.params.id, req.body, function(err, result) {
                if (err) {
                    next(err);
                } else {
                    res.status(200).send(result);
                }
            });
        });*/


        
        controllerObj.deleteDsFieldMapping = function(req, callback) {
            model.decisionFieldMapping.deleteDsFieldMapping(req.query)
            .then(function(result) {
                callback(null,result);
            })
            .catch(callback);
        }

        app.delete('/decision/decisionFieldMapping', function(req, res, next) {
            controllerObj.deleteDsFieldMapping(req, function(err, result) {
                if (err) {
                    next(err);
                } else {
                    res.status(204).end();
                }
            });
        });

    }

})(module.exports);