(function(feederController) {
	feederController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject) {
		var request = require('request-promise');
		var errorStatus = {}, globalError = {};
		//working
		app.get('/testingQuery', function(req, res) {
			var jsonQuery = [
			  {
				"type": "element",
				"id": "1",
				"name": "a",
				"object_Maping": "Account_",
				"field": "Name",
				"condition": {
				  "type": "TextValueCondition",
				  "operator": "equal",
				  "value": "b"
				}
			  },
			  {
				"type": "element",
				"id": "1",
				"name": "b",
				"operator": "AND",
				"condition": {
				  "type": "TextValueCondition",
				  "operator": "equal",
				  "value": "b"
				}
			  },
			  {
				"type": "group",
				"operator": "OR",
				"elements": [
				  {
					"type": "element",
					"id": "1",
					"name": "c",
					"condition": {
					  "type": "TextValueCondition",
					  "operator": "equal",
					  "value": "b"
					}
				  },
				  {
					"type": "element",
					"id": "1",
					"name": "d",
					"operator": "AND",
					"condition": {
					  "type": "TextValueCondition",
					  "operator": "equal",
					  "value": "b"
					}
				  },
				  {
					"type": "group",
					"operator": "AND",
					"elements": [
					  {
						"type": "element",
						"id": "1",
						"name": "e",
						"condition": {
						  "type": "TextValueCondition",
						  "operator": "equal",
						  "value": "b"
						}
					  },
					  {
						"type": "element",
						"id": "1",
						"name": "f",
						"operator": "AND",
						"condition": {
						  "type": "TextValueCondition",
						  "operator": "equal",
						  "value": "b"
						}
					  }
					]
				  }
				]
			  }
			]

			
			var string = '';
			var query = '';
			
			function goThroughthis(array){
				for(var i=0; i<array.length; i++){
					if(array[i].type == 'group'){
						string += " " + array[i].operator + ' ( ' ;
						
						goThroughthis(array[i].elements);	
						
						string += ' ) ';
						
					} else{
						if(i){
							
							string += " " + array[i].operator + " " + array[i].name;
							
						} else {
						
							string += array[i].name + " ";
							
						}
					}
				}
			}
			
			goThroughthis(jsonQuery);
			
			res.send(string);
			
		});			


	  app.use(function(err, req, res, next) {
	    res.status(err.status || 500);
	    errorStatus.err = err;
	    globalError.err = errorStatus;
	    res.send({
	        message: err.message,
	        error: err
	    });
	  });

	  
	}
	
})(module.exports)





