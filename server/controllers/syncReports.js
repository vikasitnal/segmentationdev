(function(syncReportsController) {
	syncReportsController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj) {

		// app.post('/syncReports/executions', controllerObj.syncReportsEx);

		var syncReportsObj = {};

		function restCall(options) {
			switch(options.method.toLowerCase()){
				case 'post':
					app.post(options.route,function(req,res) {
						try{
							if(controllerObj[options.callbackName])
								controllerObj[options.callbackName].apply(null,arguments);
							else{
								console.log('callback function '+ options.callbackName + " not defined");
								res.send('callback function '+ options.callbackName + " not defined");
							}
						}catch(error){
							console.log("Arguments",arguments);
							console.log("Error",error);
							res.send({
								error:error.toString()
							});
						}
					});
					break;
				case 'get':
					app.get(options.route,function(req,res) {
						try{
							if(controllerObj[options.callbackName])
								controllerObj[options.callbackName].apply(null,arguments);
							else{
								console.log('callback function '+ options.callbackName + " not defined");
								res.send('callback function '+ options.callbackName + " not defined");
							}
						}catch(error){
							console.log("Arguments",arguments);
							console.log("Error",error);
							res.send({
								error:error.toString()
							});
						}
					});
					break;
				case 'put':
					app.put(options.route,function(req,res) {
						try{
							if(controllerObj[options.callbackName])
								controllerObj[options.callbackName].apply(null,arguments);
							else{
								console.log('callback function '+ options.callbackName + " not defined");
								res.send('callback function '+ options.callbackName + " not defined");
							}
						}catch(error){
							console.log("Arguments",arguments);
							console.log("Error",error);
							res.send({
								error:error.toString()
							});
						}
					});
					break;
				case 'delete':
					app.delete(options.route,function(req,res) {
						try{
							if(controllerObj[options.callbackName])
								controllerObj[options.callbackName].apply(null,arguments);
							else{
								console.log('callback function '+ options.callbackName + " not defined");
								res.send('callback function '+ options.callbackName + " not defined");
							}
						}catch(error){
							console.log("Arguments",arguments);
							console.log("Error",error);
							res.send({
								error:error.toString()
							});
						}
					});
					break;
				default:
					console.log(options.method+" not available");
			}
		}




		/**********Execution Numbers************/
		controllerObj.syncReportExecutionNumbers = function(req,res) {
			var appInfo = req.session.appInfo;

			model.syncReports.getExecutionNumbers(appInfo.instanceid)
			.then(function(result) {
				res.send(result[0]);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/executionNumbers",
			callbackName: 'syncReportExecutionNumbers'
		});



		/********* Executions **************/
		controllerObj.syncReportExecutions = function(req,res) {
			var appInfo = req.session.appInfo;
			model.syncReports.getExecutions(appInfo.instanceid)
			.then(function(result) {
				res.send(result);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/executions",
			callbackName: 'syncReportExecutions'
		});


		/********* Paginate Imports ***********/
		controllerObj.syncReportPaginateImports = function(req,res) {

			var appInfo = req.session.appInfo;
			var params = req.query;

			var response = {};

			model.syncReports.getPaginateImports(appInfo.instanceid,params)
			.then(function(result) {
				// res.send(result);
				response.data = result;
				return model.syncReports.getImportsCount(appInfo.instanceid,params)
			})
			.then(function(result) {
				response.totalImportsCount = result[0].totalImportsCount;
				res.send(response);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/paginateImports",
			callbackName: 'syncReportPaginateImports'
		});


		/********* Paginate Errors ***********/
		controllerObj.syncReportPaginateErrors = function(req,res) {

			var appInfo = req.session.appInfo;
			var params = req.query;

			var response = {};

			model.syncReports.getPaginateErrors(appInfo.instanceid,params)
			.then(function(result) {
				// res.send(result);
				response.data = result;
				return model.syncReports.getErrorsCount(appInfo.instanceid,params)
			})
			.then(function(result) {
				response.totalErrorsCount = result[0].totalErrorsCount;
				res.send(response);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/paginateErrors",
			callbackName: 'syncReportPaginateErrors'
		});

		

		/********* Recent Imports ***********/
		controllerObj.syncReportRecentImports = function(req,res) {
			var appInfo = req.session.appInfo;
			model.syncReports.getRecentImports(appInfo.instanceid)
			.then(function(result) {
				res.send(result);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/recentImports",
			callbackName: 'syncReportRecentImports'
		});



		/*********** Recent Actions ************/
		controllerObj.syncReportRecentActions = function(req,res) {
			var appInfo = req.session.appInfo;
			model.syncReports.getRecentActions(appInfo.instanceid)
			.then(function(result) {
				res.send(result);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/recentActions",
			callbackName: 'syncReportRecentActions'
		});

		/*********** Recent Errors ************/
		controllerObj.syncReportRecentErrors = function(req,res) {
			var appInfo = req.session.appInfo;
			model.syncReports.getRecentErrors(appInfo.instanceid)
			.then(function(result) {
				res.send(result);
			})
			.catch(function(error) {
				res.send(error);
			});
		}

		restCall({
			method:"GET",
			route:"/syncReports/recentErrors",
			callbackName: 'syncReportRecentErrors'
		});
	}
})(module.exports)