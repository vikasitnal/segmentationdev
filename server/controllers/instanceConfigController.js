var lodash = require('lodash');
(function(instanceConfigController) {
	instanceConfigController.init = function(app, model, config, eloquaCall, sfdcCall, updateExpiryTime,
	 getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj) {
		var request = require('request-promise');
		var sf = require('node-salesforce');
		var _ = require('lodash');
        var utility = require('./../modules/all');
	    var config  = require('config');


		app.get('/instance/status', function(req, res) {
			res.status(200).send('OK');
		});

		controllerObj.sfCtrlStatusUpdate = function(req,res) {
			console.log(req.body);
			var input = req.body;
			var appInfo = req.session.appInfo;
			input.siteId = appInfo.siteid||appInfo.siteId;





			model.instanceModel.updateSFControllerStatus(req.body)
			.then(function(result) {
				res.send({
					status:result.affectedRows
				});
			})
			.catch(function(error) {
				res.send(error)
			})





		}

		app.post('/instance/sfCtrlStatusUpdate',function(req,res) {
			controllerObj.sfCtrlStatusUpdate(req,res);
		});

		controllerObj.sfCtrlStatusGet = function(req,res) {
			var input = req.body;
			var appInfo = req.session.appInfo;
			input.siteId = appInfo.siteid||appInfo.siteId;





			model.instanceModel.getSFControllerStatus(req.body)
			.then(function(result) {
				console.log(result);
				res.send({
					status:(result[0].salesforce_controller_confirmation == 'true')? true:false
				});
			})
			.catch(function(error) {
				res.send(error)
			})





		}

		app.post('/instance/sfCtrlStatusGet',function(req,res) {
			controllerObj.sfCtrlStatusGet(req,res);
		})

		//--passing data to ui (external source system, source instances,  all saved instance)
		controllerObj.getAdminData = function(req,res) {
			var noOfCallsDone = 0;
			var resultantData = {};
			var completeAsync = function(){
				if(noOfCallsDone==3){
					res.send(resultantData)
				}
			}
			//--getting all external system (like: salesforce, msdynamics)
			model.adminConfigModel.getExternalSourceSystem()
			.then(function(ExternalSourceSystem) {
				resultantData.ExternalSourceSystem = ExternalSourceSystem;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.log(error)
				res.status(404).send(error);
			});
			//--getting all external system instance(like: dev, production, sendbox)
			model.adminConfigModel.getSourceSystemInstance()
			.then(function(SourceSystemInstance) {
				resultantData.SourceSystemInstance = SourceSystemInstance;
				noOfCallsDone++;
				completeAsync();
			})
			.catch(function(error) {
				console.log(error)
				res.status(404).send(error);
			});

			var sess 	= req.session.appInfo;
			var siteId 	= sess.siteid||sess.siteId;
			//--getting all saved instances
			model.adminConfigModel.getInstanceInfo(siteId)
			.then(function(InstanceInfo) {
				resultantData.InstanceInfo = InstanceInfo;
				noOfCallsDone++;
				completeAsync();

			})
			.catch(function(error) {
				console.log(error)
				res.status(404).send(error);
			});
		}

		app.get('/instance/getAdminData', function(req, res) {
			controllerObj.getAdminData(req,res);
		});

		//---for testing SFDC connection
		controllerObj.checkConnSfdc = function(req,res) {
			var postData = 		req.body;
			var username = 		postData.userName;
			var password = 		postData.password;
			var securityToken = postData.securityToken;

			var conn = new sf.Connection({

			});

			if(password && password.length > 0){
				conn.login(username, password+securityToken, function(err, userInfo) {
					if (err) {
						res.send({status:0});
					}
					else{
						res.send({status:1});
					}
				});
			} else {
            	if(postData.Source_Instance_Config_Id){
                	model.instanceModel.get_Source_Instance_Config_By_Id(postData.Source_Instance_Config_Id)
                	.then(function(source_instance_config){
		            	var password = utility.decrypt(source_instance_config.Password, config.eloquaConfig.AppId);
	                    conn.login(username, password+securityToken, function(err, userInfo) {
							if (err) {
								res.send({status:0});
							}
							else{
								res.send({status:1});
							}
						});
                	},function(err){
                  		res.send({status: 0});
                	});	
				} else {
					res.send({status: 0});
				}
			}

		}
		app.post('/instance/checkConnSfdc', function(req, res){
			controllerObj.checkConnSfdc(req,res);
		});

		//---for syncing data from sfdc
		controllerObj.syncSfdcData = function(req, res) {
			getSfdcObject.get({
				Source_Instance_Config_Id	: req.body.Source_Instance_Config_Id,
				filteredObjects				: true,
				siteId						: req.body.siteId
			})
			.then(function(result){
				if(result.status == false){
					res.send({
						status:false
					})
				} else {
					res.send({
						status:true
					});
				}
			});
		}

		app.post('/instance/syncSfdcData', function(req, res){
			controllerObj.syncSfdcData(req, res);
		});

		// Code done by RavikumarMG on 9/06/2017
		// getting all instance

		controllerObj.addInstance = function(req,res, next) {

			var sess 	= req.session.appInfo;
			var userData = {
                siteId	: sess.siteId,
                siteName: sess.siteName,
                status	: "logged_in",
            }

			var reqData = {
                username		: req.body.User_name,
                password		: req.body.Password,
                token			: req.body.Token,
                instance_type	: req.body.Instance_type,
                description		: req.body.Description,
                SourceSystemId	: req.body.Source_System_Id,
                status			: req.body.Status,
                CategoryId		: req.body.CategoryId,
                proceed			: req.body.proceed,
                email           : req.body.Email
            };
            if(req.body.CategoryId){
            	reqData.Source_Instance_Config_Id = req.body.CategoryId;
            }
            var status;


            if(userData.status == "logged_in"){

            	if(reqData.proceed){
            		saveInstance();
            		return;
            	}

            	if(reqData.status == 'Inactive'){
	            	model.adminConfigModel.campaignDependencyExistenceForSourceInstance(reqData)
					.then(function(result) {


						if(result.length){
							res.send({status:0,dependencies:result})
						}else{

							/*return model.adminConfigModel.deleteSfdcOrg(postData)*/
							saveInstance();
						}

					});
            	} else {
            		saveInstance();
            	}

            	function saveInstance() {
					model.adminConfigModel.postInstance(reqData, (sess.siteId||sess.siteid))
	                .then(function(result){
	                	status = result.status;
						return model.adminConfigModel.getInstanceInfo(req.body.Site_Id);
	                }, function(err){
	                	next(err);
	                })
					.then(function(result) {
						res.send({
							externalSystems:result,
							status:(status)?status:0
						});
					}, function(err){
						next(err);
					})
            	}
            } else {
            	var err = new Error("user not logged in");
            	res.status(500).send(err);
            }
		}






		controllerObj.activateInstance = function(req,res, next) {
			var sess 	= req.session.appInfo;
			var userData = {
                siteId	: sess.siteId,
                siteName: sess.siteName,
                status	: "logged_in",
            }
            if(userData.status == "logged_in"){
				model.adminConfigModel.activateInstance(req.body.Source_Instance_Config_Id)
                .then(function(result){
                  	res.status(200).send(result);
                }, function(err){
                	next(err);
                })
            } else {
            	var err = new Error("user not logged in");
            	res.status(500).send(err);
            }
		}



		controllerObj.deactivateInstance = function(req,res, next) {
			var sess 	= req.session.appInfo;
			var userData = {
                siteId	: sess.siteId,
                siteName: sess.siteName,
                status	: "logged_in",
            }
            if(userData.status == "logged_in"){
				model.adminConfigModel.deactivateInstance(req.body.Source_Instance_Config_Id)
                .then(function(result){
                	res.status(200).send(result);
                }, function(err){
                	next(err);
                })
            } else {
            	var err = new Error("user not logged in");
            	res.status(500).send(err);
            }
		}








		// Code done by RavikumarMG on 9/06/2017
		// getting all instance

		controllerObj.deleteInstance = function(req,res, next) {

			var instanceId 	= req.query.id;


 	         model.adminConfigModel.deleteInstance(instanceId)
	                .then(function(result){
	                	status = result.status;
						return model.adminConfigModel.getInstanceInfo(req.query.Site_Id);
	                }, function(err){
	                	next(err);
	                })
					.then(function(result) {
						res.send({
							externalSystems:result,
							status:(status)?status:0
						});
					}, function(err){
						next(err);
					})
		}


		app.post('/instance/addInstance', function(req, res, next){
			controllerObj.addInstance(req,res, next);
        });

        app.post('/instance/activateInstance', function(req, res, next){
			controllerObj.activateInstance(req,res, next);
        });

        app.post('/instance/deactivateInstance', function(req, res, next){
			controllerObj.deactivateInstance(req,res, next);
        });

        app.delete('/instance/deleteInstance', function(req, res, next){
			controllerObj.deleteInstance(req,res, next);
        });

		//--getting sfdc Objects from local if expiry time match otherwise call function - "/instance/getSfdcObjects" time is 4 hour
		controllerObj.instanceConfiguration = function(req,res) {
			var sess 			= req.session.appInfo;
			var siteId 			= sess.siteid;
			var getData 		= req.body;
			var noOfCallsDone 	= 0;
			var resultantData 	= {};
			var completeAsync 	= function(){
				if(noOfCallsDone==2){
					res.send(resultantData);
				}
			}
			//getting all objects if expiry time is expired
			//no need to call api again for next 4 hour.
			//we are saving objects in local db .
			model.adminConfigModel.getObjectsBeforeExpiryTime()
			.then(function(getObjectsBeforeExpiryTime) {
				resultantData.getObjectsBeforeExpiryTime = getObjectsBeforeExpiryTime;
				noOfCallsDone++;
				completeAsync();
			});

			model.adminConfigModel.getObjects(getData)
			.then(function(getObjects) {
				resultantData.getObjects = getObjects;
				noOfCallsDone++;
				completeAsync();
			});
		}

		app.post('/instance/instance_configuration', function(req, res) {
			controllerObj.instanceConfiguration(req,res);
		});

		//--doing api call to get sfdc Objects if expiry time of objects is more than 4 hour
		controllerObj.getSfdcObjects = function(req,res) {
			var objectJson, prioritys;
			model.adminConfigModel.getObjectJson(req.query.Source_Instance_Config_Id, req.query.siteId)
			.then(function(result){
				res.send(result);
			});
		}
		app.get('/instance/getSfdcObjects', function(req, res) {
			controllerObj.getSfdcObjects(req, res)
		});


		function searchuser(siteId, baseUrl, token, query, res, page, totalpages, mainresult) {
		    var url = baseUrl+"/API/REST/2.0/system/users?search=%27*" + query + "*%27&depth=complete&page=" + page;

		    eloquaCall.get({
		        host: url,
		        oauth: true,
		        token: result[0].Token,
		        siteId:siteId
		    }).then(function(response) {
		        var result = response && response.result;


		        mainresult.elements.push(result.elements);

		        
		        if (page == totalpages) {
		            res.status(200).send(mainresult);
		        } else {
		            searchuser(query, res, page++, totalpages, mainresult);
		        }
		    });
		}



		//--search eloqua users
		controllerObj.searchusers = function(req, res) {

		    var loginInfo = req.session.appInfo;
		    loginInfo.siteId = loginInfo.siteid||loginInfo.siteId;




		    model.instanceModel.checkToken(loginInfo)
		        .then(function(result) {

					var token = result[0].Token;
					var baseUrl = result[0].Base_Url;



		        	if(loginInfo.siteId){
			            if (req.query.paginate) {
			                var url = baseUrl+"/API/REST/2.0/system/users?search=%27*" + req.query.query + "*%27&depth=complete&count=10&page=" + req.query.page;
			                

			                eloquaCall.get({
			                    host: url,
			                    oauth: true,
			                    token: token,
								siteId: loginInfo.siteId
			                }).then(function(response) {
			                    var result = response && response.result;

			                    if(result){
			                    	console.log(result.length);
			                    }

			                    result = JSON.parse(result);

			                    res.status(200).send(result);
			                })
			                .catch(function(error) {
			                	console.log(error);
			            		res.status(500).send(error);
			                });
			            } else {



			                var url = baseUrl+"/API/REST/2.0/system/users?search=%27*" + req.query.query + "*%27&depth=complete";

			                eloquaCall.get({
			                    host: url,
			                    oauth: true,
			                    token: token,
								siteId: loginInfo.siteId
			                }).then(function(response) {



			                    var result = response && response.result;

			                    result = JSON.parse(result);
			                    var totalpages = parseInt(result.total / 1000) + 1;
			                    if (totalpages <= 1) {
			                        res.status(200).send(result);
			                    } else {
			                        searchuser(loginInfo.siteId, baseUrl, token, req.query.query, res, 2, totalpages, result);
			                    }
			                })
			                .catch(function(error) {
			                	console.log(error);
			            		req.status(500).send(error);
			                });
			            }
		        	} else {

		            	res.status(500).send("siteId not defined");
		        	}
		        }, function(error) {
		            res.status(500).send(error);
		        })
		        .catch(function(error) {
		            res.status(500).send(error);
		        });
		}
		app.get('/instance/users', function(req, res) {
			controllerObj.searchusers(req, res)
		});


		// to add import priority
		// this is half done
		// api things are done and data is coming
		// only have to add to view
		controllerObj.instanceImportPriority = function(req,res) {

			var sess 	= req.session.appInfo;
			var getData = {
                siteId:		sess.siteid || sess.siteId,
                siteName: 	sess.site_name,
                status:		"logged_in",
            }
			model.instanceModel.checkToken(getData)
			.then(function(result){
				if(result)
				{
					if(Math.floor(Date.now()/1000 + 1800) > result[0].Token_Expiry_Time)
					{
						updateExpiryTime.get({
							body: getData,
							token: result[0].Refresh_Token
						})
					}
					return model.instanceModel.checkToken(getData);
				}
			})
			//getting endpoint from db
			.then(function(result){
				tokenResult 		= result;
				var source 			= 'Eloqua';
				var Endpoint_name 	= 'Priority';
				return model.adminConfigModel.getSfdcEndpoint(source, Endpoint_name)
			})
			//getting mapped fields
			.then(function(result){
				endpoint 		= result;
				var url 		= tokenResult[0].Base_Url + endpoint[0].endpoint;
				if(result){
					return eloquaCall.get({
						host:	url,
						oauth: 	true,
						token: 	tokenResult[0].Token
					})
				}
			})
			.then(function(response){
				var result = response && response.result;
				res.send(result);
			})

		}
		app.post('/instance/importPriority', function(req, res) {
			controllerObj.instanceImportPriority(req,res);
		});

		controllerObj.setGetImportPriority=function (req,res){
			model.instanceModel.setGetImportPriority(req.body)
			.then(function(result){
				res.send(result);
			});
		}

		app.post('/instance/setGetImportPriority',function(req,res) {
			controllerObj.setGetImportPriority(req,res);
		});

		//---for saving inside adminConfigModel(like: Contact)


		controllerObj.addSalesforceObjects = function(req,res) {

			var input = 		req.body;
			model.adminConfigModel.addSourcePrimaryObject(input)
			.then(function(data) {
				return model.adminConfigModel.getObjects(input)
			})
			.then(function(getObjects) {
				// model.adminConfigModel.getMappingFields(postData.Source_Object_Id);
				res.send(getObjects);
			})

		}
		app.post('/instance/postObject', function(req, res){
			controllerObj.addSalesforceObjects(req,res);
		});

		//getting eloqua and sfdc objects fields for mapping
		controllerObj.instanceObjectMapping = function(req,res,next){
			var sess = req.session.appInfo;

            var eloquaObjects, selected_SFDC_Objects, mappedFields, SFDCOjects, host,allFilters;

            var userData = {
                siteId:		sess.siteid || sess.siteId,
                siteName: 	sess.site_name,
                status:		"logged_in",
            }

            if(userData.status == "logged_in"){
				model.eloqua_model.getEloquaObject(userData, req.body.Source_Instance_Config_Id )
                .then(function(result){
					host 		= result[0];
					getParams 	= userData;
					return model.instanceModel.checkToken(getParams);
                })
				.then(function(result) {
					if(result)
					{
						if(Math.floor(Date.now()/1000 + 1800) > result[0].Token_Expiry_Time)
						{
							//if expiry time is more than 8 hour then regenrate token
							updateExpiryTime.get({
								body: getParams,
								token: result[0].Refresh_Token
							})
						}
						return model.instanceModel.checkToken(getParams);
					}
				})
				.then(function(result){
					if(result){
						return eloquaCall.get({
							host:	host,
							oauth: 	true,
							token: 	result[0].Token
						})
					}
				})
                .then(function(response){
					var result = response && response.result;

					eloquaObjects 	= result;

					return model.adminConfigModel.getObjectFieldJson(req.body.Source_Instance_Config_Id, userData.siteId, req.body.Source_Object_Id)
                })
				.then(function(result){
					SFDCOjects = JSON.parse(result[0].Object_Json).fields;
					return model.adminConfigModel.addEmailMapping(
						req.body.Source_Instance_Config_Id,
						req.body.Source_Primary_Object_Id,
						req.body.Source_Object_Id,
						SFDCOjects
					)
                })
				.then(function(result){
					return model.adminConfigModel.getMappingFields(req.body.Source_Primary_Object_Id);
                })
				.then(function(result){
					allFilters 	= result;
					var bulkInput 	= {
						name				: "Eloqua External Segmentation Dev",
						updateRule 			: "always",
						fields				: {},
						syncActions			: [
							{
								destination		: "",
								action			: "setStatus",
								status			: "complete"
							}
						],
						identifierFieldName : "C_EmailAddress",
						isSyncTriggeredOnImport: false
					}



					allFilters.forEach(function(item){
						bulkInput.fields[item.Target_Field_Id] 	= "{{Contact.Field("+ item.Target_Field_Id +")}}";
					});


					return model.feederModel.updateImportDefination(req.body.Source_Primary_Object_Id, bulkInput);

                })
				.then(function(result){


					var data = {
						EloquaObjects		:JSON.parse(eloquaObjects),
						SelectedSFDCObjects	:selected_SFDC_Objects,
						MappedFields		:allFilters,
						SFDCOjects			:SFDCOjects
					};
					res.send(data);
				}).catch(next)
			}
		}


        app.post('/instance/objectMapping', function(req, res, next){
        	controllerObj.instanceObjectMapping(req,res, next);
        });


		//add mapping with import defination
		controllerObj.instanceAddMapping = function(req,res) {

			var postData = req.body;
			model.adminConfigModel.addMapping(postData)
			.then(function(data) {
				return model.adminConfigModel.getMappingFields(postData.Source_Object_Id);
			})
			.then(function(data){
				var allFilters 	= data;
				var a = 0;
				var bulkInput 	= {
					name				: "Eloqua External Segmentation Dev",
					updateRule 			: "always",
					fields				: {},
					syncActions			: [
						{
							destination		: "",
							action			: "setStatus",
							status			: "complete"
						}
					],
					identifierFieldName : "C_EmailAddress",
					isSyncTriggeredOnImport: false
				}

				allFilters.forEach(function(item){
					bulkInput.fields[item.Target_Field_Id] 	= "{{Contact.Field("+ item.Target_Field_Id +")}}";
				});

				model.feederModel.updateImportDefination(postData.Source_Object_Id, bulkInput);
                var fields = data.map(function(item) {
                                    var field;
                                    if(item.Query_Relationship !== null && item.Query_Relationship.indexOf('.') > 0){
                                        field = item.Query_Relationship.replace(/\w{1,}./,'')+ "." + item.Source_Field_Id;
                                    } else {
                                        field = item.Source_Field_Id;
                                    }

                                    return field;

                                });
                 model.feederModel.get_filter_By_Object(postData.Source_Object_Id).then(function(result){
                        var _filters = JSON.parse(JSON.stringify(result));
                        if(_filters.length > 0){
	                       var _segmentids = _.uniqBy(_.map(_filters, 'Segment_Id'));
	                 	    model.feederModel.get_Segments_By_Id(_segmentids).then(function(result){

	                 	    	  var _segments = JSON.parse(JSON.stringify(result));
	                 	    	  if(_segments.length > 0){
		                            	 _segments.forEach(function(item){
						                 	item = updateQuery(item, fields);
						                 	model.feederModel.updateSegmentQueryPrefix(item.Query_Prefix, item.Query_Suffix, item.Segment_ID)
						                 });
						                 _filters.forEach(function(item){
						                 	item = updateQuery(item, fields);
						                 	model.feederModel.updateFilterQueryPrefix(item.Filter_Query_Prefix, item.Filter_Query_Suffix, item.Filters_Id);
						                 });

						                 model.feederModel.get_Feeder_Instances_By_Segment_Id(_segmentids).then(function(result){
						                     res.status(200).send(allFilters);
						                 });
	                 	    	  } else {
		                 	             res.status(200).send(allFilters);
	                 	    	  }
	                        });
                        } else {
						     res.status(200).send(allFilters);
                        }
                 });
			})
		}






		app.post('/instance/addMapping', function(req, res) {
			controllerObj.instanceAddMapping(req,res);
		});


		controllerObj.deleteOrg = function(req,res) {
			var postData = req.body;
			model.adminConfigModel.deleteOrg(postData)
			.then(function(data) {
				res.send({status:true});
			})
		}

		//deleting instance
		app.post('/instance/deleteOrg', function(req, res) {
			controllerObj.deleteOrg(req,res);
		});


		controllerObj.deleteSalesforceObject = function(req,res) {
			// console.log("")

			var postData = req.body;


			model.adminConfigModel.campaignDependencyExistence(postData)
			.then(function(result) {


				if(result.length){
					res.send({status:0,dependencies:result})
				}else{

					/*return model.adminConfigModel.deleteSfdcOrg(postData)*/
					deleteSfdcObject();
				}

			});

			function deleteSfdcObject() {
				model.adminConfigModel.deleteSfdcOrg(postData)
				.then(function(data){
					res.send({
						status:1
					});
				})
			}
		}

		app.post('/instance/deleteSfdcOrg', function(req, res) {
			//need to get eloqua object and sfdc object and sfdc object child
			controllerObj.deleteSalesforceObject(req,res);

		});


		app.post('/instance/checkfeederinstances', function(req, res) {
			//need to get eloqua object and sfdc object and sfdc object child
			controllerObj.checkfeederinstances(req,res);

		});



		controllerObj.checkfeederinstances = function(req,res) {
			var postData = req.body;
			model.adminConfigModel.getMappingFields(postData.Source_Object_Id)
			.then(function(data) {
				data = JSON.parse(JSON.stringify(data));
				var allFilters 	= data;

			     var fields = _.map(data, 'Source_Field_Id');

                 model.feederModel.get_filter_By_Object(postData.Source_Object_Id).then(function(result){
                        var _filters = JSON.parse(JSON.stringify(result));
                        var _segmentids = _.uniqBy(_.map(_filters, 'Segment_Id'));
                        if(_filters.length > 0){
		                        model.feederModel.get_Feeder_Instances_By_Segment_Id(_segmentids).then(function(result){
		                        	var feederinstances = result;
		                             model.feederModel.get_Segments_By_Id(_segmentids).then(function(result){

		                 	    	  var _segments = JSON.parse(JSON.stringify(result));
		                 	    	   res.status(200).send({status:1, segments: _segments, feederinstances: feederinstances });
		                              });
				                });
                        } else {
						              res.status(200).send({status:1, feederinstances: []});
                        }
                 });
			});
		}



		app.get('/instance/checkfeederinstancesbysourceconfig', function(req, res) {
			//need to get eloqua object and sfdc object and sfdc object child
			controllerObj.checkfeederinstancesbysourceconfig(req,res);

		});



		controllerObj.checkfeederinstancesbysourceconfig = function(req,res) {
			var id = req.query.id;
	          model.feederModel.get_Segments_By_Source_Instance_Config_Id(id).then(function(result){
	            var _segments = JSON.parse(JSON.stringify(result));
	            var _segmentids = _.uniqBy(_.map(_segments, 'Segment_ID'));
	            if(_segments.length > 0){
	                    model.feederModel.get_Feeder_Instances_By_Segment_Id(_segmentids).then(function(result){
	                    	var feederinstances = result;
	                     	res.status(200).send({segments: _segments, feederinstances: feederinstances });
		                }, function(err){
		                	res.status(500).send(err);
		                });
	            } else {
				        res.status(200).send({segments: _segments, feederinstances: []});
	            }
             }, function(err){
             	res.status(500).send(err);
             });

		}

		//rahul 01-06

		var updateQuery = function(item, fields){
			    if(item.Query_Prefix){
			      if(item.Query_Prefix != null){
	              var _queryPrefixArr = item.Query_Prefix.split("FROM");
	              item.Query_Prefix = "SELECT "  + fields.join() + " FROM " + _queryPrefixArr[1];
			      }
			    } else {
			      if(item.Filter_Query_Prefix != null){
	              var _queryPrefixArr = item.Filter_Query_Prefix.split("FROM");
	              item.Filter_Query_Prefix = "SELECT "  + fields.join() + " FROM " + _queryPrefixArr[1];
			      }
			    }
	            return item;
		}

		controllerObj.deleteSfdcMapping = function(req,res) {
			var postData = req.body;



			model.adminConfigModel.deleteSfdcMapping(postData)
			.then(function(data) {

				return model.adminConfigModel.getMappingFields(postData.Source_Object_Id);
			})
			.then(function(data) {



				data = JSON.parse(JSON.stringify(data));
				var allFilters 	= data;
				var a = 0;
				var bulkInput 	= {
					name				: "Eloqua External Segmentation Dev",
					updateRule 			: "always",
					fields				: {},
					syncActions			: [
						{
							destination		: "",
							action			: "setStatus",
							status			: "complete"
						}
					],
					identifierFieldName : "C_EmailAddress",
					isSyncTriggeredOnImport: false
				}



				allFilters.forEach(function(item){
					bulkInput.fields[item.Target_Field_Id] 	= "{{Contact.Field("+ item.Target_Field_Id +")}}";
				});

				model.feederModel.updateImportDefination(postData.Source_Object_Id, bulkInput);
                var fields = data.map(function(item) {
                                    var field;
                                    if(item.Query_Relationship !== null && item.Query_Relationship.indexOf('.') > 0){
                                        field = item.Query_Relationship.replace(/\w{1,}./,'')+ "." + item.Source_Field_Id;
                                    } else {
                                        field = item.Source_Field_Id;
                                    }

                                    return field;

                                });
                 model.feederModel.get_filter_By_Object(postData.Source_Object_Id).then(function(result){
                        var _filters = JSON.parse(JSON.stringify(result));
                        if(_filters.length > 0){
	                       var _segmentids = _.uniqBy(_.map(_filters, 'Segment_Id'));
	                 	    model.feederModel.get_Segments_By_Id(_segmentids).then(function(result){

	                 	    	  var _segments = JSON.parse(JSON.stringify(result));
	                 	    	  if(_segments.length > 0){
		                            	 _segments.forEach(function(item){
						                 	item = updateQuery(item, fields);
						                 	model.feederModel.updateSegmentQueryPrefix(item.Query_Prefix, item.Query_Suffix, item.Segment_ID)
						                 });
						                 _filters.forEach(function(item){
						                 	item = updateQuery(item, fields);
						                 	model.feederModel.updateFilterQueryPrefix(item.Filter_Query_Prefix, item.Filter_Query_Suffix, item.Filters_Id);
						                 });
						                 model.feederModel.get_Feeder_Instances_By_Segment_Id(_segmentids).then(function(result){
						                     res.status(200).send({status:1, feederinstances: JSON.parse(JSON.stringify(result)), segments: _segments});
						                 });
	                 	    	  } else {
		                 	             res.status(200).send({status:1});
	                 	    	  }
	                        });
                        } else {
						     res.status(200).send({status:1});
                        }
                 });


			});
		}
		app.post('/instance/deleteSfdcMapping', function(req, res) {
			controllerObj.deleteSfdcMapping(req,res);
		});


		controllerObj.getInstanceParentObjects = function(req,res) {
			model.adminConfigModel.getParentObjects({
				instanceConfigId:req.body.instanceConfigId,
				objectName:req.body.objectName,
				siteId:req.session.appInfo.siteid || req.session.appInfo.siteId
			}).then(function(result) {
				if(result.length){
					var sfdcObject = JSON.parse(result[0].json);
					var fields = lodash.filter(sfdcObject.fields,{type:'reference'});
					fields = lodash.reject(fields,{relationshipName:'MasterRecord'});

					var j = 0,respArray=[];
					fields.forEach(function(fieldItem) {

						if(fieldItem.referenceTo.length == 1 && fieldItem.relationshipName){


							fieldItem.referenceTo.forEach(function(refItem) {
								respArray.push({
									"custom":fieldItem.custom,
									"name":fieldItem.name,
									"label":fieldItem.label,
									"referenceTo":refItem,
									"type":fieldItem.type,
									"instanceConfigId":req.body.instanceConfigId,
									"relationshipName":fieldItem.relationshipName,
									"fieldToUse":/*(fieldItem.custom)?fieldItem.relationshipName :refItem*/fieldItem.relationshipName
								});
								j++;
							});
						}
					});


					res.send(respArray);
				} else {
					res.send([]);
				}
			});

		}
		app.post('/instance/getParentObjects',function(req,res) {
			controllerObj.getInstanceParentObjects(req,res);
		})






		controllerObj.getInstanceChildObjects = function(req,res) {
			model.adminConfigModel.getParentObjects({
				instanceConfigId:req.body.instanceConfigId,
				objectName:req.body.objectName,
				siteId:req.session.appInfo.siteid || req.session.appInfo.siteId
			}).then(function(result) {
				if(result.length){
					var sfdcObject = JSON.parse(result[0].json);
					var childObjects = [];
					sfdcObject.childRelationships.forEach(function(item) {
						item.instanceConfigId = req.body.instanceConfigId;
						item.objectName = item.childSObject;
						childObjects.push(item);
					});
					// var fields = lodash.filter(sfdcObject.fields,{type:'reference'});
					// fields = lodash.reject(fields,{relationshipName:'MasterRecord'});

					// var j = 0,respArray=[];
					// fields.forEach(function(fieldItem) {

					// 	if(fieldItem.referenceTo.length == 1 && fieldItem.relationshipName){


					// 		fieldItem.referenceTo.forEach(function(refItem) {
					// 			respArray.push({
					// 				"custom":fieldItem.custom,
					// 				"name":fieldItem.name,
					// 				"label":fieldItem.label,
					// 				"referenceTo":refItem,
					// 				"type":fieldItem.type,
					// 				"instanceConfigId":req.body.instanceConfigId,
					// 				"relationshipName":fieldItem.relationshipName,
					// 				"fieldToUse":/*(fieldItem.custom)?fieldItem.relationshipName :refItem*/fieldItem.relationshipName
					// 			});
					// 			j++;
					// 		});
					// 	}
					// });


					res.send(childObjects);
				} else {
					res.send([]);
				}
			});

		}



		app.post('/instance/getChildObjects',function(req,res) {
			controllerObj.getInstanceChildObjects(req,res);
		});





		controllerObj.getParentObjectFields = function (req,res) {
			model.adminConfigModel.getParentObjectFields({
				instanceConfigId:req.body.instanceConfigId,
				objectName:req.body.objectName,
				siteId:req.session.appInfo.siteid || req.session.appInfo.siteId
			}).then(function(result) {
				if(result && result.json && result.json.length){
					var result = JSON.parse(result.json);
					res.send(result.fields);
				} else {
					res.send([]);
				}
			})
		}
		app.post('/instance/getParentObjectFields',function(req,res) {
			controllerObj.getParentObjectFields(req,res);
		})
	}
})(module.exports)
