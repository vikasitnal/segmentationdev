// (function(controllers) {
//     var fs = require('fs');
//     /*for debug*/
//     // var reload = require('require-reload')(require);

//     controllers.init = function (){
//         var controllerDependencies = arguments;
//         fs.readdir(__dirname, function(err, files) {
//             if (err) throw err;
//             files.forEach(function(file) {
//                 if(file.indexOf('index')<0){
//                     if(file.substr(-3) == '.js') {
//                         var m;
// 						try{
// 							var controllerInit = require('./' + file).init;
// 							if(controllerInit)
// 								controllerInit.apply(m,controllerDependencies);
// 							else
// 								console.log("Please provide the structure for the controller 2342345 of "+ file);
// 						}
// 						catch(error){
// 							console.log(error)
// 						}
//                     }
//                 }
//             });
//         });
//     };



// })(module.exports);

(function(controllers) {
    var fs = require('fs');

    controllers.init = function (){
        var controllerDependencies = arguments;
        fs.readdir(__dirname, function(err, files) {
            if (err) throw err;
            files.forEach(function(file) {
                if(file.indexOf('index')<0){
                    if(file.substr(-3) == '.js') {
                        var m;
                        try{
                            var controllerNameArray = file.split('.');

                            var controllerInit = require('./' + file).init;
                            if(controllerInit)
                                controllerInit.apply(m,controllerDependencies);
                            else{
                                console.log(controllerInit);
                                console.log("Please provide the structure for the controller 2342345 of "+ file);
                            }
                            controllers[controllerNameArray[0]] =  require('./' + file);
                        }
                        catch(error){
                            console.log(error)
                        }
                    }
                }
            });
        });
    };

    controllers.reload = function() {
        /*for debug*/
        var reload = require('require-reload')(require);
        var controllerDependencies = arguments;
        fs.readdir(__dirname, function(err, files) {
            if (err) throw err;
            files.forEach(function(file) {
                if(file.indexOf('index')<0){
                    if(file.substr(-3) == '.js') {
                        var m;
                        try{
                            var controllerInit = reload('./' + file).init;
                            if(controllerInit)
                                controllerInit.apply(m,controllerDependencies);
                            else{
                                console.log(controllerInit);
                                console.log("Please provide the structure for the controller 2342345 of "+ file);
                            }
                        }
                        catch(error){
                            console.log(error)
                        }
                    }
                }
            });
        });
    }



})(module.exports);
