(function(updateExpiryTime) {
	var request = require('request-promise');
	var eloquaCall = require('./eloquaCallBasic');
	var config = require('config');
	var instanceModel = require('../models/instanceModel');
	var Q = require('q');

	
	updateExpiryTime.get = function(userInformation,resultantFunction) {
		var deferred = Q.defer();


		var eloquaConfig 	= config.get('eloquaConfig');						
		// --checking if token is expired or not
		// --if expired then continue (access token is valid for 8 hour)
		// --using Refresh token to regenerate access token.
		var getParams = userInformation.body;
		eloquaCall.post({
			host 	 : eloquaConfig.TokenUrl,
			username : eloquaConfig.AppId,
			password : eloquaConfig.ClientSecret,
			body: {
				'grant_type' : 'refresh_token',
				'refresh_token' : userInformation.token,
				'scope'	: "full", 
				'redirect_uri' : eloquaConfig.redirectUri,
			}
		})
		.then(function(response) {
			
			var tokenResult = response && response.result;


			instanceModel.updateExpiryTime(getParams, tokenResult)
			.then(function(result) {
				if(result.status = 'true')
				{
					var resultPost = {
						'success' : true,		
						'token' : tokenResult
					}
				}
				else
				{
					var resultPost = {
						'success' : false,		
						'token' : tokenResult
					}
					
				}
				
				deferred.resolve(true);
			})							
		})


		return deferred.promise;
	}
})(module.exports);