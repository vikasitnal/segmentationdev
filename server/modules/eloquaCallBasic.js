(function(eloquaCall) {
	var request = require('request-promise');
	var error = require('request-promise/errors');
	var config = require('config');
	var eloquaConfig 	= config.get('eloquaConfig');
	var instanceModel = require('../models/instanceModel');

	var Q = require('q');

	var updateToken = function(siteId) {
		var deferred = Q.defer();
		instanceModel.checkToken({
			siteId:siteId
		}).then(function(result) {
			getUpdatedToken(result[0].Refresh_Token);
		});

		function getUpdatedToken(refreshToken) {
			eloquaCall.post({
				host 	 : eloquaConfig.TokenUrl,
				username : eloquaConfig.AppId,
				password : eloquaConfig.ClientSecret,
				body: {
					'grant_type' : 'refresh_token',
					'refresh_token' : refreshToken,
					'scope'	: "full",
					'redirect_uri' : eloquaConfig.redirectUri,
				}
			})
			.then(function(response) {
				var result = response && response.result;
				updateDataBaseToken({
					siteId:siteId
				},result);
			})
			.catch(function(error) {

					deferred.reject(error);
			});
		}


		function updateDataBaseToken(urlParams,tokenResult) {
			instanceModel.updateExpiryTime(urlParams, tokenResult)
			.then(function(result) {
				if(result){
					deferred.resolve(tokenResult);
				} else {
					deferred.reject("database error");
				}
			})
		}
		return deferred.promise;
	}

	eloquaCall.forceUpdateToken = function(siteId) {
		var deferred = Q.defer();
		updateToken(siteId)
		.then(function(result) {
			deferred.resolve(result);
		})
		return deferred.promise;
	}

	eloquaCall.get = function(userInformation,resultantFunction) {

		var response = {};


		if(userInformation.oauth){
			var deferred = Q.defer();
			var authenticationHeader = "Bearer " + userInformation.token;
			var options = {
			    url : userInformation.host,
		        headers : { "Authorization" : authenticationHeader }
		    };


			request(options,resultantFunction)
				.then(function(result) {


					response.result = result;
					deferred.resolve(response);
				})
				.catch(function(error) {
					if(error.statusCode === 401){
						updateToken(userInformation.siteId)
						.then(function(result) {
							resendRequest(result);
						})
						.catch(function(error) {
							deferred.reject(error);
						})
					} else {
						deferred.reject(error);
					}
				});

			function resendRequest(response) {
				var tokenInfo = response;
				var authenticationHeader = "Bearer " + tokenInfo.access_token;
				options.headers = { "Authorization" : authenticationHeader };

				request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					response.token = tokenInfo.access_token;
					deferred.resolve(response);
				})
				.catch(function(error) {
					deferred.reject(error);
				})
			}

			return deferred.promise;
		} else{
			var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
			var options = {
			    url : userInformation.host,
		        headers : { "Authorization" : authenticationHeader }
		    };
			return request(options, resultantFunction)
					.catch(function(error) {
						console.log(new Date()+': Error in Eloqua ->',error);

					});
		}
	}

	eloquaCall.put = function(userInformation,resultantFunction) {

		var response = {};


		if(userInformation.oauth){
			var deferred = Q.defer();
			var authenticationHeader = "Bearer " + userInformation.token;

			var options = {
				url 		: userInformation.host,
				headers 	: { "Authorization" : authenticationHeader },
				method		: 'PUT',
				body		: userInformation.body
			}

			if(userInformation.json){
				options.json = true;
			}

			request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					deferred.resolve(response);
				})
				.catch(function(error) {
					if(error.statusCode === 401){
						updateToken(userInformation.siteId)
						.then(function(result) {
							resendRequest(result);
						})
					} else {
						// console.log(new Date()+': Error in Eloqua ->',error.statusCode);
						// console.log("_________________________________");
						// console.log(new Date()+': Error in Eloqua ->',error);
						deferred.reject(error);
					}
				});

			function resendRequest(response) {

				var tokenInfo = response;
				var authenticationHeader = "Bearer " + tokenInfo.access_token;
				options.headers = { "Authorization" : authenticationHeader };

				request(options,resultantFunction)
				.then(function(result) {

					response.result = result;
					response.token = tokenInfo.access_token;
					deferred.resolve(response);
				})
				.catch(function(error) {
					deferred.reject(error);
				})
			}

			return deferred.promise;
		} else{
			var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
			var options = {
				url 		: userInformation.host,
				headers 	: { "Authorization" : authenticationHeader },
				method		: 'PUT',
				body		: userInformation.body
			}
			return request(options, resultantFunction)
					.catch(function(error) {
						console.log(new Date()+': Error in Eloqua ->',error);
					});
		}
	}

	eloquaCall.post = function(userInformation,resultantFunction) {

		var response = {};


		if(userInformation.oauth){
			var deferred = Q.defer();

			var authenticationHeader = "Bearer " + userInformation.token;
			var options = {
				url 		: userInformation.host,
				headers 	: { "Authorization" : authenticationHeader, "Connections": "keep-alive" } ,
				method		: 'POST',
				body		: userInformation.body,
				json		: true,
			}


			request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					deferred.resolve(response);
				})
				.catch(function(error) {
					if(error.statusCode === 401){
						updateToken(userInformation.siteId)
						.then(function(result) {
							resendRequest(result);
						})
					} else {
						deferred.reject(error);
						// console.log(new Date()+': Error in Eloqua ->',error.statusCode);
						// console.log("_________________________________");
						// console.log(new Date()+': Error in Eloqua ->',error);
					}
				});

			function resendRequest(response) {

				var tokenInfo = response;
				var authenticationHeader = "Bearer " + tokenInfo.access_token;
				options.headers = { "Authorization" : authenticationHeader };



				request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					response.token = tokenInfo.access_token;
					deferred.resolve(response);
				})
				.catch(function() {
					deferred.reject(error);
					console.log(new Date()+': Error in Eloqua ->',error);
				})
			}

			return deferred.promise;

		}
		else if(userInformation.data){
			var deferred = Q.defer();

			var authenticationHeader = "Bearer " + userInformation.token;
			var options = {
				url 		: userInformation.host,
				headers 	: { "Authorization" : authenticationHeader } ,
				method		: 'POST',
				body		: userInformation.body,
				json		: false,
			}
			request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					deferred.resolve(response);
				})
				.catch(function(error) {
					if(error.statusCode === 401){
						updateToken(userInformation.siteId)
						.then(function(result) {
							resendRequest(result);
						})
					} else {
						deferred.reject(error);
						// console.log(new Date()+': Error in Eloqua ->',error.statusCode);
						// console.log("_________________________________");
						// console.log(new Date()+': Error in Eloqua ->',error);
					}
				});

			function resendRequest(response) {
				var tokenInfo = response;
				var authenticationHeader = "Bearer " + tokenInfo.access_token;
				options.headers = { "Authorization" : authenticationHeader };

				request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					response.token = tokenInfo.access_token;
					deferred.resolve(response);
				})
				.catch(function(error) {
					// console.log(new Date()+': Error in Eloqua ->',error);
				})
			}

			return deferred.promise;
		}
		else
		{

			var deferred = Q.defer();
			var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
			var options = {
				url 		: userInformation.host,
				headers 	: { "Authorization" : authenticationHeader } ,
				method		: 'POST',
				body		: userInformation.body,
				json		: true,
			}

			request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					deferred.resolve(response);
				})
				.catch(function(error) {
					if(error.statusCode === 401){
						updateToken(userInformation.siteId)
						.then(function(result) {
							resendRequest(result);
						})
					} else {
						deferred.reject(error);
						console.log(new Date()+': Error in Eloqua ->',error.statusCode);
						console.log("_________________________________");
						console.log(new Date()+': Error in Eloqua ->',error);
					}
				});

			function resendRequest(response) {
				var tokenInfo = response;
				var authenticationHeader = "Bearer " + tokenInfo.access_token;
				options.headers = { "Authorization" : authenticationHeader };

				request(options,resultantFunction)
				.then(function(result) {
					response.result = result;
					response.token = tokenInfo.access_token;
					deferred.resolve(response);
				})
				.catch(function(error) {
					deferred.reject(error);
					console.log(new Date()+': Error in Eloqua ->',error);
				})
			}



			return deferred.promise;
		}
	}



	eloquaCall.delete = function(userInformation,resultantFunction) {
		var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
		var options = {
	        url : userInformation.host,
	        headers : { "Authorization" : authenticationHeader } ,
	        method:'DELETE',
			json: true,
	    }
		return request(options,resultantFunction)
				.catch(function(error) {
					console.log(new Date()+': Error in Eloqua ->',error);
				});
	}
})(module.exports);
