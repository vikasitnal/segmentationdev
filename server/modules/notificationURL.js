(function(notificationURL) {
    var request = require('request-promise');
    var sfdcCall = require('./sfdcCallBasic');
    var config = require('config');
    var adminConfigModel = require('../models/adminConfigModel');
    var instanceModel = require('../models/instanceModel');
    var feederModel = require('../models/feederModel');
    var getSfdcObject = require('../modules/getSfdcObject');
    var eloquaCall = require('../modules/eloquaCallBasic');
    var updateExpiryTime = require('../modules/updateExpiryTime');
    var Q = require('q');
    var sf = require('node-salesforce');

    var i=0;


    notificationURL.active = function(userInformation) {
        // body...
    }

    notificationURL.get = function(userInformation) {
    	var deferred = Q.defer();

    	// console.log("&&&&&&&&&&&&&&")
    	// console.log("&&&&&&&&&&&&&&")
    	// console.log("&&&&&&&&&&&&&&")

    	// console.log(userInformation);

    	// console.log("&&&&&&&&&&&&&&")
    	// console.log("&&&&&&&&&&&&&&")
    	// console.log("&&&&&&&&&&&&&&")


    	/*var deferred = Q.defer();
    	deferred.resolve(userInformation);
    	return deferred.promise;*/
















        var queryData, uri, endpoint, instanceData, data_string_uri, elo, mappedFieldResult, instanceIdfordb, importDefination;

        var getData = {
            'instanceid': userInformation.instanceId,
            'executionId': userInformation.feeder_execution_id,
            'siteId':'250973722'
        }



        // console.log("------------------")
        // console.log("------------------")
        // console.log("------------------")
        // console.log("------------------")
        // console.log('paranoma',userInformation)
        // console.log("Happy",getData);
        // console.log("------------------")
        // console.log("------------------")
        // console.log("------------------")



        feederModel.getSrcInsId(getData)
            .then(function(result) {

            	// console.log("$$$$$$$$$$$$$")
            	// console.log("$$$$$$$$$$$$$")
            	// console.log("$$$$$$$$$$$$$")
            	// console.log(result);
            	// console.log("$$$$$$$$$$$$$")
            	// console.log("$$$$$$$$$$$$$")
            	// console.log("$$$$$$$$$$$$$")




                //all filters
                instanceData = result;




                var sfdcDataCron = [];
                var n = 0;


                //updating status when cron job start			
                feederModel.getMappedFieldForImportDefination(getData)
                    .then(function(result) {
                        mappedFieldResult = result;
                    })

                function demoFunction(item, index) {
                    getSfdcObject.get({
                            Source_Instance_Config_Id: item.Source_Instance_Config_Id,
                            siteId: item.SiteId,
                            query: item.Filter_Query
                        })
                        .then(function(result) {
                            n++;
                            var count = result.records.length;
                            var dateToTimeStamp = Date.parse(result.records[count - 1].LastModifiedDate);
                            var newestTimestamp = dateToTimeStamp / 1000;
                            var sfdcDataStringify = JSON.stringify(result.records);



                            //get sfdc data timestamp 
                            feederModel.getSfdcDataTimeStamp(item.Filters_Id, item.Segment_Id, item.Instance_Id);


                            feederModel.updateRecordsCount(item.Filters_Id, result.totalSize, item.Segment_Id, item.Instance_Id);


                            //insert sfdc data with newest timetamp
                            feederModel.insertSfdcData(item.Filters_Id, item.Segment_Id, item.Instance_Id, sfdcDataStringify, newestTimestamp, getData.executionId);


                            //all sfdc data what we have to send
                            sfdcDataCron.push({
                                "index": index,
                                "result": result.records,
                                "Filters_Id": item.Filters_Id,
                                "Segment_Id": item.Segment_Id,
                                "instanceId": item.Instance_Id
                            })
                            sendData()
                        });
                }
                instanceData.forEach(demoFunction)

                var deferred = Q.defer();

                function sendData() {
                    if (n == result.length) {
                        deferred.resolve(sfdcDataCron, "status:true");
                    }
                }
                return deferred.promise;
            })
            //data formating
            .then(function(result) {

                queryData = result;
                data_string_uri = [];
                queryData.forEach(function(item) {
                    item.result.forEach(function(item2) {
                        var itx = {}
                        for (key in item2) {
                            if (key != 'attributes' && key != 'LastModifiedDate') {
                                mappedFieldResult.forEach(function(mappedfieldinfor) {
                                    if (mappedfieldinfor.Source_Field_Id == key) {
                                        itx[mappedfieldinfor.Target_Field_Id] = item2[key];
                                    }
                                });
                            }
                        }
                        data_string_uri.push(itx);


                    })
                })

                // console.log("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")
                // console.log("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")
                // console.log("uuuuuuuudata_string_uriuuuuuuu")
                // console.log(data_string_uri);
                // console.log("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")
                // console.log("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")

                return instanceModel.checkToken(getData);
            })
            //eloqua token checking
            .then(function(result) {
            	try{

	                // console.log("*********************")
	                // console.log("*********************")
	                // console.log("*******result********")
	                // console.log(result);
	                // console.log("*********************")
	                // console.log("*********************")
	                // console.log("*********************")


	                if (result) {
	                    if (Math.floor(Date.now() / 1000 + 1800) > result[0].Token_Expiry_Time) {
	                        updateExpiryTime.get({
	                            body: getData,
	                            token: result[0].Refresh_Token
	                        })
	                    }
	                    return instanceModel.checkToken(getData);
	                }
            	}catch(e){
            		// console.log("@@@2",e);
            	}
            })
            //getting endpoint from db
            .then(function(result) {
                tokenResult = result;
                var source = 'Eloqua';
                var Endpoint_name = 'Imports';
                return adminConfigModel.getSfdcEndpoint(source, Endpoint_name)
            })
            //getting mapped fields 
            .then(function(result) {
                endpoint = result;
                return feederModel.getMappedFields(instanceData[0].Master_Object)
            })
            //geting import defination and pushing in array
            .then(function(result) {

                var url = tokenResult[0].Base_Url + endpoint[0].endpoint;

                // console.log("*********************")
                // console.log("*********************")
                // console.log("*******url***********")
                // console.log(url);
                // console.log("*********************")
                // console.log("*********************")
                // console.log("*********************")

                importDefination = JSON.parse(result[0].importDefination);
                var inst = getData.instanceid.replace(/-/g, "");
                importDefination.syncActions[0].destination = "{{FeederInstance(" + inst + ")}}";




                console.log("*********************")
                console.log("*********************")
                console.log("***importDefination**")
                console.log("url",url);
                console.log("*********************")
                console.log("*********************")
                console.log("tokenResult",tokenResult);
                console.log("*********************")
                console.log("*********************")
                console.log("importDefination",importDefination);
                console.log("*********************")
                console.log("*********************")
                console.log("*********************")


                return eloquaCall.post({
                    host: url,
                    oauth: true,
                    body: importDefination,
                    token: tokenResult[0].Token
                })
            })
            //creating Import Defination and posting the data
            .then(function(result) {

                var allFilters = result;



                // console.log("*********************")
                // console.log("*********************")
                // console.log("******allFilters*****")
                // console.log(allFilters);
                // console.log("*********************")
                // console.log("*********************")
                // console.log("*********************")

                uri = allFilters.uri;

                // console.log("*********************")
                // console.log("*********************")
                // console.log("*******uri***********")
                // console.log(uri);
                // console.log("*********************")
                // console.log("*********************")
                // console.log("*********************")



                if (uri) {
                    getChunks = function(str) {
                        var chunks = [];
                        var chunkSize = 3;

                        while (str) {
                        	// console.log("**********str**********")
                        	// console.log("**********str**********")
                        	// console.log(str);
                        	// console.log("**********str**********")
                        	// console.log("**********str**********")
                        	// console.log("**********str**********")
                            if (str.length < chunkSize) {
                                chunks.push(str);
                                break;
                            } else {
                                var chunkData = str.slice(0, chunkSize);
                                var url = tokenResult[0].Base_Url + '/api/bulk/2.0' + uri + '/data';
                                var status = "";
                                var chunkDataStringify = JSON.stringify(chunkData);
                                var importDefinationStringify = JSON.stringify(importDefination);
                                //inserting into db
                                var timestamp = Math.floor(Date.now() / 1000);
                                feederModel.updateExecutionDataImportAndURI(importDefinationStringify, getData.instanceid, chunkDataStringify, status, uri, timestamp, getData.executionId);
                                feederModel.updateExecutionImportAndURI(importDefinationStringify, uri, getData.executionId);
                                if (chunkData.length == 3) {

                                    console.log("*********************")
                                    console.log("*********************")
                                    console.log("***importDefination2**")
                                    console.log("url",chunkData);
                                    console.log("*********************")
                                    console.log("*********************")
                                    console.log("*********************")

                                    eloquaCall.post({
                                            host: url,
                                            oauth: true,
                                            body: chunkData,
                                            token: tokenResult[0].Token
                                        })
                                        .then(function(result) {
                                            //find a way to get status code
                                            if (result == undefined) {
                                                var data_string_syncs = {
                                                    "syncedInstanceUri": uri
                                                };
                                                var url_syncs = tokenResult[0].Base_Url + '/api/bulk/2.0/syncs';
                                                console.log("*********************")
                                                console.log("*********************")
                                                console.log("***3224422244224424**")
                                                console.log("url_syncs",url_syncs);
                                                console.log("*********************")
                                                console.log("*********************")
                                                console.log("*********************")
                                                console.log("*********************")
                                                console.log("*********************")
                                                console.log("***data_string_syncs**")
                                                console.log("data_string_syncs",data_string_syncs);
                                                console.log("*********************")
                                                console.log("*********************")
                                                console.log("*********************")

                                                eloquaCall.post({
                                                        host: url_syncs,
                                                        oauth: true,
                                                        body: data_string_syncs,
                                                        token: tokenResult[0].Token
                                                    })
                                                    .then(function(result) {

                                                        feederModel.updateExecutionImportAndURI(importDefinationStringify, uri, getData.executionId);


                                                    })
                                            }
                                        });
                                }
                                chunks.push(str.slice(0, chunkSize));
                                str = str.slice(chunkSize);
                            }
                        }


                        // console.log("%%%%%%%%%%%%%%%%%%%%%")
                        // console.log("%%%%%%%%%%%%%%%%%%%%%")
                        // console.log("%%%%%%%%%%%%%%%%%%%%%")
                        // console.log(chunks);
                        // console.log("%%%%%%%%%%%%%%%%%%%%%")
                        // console.log("%%%%%%%%%%%%%%%%%%%%%")
                        // console.log("%%%%%%%%%%%%%%%%%%%%%")
                        return chunks;
                    }
                    getChunks(data_string_uri)

                }
            })
        // timestamp and feeder execution id in feeder execution table --  done
        // remove duplicates from feeder execution data (chunk data)
        // view Contacts -- halfway done
        // and upadate total recoreds for single filter criteria --  done
    	return deferred.promise;
    }

})(module.exports);