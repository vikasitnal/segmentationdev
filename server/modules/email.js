(function(emailService) {
    var Email = require('email-templates');
    var nodemailer = require('nodemailer');
    var Q = require('q');
    var notificationModel         = require('../models/notificationModel');

    var smtpConfig = {
        host: 'smtp-mail.outlook.com',
        port: 587,
        secure: false, // upgrade later with STARTTLS
        auth: {
            user: 'support@portqii.com',
            pass: 'p0rtq1!@'
        }
    };

    var transport = nodemailer.createTransport(smtpConfig);

    var email = new Email({
        message: {
            from: 'support@portqii.com'
        },
        // uncomment below to send emails in development/test env:
        // send: true
        transport: transport
    });


    emailService.send = function(to, obj, template, Site_Id, Instance_Id, User_name) {
        var deferred = Q.defer();
        var notification = {
                    To: to,
                    Type: template,
                    Site_Id: Site_Id,
                    Instance_Id: Instance_Id,
                    User_name: User_name
                };
        email
            .send({
                template: template,
                message: {
                    to: to
                },
                locals: obj
            })
            .then(function(res) {
                notification.Status = 'sent';
                notificationModel.createNotification(notification);
                deferred.resolve(res);
            })
            .catch(function(err) {
                notification.Status = 'failed';
                notificationModel.createNotification(notification)
                deferred.reject(err);
            });
        return deferred.promise;
    }



})(module.exports);