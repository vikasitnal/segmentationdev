(function(buildSOQL){
    var mysql = require('mysql');// mysql package
    var Q = require('q');// promise
    var request = require('request-promise');
    var _ = require('lodash');
    var query_json ="";
    var operators_all =[];
    var self;
    var feederModel = require('../models/feederModel');
    // var segmentId = 2;


    var __query ='';
    var cnt = 0;
    var groupLength = 0;
    var qString = '';
    var criteria_length ='';
    var iterations = 0;
    var brCnt = 0;
    var brCntTemp = 0;
    var prevGroupLength = [];
    var prevIterationValue = [];
    var isGroupOutter = false;
    var orCount = 0;
    var orGCount = 0;
    var groupLevel = 0;
    var opCount = 0;
    var prevGroupLevel = [];
    var eleCount = 0;
    var elseCount = 0;
    var prevCriteriaLength = [];
    var prevGroupOpCount = [];
    var sub_prevIteraionValue = [];
    var subIteration = 0;
    function sub_query(criteria, operatorsAll, group, haveBaseObject, baseObject, mappedFields, criteria_len){// to build soql query using filter criterias
        groupLength = criteria_len;
        var inner_iterations = 0;
        console.log("groupLength ", groupLength);
        // console.log("iterations starting ", iterations);
        if(haveBaseObject){
            __query = '';
            iterations++;
            for(var b=0; b<mappedFields.length; b++){
                if(b == mappedFields.length-1){
                    qString += mappedFields[b].Source_Field_Id;
                }else{
                    qString += mappedFields[b].Source_Field_Id + ",";
                }
            }
            qString = "SELECT "+ qString + " FROM "+ baseObject + " WHERE ";
            console.log("qString ", qString);

            criteria_length = criteria.length;
            console.log("criteria length ", criteria_length);
        }
        var deferred = Q.defer();
        var count = 0;
        var isGroup = group;
        var __sub_query = '';
        // _.forEach(criteria, function(crt){
        for(var i=0; i<criteria.length; i++){
            var criteriaLength = criteria.length;
            // prevCriteriaLength.push(criteriaLength);
            console.log("criteriaLength ", criteriaLength);
            console.log("value of i ", i );

            ++inner_iterations;
            ++subIteration;
            console.log("inner_iterations at the start ", inner_iterations);
            console.log("subIteration at the start ", subIteration);
            if(criteria[i].type == "group"){
                console.log("type ",criteria[i].type);
                if(criteria[i].operator){
                    __query += " "+ criteria[i].operator + " ";
                    // __query += "(";
                    console.log("operator from group ", criteria[i].operator);
                    console.log("__query if operator ", __query);

                }
                console.log("subIteration value before pushing ", subIteration);
                sub_prevIteraionValue.push(subIteration);
                subIteration = 0;
                prevIterationValue.push(inner_iterations);
                console.log("prevIterationValue ", prevIterationValue);
                var group_elements = criteria[i].elements;
                __query += "(";
                isGroup = true;
                haveBaseObject = false;
                group_length = group_elements.length;
                prevGroupLength.push(group_length);
                console.log("prevGroupLength ", prevGroupLength);
                baseObject = '';
                sub_query(group_elements, operatorsAll, isGroup, haveBaseObject, baseObject, mappedFields, group_length);
            }else{
                // inner_iterations--;
                console.log("inner_iterations at the start of else ", inner_iterations);
                console.log("subIteration at the start of else ", subIteration);
                console.log("in else");//working
                if(isGroup){
                    count++;
                    console.log("count ", count);
                }
                var fieldName = criteria[i].fieldName;
                console.log("fieldName ", fieldName);
                var condition = criteria[i].condition;
                console.log("condition ", condition);
                var operator = condition.operator;
                console.log("operator ", operator);
                var data_type = condition.type;
                console.log("data_type ", data_type);
                var translation ='';
                var value = condition.value;
                console.log("value", value);//working
                var end = '';
                var start = '';
                if(operator == 'between' || operator == 'notBetween'){
                    end = condition.end;
                    start = condition.start;
                }
                _.forEach(operatorsAll, function(opt){
                    if(data_type ==  opt.Field_Type_Name){
                        if(operator ==  opt.Operator_Name){
                            translation = opt.Operator_Translation;
                            if(operator == 'between' || operator == 'notBetween'){
                                translation = translation.replace("FN", fieldName);
                                translation = translation.replace("x", start);
                                translation = translation.replace("y", end);
                            }
                            translation = translation.replace("FN", fieldName);
                            translation = translation.replace("OPND", value);
                            console.log("translation ", translation);

                            //  __query += translation;

                            if(criteria[i].operator){
                                console.log("operator here ", criteria[i].operator);
                                __query += " "+ criteria[i].operator + " ";
                                console.log("__query inside if operator ", __query);
                            }

                            __query += translation;
                            console.log("__query after appending translation ", __query);

                            if(isGroup){
                                console.log("inner_iterations inside isGroup ", inner_iterations);
                                console.log("groupLength inside isGroup ", groupLength);
                                console.log("subIteration inside isGroup ", subIteration);
                                // if(inner_iterations == groupLength){
                                if(subIteration == groupLength){
                                    console.log("if subIteraion = groupLength", sub_prevIteraionValue);
                                    if(sub_prevIteraionValue.length > 0){
                                        __query += ")";
                                    }
                                    console.log("__query if subIteraion = groupLength ", __query);

                                    if(prevGroupLength.length > 0){
                                        prevGroupLength.pop(group_length);
                                        groupLength = prevGroupLength[prevGroupLength.length - 1];
                                        console.log("groupLength ", groupLength);
                                        subIteration = sub_prevIteraionValue.pop(subIteration);
                                        // subIteration = sub_prevIteraionValue[sub_prevIteraionValue.length - 1];
                                        console.log("subIteration value after first pop ", subIteration);
                                        while((subIteration == groupLength) && (groupLength>0) && (subIteration>0)){
                                            console.log("inside while");
                                            // if(criteria[i].type == "element"){
                                                __query += ")";
                                            // }
                                            groupLength = prevGroupLength.pop(group_length);
                                            subIteration = sub_prevIteraionValue.pop(subIteration);
                                        }
                                    }
                                }
                            }

                        console.log("inside __query ", __query);
                        }
                    }
                });
                console.log("__query ", __query);
            }
        }

        // });
        // console.log("iterations ", iterations);
        // console.log("criteria_length ", criteria_length);
        iterations = 0;
        inner_iterations = 0;
        criteria_length = 0;
        // console.log("__query outside ", __query);
        deferred.resolve(qString + __query);
        return deferred.promise;
    }

    buildSOQL.build = function(segmentId){// function which builds soql query for given json
        var a = 0;
        var deferred = Q.defer();
        var master_object_id;
        feederModel.get_operators_all()
        .then(function(result){
            operators_all = result;
            return feederModel.get_json(segmentId);
        })
        .then(function(result){
            var json = JSON.parse(result[0].SegmentJSON);
            var segment_elements = json.elements;
            var sourceInstanceConfigId = json.sourceInstanceConfigId;
            var statements =[];
            var a = 0;
            _.forEach(segment_elements, function(element){
                var baseObject = element.MasterObject;
                var filter = element.filter;
                var filter_name = filter.name;
                var criteria = filter.elements;
                var isGroup = false;
                var haveBaseObject = true;
                feederModel.get_master_object_id(baseObject,sourceInstanceConfigId)
                .then(function(result){
                    console.log("master object id ", result[0].Source_Primary_Object_Id);
                    master_object_id = result[0].Source_Primary_Object_Id;
                    return feederModel.get_mapped_fields(master_object_id);
                })
                .then(function(result){
                    console.log("mapped fields ", result);
                    var mappde_fields = result;
                    qString = "";
                    return sub_query(criteria, operators_all, isGroup, haveBaseObject, baseObject, mappde_fields, criteria.length);
                })
                .then(function(result){
                    console.log("result from sub_query");
                    statements[a] = {
                            filterName:filter_name,
                            filterQuery:result,
                            filterJson:filter,
                            masterObjectId:master_object_id
                        }
                        if(a == segment_elements.length-1){
                            deferred.resolve(statements);
                            console.log("statements ", statements);
                        }
                        a++;
                })
            });

        })
        return deferred.promise;
    }

})(module.exports);
