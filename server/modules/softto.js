var crypto = require("crypto");
var Q = require('q');
var url = require('url');
var hmacSha1 = function(value, key) {
    var encrypted = crypto.createHmac('sha1', key).update(value).digest('base64')
    return encrypted;
}
softTo = {
    oAuthSigning: function(req, config) {
        var endpoint = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.path
        });

        var sessionData = req.query;

        var deferred = Q.defer();
        if (sessionData) {
            var queryParamDecoded = null;

            if (sessionData['appId']) {
                queryParamDecoded = "appId=" + sessionData['appId'];
            }
            if (sessionData['installId']) {
                queryParamDecoded = queryParamDecoded + "&installId=" + sessionData['installId'];
            }
            if (sessionData['oauth_consumer_key']) {
                queryParamDecoded = queryParamDecoded + "&oauth_consumer_key=" + sessionData['oauth_consumer_key'];
            }
            if (sessionData['oauth_nonce']) {
                queryParamDecoded = queryParamDecoded + "&oauth_nonce=" + sessionData['oauth_nonce'];
            }
            if (sessionData['oauth_signature_method']) {
                queryParamDecoded = queryParamDecoded + "&oauth_signature_method=" + sessionData['oauth_signature_method'];
            }
            if (sessionData['oauth_timestamp']) {
                queryParamDecoded = queryParamDecoded + "&oauth_timestamp=" + sessionData['oauth_timestamp'];
            }
            if (sessionData['oauth_version']) {
                queryParamDecoded = queryParamDecoded + "&oauth_version=" + sessionData['oauth_version'];
            }
            if (sessionData['siteId']) {
                queryParamDecoded = queryParamDecoded + "&siteId=" + sessionData['siteId'];
            }
            if (sessionData['siteName']) {
                queryParamDecoded = queryParamDecoded + "&siteName=" + sessionData['siteName'];
            }
            if (sessionData['UserCulture']) {
                queryParamDecoded = queryParamDecoded + "&UserCulture=" + sessionData['UserCulture'];
            }
            if (sessionData['UserId']) {
                queryParamDecoded = queryParamDecoded + "&UserId=" + sessionData['UserId'];
            }
            if (sessionData['userName']) {
                queryParamDecoded = queryParamDecoded + "&userName=" + sessionData['userName'];
            }

            var urlString = endpoint;
            var clientId = config.eloquaConfig.AppId;
            var hash_key = config.eloquaConfig.ClientSecret + "&";
            var hash_message = req.originalMethod + "&" + encodeURIComponent(urlString) + "&" + encodeURIComponent(queryParamDecoded);
            if (sessionData['oauth_consumer_key'] == clientId) {
                var currentTime = Math.floor(Date.now()/1000);
                var oauth_time = +sessionData['oauth_timestamp'];
                var timeGap = currentTime - oauth_time;

                if (timeGap <= 300) {
                    var signatureCalculated = hmacSha1(hash_message, hash_key);
                    if (signatureCalculated == sessionData['oauth_signature']) {
                        deferred.resolve();
                    } else {
                        deferred.reject({
                            error:"Portqii is unable to authorize the user."
                        });
                    }
                } else {
                    deferred.reject({
                        error:"5 minutes has passed since url has hit."
                    });
                }
            } else {
                deferred.reject({
                    error:"Unable to find the clientId related to the application."
                });
            }
        } else {
            deferred.reject({
                error:"Authorization Error"
            });
        }

        return deferred.promise;
    }
}
module.exports = softTo;