(function(sfdcCall) {
	var request = require('request-promise');
	sfdcCall.get = function(userInformation,resultantFunction) {
		// var deferred = Q.defer();
		var authenticationHeader = "OAuth " + userInformation.header;
		var options = {  
			url : userInformation.host,  
			headers : { "Authorization" : authenticationHeader }  
		};
		var requestObject = request(options, resultantFunction)
		return requestObject;
		// deferred.resolve(requestObject,"status:true");
		// return deferred.promise;
	}

	// sfdcCall.put = function(userInformation,resultantFunction) {
		
		// var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
		// var options = {  
	        // url : userInformation.host,  
	        // headers : { "Authorization" : authenticationHeader } ,
	        // method:'PUT',
			// body: userInformation.body,
			// json: true,
	    // }

		// return request(options,resultantFunction);
	// }

	sfdcCall.post = function(userInformation,resultantFunction) {

		var options = {  
	        url : userInformation.host,  
	        method:'POST',
			json: true,
	    }

		return request(options,resultantFunction);
	}
	
	sfdcCall.delete = function(userInformation,resultantFunction) {
		var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
		var options = {  
	        url : userInformation.host,  
	        headers : { "Authorization" : authenticationHeader } ,
	        method:'DELETE',
			json: true,
	    }
		return request(options,resultantFunction);
	}
})(module.exports);