(function(getSfdcObject) {
	var request 			= require('request-promise');
	var sfdcCall 			= require('./sfdcCallBasic');
	var config 				= require('config');
	var adminConfigModel 	= require('../models/adminConfigModel');
	var feederModel 		= require('../models/feederModel');
	var Q 					= require('q');
	var sf 					= require('node-salesforce');
    var _ 					= require('lodash');
	var jp 					= require('jsonpath');
	var jsforce				= require('jsforce');
    var utility = require('./all');

    /*Extra functions*/


    function dateValidated(value) {
    	var pattern = /^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.[0-9]+)?(Z|.*)?$/g; 
    	if(typeof value === 'string'){

	    	var cond = value.match(pattern);
	    	if(cond && cond.length){
	    		return value.replace(/(T)/g,' ').split('.')[0];

	    	} else {
	    		return value;
	    	}
    	} else {
    		return value;
    	}
    }

    function specialCasesSegment(segmentInfo) {
		var deferred = Q.defer();
		var tempQuery = segmentInfo.query;


		feederModel.getLastSyncDate(segmentInfo.id)
		.then(function(result){
			if(result && result.length>0){
				var lastSyncTime = parseInt(result[0].create_timestamp) * 1000;
				var tempDate = {
					date:new Date(lastSyncTime)
				};
				var strTempDate = JSON.stringify(tempDate);
				var objTempDate = JSON.parse(strTempDate);
				tempQuery = tempQuery.replace(/\(sinceLastSync\)/g,objTempDate.date);
				tempQuery = tempQuery.replace(/\(sinceLastSync\{date\}\)/g,objTempDate.date.split('T')[0]);
				deferred.resolve(tempQuery);
			}
			else
			{
				var lastSyncTime =0;
				var tempDate = {
					date:new Date(lastSyncTime)
				};
				var strTempDate = JSON.stringify(tempDate);
				var objTempDate = JSON.parse(strTempDate);
				tempQuery = tempQuery.replace(/\(sinceLastSync\)/g,objTempDate.date);
				tempQuery = tempQuery.replace(/\(sinceLastSync\{date\}\)/g,objTempDate.date.split('T')[0]);



				deferred.resolve(tempQuery);
			}
		});

		return deferred.promise;
	}



    function specialCases(filter,method) {

		var deferred = Q.defer();
		var tempQuery = filter.query;

		feederModel.getLastSyncDate(filter.SegmentId)
		.then(function(result){
			if(result && result.length>0){
				var lastSyncTime = parseInt(result[0].create_timestamp) * 1000;
				var tempDate = {
					date:new Date(lastSyncTime)
				};
				var strTempDate = JSON.stringify(tempDate);
				var objTempDate = JSON.parse(strTempDate);
				tempQuery = tempQuery.replace(/\(sinceLastSync\)/g,objTempDate.date);
				tempQuery = tempQuery.replace(/\(sinceLastSync\{date\}\)/g,objTempDate.date.split('T')[0]);
				deferred.resolve(tempQuery);
			}
			else
			{
				var lastSyncTime =0;
				var tempDate = {
					date:new Date(lastSyncTime)
				};
				var strTempDate = JSON.stringify(tempDate);
				var objTempDate = JSON.parse(strTempDate);
				tempQuery = tempQuery.replace(/\(sinceLastSync\)/g,objTempDate.date);
				tempQuery = tempQuery.replace(/\(sinceLastSync\{date\}\)/g,objTempDate.date.split('T')[0]);
				deferred.resolve(tempQuery);
			}
		});

		return deferred.promise;
	}




    /* Decision Related */
	getSfdcObject.decisionModifiedQueryItems = function(query) {

		if (query) {

	        /*There can be multiple where string*/
	        var firstWhereIndex = query.toLowerCase().indexOf('where');

	        var beforeFirstWhere = query.substring(0, firstWhereIndex);
	        var afterFirstWhere = query.substring(firstWhereIndex + 5, query.length);

	        var emailColTr = beforeFirstWhere.match('SELECT.*?FROM')[0].replace('SELECT', '').replace('FROM', '').trim().split(',')[0];
	        var countQryTr = beforeFirstWhere; //.replace(/SELECT.*?FROM/, 'SELECT '+emailColTr+' FROM');

	        samQuery = countQryTr + ' WHERE ' + afterFirstWhere + ' ORDER BY ' + emailColTr;

	        return {
	        	query:samQuery,
	        	field:emailColTr
	        }



	    } else {
	        return '';
	    }
	}

	getSfdcObject.getDecisionRecords = function(userInfo,query,field,fieldMapping,settings) {



		var deferred = Q.defer();



		var conn = new jsforce.Connection();
	    var password = utility.decrypt(userInfo.password, config.eloquaConfig.AppId);


	    conn.login(userInfo.userName, password + userInfo.securityToken,function(err,logInfo) {
	    	if (err) {
                var errorMessage = err.toString()
                    .replace(/(?:\r\n|\r|\n)/g, ' ')
                    .replace('MALFORMED_QUERY: ', ' ')
                    .replace(/[\s-]+/g, ' ');


	            deferred.resolve({
	                dataResult: [],
	                issues: [errorMessage]
	            });

	            return;
	        }

	        if (query.indexOf('##LAST') > 0) {

                var metaValuesArr = query.match(/##LAST.*LAST##/g)[0]
                    .replace('##LAST', '')
                    .replace('LAST##', '')
                    .trim()
                    .split(',');

                var condition = metaValuesArr[0];
                var value = parseInt(metaValuesArr[1]);

                var timesJsonString = JSON.stringify({
                    lastTime: new Date(Date.now() - value * Hour),
                    currentTime: new Date()
                })

                var parsedTime = JSON.parse(timesJsonString);

                condition = condition.replace('VALUE', parsedTime.lastTime);
                condition = condition.replace('CURRENT', parsedTime.currentTime);

                query = query.replace(/##LAST.*LAST##/g, condition);

            }

            if (query.indexOf('##NEXT') > 0) {

                var metaValuesArr = query.match(/##NEXT.*NEXT##/g)[0]
                    .replace('##NEXT', '')
                    .replace('NEXT##', '')
                    .trim()
                    .split(',');


                var condition = metaValuesArr[0];
                var value = parseInt(metaValuesArr[1]);

                var timesJsonString = JSON.stringify({
                    lastTime: new Date(Date.now() + value * Hour),
                    currentTime: new Date()
                })

                var parsedTime = JSON.parse(timesJsonString);

                condition = condition.replace('VALUE', parsedTime.lastTime);
                condition = condition.replace('CURRENT', parsedTime.currentTime);

                query = query.replace(/##NEXT.*NEXT##/g, condition);


            }


			if(query.indexOf('sinceLastSync')>0)
			{
				specialCases(item,'getQueriesCount')
				.then(function(query){
					querySFDC(query);
				});
			}
			else
			{
				querySFDC(query);
			}

			function queryResult(err, result) {
				var emailCollection = [],
					modifiedData = [];


				if (err) {
					settings.onError(err);
				}


				if(result.records && result.records.length){
					result.records.forEach(function(record) {
                        var newObj = {},
                            empStat = false;

						var value = jp.query(record, '$..'+field)[0];



						if(value && value.length){
							emailCollection.push(value);
						}

						newObj['C_EmailAddress'] = value;



						fieldMapping.forEach(function(col, index) {
                            if (index == 0) {
                                empStat = !record[col.sourceField];
                            }

                            if (col.relName && col.relName.indexOf('.')>-1) {
                                var jsonPath = '$..' + col.relName.replace(/[A-z]+\./, '') + '.' + col.sourceField;

                                var value = jp.query(record, jsonPath)[0];


                                newObj[col.targetField] = value || ' ';
                            } else {
                                newObj[col.targetField] = record[col.sourceField] || '';
                            }

                        });
                        


						modifiedData.push(newObj);
					});
				}


				settings.sendResult(emailCollection,modifiedData,result.done,function() {
					if (!result.done) {
						// you can use the locator to fetch next records set.
						// Connection#queryMore()

						
						conn.queryMore(result.nextRecordsUrl,queryResult);

					}
				})

			}


			/*cstmzdQry => customized Query*/
			function querySFDC(cstmzdQry) {
				conn.query(cstmzdQry, queryResult);
			}

	    })

	    return deferred.promise;
	}

	function getEloquaDate(value,dateType) {
		var pattern = /\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2]\d|3[0-1])\s(?:[0-1]\d|2[0-3]):[0-5]\d:[0-5]\d/g;
		var cond = pattern.test(value);





		if(cond){
			var newDateValue = JSON.stringify(new Date(value));
			if(dateType)
				return newDateValue.replace(/\"/g,'').split('T')[0];
			else
				return newDateValue.replace(/\"/g,'');
		} else {
			return "'"+value.replace(/\'/g,"\\'")+"'";
		}
	}

	function validateNumTypeValue(value) {
		value = value.trim();

		if(value.length == 0)
			value = 'null';

		if(value.length >= 10)
			value = '<SOQLSegmentationErrored>';

		return value;
	}

	function replaceMeaningFullValue(item,key,replacedLogic,dataType) {
		var pattern = /\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2]\d|3[0-1])\s(?:[0-1]\d|2[0-3]):[0-5]\d:[0-5]\d/g;
		var value = item[key];
		var cond = pattern.test(value);

		var newValue;

		if(cond){
			var newDateValue = JSON.stringify(new Date(value));


			var someLogic = replacedLogic.replace(new RegExp('<'+key+'-date>', "g"),newDateValue.replace(/\"/g,'').split('T')[0]);
				someLogic = someLogic.replace(new RegExp('<'+key+'-datetime>', "g"),newDateValue.replace(/\"/g,''));
		} else {
			var someLogic = replacedLogic.replace(new RegExp('%<'+key+'>%', "g"),"'%"+value.replace(/\'/g,"\\'")+"%'");
				someLogic = someLogic.replace(new RegExp('%<'+key+'>', "g"),"'%"+value.replace(/\'/g,"\\'")+"'");
				someLogic = someLogic.replace(new RegExp('<'+key+'>%', "g"),"'"+value.replace(/\'/g,"\\'")+"%'");





				/*num-type*/
				var quickValidValue = validateNumTypeValue(value.replace(/\'/g,"\\'"));
				someLogic = someLogic.replace(new RegExp('<'+key+'-num>', "g"),quickValidValue);


				someLogic = someLogic.replace(new RegExp('>= <'+key+'>', "g"),
					(quickValidValue !== 'null')?' >= '+quickValidValue:' = '+'null'
				);
				someLogic = someLogic.replace(new RegExp('<= <'+key+'>', "g"),
					(quickValidValue !== 'null')?' <= '+quickValidValue:' = '+'null'
				);
				someLogic = someLogic.replace(new RegExp('> <'+key+'>', "g"),
					(quickValidValue !== 'null')?' > '+quickValidValue:' = '+'null'
				);
				someLogic = someLogic.replace(new RegExp('< <'+key+'>', "g"),
					(quickValidValue !== 'null')?' < '+quickValidValue:' = '+'null'
				);




				/*type-date*/
				someLogic = someLogic.replace(new RegExp('!= <'+key+'-date>', "g"),' != null');
				someLogic = someLogic.replace(new RegExp('>= <'+key+'-date>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('<= <'+key+'-date>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('> <'+key+'-date>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('< <'+key+'-date>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('<'+key+'-date>', "g"),'null');



				/*type-datetime*/
				someLogic = someLogic.replace(new RegExp('!= <'+key+'-datetime>', "g"),' != null');
				someLogic = someLogic.replace(new RegExp('>= <'+key+'-datetime>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('<= <'+key+'-datetime>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('> <'+key+'-datetime>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('< <'+key+'-datetime>', "g"),' = null');
				someLogic = someLogic.replace(new RegExp('<'+key+'-datetime>', "g"),'null');



				/*Normal Values*/
				someLogic = someLogic.replace(new RegExp('<'+key+'>', "g"),"'"+value.replace(/\'/g,"\\'")+"'");



		}
		return someLogic;
	}

	getSfdcObject.getDecisionRecordsThroughController = function(userInfo,sfdcCourierJSON) {
		var deferred = Q.defer();

		var query = sfdcCourierJSON.query;

		firstWhereIndex = query.toLowerCase().indexOf('where');

        var beforeFirstWhere = query.substring(0, firstWhereIndex + 5);
        var afterFirstWhere = query.substring(firstWhereIndex + 5, query.length);


        var groupNo = 0;

        try{
			sfdcCourierJSON.data.forEach(function(item) {


				var replacedLogic = afterFirstWhere;

				for(var key in item){
					if(key !== 'ContactID' && key !== 'EmailAddress'){
						replacedLogic = replaceMeaningFullValue(item,key,replacedLogic);
					}
				}

				item.query = beforeFirstWhere + replacedLogic;

				item.afterQuery = replacedLogic;


				item.resultSize = 0;
				item.result = {};


				return item;
			});
        } catch(error){
        	console.log(error);
        }
        
		var grouped = _.groupBy(sfdcCourierJSON.data,'afterQuery');

		var uniqueQueries = [];
		var uniqueQueriesSet = [];
		var commonIndex = 0;

		for(key in grouped){
			var index = 0;
			grouped[key].forEach(function(item) {
				if(item.query.indexOf('<SOQLSegmentationErrored>')>-1){
					uniqueQueriesSet.push({
						EmailAddress:item.EmailAddress,
						Errored:true
					});
				} else {
					uniqueQueriesSet.push({
						EmailAddress:item.EmailAddress,
						commonIndex:commonIndex,
						index:index
					});
				}
				index++;
			});


			if(key.indexOf('<SOQLSegmentationErrored>')>-1){

			} else {
				uniqueQueries.push({
					query:beforeFirstWhere+key,
					commonIndex:commonIndex,
					resultSize:0,
					result:[]
				});
			}

			commonIndex++;
		}

		var body = {
			jsonBodyList:uniqueQueries
		};
		





		var conn = new jsforce.Connection();
	    var password = utility.decrypt(userInfo.password, config.eloquaConfig.AppId);


	    conn.login(userInfo.userName, password + userInfo.securityToken,function(err,logInfo) {
	    	if(sfdcCourierJSON.withoutFieldsStatus){

	    		conn.apex.post("/PortqiiWithCount/", body, function(err, res) {
					if (err) {
						deferred.reject(err);
					} else {
						try{


							uniqueQueriesSet = uniqueQueriesSet.map(function(item) {
								var obj = {};
								var found = _.find(res,{commonIndex:item.commonIndex});

								if(found)
									obj.resultSize = found.resultSize;
								else{
									obj.resultSize = 2;
								}

								obj.EmailAddress = item.EmailAddress;


								return obj;
							});

							deferred.resolve(uniqueQueriesSet);
						} catch(error){
							console.log("x",error);
						}
					}
				});
	    	} else {



				conn.apex.post("/PortqiiWithFields/", body, function(err, res) {
					if (err) {
						deferred.reject(err);
					} else {
						uniqueQueriesSet = uniqueQueriesSet.map(function(item) {
							var obj = {};
							var found = _.find(res,{commonIndex:item.commonIndex});

							obj.resultSize = found.resultSize;


							if(obj.resultSize == 1){
								obj.result = found.result[0];
							} else {
								obj.result = {};
							}
							obj.EmailAddress = item.EmailAddress;
							return obj;
						});

						deferred.resolve(uniqueQueriesSet);
					}
				});
	    	}
	    });



		return deferred.promise;
	}



    /*Feeder Related*/
    function getCountQuery(query) {
	    if (query) {

	        /*There can be multiple where string*/
	        var firstWhereIndex = query.toLowerCase().indexOf('where');

	        var beforeFirstWhere = query.substring(0, firstWhereIndex);
	        var afterFirstWhere = query.substring(firstWhereIndex + 5, query.length);

	        var emailColTr = beforeFirstWhere.match('SELECT.*?FROM')[0].replace('SELECT', '').replace('FROM', '').trim().split(',')[0];
	        var countQryTr = beforeFirstWhere.replace(/SELECT.*?FROM/, 'SELECT count() FROM');

	        return samQuery = countQryTr + ' WHERE ' + '(' + afterFirstWhere + ')' + ' AND ' + emailColTr + " <> ''" + ' AND ' + emailColTr + " <> null";



	    } else {
	        return '';
	    }

	}





	getSfdcObject.getQueriesCount = function(userInfo,filters,segmentID) {
		var deferred = Q.defer();
		var segmentCount = 0;
		var filterRecords = [],followIssues = [],overallIssue = [];
		var loopAsyncComplete = function() {
			var total = 0,uniqColName='';
			var records = [],issues=[],uniqRecords=[];
			var filtersCountArr = Array(filterRecords.length).fill(0);
			var total = 0;
			filterRecords.forEach(function (item) {
				if(item.recordlength){
					filtersCountArr[item.filterIndex] = item.recordlength;
				} else {
					filtersCountArr[item.filterIndex] = 0;
				}
			});

			deferred.resolve({
				totalCount:segmentCount,
				issues:followIssues,
				segmentIssue:overallIssue,
				counts:filtersCountArr
			});
		}



		var conn = new jsforce.Connection();
		var password = utility.decrypt(userInfo.password, config.eloquaConfig.AppId);
		conn.login(userInfo.userName, password+userInfo.securityToken, function(err, userInfo) {




			if (err) {

				var errorString = err.toString()
							.replace(/(?:\r\n|\r|\n)/g, ' ')
							.replace('MALFORMED_QUERY: ',' ')
							.replace(/[\s-]+/g,' ');

				deferred.resolve({
					totalCount:0,
					issues:[errorString],
					counts:Array(filters.length).fill(0)
				});
				return;
			}
			var asyncCount = 0;
			filters[filters.length] ={ query:filters[0].segmentQuery, type:'segment',SegmentId:segmentID};
			filters.forEach(function(item,index) {

				var currentTime = Date.now();
				var Hour = 1000*60*60;


				if(item.query.indexOf('##LAST')>0){

					var metaValuesArr = item.query.match(/##LAST.LAST##/g)[0]
									.replace('##LAST','')
									.replace('LAST##','')
									.trim()
									.split(',');

					var condition = metaValuesArr[0];
					var value = parseInt(metaValuesArr[1]);

					var timesJsonString = JSON.stringify({
						lastTime:new Date(Date.now() - value*Hour),
						currentTime:new Date()
					})

					var parsedTime = JSON.parse(timesJsonString);

					condition = condition.replace('VALUE',parsedTime.lastTime);
					condition = condition.replace('CURRENT',parsedTime.currentTime);

					item.query = item.query.replace(/##LAST.LAST##/g,condition);

				}

				if(item.query.indexOf('##NEXT')>0){

					var metaValuesArr = item.query.match(/##NEXT.NEXT##/g)[0]
									.replace('##NEXT','')
									.replace('NEXT##','')
									.trim()
									.split(',');


					var condition = metaValuesArr[0];
					var value = parseInt(metaValuesArr[1]);

					var timesJsonString = JSON.stringify({
						lastTime:new Date(Date.now() + value*Hour),
						currentTime:new Date()
					})

					var parsedTime = JSON.parse(timesJsonString);

					condition = condition.replace('VALUE',parsedTime.lastTime);
					condition = condition.replace('CURRENT',parsedTime.currentTime);

					item.query = item.query.replace(/##NEXT.NEXT##/g,condition);


				}




				if(item.query.indexOf('sinceLastSync')>0)
				{


					specialCases(item,'getQueriesCount')
					.then(function(query){
						querySFDC(query);
					});
				}
				else
				{
					querySFDC();
				}

				function querySFDC(query){

					if(query)
					{
						item.query = query;
					}
					var result = {};
					var records = [];




					var cntQry = getCountQuery(item.query);


					conn.query(cntQry,function(error,record) {
						asyncCount++;
						if(error){


							var errorMessage = error.toString()
								.replace(/(?:\r\n|\r|\n)/g, ' ')
								.replace('MALFORMED_QUERY: ',' ')
								.replace(/[\s-]+/g,' ');


							if(item.type === 'segment'){
								overallIssue = [errorMessage];
							} else {
								followIssues.push(errorMessage);
							}



							if(asyncCount==filters.length){
								loopAsyncComplete();
							}
							return;
						}

						if(item.type){

							segmentCount = record.totalSize;
						}
						else{


							result.filterIndex = index;
							result.recordlength = record.totalSize;

							filterRecords.push(result);
						}

						if(asyncCount==filters.length){
							loopAsyncComplete();
						}

					});

				}
			});

		});
		return deferred.promise;
	}


	getSfdcObject.getQueriesRecords2 = function(userInfo, filters, segmentInfo, fields, feederExId,callBackMultiple,onQueryComplete,onerror,onStart) {
	    var deferred = Q.defer();
	    var filteredRecords = [],followIssues= [];


	    var loopAsyncComplete = function() {
	        var total = 0,
	            uniqColName = '';
	        var issues = [],
	            uniqRecords = [];


	        deferred.resolve({
	            dataResult: filteredRecords,
	            issues: followIssues
	        });
	        loopAsyncComplete = null;
	    }



	    var saveSalesforceData = function(input) {
	        if (feederExId) {
	        	if(input.length){
		            dataJSON = JSON.stringify(input);

		            feederModel.setSalesforceData({
		                    feederExId: feederExId,
		                    segmentId: segmentInfo.id,
		                    salesforceData: dataJSON
		                })
		                .then(function(result) {
		                	input = null;
		                })
		                .catch(function(error) {

		                })
	        	}
	        }


	    }

	    var conn = new jsforce.Connection();
	    var password = utility.decrypt(userInfo.password, config.eloquaConfig.AppId);
	    conn.login(userInfo.userName, password + userInfo.securityToken, function(err, userInfo) {

	        if (err) {
                var errorMessage = err.toString()
                    .replace(/(?:\r\n|\r|\n)/g, ' ')
                    .replace('MALFORMED_QUERY: ', ' ')
                    .replace(/[\s-]+/g, ' ');


	            deferred.resolve({
	                dataResult: [],
	                issues: [errorMessage]
	            });

	            return;
	        }

	        if (segmentInfo.query.indexOf('##LAST') > 0) {

                var metaValuesArr = segmentInfo.query.match(/##LAST.*LAST##/g)[0]
                    .replace('##LAST', '')
                    .replace('LAST##', '')
                    .trim()
                    .split(',');

                var condition = metaValuesArr[0];
                var value = parseInt(metaValuesArr[1]);

                var timesJsonString = JSON.stringify({
                    lastTime: new Date(Date.now() - value * Hour),
                    currentTime: new Date()
                })

                var parsedTime = JSON.parse(timesJsonString);

                condition = condition.replace('VALUE', parsedTime.lastTime);
                condition = condition.replace('CURRENT', parsedTime.currentTime);

                segmentInfo.query = segmentInfo.query.replace(/##LAST.*LAST##/g, condition);

            }

            if (segmentInfo.query.indexOf('##NEXT') > 0) {

                var metaValuesArr = segmentInfo.query.match(/##NEXT.*NEXT##/g)[0]
                    .replace('##NEXT', '')
                    .replace('NEXT##', '')
                    .trim()
                    .split(',');


                var condition = metaValuesArr[0];
                var value = parseInt(metaValuesArr[1]);

                var timesJsonString = JSON.stringify({
                    lastTime: new Date(Date.now() + value * Hour),
                    currentTime: new Date()
                })

                var parsedTime = JSON.parse(timesJsonString);

                condition = condition.replace('VALUE', parsedTime.lastTime);
                condition = condition.replace('CURRENT', parsedTime.currentTime);

                segmentInfo.query = segmentInfo.query.replace(/##NEXT.*NEXT##/g, condition);


            }

            if (segmentInfo.query.indexOf('sinceLastSync') > 0) {
                specialCasesSegment(segmentInfo)
                    .then(function(query) {
                        querySFDC(query);
                    });
            } else {
                querySFDC(segmentInfo.query);
            }

            function querySFDC(filteredQuery) {




            	if (!feederExId) {
                    filteredQuery = filteredQuery + ' LIMIT 100';
                }
                var nextRecords = [],bufferRecords = [], noEmailRecords = [], size = 0;
                try{
	            	if(onStart){
	            		onStart(filteredQuery);
	            	}
                } catch(error){
                	console.log("x",error);
                }

                var query = conn.query(filteredQuery)
                    .on("record", function(record,index,tempValue) {
                        var newObj = {},
                            empStat = false;

                        fields.forEach(function(col, index) {
                            if (index == 0) {
                                empStat = !record[col.sourceField];
                            }



                            if (col.relName && col.relName.indexOf('.')>-1) {
                                var jsonPath = '$..' + col.relName.replace(/[A-z]+\./, '') + '.' + col.sourceField;

                                var value = jp.query(record, jsonPath)[0];
                                
                            	value = dateValidated(value);
                                newObj[col.targetField] = value || ' ';

                            } else {
                                record[col.sourceField] = dateValidated(record[col.sourceField]);



                                newObj[col.targetField] = record[col.sourceField] || '';
                            }
                        });


                        /**/
                        if (!empStat){
							if(feederExId){
	                            nextRecords.push(newObj);
	                            bufferRecords.push(newObj);
                        	}
	                        else
                            	filteredRecords.push(newObj);
                        } else {
                        	noEmailRecords.push(record);
                        }



                        if(index%2000===0 && feederExId && index){
                        	var x = all.sizeof(bufferRecords);
	                        size += x;
	                        bufferRecords = null;
	                        bufferRecords = [];

	                        if(size/(1024*1024)>10){
	                        	callBackMultiple(nextRecords);
	                        	nextRecords = null;
	                        	nextRecords = [];
	                        	size = 0;
	                        }
                        } else if(index+1 === tempValue.totalSize && feederExId) {
                        	callBackMultiple(nextRecords);
                        	nextRecords = null;
                        	nextRecords = [];

                        } else if(index==0 && tempValue.totalSize == 1 && feederExId){

                        	callBackMultiple(nextRecords);
                        	nextRecords = null;
                        	nextRecords = [];


                        }

                    })
                    .on("end", function() {

                        if (!feederExId) {
                            loopAsyncComplete();
                        } else{

                        	if(nextRecords.length == 0){
                        		callBackMultiple([]);
                        	}


                        	saveSalesforceData(noEmailRecords)
                        	onQueryComplete();
                        }
                        nextRecords		= null;
						bufferRecords	= null;
						filteredRecords	= null;
						noEmailRecords	= null;
						followIssues	= null;
						saveSalesforceData	= null;
						userInfo = null, feederExId = null;

                    })
                    .on("error", function(err) {
                        var errorMessage = err.toString()
                            .replace(/(?:\r\n|\r|\n)/g, ' ')
                            .replace('MALFORMED_QUERY: ', ' ')
                            .replace(/[\s-]+/g, ' ');



                        followIssues.push(errorMessage);
                        loopAsyncComplete();
                        onerror(errorMessage);
                        nextRecords		= null;
						bufferRecords	= null;
						filteredRecords	= null;
						noEmailRecords	= null;
						followIssues	= null;
						saveSalesforceData	= null;
						userInfo = null, feederExId = null,callBackMultiple =null,onQueryComplete =null;
                    })
                    .run({
                        autoFetch: true,
                        maxFetch: 1000000
                    });
            }


	    });
	    return deferred.promise;
	}
	// getSfdcObject.getQueriesRecords = function(userInfo, filters, fields, feederExId,callBackMultiple,onQueryComplete) {
	//     var deferred = Q.defer();
	//     var filteredRecords = [];


	//     var loopAsyncComplete = function() {
	//         var total = 0,
	//             uniqColName = '';
	//         var issues = [],
	//             uniqRecords = [];


	//         if (filteredRecords && filteredRecords.length) {
	//             uniqColName = Object.keys(filteredRecords[0])[0];
	//             uniqRecords = _.uniqBy(filteredRecords, uniqColName);
	//         }

	//         deferred.resolve({
	//             dataResult: filteredRecords,
	//             issues: issues
	//         });

	//     }



	//     var saveSalesforceData = function(input) {
	//         if (feederExId) {
	//             dataJSON = JSON.stringify(input.data);

	//             feederModel.setSalesforceData({
	//                     feederExId: feederExId,
	//                     filterId: input.filter.id,
	//                     salesforceData: dataJSON
	//                 })
	//                 .then(function(result) {

	//                 })
	//                 .catch(function(error) {

	//                 })
	//         }


	//     }

	//     var conn = new jsforce.Connection();
	//     var password = utility.decrypt(password, config.eloquaConfig.AppId);
	//     conn.login(userInfo.userName, userInfo.password + userInfo.securityToken, function(err, userInfo) {

	//         if (err) {
	//             deferred.resolve({
	//                 dataResult: [],
	//                 issues: [err]
	//             })
	//         }


	//         var asyncCount = 0;
	//         filters.forEach(function(item) {

	//             var currentTime = Date.now();
	//             var Hour = 1000 * 60 * 60;




	//             if (item.query.indexOf('##LAST') > 0) {

	//                 var metaValuesArr = item.query.match(/##LAST.*LAST##/g)[0]
	//                     .replace('##LAST', '')
	//                     .replace('LAST##', '')
	//                     .trim()
	//                     .split(',');

	//                 var condition = metaValuesArr[0];
	//                 var value = parseInt(metaValuesArr[1]);

	//                 var timesJsonString = JSON.stringify({
	//                     lastTime: new Date(Date.now() - value * Hour),
	//                     currentTime: new Date()
	//                 })

	//                 var parsedTime = JSON.parse(timesJsonString);

	//                 condition = condition.replace('VALUE', parsedTime.lastTime);
	//                 condition = condition.replace('CURRENT', parsedTime.currentTime);

	//                 item.query = item.query.replace(/##LAST.*LAST##/g, condition);

	//             }

	//             if (item.query.indexOf('##NEXT') > 0) {

	//                 var metaValuesArr = item.query.match(/##NEXT.*NEXT##/g)[0]
	//                     .replace('##NEXT', '')
	//                     .replace('NEXT##', '')
	//                     .trim()
	//                     .split(',');


	//                 var condition = metaValuesArr[0];
	//                 var value = parseInt(metaValuesArr[1]);

	//                 var timesJsonString = JSON.stringify({
	//                     lastTime: new Date(Date.now() + value * Hour),
	//                     currentTime: new Date()
	//                 })

	//                 var parsedTime = JSON.parse(timesJsonString);

	//                 condition = condition.replace('VALUE', parsedTime.lastTime);
	//                 condition = condition.replace('CURRENT', parsedTime.currentTime);

	//                 item.query = item.query.replace(/##NEXT.*NEXT##/g, condition);


	//             }

	//             if (item.query.indexOf('sinceLastSync') > 0) {
	//                 specialCases(item, 'getQueriesCount')
	//                     .then(function(query) {
	//                         querySFDC(query);
	//                     });
	//             } else {
	//                 querySFDC();
	//             }




	//             function querySFDC(query) {
	//                 if (query) {
	//                     item.query = query;
	//                 }

	//                 if (!feederExId) {
	//                     item.query = item.query + ' LIMIT 100';
	//                 }

	//                 var result = {};
	//                 var nextRecords = [];
	//                 var query = conn.query(item.query)
	//                     .on("record", function(record,index,tempValue) {
	//                         var newObj = {},
	//                             empStat = false;

	//                         fields.forEach(function(col, index) {
	//                             if (index == 0) {
	//                                 empStat = !record[col.sourceField];
	//                             }
	//                             if (!col.relName) {
	//                                 newObj[col.sourceField] = record[col.sourceField] || '';
	//                             } else {
	//                                 var jsonPath = '$..' + col.relName.replace(/[A-z]+\./, '') + '.' + col.sourceField;
	//                                 var value = jp.query(record, jsonPath)[0];
	//                                 newObj['key' + index + col.sourceField] = value || ' ';
	//                             }
	//                         })
	//                         /**/
	//                         if (!empStat){
	//                             filteredRecords.push(newObj);
	//                             nextRecords.push(newObj);
	//                         }


	//                         if(index === 2000 || index+1 === tempValue.totalSize){

	//                         	saveSalesforceData({
	// 	                            data: nextRecords,
	// 	                            filter: item
	// 	                        });


	//                         	callBackMultiple(nextRecords);
	//                         	nextRecords = [].slice();
	//                         }


	//                     })
	//                     .on("end", function() {

	//                         asyncCount++;

	//                         if (asyncCount == filters.length && !feederExId) {
	//                             loopAsyncComplete();
	//                         } else{
	//                         	onQueryComplete();
	//                         }
	//                     })
	//                     .on("error", function(err) {

	//                         var errorMessage = err.toString()
	//                             .replace(/(?:\r\n|\r|\n)/g, ' ')
	//                             .replace('MALFORMED_QUERY: ', ' ')
	//                             .replace(/[\s-]+/g, ' ')
	//                         issues.push(errorMessage);
	//                         asyncCount++;
	//                         if (asyncCount == filters.length) {
	//                             loopAsyncComplete();
	//                         }
	//                     })
	//                     .run({
	//                         autoFetch: true,
	//                         maxFetch: 100000
	//                     });
	//             }

	//         });

	//     });
	//     return deferred.promise;
	// }


	getSfdcObject.get = function(userInformation,resultantFunction) {
		var deferred = Q.defer();
		var tokenResult, gettingEndpoint, url , conditionalPromise;
		if(userInformation.username)
		{
			var username 		= userInformation.username;
            var password = utility.decrypt(userInformation.password, config.eloquaConfig.AppId);
			var securityToken 	= userInformation.securityToken;

			var conn = new jsforce.Connection({

			});

			conn.login(username, password+securityToken, function(err, userInfo) {
				if (err) {
					deferred.resolve(conn,"status:false");
				}
				else{
					deferred.resolve(conn,"status:true");
				}
			});

		}
		else
		{

			adminConfigModel.getConfiguration(userInformation)
			.then(function(result){

				if(result && result.length)
				{
					var username 		= result[0].User_name;
					var password = utility.decrypt(result[0].Password, config.eloquaConfig.AppId);
					var securityToken 	= result[0].Security_token;
					var conn = new jsforce.Connection({

					});

					conn.login(username, password+securityToken, function(err, userInfo) {
						if (err) {
							deferred.resolve(conn);
						}
						else{
							if(userInformation.query)
							{
								var res = userInformation.query.replace("SELECT", "SELECT LastModifiedDate,");
								conn.query(res, function(err, result) {
									if (err) {
										deferred.resolve(err);
									}else{
										deferred.resolve(result);
									}
								});
							}
							else{
								deferred.resolve(conn);
							}
						}
					})
				}else{
					deferred.resolve({
						status:false,
						message:'Unable to sync due to lack of Info'
					})
				}
			})
		}

		deferred.promise.then(function(result) {
			tokenResult = result;



			if(tokenResult.accessToken)
			{
				var source ='SFDC';
				var Endpoint_name ='objects';
				return adminConfigModel.getSfdcEndpoint(source, Endpoint_name)
			}
		})

		.then(function(result) {


			gettingEndpoint = result;
			if(userInformation.objectId)
			{
				url = tokenResult.instanceUrl + result[0].endpoint + userInformation.objectId + "/describe";
			}
			else if(userInformation.query)
			{

			}
			else{
				url = tokenResult.instanceUrl + result[0].endpoint;
			}

			var g = sfdcCall.get({
				host	: url,
				header	: tokenResult.accessToken
			})

			return g;
		})
		.then(function(result){


			if(userInformation.filteredObjects){
				adminConfigModel.saveSObjects(userInformation.Source_Instance_Config_Id, userInformation.siteId, result)

				var objects 		= JSON.parse(result);
				var temp 			= objects.sobjects;
				temp.forEach(demoFunction);



				var n = 0;
				function demoFunction(item, index)
				{
					var urlFields 	= tokenResult.instanceUrl + gettingEndpoint[0].endpoint + item.name + "/describe";
					var m = sfdcCall.get({
						host: urlFields,
						header	: tokenResult.accessToken
					})
					.then(function(res){

						temp[index].JSONData 	= JSON.stringify(res);
						temp[index].emailStatus = 0;
						temp[index].emailName 	= '';
						temp[index].emailLabel 	= '';

						var field 	= JSON.parse(res)
						var temp2	= field.fields;
						for(var x = 0; x < temp2.length; x++){
							if(temp2[x].type === 'email'){
								email_fields = 1;
								temp[index].emailStatus = 1;
								temp[index].emailName = temp2[x].name;
								temp[index].emailLabel = temp2[x].label;
								break;
							}
						}

						n++;
						if(n == temp.length)
						{
							saveData()
						}
					});
				}
				var throughObjectsInformation = 0;
				function saveData()
				{
					for(var i = 0; i <= temp.length-1; i++)
					{
						adminConfigModel.saveSObjectsFields(
							userInformation.Source_Instance_Config_Id,
							userInformation.siteId,
							temp[i].name,
							temp[i].label,
							temp[i].JSONData,
							temp[i].emailStatus,
							temp[i].emailName,
							temp[i].emailLabel
						).then(function() {
							throughObjectsInformation++;
							if(temp.length == throughObjectsInformation){
								adminConfigModel.updateStatus(userInformation.Source_Instance_Config_Id, userInformation.siteId)
								.then(function(result) {

								});
							}
						});
					}
				}

			}
			else if(userInformation.autoMapping)
			{
				sfdcCall.get({
					host	: url,
					header	: tokenResult.accessToken
				})
				.then(function(result){
					var field 	= JSON.parse(result);

					var temp2	= field.fields;
					var noOfEmailsCount = 0;
					temp2.some(function(item){
						if(item.type == 'email')
						{
							adminConfigModel.addEmailMapping(item, userInformation.Source_Primary_Object_Id, userInformation.objectId)
							.then(function(){
								deferred.resolve(result);
							});
							return true;
						}

					})
				});
			}
			else
			{

			}
		});
		return deferred.promise;
	}


	getSfdcObject.post = function(userInformation,resultantFunction) {
		var deferred = Q.defer();
		var tokenResult;
		var sfdcConfig 	= config.get('sfdcConfig');
		var params 		= '?grant_type=password&client_id=' + sfdcConfig.ClientId + '&client_secret=' + sfdcConfig.ClientSecret + '&username=' + sfdcConfig.Username + '&password=' + sfdcConfig.Password + sfdcConfig.SecurityToken ;
		var sfdcObjectList;

		sfdcCall.post({
			host: 	sfdcConfig.TokenUrl + params
		})
		.then(function(result) {
			tokenResult = result;
			if(tokenResult.access_token)
			{
				var source ='Eloqua';
				var Endpoint_name ='Imports';
				return adminConfigModel.getSfdcEndpoint(source, Endpoint_name)
			}
		})
		.then(function(result) {

			if(userInformation.objectId)
			{
				var url = tokenResult.instance_url + result[0].endpoint + userInformation.objectId + "/describe";
			}
			else{
				var url = tokenResult.instance_url + result[0].endpoint;
			}

			return sfdcCall.get({
				host	: url,
				header	: tokenResult.access_token
			})
		})
		.then(function(result){

			if(!userInformation.objectId)
			{
				adminConfigModel.updateSFDCObject(sfdcObjectList);
			}

			deferred.resolve(result);
		});
		return deferred.promise;
	}

})(module.exports);
