(function(parameterisedSoql) {
    var mysql = require('mysql');
    var Q = require('q');
    var _ = require('lodash');
    var decisionModel = require('../models/decisionModel');
    var fs = require('fs');

    /*************Temporary Function*************/
    var configUrl = './config/';
    function getDsElqOperators() {
        var deferred = Q.defer();
        fs.readFile(configUrl+'ds_compare_eloqua_operators.json', function(err, data) {
            if(err){
                deferred.reject(err);
                return;
            }
            deferred.resolve(JSON.parse(data.toString()));
        });         

        return deferred.promise;
    }

    /*******************************************/



    /*Second Level*/
    function buildElqComparisons(ops,element) {


        var logic = _.find(ops.elqCmpOps.logics,{
            operatorName:element.condition.operator
        });

        console.log(logic);

        
        if(element.relationship){
            element.fieldName = element.relationship.replace(/\[P\]/g,'')+'.'+element.fieldName;
        }


        if(element.condition.isNot){
            var translation = logic.notTranslation.replace('OPND',element.fieldName);

        } else {
            var translation = logic.translation.replace('OPND',element.fieldName);
        }
            
        if(element.condition.type.indexOf('date')>-1){
            translation = translation.replace('%ELQFIELD%',element.condition.elqField+'-'+element.condition.type);
        } else {
            translation = translation.replace('%ELQFIELDNUM%',element.condition.elqField+'-num');
            translation = translation.replace('%ELQFIELD%',element.condition.elqField);
        }

        




        if(element.operator)
            translation = " " + element.operator + " " + translation;

        return translation;

    }


    function buildNormalEquations(ops,element) {

        var translation = _.find(ops.elOps,{
            fieldType:element.condition.type,
            operatorName:element.condition.operator
        }).operatorTranslation;


        var value = '';
        var end = '';
        var start = '';

        if(element.relationship){
            element.fieldName = element.relationship.replace(/\[P\]/g,'')+'.'+element.fieldName;
        }



        if (element.condition.value) {
            if (element.condition.operator == 'quickList' || element.condition.operator == 'notquickList') {
                var array = element.condition.value.split(',');
                var stringarray = JSON.stringify(array);
                stringarray = stringarray.replace(/\[/gi, '');
                stringarray = stringarray.replace(/\]/gi, '');
                stringarray = stringarray.replace(/"/gi, "'");
                value = stringarray;
            } else if (element.condition.operator == 'in' || element.condition.operator == 'notin') {
                var stringarray = JSON.stringify(element.condition.value);
                stringarray = stringarray.replace(/\[/gi, '');
                stringarray = stringarray.replace(/\]/gi, '');
                stringarray = stringarray.replace(/"/gi, "'");
                value = stringarray;
            } else {
                value = element.condition.value;
            }
        }

        if (element.condition.operator == 'between' || element.condition.operator == 'notbetween' || element.condition.operator == 'dynamicWithin') {
            end = condition.end;
            start = condition.start;
        }

        if (element.condition.operator == 'between' || element.condition.operator == 'notbetween' || element.condition.operator == 'dynamicWithin') {
            translation = translation.replace(/FN/gi, element.fieldName);
            translation = translation.replace("(x)", start);
            translation = translation.replace("(y)", end);
            translation = "( " + translation + " )";
        } else if(element.condition.operator == 'withinLast' || element.condition.operator == 'withinNext'){
            var set = value.split(' ');
            var first = set[1];
            var second = set[0];
            if(first.indexOf('Hours')==-1){
                translation = translation.replace("FN", element.fieldName);
                translation = translation.replace("OPND",first.toUpperCase());
                translation+= ":"+second;
            } else {
                switch(element.condition.operator){
                    case 'withinLast':
                        translation = "##LAST ("+element.fieldName+" >= "+"VALUE AND "+element.fieldName+" <= CURRENT),"+second+" LAST##";
                        break;
                    case 'withinNext':
                        translation = "##NEXT ("+element.fieldName+" <= "+"VALUE AND "+element.fieldName+" >= CURRENT),"+second+" NEXT##";
                        break;
                }
            }
        } else {
            translation = translation.replace("FN", element.fieldName);


            if(element.condition.type == 'currency' && element.condition.operator == 'equal'){
                translation = translation.replace("'OPND'", value);
            } else {
                translation = translation.replace("OPND", value);
            }


        }


        if(translation.indexOf("NOT")>-1){
            translation = "("+translation+")";
        }


        if(element.operator)
            translation = " " + element.operator + " " + translation;

        return translation;
    }

    function buildGroupEquation(ops,group) {
        var groupQuery = '';

        group.elements.forEach(function(item) {

            switch(item.type){
                case 'elqcmp':
                    groupQuery += buildElqComparisons(ops,item);
                    break;
                case 'element':
                    groupQuery += buildNormalEquations(ops,item);
                    break;
                case 'group':
                    groupQuery += buildGroupEquation(ops,item);
                default:
                    break;
            }
        });

        if(group.operator){
            groupQuery = " " + group.operator + " ( "+groupQuery+" ) ";
        } else {
            groupQuery = " ( "+groupQuery+" ) ";
        }


        return groupQuery;
    }

    /*First Level*/

    function buildFilters(ops,filter) {
        return buildGroupEquation(ops,filter);
    }

    function buildDirectSOQL(ops,SOQLItem) {
        return " ( "+SOQLItem.filter.query+" ) ";
    }

    parameterisedSoql.build = function(segment,fields) {

        var deferred = Q.defer();
        var elementOperators,
            eloquaCmpOperators;

        decisionModel.getAllElementsOperators()
        .then(function(result) {
            elementOperators = result;
            return getDsElqOperators();

        })
        .then(function(result){

            eloquaCmpOperators = result;

            var ops = {
                elOps:elementOperators,
                elqCmpOps:eloquaCmpOperators
            }

            var filterSuffixQuery = '';
            var filtersArray = [];

            segment.elements.forEach(function(item,index) {
                var suffixQuery = '';
                switch(item.filter.type){
                    case 'Filter': 
                        suffixQuery = buildFilters(ops,item.filter);
                        break;
                    case 'SOQL':
                        suffixQuery = buildDirectSOQL(ops,item);
                        break;
                    default:
                        suffixQuery = '';
                        break;
                }
                if(index){
                    filterSuffixQuery = filterSuffixQuery + " OR " + suffixQuery;
                } else {
                    filterSuffixQuery = suffixQuery;
                }

                var beforeWhereString = '';

                if(fields && fields.length){
                    beforeWhereString += fields.join();
                } else {
                    beforeWhereString += 'count()';
                }




                

                filtersArray[index] = {
                    filterName: item.filter.name,
                    filterQuery: "SELECT "+beforeWhereString+" FROM "+item.MasterObject+" WHERE "+suffixQuery,
                    filterJson: item,
                    filterQueryPrefix: "SELECT "+beforeWhereString+" FROM "+item.MasterObject+" WHERE ",
                    filterQuerySuffix: suffixQuery,
                    masterObjectId: item.MasterObject
                }
            });

            deferred.resolve(filtersArray);
        })
        .catch(function(err) {
            deferred.reject(err);
        })





        return deferred.promise;
    }



})(module.exports);
