(function(importDefination) {
	var request 			= require('request-promise');
	var sfdcCall 			= require('./sfdcCallBasic');
	var config 				= require('config');
	// var adminConfigModel 	= require('../models/adminConfigModel');
	// var instanceModel 		= require('../models/instanceModel');
	var feederModel 		= require('../models/feederModel');
	var getSfdcObject 		= require('../modules/getSfdcObject');
	var eloquaCall 			= require('../modules/eloquaCallBasic');
	var updateExpiryTime	= require('../modules/updateExpiryTime');
	var Q 					= require('q');
	var sf 					= require('node-salesforce');
	
	importDefination.get = function(userInformation,resultantFunction) {
		console.log('coming here')
		console.log('coming here')
		// var instanceData, instanceIdfordb, inst, getdata;
		// var session = req.session;
		var getData = {
			'instanceid': userInformation.data.instanceid,
			'siteId' 	: userInformation.data.siteId
		}
		feederModel.getSrcInsId(getData)
		.then(function(result){
			instanceData 	= result;
			var MappedField = [];
			var x 			= 0;
			function demoFunction1(item, index)
			{
				feederModel.getMappedFields(item.Master_Object)
				.then(function(result1){
					console.log('getMappedFields')
					console.log(getMappedFields)
					x++
					MappedField.push({"index": index, "result" : result1,'Master_Object': item.Master_Object, "Filters_Id" : item.Filters_Id, "Segment_ID" : item.Segment_ID, 'instance' : item.Instance_Id})
					sendData1()							
				});
			}
			
			instanceData.forEach(demoFunction1)

			var deferred 		= Q.defer();
			function sendData1()
			{
				if(x == instanceData.length)
				{
					deferred.resolve(MappedField,"status:true");
				}
			}
			return deferred.promise;
		})
		//creating Import Defination and posting the data
		.then(function(result){
			console.log('result from mapped fields')
			console.log(result)
			var allFilters 	= result;
			var importDefination 	= {
				name				: "Eloqua External Segmentation Dev",
				updateRule 			: "always",
				fields				: {},
				syncActions			: [
					{
						destination		: "{{FeederInstance(" + allFilters[0].instance.replace(/-/g, "") + ")}}",
						action			: "setStatus",
						status			: "complete"
					}
				],
				identifierFieldName : "C_EmailAddress",
				"isSyncTriggeredOnImport": true
			}

			allFilters.forEach(function(item){
				instanceIdfordb 					= item.instance;
				inst 								= item.instance.replace(/-/g, "");
				//repetation
				importDefination.syncActions[0].destination 	= "{{FeederInstance(" + inst + ")}}",
				
				item.result.forEach(function(item2){
					importDefination.fields[item2.Target_Field_Id] 	= "{{Contact.Field("+ item2.Target_Field_Id +")}}"
				});
				a++;
				mergeData();
			})
			function mergeData()
			{
				if(a == result.length)
				{
					console.log('importDefination');
					console.log(importDefination);
					// feederModel.updateImportDefination(inst, importDefination)
				}
			}
		});
		
		
		
	}
	
})(module.exports);