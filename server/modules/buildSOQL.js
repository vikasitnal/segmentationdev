(function(buildSOQL) {
    var mysql = require('mysql');
    var Q = require('q');
    var request = require('request-promise');
    var _ = require('lodash');
    var query_json = "";
    var operators_all = [];
    var self;
    var feederModel = require('../models/feederModel');

    var resultString = '';
    var relStringChg = '';

    function sliceString(relString, recur) {
        if (recur == false) {
            resultString = "";
        }
        var relInd = relString.indexOf(".");
        relStringChg = relString.substring((relInd + 1), relString.length);
        relInd = relStringChg.indexOf(".");
        if (relInd >= 0) {
            if (resultString == "") {
                resultString = relStringChg.substring(3, relInd);
            } else {
                resultString = resultString + "." + relStringChg.substring(3, relInd);
            }
            recur = true;
            sliceString(relStringChg, recur);
        } else {
            if (resultString == "") {
                resultString = relStringChg.substring(3, relStringChg.length);
            } else {
                resultString = resultString + "." + relStringChg.substring(3, relStringChg.length);
            }
        }
        return resultString;
    }


    var __query = '';
    var groupLength = 0;
    var qString = '';
    var criteria_length = '';
    var iterations = 0;
    var prevGroupLength = [];
    var prevIterationValue = [];
    var sub_prevIteraionValue = [];
    var subIteration = 0;
    var masterObject = '';

    function sub_query(criteria, operatorsAll, group, haveBaseObject, baseObject, mappedFields, criteria_len) {
        groupLength = criteria_len;
        var inner_iterations = 0;
        if (haveBaseObject) {
            masterObject = baseObject;
            __query = '';
            iterations++;


            for (var b = 0; b < mappedFields.length; b++) {
                var field = '';


                if(mappedFields[b].Query_Relationship !== null && mappedFields[b].Query_Relationship.indexOf('.') > 0){
                    field = mappedFields[b].Query_Relationship.replace(/\w{1,}./,'')+ "." + mappedFields[b].Source_Field_Id;
                } else {
                    field = mappedFields[b].Source_Field_Id;
                }

                if (b == mappedFields.length - 1) {
                    qString += field;
                } else {

                    qString += field + ",";
                }
            }
            qString = "SELECT " + qString + " FROM " + baseObject + " WHERE ";


            criteria_length = criteria.length;
        }
        var deferred = Q.defer();
        var count = 0;
        var isGroup = group;
        var __sub_query = '';
        for (var i = 0; i < criteria.length; i++) {
            var criteriaLength = criteria.length;


            if(criteria[i].treeViewData)
                criteria[i].treeViewData = undefined;



            ++inner_iterations;
            ++subIteration;
            if (criteria[i].type == "group") {
                if (criteria[i].operator) {
                    __query += " " + criteria[i].operator + " ";

                }
                sub_prevIteraionValue.push(subIteration);
                subIteration = 0;
                prevIterationValue.push(inner_iterations);
                var group_elements = criteria[i].elements;
                __query += "(";
                isGroup = true;
                haveBaseObject = false;
                group_length = group_elements.length;
                prevGroupLength.push(group_length);
                baseObject = '';
                sub_query(group_elements, operatorsAll, isGroup, haveBaseObject, baseObject, mappedFields, group_length);
            } else {
                if (isGroup) {
                    count++;
                }
                var fieldName = criteria[i].fieldName;
                var pObject = '';
                var ind = 0;
                var relShip = '';
                var tempStr1 = '';
                var objType = '';
                var tempStr2 = '';
                var obj = '';
                if (criteria[i].relationship) {
                    pObject = criteria[i].relatedObject;
                    relationship = criteria[i].relationship;
                    var recur = false;
                    obj = sliceString(relShip, recur);




                    fieldName = relationship.replace(/\[P\]/gi, '')+ "." + fieldName;



                }

                var condition = criteria[i].condition;
                var operator = condition.operator;
                var data_type = condition.type;
                var translation = '';
                var translation1 = '';
                var translation2 = '';
                var value = '';
                var end = '';
                var start = '';

                if(condition.value)
                   if(operator == 'quickList' || operator == 'notquickList'){
                     var array = condition.value.split(',');
                     var stringarray = JSON.stringify(array);
                     stringarray = stringarray.replace(/\[/gi, '');
                     stringarray = stringarray.replace(/\]/gi, '');
                     stringarray = stringarray.replace(/"/gi, "'");
                     value = stringarray;
                   } else if (operator == 'in' || operator == 'notin') {
                     var stringarray = JSON.stringify(condition.value);
                     stringarray = stringarray.replace(/\[/gi, '');
                     stringarray = stringarray.replace(/\]/gi, '');
                     stringarray = stringarray.replace(/"/gi, "'");
                     value = stringarray;
                   } else {
                     value = condition.value;
                   }
                if (operator == 'between' || operator == 'notbetween' || operator == 'dynamicWithin') {
                    end = condition.end;
                    start = condition.start;
                }







                _.forEach(operatorsAll, function(opt) {



                    if (data_type == opt.Field_Type_Name) {


                        if (operator == opt.Operator_Name) {

                            translation = opt.Operator_Translation;

                            if (operator == 'between' || operator == 'notbetween' || operator == 'dynamicWithin') {
                                translation = translation.replace(/FN/gi, fieldName);
                                translation = translation.replace("(x)", start);
                                translation = translation.replace("(y)", end);
                                translation = "( " + translation + " )";
                            } else if(operator == 'withinLast'||operator == 'withinNext'||operator == 'notwithinLast'||operator == 'notwithinNext'){
                                var set = value.split(' ');
                                var first = set[1];
                                var second = set[0];
                                if(first.indexOf('Hours')==-1){
                                    translation = translation.replace("FN", fieldName);
                                    translation = translation.replace("OPND",first.toUpperCase());
                                    translation+= ":"+second;
                                } else {
                                    switch(operator){
                                        case 'withinLast':
                                            translation = "##LAST ("+fieldName+" >= "+"VALUE AND "+fieldName+" <= CURRENT),"+second+" LAST##";
                                            break;
                                        case 'withinNext':
                                            translation = "##NEXT ("+fieldName+" <= "+"VALUE AND "+fieldName+" >= CURRENT),"+second+" NEXT##";
                                            break;
                                    }
                                }
                            } else {




                                translation = translation.replace("FN", fieldName);
                                translation = translation.replace("OPND", value);
                            }



                            if(translation.indexOf("NOT")>-1){
                                translation = "("+translation+")";
                            }




                            if (criteria[i].operator) {
                                __query += " " + criteria[i].operator + " ";
                            }



                            __query += translation;




                            if (isGroup) {
                                if (subIteration == groupLength) {
                                    if (sub_prevIteraionValue.length > 0) {
                                        __query += ")";
                                    }

                                    if (prevGroupLength.length > 0) {
                                        prevGroupLength.pop(group_length);
                                        groupLength = prevGroupLength[prevGroupLength.length - 1];
                                        subIteration = sub_prevIteraionValue.pop(subIteration);
                                        while ((subIteration == groupLength) && (groupLength > 0) && (subIteration > 0)) {
                                            __query += ")";
                                            prevGroupLength.pop(group_length);
                                            groupLength = prevGroupLength[prevGroupLength.length - 1];
                                            subIteration = sub_prevIteraionValue.pop(subIteration);
                                        }
                                    }
                                }
                            }

                        }
                    }
                });
            }
        }

        iterations = 0;
        inner_iterations = 0;
        criteria_length = 0;
        deferred.resolve({
            queryPrefix:qString,
            querySuffix:__query
        });
        return deferred.promise;
    }

    buildSOQL.build = function(segmentId) {
        var a = 0;
        var deferred = Q.defer();
        var master_object_id;


        feederModel.get_operators_all()
            .then(function(result) {
                operators_all = result;
                return feederModel.get_json(segmentId);
            })
            .then(function(result) {



                var json = JSON.parse(result[0].SegmentJSON);
                var segment_elements = json.elements;
                var sourceInstanceConfigId = json.sourceInstanceConfigId;
                var statements = [];
                var a = 0;


                _.forEach(segment_elements, function(element) {




                    var baseObject = element.MasterObject;
                    var filter = element.filter;
                    var filter_name = filter.name;
                    var criteria = filter.elements;
                    var isGroup = false;
                    var haveBaseObject = true;
                    var soqlStatus = false,soqlQryObj = null;


                    feederModel.get_master_object_id(baseObject, sourceInstanceConfigId)
                        .then(function(result) {

                            master_object_id = result[0].Source_Primary_Object_Id;

                            return feederModel.get_mapped_fields(master_object_id);
                        })
                        .then(function(result) {
                            var mappde_fields = result;




                            _fields = mappde_fields.map(function(item) {
                                var field;
                                if(item.Query_Relationship !== null && item.Query_Relationship.indexOf('.') > 0){
                                    field = item.Query_Relationship.replace(/\w{1,}./,'')+ "." + item.Source_Field_Id;
                                } else {
                                    field = item.Source_Field_Id;
                                }

                                return field;

                            });


                            var joined_fields = _fields.join();

                            if(filter.type == 'SOQL'){
                              // qString = 'SELECT '+  joined_fields +' FROM ' + baseObject + ' WHERE '  + filter.query.replace(/"/g, '\\"');
                              soqlStatus = true;
                              soqlQryObj = {};
                              soqlQryObj.queryPrefix = 'SELECT '+  joined_fields +' FROM ' + baseObject + ' WHERE ';
                              soqlQryObj.querySuffix =  filter.query.replace(/"/g, '\\"');


                            } else {
                              qString = '';
                              return sub_query(criteria, operators_all, isGroup, haveBaseObject, baseObject, mappde_fields, criteria.length);
                            }


                        })
                        .then(function(result) {

                            result = result || soqlQryObj;



                            statements[a] = {
                                filterName: filter_name,
                                filterQuery: result.queryPrefix+result.querySuffix,
                                filterJson: filter,
                                filterQuerySuffix: result.querySuffix,
                                filterQueryPrefix:result.queryPrefix,
                                masterObjectId: master_object_id
                            }
                            if (a == segment_elements.length - 1) {

                                deferred.resolve(statements);
                            }
                            a++;
                        })
                        .catch(function(error) {
                            deferred.reject(error);
                        })
                });

            })
            .catch(function(error) {
                deferred.reject(error);
            });

            
        return deferred.promise;
    }

})(module.exports);
