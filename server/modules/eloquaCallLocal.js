(function(eloquaCall) {
	var request = require('request-promise');
	eloquaCall.get = function(userInformation,resultantFunction) {
		var authenticationHeader = "Basic " + new Buffer('TechnologyPartnerPORTQIIpteltd\\Arvind.Aiah' + ":" + 'p0rtQ1234').toString("base64");
		var options = {
		    url : userInformation.host,
	        headers : { "Authorization" : authenticationHeader }
	    };
		return request(options, resultantFunction);
	}

	eloquaCall.put = function(userInformation,resultantFunction) {

		var authenticationHeader = "Basic " + new Buffer('TechnologyPartnerPORTQIIpteltd\\Arvind.Aiah' + ":" + 'p0rtQ1234').toString("base64");
		var options = {
	        url : userInformation.host,
	        headers : { "Authorization" : authenticationHeader } ,
	        method:'PUT',
			body: userInformation.body,
			json: true,
	    }

		return request(options,resultantFunction);
	}

	eloquaCall.post = function(userInformation,resultantFunction) {

		var authenticationHeader = "Basic " + new Buffer('TechnologyPartnerPORTQIIpteltd\\Arvind.Aiah' + ":" + 'p0rtQ1234').toString("base64");
		var options = {
	        url : userInformation.host,
	        headers : { "Authorization" : authenticationHeader } ,
	        method:'POST',
			body: userInformation.body,
			json: true,
	    }

		return request(options,resultantFunction);
	}
	eloquaCall.delete = function(userInformation,resultantFunction) {
		var authenticationHeader = "Basic " + new Buffer('TechnologyPartnerPORTQIIpteltd\\Arvind.Aiah' + ":" + 'p0rtQ1234').toString("base64");
		var options = {
	        url : userInformation.host,
	        headers : { "Authorization" : authenticationHeader } ,
	        method:'DELETE',
			json: true,
	    }
		return request(options,resultantFunction);
	}
})(module.exports);
