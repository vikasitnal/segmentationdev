/*Node Js In built*/
var fs = require('fs');
var path = require('path');
var env = process.env.NODE_ENV;
/*For Developent Purpose*/
var watch = require('node-watch');
/*Node Js Custom Installed*/
var express = require('express');
var https 	= require('https');
var app = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override')
var multer = require('multer');
var upload = multer();
var session = require('express-session');
var cookieParser = require('cookie-parser');
var config = require('config');

// var port = process.env.PORT || 8081;

app.use(express.static('client'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride())
app.use(upload.array());
app.use(cookieParser());
app.use(session({secret: "Segmentation"}));

var controllers 		= require("./../server/controllers");
var models 				= require("./../server/models");
var db 					= require('./../server/db/dbConnection');
require('./../server/modules/all');
var eloquaCall 			= require('./../server/modules/eloquaCallBasic');
var sfdcCall 			= require('./../server/modules/sfdcCallBasic');
var updateExpiryTime 	= require('./../server/modules/updateExpiryTime');
var getSfdcObject 		= require('./../server/modules/getSfdcObject');
var notificationURL 	= require('./../server/modules/notificationURL');
var buildSOQL 			= require('./../server/modules/buildSOQL');
var importDefination 	= require('./../server/modules/importDefination');
var emailService        = require('./../server/modules/email');
var controllerObj = {};
var chai = require('chai')
  , expect = chai.expect
  , should = chai.should();
var config = {
  "host": "localhost",
  "port": 9999,
  "dbConfig":{
  	"host":			"localhost",
  	"user":			"root",
  	"password":		"root",
  	"database":		"unittesting"
  },
   "eloquaConfig":{
    "TokenUrl" : 	"https://login.eloqua.com/auth/oauth2/token/",
    "redirectUri" : "https://apps.portqii.com/edidev/instance/saveToken",
    "AppId" : 		"f7b33a3e-b64d-4d14-969d-20be1620217c",
    "ClientSecret": "1SPZP3LoJXkaAVDEeJdRSedYpvtq865Kl0841afPpMsxC~KHY2PVfnNGDY3PQFV7flehKd0Q4blUH20xSWO0bVMvIgXu29-ZOxYZ"
  },
   "sfdcConfig":{
    "ClientId" : 	"3MVG9ZL0ppGP5UrAPt8LHhefUfGZ96qc1dpWfISu_Wr_d4R1ol_FFpToKg2swql.9IBorOqbe0xRM4pZrkPRU",
    "ClientSecret":	"3223318913841702787",
    "Username" : 	"rahul.sharma@crmsci.com",
    "Password" : 	"Krishna22",
    "SecurityToken":"DC43Z8gAZfdL2w8daI8iLtFw",
    "LoginUri" : 	"http://login.salesforce.com",
    "TokenUrl" : 	"https://login.salesforce.com/services/oauth2/token"
  }
};
db.init(config);

models.init(db,function(modelsArray) {
  // Setup the routes
  controllers.init(app, modelsArray, config, eloquaCall, sfdcCall, updateExpiryTime, getSfdcObject, notificationURL, buildSOQL, importDefination,controllerObj, emailService);
});

beforeEach(function() {
});
//Setup the models



  describe('acl', function() {
    describe('#get({page:1})', function() {
      it('should receive zero results and no error', function() {
        // testing get acl method for 0 results
        controllers.accessControlController.get({page: 1}, function(err, result){
          expect(err).to.be(null);
          expect(result).to.be.a('array');
          expect(result.length).to.be(0)
        });
      });
    });


    describe('#post({UserId:1, Email: harishkumarchellappa@gmail.com, Name: harish kumar}) and retry should throw "user already has access" error ', function() {
      it('should create one row in acl and return array with one result', function() {
        // testing get acl method for 0 results
        controllers.accessControlController.post({UserId: 1, Email: 'harishkumarchellappa@gmail.com',Name: 'harish kumar'}, function(err, result){
          expect(err).to.be(null);
          expect(result).to.be.a('array');
          expect(result.length).to.be(1);
          expect(result[0].UserId).to.be(1);
        });
      });

      it('should retry same payload and get a duplicate try error', function() {
        // testing get acl method for 0 results
        setTimeout(function () {
          controllers.accessControlController.post({UserId: 1, Email: 'harishkumarchellappa@gmail.com',Name: 'harish kumar'}, function(err, result){
            err.should.not.equal(null);
            expect(err.message).to.be('user already has access')
          });
        }, 5000);
      });

    });



    describe('#get({page:1})', function() {
      it('should return array with one result', function() {
        // testing get acl method for 0 results
        controllers.accessControlController.get({page: 1}, function(err, result){
          expect(err).to.be(null);
          expect(result).to.be.a('array');
          expect(result.length).to.be(1)
          expect(result[0].UserId).to.be(1);
        });
      });
    });


    describe('#getuserids()', function() {
      it('should return array with one userId', function() {
        // testing get acl method for 0 results
        controllers.accessControlController.getuserids(function(err, result){
          expect(err).to.be(null);
          expect(result).to.be.a('array');
          expect(result.length).to.be(1)
          expect(result[0]).to.be(1);
        });
      });
    });


    describe('#_delete(1)', function() {
      it('should delete one row and no error should return', function() {
        setTimeout(function () {
          // testing get acl method for 0 results
          controllers.accessControlController._delete({id: '1'}, function(err, result){
            expect(err).to.be(null);
          });
        }, 5000);
      });
    });

  });
