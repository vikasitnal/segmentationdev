require('it-each')();
var chai = require('chai')
  , expect = chai.expect
  , should = chai.should();
var async = require('async');
var chaiHttp = require('chai-http');
var server = require('../server');


chai.use(chaiHttp);
var agent = chai.request.agent(server)
var conditions = [
    {
        "type": "OtherState exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "equal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude between",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublebetween",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "between",
                                        "start": 1.11,
                                        "end": 10.45
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "double0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notequal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "blank",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude greater than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublegreater than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "greater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude less than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleless than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "less",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude at most",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat most",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notgreater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLatitude at least",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat least",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notless",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "equal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude between",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublebetween",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "between",
                                        "start": 1.11,
                                        "end": 10.45
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "double0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notequal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "blank",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude greater than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublegreater than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "greater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude less than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleless than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "less",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude at most",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat most",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notgreater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherLongitude at least",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat least",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notless",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareaexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareastarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareaends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareacontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareais blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingState 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingState",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingPostalCode 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingPostalCode",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingCountry 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingCountry",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "equal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude between",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublebetween",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "between",
                                        "start": 1.11,
                                        "end": 10.45
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "double0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notequal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "blank",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude greater than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublegreater than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "greater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude less than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleless than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "less",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude at most",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat most",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notgreater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLatitude at least",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat least",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLatitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notless",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "equal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude between",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublebetween",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "between",
                                        "start": 1.11,
                                        "end": 10.45
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "double0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notequal",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "blank",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude greater than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doublegreater than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "greater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude less than",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleless than",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "less",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude at most",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat most",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notgreater",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MailingLongitude at least",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "doubleat least",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MailingLongitude",
                                    "operator": null,
                                    "condition": {
                                        "type": "double",
                                        "operator": "notless",
                                        "value": 1.11
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Phone exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Phone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "equal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Phone starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonestarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Phone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "startsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Phone ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Phone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "endsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Phone contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonecontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Phone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "contains",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Phone 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phone0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Phone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "notequal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Fax exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Fax",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "equal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Fax starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonestarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Fax",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "startsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Fax ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Fax",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "endsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Fax contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonecontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Fax",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "contains",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Fax 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phone0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Fax",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "notequal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MobilePhone exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MobilePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "equal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MobilePhone starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonestarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MobilePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "startsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MobilePhone ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MobilePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "endsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MobilePhone contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonecontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MobilePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "contains",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "MobilePhone 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phone0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "MobilePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "notequal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "HomePhone exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "HomePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "equal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "HomePhone starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonestarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "HomePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "startsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "HomePhone ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "HomePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "endsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "HomePhone contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonecontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "HomePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "contains",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "HomePhone 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phone0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "HomePhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "notequal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPhone exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "equal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPhone starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonestarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "startsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPhone ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "endsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPhone contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonecontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "contains",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherPhone 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phone0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "notequal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantPhone exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "equal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantPhone starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonestarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "startsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantPhone ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phoneends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "endsWith",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantPhone contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phonecontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "contains",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantPhone 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "phone0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantPhone",
                                    "operator": null,
                                    "condition": {
                                        "type": "phone",
                                        "operator": "notequal",
                                        "value": "90"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Email exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "emailexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Email",
                                    "operator": null,
                                    "condition": {
                                        "type": "email",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Email starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "emailstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Email",
                                    "operator": null,
                                    "condition": {
                                        "type": "email",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Email ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "emailends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Email",
                                    "operator": null,
                                    "condition": {
                                        "type": "email",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Email contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "emailcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Email",
                                    "operator": null,
                                    "condition": {
                                        "type": "email",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Email 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "email0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Email",
                                    "operator": null,
                                    "condition": {
                                        "type": "email",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Email is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "emailis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Email",
                                    "operator": null,
                                    "condition": {
                                        "type": "email",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Title 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Title",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Department 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Department",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "AssistantName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "AssistantName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LeadSource equals",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklistequals",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LeadSource",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "picklist",
                                        "value": "Web"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LeadSource 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklist0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LeadSource",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "notpicklist",
                                        "value": "Web"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "date0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "date0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicOn",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Birthdate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Birthdate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareaexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareastarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareaends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareacontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareais blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Description 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Description",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CreatedDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CreatedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastModifiedDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastModifiedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "SystemModstamp since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "SystemModstamp",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "date0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "dateon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "date0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicOn",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastActivityDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastActivityDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "date",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCURequestDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCURequestDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastCUUpdateDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastCUUpdateDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastViewedDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastViewedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastReferencedDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastReferencedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedReason 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedReason",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "blank",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetime0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notbetween",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate within the last ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the last ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinLast",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate within the next ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin the next ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "withinNext",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimebefore",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "less",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeafter",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "greater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notgreater",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "notLess",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate on",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimeon",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "equal",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimewithin",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "between",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate dynamically equal to ",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically equal to ",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicEqual",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate dynamically before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate dynamically after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate dynamically on or before",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or before",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRBefore",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate dynamically on or after",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically on or after",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicOnRAfter",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate dynamically within",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimedynamically within",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "dynamicWithin",
                                        "start": "2017-08-31T18:30:00.000Z",
                                        "end": "2017-09-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "EmailBouncedDate since last sync",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "datetimesince last sync",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "EmailBouncedDate",
                                    "operator": null,
                                    "condition": {
                                        "type": "datetime",
                                        "operator": "sinceLastSync",
                                        "value": "2017-08-31T18:30:00.000Z"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "PhotoUrl exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "urlexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "PhotoUrl",
                                    "operator": null,
                                    "condition": {
                                        "type": "url",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "PhotoUrl starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "urlstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "PhotoUrl",
                                    "operator": null,
                                    "condition": {
                                        "type": "url",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "PhotoUrl ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "urlends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "PhotoUrl",
                                    "operator": null,
                                    "condition": {
                                        "type": "url",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "PhotoUrl contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "urlcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "PhotoUrl",
                                    "operator": null,
                                    "condition": {
                                        "type": "url",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "PhotoUrl 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "url0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "PhotoUrl",
                                    "operator": null,
                                    "condition": {
                                        "type": "url",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "PhotoUrl is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "urlis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "PhotoUrl",
                                    "operator": null,
                                    "condition": {
                                        "type": "url",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Jigsaw 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Jigsaw",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "JigsawContactId 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "JigsawContactId",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CleanStatus equals",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklistequals",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CleanStatus",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "picklist",
                                        "value": "Matched"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "CleanStatus 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklist0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "CleanStatus",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "notpicklist",
                                        "value": "Matched"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Level__c equals",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklistequals",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Level__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "picklist",
                                        "value": "Secondary"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Level__c 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklist0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Level__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "notpicklist",
                                        "value": "Secondary"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Languages__c 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Languages__c",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Salutation equals",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklistequals",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Salutation",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "picklist",
                                        "value": "Mr."
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Salutation 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "picklist0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Salutation",
                                    "operator": null,
                                    "condition": {
                                        "type": "picklist",
                                        "operator": "notpicklist",
                                        "value": "Mr."
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "FirstName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "FirstName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareaexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareastarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareaends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareacontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textareais blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherStreet 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "textarea0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherStreet",
                                    "operator": null,
                                    "condition": {
                                        "type": "textarea",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "OtherCity 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "OtherCity",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "LastName 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "LastName",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name exactly",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringexactly",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "equal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name starts with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringstarts with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "startsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name ends with",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringends with",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "endsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name contains",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringcontains",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "contains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notequal",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name is blank",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "stringis blank",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "blank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notstartsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notendsWith",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notcontains",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    },
    {
        "type": "Name 0",
        "json": {
            "json": {
                "name": "unit testing",
                "setFrequency": "Run Once",
                "sourceInstanceConfigId": "2",
                "createdAt": 1523437075,
                "updatedAt": 1523437075,
                "elements": [
                    {
                        "MasterObject": "Contact",
                        "filter": {
                            "name": "string0",
                            "type": "Filter",
                            "elements": [
                                {
                                    "type": "element",
                                    "elementType": 1,
                                    "fieldName": "Name",
                                    "operator": null,
                                    "condition": {
                                        "type": "string",
                                        "operator": "notblank",
                                        "value": "a"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "id": null
            }
        }
    }
]

before(function() {

});

//Setup the models
function testcondtion(condition){
  it(condition.type, function(done) {
    agent.get('/feederService/configure?userid=12&instanceid=b9cda3d2-78a6-47p3-bbdc-8db5ec423e07&assetid=1109&siteName=TechnologyPartnerPORTQIIPteLtd&siteId=250973722&assetName=ES_07022018_1')
    .then((res) => {
      try {
        expect(res).to.redirectTo('http://' + res.req._headers.host + '/#/feeder');
      } catch (e) {
        done(e)
      }
      agent.post('/feederService/saveSegmentJson')
       .send(condition.json)
       .then((res) => {
         try {
           expect(res).to.have.status(200);
           expect(res.body).to.be.a('object');
         } catch (e) {
           done(e)
         }
         segmentId = res.body.segmentId;
         agent.post('/feederService/getContactsCount')
          .send({"instanceConfigId":condition.json.json.sourceInstanceConfigId,"segmentID": res.body.segmentId})
          .then((res) => {
            try {
              expect(res).to.have.status(200);
              expect(res.body).to.be.a('object');
              expect(res.body.queryIssues.length).to.equal(0);
              done();
            } catch (e) {
              done(e)
            }
          },(err) => {
           done(err)
          });
       },(err) => {
         done(err);
       });
    },(err) => {
      done(err);
    });
 });
}

describe('should test each field of contact object',function(){
  for (var i = 0; i < conditions.length; i++) {
    testcondtion(conditions[i]);
  }
});
