angular.module('d3Module', [])
.service('domManipulation', function($q) {
	console.log($q)
})
.directive("multiDataTypeBar", function() {
    return {
    	scope:{
    		data:"=",
    		format:"=",
    		onDataSelect:"=",
    		options:"="
    	},
		controller:function($scope,$element,$filter) {

			$scope.options = {};

			var elementNodeWithEvents = [];


			$scope.options.render = function(rawData,format) {
				// console.log(elementNodeWithEvents);
				elementNodeWithEvents.forEach(function(item) {
					item = null;
				})
				elementNodeWithEvents = [];

				$element.empty();


				if(!format){
					format = $scope.format;
				}

				if(rawData){
					var datax = filterData(rawData);
				}
				else{
					var datax = filterData($scope.data);
				}

		    	var d3Element = d3.select($element[0]);
		    	d3Element.append('svg');
		    	var svg = $($element[0]).find('svg')[0];
		    	
		    	init(datax,svg,format);

			}

			



			$scope.options.render();

			var mouseMovementPos = {
				x:0,
				y:0,
				setX :function(actualX) {
					this.x = actualX;
				},
				setY :function(actualY) {
					this.y = actualY;
				}
			}

			$element.on('mousemove',function(e) {
				var x = e.originalEvent.layerX;
				var y = e.originalEvent.layerY;
				mouseMovementPos.setX(x);
				mouseMovementPos.setY(y);
			});



			

		    function init(d3Data,chartDom,format) {
		    	var margin = {
			            top: 20,
			            right: 0,
			            bottom: 20,
			            // left: 100
			            left: 50
			        },
		            barWidth = 50,
		            width = $(chartDom).parent().width(),//barWidth*d3Data.length + margin.left + margin.right,
		            height = 150 - margin.top - margin.bottom,
		            padding = 0.5;

				var tooltip = d3.select($element[0])
					.append("div")
					.style("position", "absolute")
					.style("width", "100px")
					.style("z-index", "10")
					.style("background", "black")
					.style("text-align", "center")
					.style("opacity", "0.7")
					.style("color", "white")
					.style("border", "0.5px solid white")
					.style("border-radius", "4px")
					.style("padding", "4px")
					.style("visibility", "hidden")
					.text("");



		        /*Tool Tip*/

		        var svg = d3.select(chartDom)
		            .attr("height", height + margin.top + margin.bottom+20)
		            .attr("width", width);

			    var chart = svg.append("g")
			        .attr("width",width)
			        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


			    var data = d3Data.map(function(d) {
			        d.date = $filter('date')(parseInt(d.timestamp*1000),format);
			        return d;
			    });

			    var x = d3.scaleBand()
			        .domain(data.map(function(d) {
			            return d.date;
			        }))
			        .range([0, width-margin.left])
			        .padding(padding);

			    var totalMax = d3.max(data.map(function(d) {
			        return d.totalCreated+d.totalUpdated+d.totalFailed;
			    }));

			    var maxDigits = (totalMax+'').length;

			    if(maxDigits>3){
			    	var increment = 3;
			    } else {
			    	var increment = (totalMax+'').length;
			    }


			    var totalAxis = Math.pow(10, increment-1);

			    if(totalMax<10)
			    	totalMax = 10;


			    var y = d3.scaleLinear()
			        .domain([0, totalMax+totalAxis])
			        .range([height, 0]);

			    var xAxis = d3.axisBottom(x)
			        .ticks(d3.timeYear);


			    var yAxis = d3.axisRight(y)
			    	.ticks(4)
			        .tickSize(width)
			        .tickFormat(function(d) {
			            return d;
			        });

			    /*y.ticks = function() {
			    	var d = y.domain();
			    	var start = d[0],
			    		stop = d[d.length-1],
			    		ticks = 3;

			    	var zeroes = Math.floor(Math.log10(stop));
			    	console.log(zeroes);
			    	if(zeroes>3){
			    		firstDigit = parseInt(stop / Math.pow(10, zeroes-1));
			    		stop = firstDigit * Math.pow(10,zeroes-1);
			    	}
			    	else{
			    		firstDigit = parseInt(stop / Math.pow(10, zeroes));
			    		console.log(firstDigit);
			    		stop = firstDigit * Math.pow(10,zeroes);
			    	}

			    	console.log(stop);

			    	eachStep = Math.floor((stop-start)/ticks);

			    	var tickArr = [];
			    	for (var i = start; i <= stop; i+=eachStep) {
			    		tickArr.push(i);
			    	}
			    	console.log(tickArr);
			    	return tickArr;
			    }*/

			    chart.append("g")
			        .attr("transform", "translate(0," + height + ")")
			        .call(customXAxis);

			    chart.append("g")
			        .call(customYAxis);

			    function customXAxis(g) {
			        g.call(xAxis);
			        g.select(".domain").remove();
			    }


			    function customYAxis(g) {
			        g.call(yAxis);
			        g.select(".domain").remove();
			        g.selectAll(".tick:not(:first-of-type) line").attr("stroke", "#e3dad4");
			        g.selectAll(".tick text")
			        .attr("x", 4)
			        .attr("dy", 3)
			        .attr("dx", -totalMax.toString().length*8);
			    }


		    
	            chart.selectAll(".bar")
	                .data(data)
	                .enter()
	                .append("rect")
	                .classed("bar-created", true)
	                .attr("x", function(d) {
	                    return x(d.date);
	                })
	                .attr("y", function(d) {
	                    return y(d.totalCreated);
	                })
	                .attr("height", function(d) {
	                    return height - y(d.totalCreated);
	                })
                	.attr("width", barWidth)
                	.on("click",function(d) {
                		if($scope.options.stopSelection)
                			return;
                		$scope.onDataSelect(d);
                	})
					.on("mouseover", function(d){
						return tooltip
						.style("visibility", "visible")
						.text("Created : "+d.totalCreated);
					})
					.on("mousemove", function(){
						return tooltip.style("top", (mouseMovementPos.y-10)+"px").style("left",(mouseMovementPos.x+30)+"px");
					})
					.on("mouseout", function(){
						return tooltip.style("visibility", "hidden");
					});


	            chart.selectAll(".bar")
	                .data(data)
	                .enter()
	                .append("rect")
	                .attr("class", "bar-updated")
	                .attr("x", function(d) {
	                    return x(d.date);
	                })
	                .attr("y", function(d) {
	                    return y(d.totalCreated+d.totalUpdated);
	                })
	                .attr("height", function(d) {
	                    return height - y(d.totalUpdated);
	                })
	                .attr("width", barWidth)
                	.on("click",function(d) {
                		if($scope.options.stopSelection)
                			return;
                		$scope.onDataSelect(d);
                	})
					.on("mouseover", function(d){
						return tooltip
						.style("visibility", "visible")
						.text("Updated : "+d.totalUpdated);
					})
					.on("mousemove", function(){
						return tooltip.style("top", (mouseMovementPos.y-10)+"px").style("left",(mouseMovementPos.x+10)+"px");
					})
					.on("mouseout", function(){
						return tooltip.style("visibility", "hidden");
					});


	            chart.selectAll(".bar")
	                .data(data)
	                .enter()
	                .append("rect")
	                .attr("class", "bar-failed")
	                .attr("x", function(d) {
	                    return x(d.date);
	                })
	                .attr("y", function(d) {

	                    return y(d.totalCreated+d.totalUpdated+d.totalFailed);
	                })
	                .attr("height", function(d) {
	                    return height - y(d.totalFailed);
	                })
	                .attr("width", barWidth)
                	.on("click",function(d) {
                		if($scope.options.stopSelection)
                			return;
                		$scope.onDataSelect(d);
                	})
					.on("mouseover", function(d){
						return tooltip
						.style("visibility", "visible")
						.text("Failed : "+d.totalFailed);
					})
					.on("mousemove", function(){
						return tooltip.style("top", (mouseMovementPos.y-10)+"px").style("left",(mouseMovementPos.x+10)+"px");
					})
					.on("mouseout", function(){
						return tooltip.style("visibility", "hidden");
					});

				elementNodeWithEvents.push(chart);
		 
		    }
		    
		    function filterData(rawData,format) {
		    	var data = [];
		    	/*for (var i = 0; i < 5; i++) {
		    		var created = Math.floor(Math.random()*1000);
		    		var updated = Math.floor(Math.random()*1000);
		    		var failed = Math.floor(Math.random()*1000);
		    		var total =	created + updated + failed;
		    		data.push({
					    "timestamp": Math.floor((Date.now()+i*24*36e5)/1000)+'',
					    "totalCreated": created,
					    "totalUpdated": updated,
					    "totalFailed": failed,
					    "total": total
		    		})
		    	}*/




		    	data = rawData.map(function(item) {
		    		item.date = $filter('date')(parseInt(item.timestamp)*1000,format);
		    		item.total = item.totalCreated+item.totalUpdated+item.totalFailed;
		    		return item;
		    	});


		    	

		    	return data;
		    }

		}
    };
})
.directive("multiDataTypeBar2", function() {
    return {
    	scope:{
    		data:"=",
    		format:"=",
    		onDataSelect:"=",
    		options:"="
    	},
		controller:function($scope,$element,$filter) {

			$scope.options = {};

			var elementNodeWithEvents = [];


			$scope.options.render = function(rawData,format) {
				// console.log(elementNodeWithEvents);
				elementNodeWithEvents.forEach(function(item) {
					item = null;
				})
				elementNodeWithEvents = [];

				$element.empty();


				if(!format){
					format = $scope.format;
				}

				if(rawData){
					var datax = filterData(rawData);
				}
				else{
					var datax = filterData($scope.data);
				}

		    	var d3Element = d3.select($element[0]);
		    	d3Element.append('svg');
		    	var svg = $($element[0]).find('svg')[0];
		    	
		    	init(datax,svg,format);

			}

			



			$scope.options.render();

			var mouseMovementPos = {
				x:0,
				y:0,
				setX :function(actualX) {
					this.x = actualX;
				},
				setY :function(actualY) {
					this.y = actualY;
				}
			}

			$element.on('mousemove',function(e) {
				var x = e.originalEvent.layerX;
				var y = e.originalEvent.layerY;
				mouseMovementPos.setX(x);
				mouseMovementPos.setY(y);
			});



			

		    function init(d3Data,chartDom,format) {
		    	var margin = {
			            top: 20,
			            right: 0,
			            bottom: 20,
			            // left: 100
			            left: 50
			        },
		            barWidth = 50,
		            width = 500,//barWidth*d3Data.length + margin.left + margin.right,
		            height = 150 - margin.top - margin.bottom,
		            padding = 0.5;

				var tooltip = d3.select($element[0])
					.append("div")
					.style("position", "absolute")
					.style("width", "100px")
					.style("z-index", "10")
					.style("background", "black")
					.style("text-align", "center")
					.style("opacity", "0.7")
					.style("color", "white")
					.style("border", "0.5px solid white")
					.style("border-radius", "4px")
					.style("padding", "4px")
					.style("visibility", "hidden")
					.text("");



		        /*Tool Tip*/

		        var svg = d3.select(chartDom)
		            .attr("height", height + margin.top + margin.bottom+20)
		            .attr("width", width);

			    var chart = svg.append("g")
			        .attr("width",width)
			        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


			    var data = d3Data.map(function(d) {
			        d.date = $filter('date')(parseInt(d.timestamp*1000),format);
			        return d;
			    });

			    var x = d3.scaleBand()
			        .domain(data.map(function(d) {
			            return d.date;
			        }))
			        .range([0, width-margin.left])
			        .padding(padding);

			    var totalMax = d3.max(data.map(function(d) {
			        return d.yesCount+d.noCount+d.erroredCount;
			    }));


			    var maxDigits = (totalMax+'').length;

			    if(maxDigits>3){
			    	var increment = 3;
			    } else {
			    	var increment = (totalMax+'').length;
			    }


			    var totalAxis = Math.pow(10, increment-1);

			    if(totalMax<10)
			    	totalMax = 10;


			    var y = d3.scaleLinear()
			        .domain([0, totalMax+totalAxis])
			        .range([height, 0]);

			    var xAxis = d3.axisBottom(x)
			        .ticks(d3.timeYear);


			    var yAxis = d3.axisRight(y)
			    	.ticks(4)
			        .tickSize(width)
			        .tickFormat(function(d) {
			            return d;
			        });

			    /*y.ticks = function() {
			    	var d = y.domain();
			    	var start = d[0],
			    		stop = d[d.length-1],
			    		ticks = 3;

			    	var zeroes = Math.floor(Math.log10(stop));
			    	console.log(zeroes);
			    	if(zeroes>3){
			    		firstDigit = parseInt(stop / Math.pow(10, zeroes-1));
			    		stop = firstDigit * Math.pow(10,zeroes-1);
			    	}
			    	else{
			    		firstDigit = parseInt(stop / Math.pow(10, zeroes));
			    		console.log(firstDigit);
			    		stop = firstDigit * Math.pow(10,zeroes);
			    	}

			    	console.log(stop);

			    	eachStep = Math.floor((stop-start)/ticks);

			    	var tickArr = [];
			    	for (var i = start; i <= stop; i+=eachStep) {
			    		tickArr.push(i);
			    	}
			    	console.log(tickArr);
			    	return tickArr;
			    }*/

			    chart.append("g")
			        .attr("transform", "translate(0," + height + ")")
			        .call(customXAxis);

			    chart.append("g")
			        .call(customYAxis);

			    function customXAxis(g) {
			        g.call(xAxis);
			        g.select(".domain").remove();
			    }


			    function customYAxis(g) {
			        g.call(yAxis);
			        g.select(".domain").remove();
			        g.selectAll(".tick:not(:first-of-type) line").attr("stroke", "#e3dad4");
			        g.selectAll(".tick text")
			        .attr("x", 4)
			        .attr("dy", 3)
			        .attr("dx", -totalMax.toString().length*8);
			    }


		    
	            chart.selectAll(".bar")
	                .data(data)
	                .enter()
	                .append("rect")
	                .classed("bar-created", true)
	                .attr("x", function(d) {
	                    return x(d.date);
	                })
	                .attr("y", function(d) {
	                    return y(d.yesCount);
	                })
	                .attr("height", function(d) {
	                    return height - y(d.yesCount);
	                })
                	.attr("width", barWidth)
                	.on("click",function(d) {
                		if($scope.options.stopSelection)
                			return;
                		$scope.onDataSelect(d);
                	})
					.on("mouseover", function(d){
						return tooltip
						.style("visibility", "visible")
						.text("Created : "+d.yesCount);
					})
					.on("mousemove", function(){
						return tooltip.style("top", (mouseMovementPos.y-10)+"px").style("left",(mouseMovementPos.x+30)+"px");
					})
					.on("mouseout", function(){
						return tooltip.style("visibility", "hidden");
					});


	            chart.selectAll(".bar")
	                .data(data)
	                .enter()
	                .append("rect")
	                .attr("class", "bar-updated")
	                .attr("x", function(d) {
	                    return x(d.date);
	                })
	                .attr("y", function(d) {
	                    return y(d.yesCount+d.noCount);
	                })
	                .attr("height", function(d) {
	                    return height - y(d.noCount);
	                })
	                .attr("width", barWidth)
                	.on("click",function(d) {
                		if($scope.options.stopSelection)
                			return;
                		$scope.onDataSelect(d);
                	})
					.on("mouseover", function(d){
						return tooltip
						.style("visibility", "visible")
						.text("Updated : "+d.noCount);
					})
					.on("mousemove", function(){
						return tooltip.style("top", (mouseMovementPos.y-10)+"px").style("left",(mouseMovementPos.x+10)+"px");
					})
					.on("mouseout", function(){
						return tooltip.style("visibility", "hidden");
					});


	            chart.selectAll(".bar")
	                .data(data)
	                .enter()
	                .append("rect")
	                .attr("class", "bar-failed")
	                .attr("x", function(d) {
	                    return x(d.date);
	                })
	                .attr("y", function(d) {

	                    return y(d.yesCount+d.noCount+d.erroredCount);
	                })
	                .attr("height", function(d) {
	                    return height - y(d.erroredCount);
	                })
	                .attr("width", barWidth)
                	.on("click",function(d) {
                		if($scope.options.stopSelection)
                			return;
                		$scope.onDataSelect(d);
                	})
					.on("mouseover", function(d){
						return tooltip
						.style("visibility", "visible")
						.text("Failed : "+d.erroredCount);
					})
					.on("mousemove", function(){
						return tooltip.style("top", (mouseMovementPos.y-10)+"px").style("left",(mouseMovementPos.x+10)+"px");
					})
					.on("mouseout", function(){
						return tooltip.style("visibility", "hidden");
					});

				elementNodeWithEvents.push(chart);
		 
		    }
		    
		    function filterData(rawData,format) {
		    	var data = [];
		    	/*for (var i = 0; i < 5; i++) {
		    		var created = Math.floor(Math.random()*1000);
		    		var updated = Math.floor(Math.random()*1000);
		    		var failed = Math.floor(Math.random()*1000);
		    		var total =	created + updated + failed;
		    		data.push({
					    "timestamp": Math.floor((Date.now()+i*24*36e5)/1000)+'',
					    "totalCreated": created,
					    "totalUpdated": updated,
					    "totalFailed": failed,
					    "total": total
		    		})
		    	}*/


		    	data = rawData.map(function(item) {
		    		item.date = $filter('date')(parseInt(item.timestamp)*1000,format);
		    		item.total = item.yesCount+item.noCount+item.erroredCount;
		    		return item;
		    	});

		    	

		    	return data;
		    }

		}
    };
});