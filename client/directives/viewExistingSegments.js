segmentationApp
.directive("viewExistingSegmentsModal", function() {
    return {
        restrict : "A",
        templateUrl : './directiveTemplates/viewExistingSegments.html'
    };
})
.controller('viewExistingSegmentsCtrl', function($scope, $uibModalInstance,$http,$timeout,$filter,ExistingSegments,$filter,ExistingSegmentID) {
	var ExistingSegmentsView = $scope.ExistingSegmentsView = {};
    var ngSort = $filter('orderBy');

	ExistingSegmentsView.init = function() {
		ExistingSegments.data = _.sortBy(ExistingSegments.data,['segmentName'])
		ExistingSegmentsView.ExistingSegments = _.reject(ExistingSegments.data,{'segmentId':ExistingSegmentID});

        ExistingSegmentsView.selSegmentIndex = -1;
        ExistingSegmentsView.selSegment = {};

	}
	ExistingSegmentsView.init();

	ExistingSegmentsView.useSegment = function() {
        $uibModalInstance.close(ExistingSegmentsView.selSegment);
	}

	var promise;
    ExistingSegmentsView.search = function() {
    	if(promise)
    		$timeout.cancel(promise);

    	promise = $timeout(function() {
    		ExistingSegmentsView.ExistingSegments = $filter('filter')(ExistingSegments.data,ExistingSegmentsView.searchText);
    	}, 500);
    }

    ExistingSegmentsView.selectSegment = function(index,selSegment) {
        if(ExistingSegmentsView.selSegmentIndex==index){
            ExistingSegmentsView.selSegmentIndex = -1;
            ExistingSegmentsView.selSegment = {};
        }else{
            ExistingSegmentsView.selSegmentIndex = index;
            ExistingSegmentsView.selSegment = selSegment;
        }
    }
    ExistingSegmentsView.saveDis = function() {
        var status = !ExistingSegmentsView.ExistingSegments.length;
        status = status || ExistingSegmentsView.selSegmentIndex == -1;
        return status;
    }

    var selectedParam="segmentName";
    ExistingSegmentsView.sort = function(param) {
        if(selectedParam.replace('-','')==param){
            if(selectedParam.indexOf('-')==-1){
                selectedParam = '-'+param;
            } else{
                selectedParam = param;
            }
        } else {
            selectedParam = param;
        }

        ExistingSegments.data = ngSort(ExistingSegments.data,selectedParam);
        ExistingSegmentsView.ExistingSegments = ngSort(ExistingSegmentsView.ExistingSegments,selectedParam);
    }
    ExistingSegmentsView.icon = function(param) {
        if(selectedParam.replace('-','')==param){
            if(selectedParam.indexOf('-')==-1){
                return ' glyphicon-triangle-bottom';
            } else{
                return ' glyphicon-triangle-top';
            }
        }else{
            return '';
        }
    }


    ExistingSegmentsView.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
