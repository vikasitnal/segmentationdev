segmentationApp
.directive("viewSyncReportsModal", function() {
    return {
        restrict : "A",
        templateUrl : './directiveTemplates/viewSyncReports.html'
    };
})
.controller('viewSyncReportsCtrl', function($scope, $uibModalInstance,$http,$timeout,$filter,apiCalls) {
    var SyncReports = $scope.SyncReports = {};
    var weekStartEnd = {
        start:null,
        end:null
    }

    SyncReports.disableNextKey = true;



    var getPrevDateBy = function(differenceFrom,differenceBy) {
        var diffTime = new Date(new Date(differenceFrom)-differenceBy*8.64e7).getTime();
        return new Date(diffTime);
    }


    SyncReports.updateThisWeek = function(next) {
        if(!weekStartEnd.end){
            weekStartEnd.end = new Date();
        } else {
            if(next)
                weekStartEnd.end = getPrevDateBy(weekStartEnd.end,-5);
            else
                weekStartEnd.end = getPrevDateBy(weekStartEnd.start,1);
        }


        if(getPrevDateBy(weekStartEnd.end,-1) >= new Date()){
            SyncReports.disableNextKey = true;
        }else {
            SyncReports.disableNextKey = false;
        }

        weekStartEnd.start = getPrevDateBy(weekStartEnd.end,4);
        SyncReports.thisWeek = $filter('suffixedDate')(weekStartEnd.start)+" - "+$filter('suffixedDate')(weekStartEnd.end);
    }

    SyncReports.prevWeekExecution = function() {
        SyncReports.updateThisWeek(false);
        SyncReports.renderDateCategorizedExecutions(false);
        SyncReports.barChartOptions.stopSelection = false;
    }

    SyncReports.nextWeekExecution = function() {
        SyncReports.updateThisWeek(true);
        SyncReports.renderDateCategorizedExecutions(false);
        SyncReports.barChartOptions.stopSelection = false;
    }


    SyncReports.init = function() {
        apiCalls.getExecutionNumbers()
        .then(function(result) {
            SyncReports.executionNumbers = result.data;
        });

    	apiCalls.getExecutions()
    	.then(function(result) {
            SyncReports.executions = result.data;
            SyncReports.renderDateCategorizedExecutions();
    	});

        apiCalls.getRecentImports()
        .then(function(result) {
            SyncReports.recentImports = result.data;
        })

        apiCalls.getRecentActions()
        .then(function(result) {
            SyncReports.recentActions = result.data;
        })

        apiCalls.getRecentErrors()
        .then(function(result) {
            SyncReports.recentErrors = result.data;
        });

        SyncReports.updateThisWeek();

    }
    SyncReports.init();
    
    SyncReports.close = function() {
        $uibModalInstance.dismiss('cancel');
    };

    SyncReports.renderDateCategorizedExecutions = function(next) {

        SyncReports.categoryDateData = [];
        var weekCategorizedExecutions = [];


        SyncReports.dateFormat = 'mediumDate';
        var dateCtgData = _.groupBy(SyncReports.executions,function(item) {
            dateCtgKey = $filter('date')(parseInt(item.timestamp*1000),SyncReports.dateFormat);
            return dateCtgKey;
        });


        
        for (var i = 0,j = 4; i < 5; i++,--j) {
            var dateAsKey = $filter('date')(getPrevDateBy(new Date(weekStartEnd.end),j),SyncReports.dateFormat);


            var newItem = {
                timestamp: Math.floor((new Date(dateAsKey)).getTime()/1000)+'',
                totalCreated:0,
                totalFailed:0,
                totalImported:0,
                totalUpdated:0,
                totalWarning:0
            }

            if(dateCtgData[dateAsKey]){
                dateCtgData[dateAsKey].forEach(function(item) {
                    newItem.totalCreated = newItem.totalCreated + item.totalCreated;
                    newItem.totalFailed = newItem.totalFailed + item.totalFailed;
                    newItem.totalImported = newItem.totalImported + item.totalImported;
                    newItem.totalUpdated = newItem.totalUpdated + item.totalUpdated;
                    newItem.totalWarning = newItem.totalWarning + item.totalWarning;
                });
            }

            weekCategorizedExecutions[i] = newItem;

        }

        SyncReports.categoryDateData = weekCategorizedExecutions;
        
        if(SyncReports.barChartOptions)
            SyncReports.barChartOptions.render(SyncReports.categoryDateData,SyncReports.dateFormat);

    }



    SyncReports.renderTimeCategorizedExecutions = function(data) {

        var dateCtgData = _.groupBy(SyncReports.executions,function(item) {
            dateCtgKey = $filter('date')(parseInt(item.timestamp*1000),SyncReports.dateFormat);
            return dateCtgKey;
        });


        SyncReports.categoryDateData = [];
        SyncReports.dateFormat = 'short';
        SyncReports.categoryDateData = dateCtgData[data.date].slice(0, 5);

        _.reverse(SyncReports.categoryDateData);

        SyncReports.barChartOptions.render(SyncReports.categoryDateData,SyncReports.dateFormat);
        SyncReports.barChartOptions.stopSelection = true;

    }
});
