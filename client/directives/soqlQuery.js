segmentationApp
    .directive("soqlEditor", function() {
        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            scope:{
                ngModel:"=",
                ngCursorPos:"="
            },
            link: function($scope, elem, attr) {
                elem.on('keyup blur',function(ev) {
                    var el = ev.target;
                    if('selectionStart' in el) {
                        pos = el.selectionStart;
                    } else if('selection' in document) {
                        el.focus();
                        var Sel = document.selection.createRange();
                        var SelLength = document.selection.createRange().text.length;
                        Sel.moveStart('character', -el.value.length);
                        pos = Sel.text.length - SelLength;
                    }
                    $scope.ngCursorPos = pos;
                });
            }
        };
    })
    .directive("querySoql", function() {
        return {
            restrict: "A",
            templateUrl: './directiveTemplates/querySoql.html',
            scope: {
                object: "=",
            },
            controller: function($scope, queryRecSer, $element, $timeout, $filter, apiCalls,$common) {

                var SoqlQuery = $scope.SoqlQuery = {};

                $scope.$watch('object',function() {
                    SoqlQuery.object = $scope.object;
                    if(!SoqlQuery.object.query)
                        SoqlQuery.object.query = '';
                });

                /*SoqlQuery.onInitObjects = function() {

                    SoqlQuery.object.selObjSOQLFields = null;

                    apiCalls.getSOQLObjectFields(SoqlQuery.object.selObject)
                    .then(function(result) {
                        SoqlQuery.object.selObjSOQLFields = result.data;
                    });
                }*/


                var prChildBoxToggle = false,isInsidePopover = false;

                function toggleOnDocument() {
                    if(isInsidePopover){
                        isInsidePopover = false;
                    } else {
                        SoqlQuery.popPrChBox();
                    }
                }

                function actionInBox() {
                    isInsidePopover = true;
                }

                SoqlQuery.popPrChBox = function(ev) {
                    if(ev){
                        var pos = $(ev.target).position();
                        prChildBoxToggle = !prChildBoxToggle;
                        $('#prChildBox').css({
                            top:(pos.top*0-8)+"px",
                            left:(pos.left*0+16)+"px",
                            display:(prChildBoxToggle)?'block':'none'
                        });

                        setTimeout(function() {
                            $(document).on('click',toggleOnDocument);
                            $('#prChildBox').on('click',actionInBox);
                        }, 10);

                    } else {
                        prChildBoxToggle = !prChildBoxToggle;
                        $('#prChildBox').css({
                            display:(prChildBoxToggle)?'block':'none'
                        });
                        $(document).off('click',toggleOnDocument);
                        $('#prChildBox').off('click',actionInBox);
                    }

                }


                SoqlQuery.setParentData = function(data) {
                    if (!data.input) {
                        var parents = [],childs = [],combo = [];
                        data.onSuccess([{
                            "custom": false,
                            "name": SoqlQuery.object.selObject.Object_Id,
                            "label": SoqlQuery.object.selObject.Object_Name,
                            "referenceTo": SoqlQuery.object.selObject.Object_Id,
                            "type": "reference",
                            "instanceConfigId": SoqlQuery.object.selObject.Source_Instance_Config_Id,
                            "relationshipName": SoqlQuery.object.selObject.Object_Id,
                            "fieldToUse": SoqlQuery.object.selObject.Object_Id,
                            "objectType": "parent"
                        }]);


                        /*apiCalls.adminGetParents({
                                "instanceConfigId": SoqlQuery.object.selObject.Source_Instance_Config_Id,
                                "objectName": SoqlQuery.object.selObject.Object_Id
                            })
                            .then(function(result) {
                                parents = result.data;
                                console.log(parents);

                                return apiCalls.adminGetChilds({
                                    "instanceConfigId": SoqlQuery.object.selObject.Source_Instance_Config_Id,
                                    "objectName": SoqlQuery.object.selObject.Object_Id
                                })
                            })                        
                            .then(function(result) {

                                childs = result.data;
                                childs = _.sortBy(childs, ['childSObject']);
                                childs = _.uniqBy(childs, 'childSObject');
                                combo = parents.concat(childs);


                                data.onSuccess(combo);
                            });*/
                    } else {
                        var parents = [],childs = [],combo = [];

                        if(data.input.node.objectType == 'child'){
                            data.input.objectName = data.input.node.objectName;
                        }


                        apiCalls.adminGetParents({
                                "instanceConfigId": data.input.instanceConfigId,
                                "objectName": data.input.objectName
                            })
                            .then(function(result) {
                                parents = result.data;
                                parents = _.sortBy(parents, ['referenceTo']);
                                return apiCalls.adminGetChilds({
                                    "instanceConfigId": data.input.instanceConfigId,
                                    "objectName": data.input.objectName
                                });
                            })
                            .then(function(result) {

                                childs = result.data;
                                childs = _.sortBy(childs, ['childSObject']);
                                childs = _.uniqBy(childs, 'childSObject');
                                /*if(data.input.node.objectType == 'child'){
                                    combo = childs;
                                } else {

                                    combo = parents.concat(childs);
                                }*/
                                combo = parents.concat(childs);

                                data.onSuccess(combo);
                            })
                    }
                }

                SoqlQuery.getObjectParentFields = function(selNode) {
                    SoqlQuery.object.selObjSOQLFields = [];

                    var objectName = (selNode.referenceTo)?selNode.referenceTo:selNode.childSObject;

                    apiCalls.getSOQLObjectFields({
                        Source_Instance_Config_Id: selNode.instanceConfigId,
                        Object_Id: objectName
                    })
                    .then(function(result) {
                        SoqlQuery.object.selObjSOQLFields = result.data;
                    })

                }

                /*SoqlQuery.setDefaultObject = function() {
                    SoqlQuery.selObjectFormat = SoqlQuery.object.selObject.Object_Name;
                    SoqlQuery.onInitObjects();
                }*/



                // console.log(SoqlQuery);
                /*SoqlQuery.queryChange = function(ev) {
                    var el = ev.target;
                    if('selectionStart' in el) {
                        pos = el.selectionStart;
                    } else if('selection' in document) {
                        el.focus();
                        var Sel = document.selection.createRange();
                        var SelLength = document.selection.createRange().text.length;
                        Sel.moveStart('character', -el.value.length);
                        pos = Sel.text.length - SelLength;
                    };

                    SoqlQuery.cursorPosition = pos;
                }*/

                SoqlQuery.validateSOQL = function(element) {
                    var payload = element;
                    payload.Object_Name = element.selObject.Object_Name;
                    payload.Source_Instance_Config_Id = element.selExtSys.Source_Instance_Config_Id;
                    apiCalls.validateSOQL(payload).then(function(res) {
                        element.validated = true;
                        element.valid = res.data.result;
                        SoqlQuery.errorMsg = res.data.msg;
                    }, function(err) {
                        element.validated = true;
                        element.valid = false;
                    });
                }



                SoqlQuery.appendToQuery = function() {

                    SoqlQuery.popPrChBox();


                    $timeout(function() {
                        if(SoqlQuery.cursorPosition && SoqlQuery.selObjectFormat){
                            if(SoqlQuery.appendableField){
                                SoqlQuery.object.query = SoqlQuery.object.query + '  ';
                                SoqlQuery.object.query = $common.insertStringAtPosition(SoqlQuery.object.query,
                                    '  ' + SoqlQuery.selObjectFormat+"."+SoqlQuery.appendableField.name,
                                    SoqlQuery.cursorPosition);


                                SoqlQuery.appendableField = null;
                            } else {
                                SoqlQuery.object.query = SoqlQuery.object.query + '  ';
                                SoqlQuery.object.query = $common.insertStringAtPosition(SoqlQuery.object.query, '  ' + SoqlQuery.selObjectFormat,SoqlQuery.cursorPosition);
                            }
                        } else {
                            if(SoqlQuery.appendableField){
                                SoqlQuery.object.query = '  ' + SoqlQuery.object.query + '  ' + SoqlQuery.selObjectFormat+"."+SoqlQuery.appendableField.name;
                                SoqlQuery.appendableField = null;
                            } else {
                                SoqlQuery.object.query = '  ' + SoqlQuery.object.query + '  ' + SoqlQuery.selObjectFormat;
                            }
                        }
                    }, 100);
                }

                SoqlQuery.appendLastSync = function(typeNum) {
                    $timeout(function() {
                        if(typeNum === 1){
                            if(SoqlQuery.cursorPosition)
                                SoqlQuery.object.query = $common.insertStringAtPosition(SoqlQuery.object.query,' (sinceLastSync) ',SoqlQuery.cursorPosition);
                            else
                                SoqlQuery.object.query = '  ' + SoqlQuery.object.query + '  ' + ' (sinceLastSync) ';
                        } else if(typeNum === 2) {
                            if(SoqlQuery.cursorPosition)
                                SoqlQuery.object.query = $common.insertStringAtPosition(SoqlQuery.object.query,' (sinceLastSync{date}) ',SoqlQuery.cursorPosition);
                            else
                                SoqlQuery.object.query = '  ' + SoqlQuery.object.query + '  ' + ' (sinceLastSync{date}) ';
                        }


                    }, 100);
                }
            }
        };
    });