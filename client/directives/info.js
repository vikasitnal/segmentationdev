segmentationApp
    .directive("informationModal", function() {
        return {
            restrict: "A",
            templateUrl: './directiveTemplates/informationModal.html',
            controller: function($rootScope,$popup,$timeout) {
                $("#informationModal").on('show.bs.modal', function () {
                    angular.extend($rootScope.POPUP,$popup.getInfo());
                    $timeout(angular.noop,100);
                });
            }
        };
    })
    .directive("closeAlert", function($rootScope) {
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                element.on('click', function(event) {
                    $rootScope.Alert = {};
                    $rootScope.Alert.messageStatus = false;
                    $rootScope.$digest();
                })
            }
        };
    })
    .directive('maskedPasswordField', ['$timeout', function($timeout) {
        return {
            restrict: "A",
            compile: function(tElement, tAttrs) {
                return function(scope, element, attrs) {

                    var couponCodePattern = "([0-9]|[a-z])";
                    // I handle key events
                    element.bind("keypress", function(event) {
                        //reseting server error if any.
                        var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                        // Allow: Ctrl+A
                        if ((keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) ||
                            // Allow: Ctrl+C
                            (keyCode == 67 && (event.ctrlKey === true || event.metaKey === true)) ||
                            // Allow: Ctrl+X
                            (keyCode == 88 && (event.ctrlKey === true || event.metaKey === true)) ||
                            // Allow: home, end, left, right
                            (keyCode >= 35 && keyCode <= 39) ||
                            //Allow : Backspace & Delete
                            (keyCode == 8 || keyCode == 46)) {
                            // let it happen, don't do anything
                            return;
                        }


                        var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.

                        // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                        if (!keyCodeChar.match(new RegExp(couponCodePattern, "i"))) {
                            event.preventDefault();
                            return false;
                        }

                    });
                    //Change event on copy paste restrict the special character.
                    element.bind('change', function(event) {
                        element.attr("data-input", element.val());
                        $timeout(function() {
                            element.attr("type", "password");
                        },800);
                    });
                    element.bind('keyup', function(event) {

                        element.attr("type", "text");
                        $timeout(function() {
                            element.attr("type", "password");
                        }, 500)
                        var key = event.keyCode;
                        // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                        // This lets us support copy and paste too
                        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40))
                            return
                        //  $browser.defer(listener) // Have to do this or changes don't get picked up properly
                    });



                };
            }
        };
    }]);