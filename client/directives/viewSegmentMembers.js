segmentationApp
.directive("viewContactsModal", function() {
    return {
        restrict : "A",
        templateUrl : './directiveTemplates/viewSegmentMembers.html'
    };
})
.controller('viewSegmentMembersCtrl', function($scope, $uibModalInstance,$http,$timeout,$filter,SegmentData,SegmentRecords,SegmentColumns,$popup) {

	var SegmentMembers = $scope.SegmentMembers = {};

	var setRowSelectionEvent = function() {
		setTimeout(function() {

			$('.modal-view-segment .ui-grid-canvas .ui-grid-row')
			.off('click')
			.on('click',function rowSelection() {
				$('.modal-view-segment .ui-grid-canvas .ui-grid-row')
					.find('.ui-grid-cell').
					removeClass('row-selected');

				$(this).find('.ui-grid-cell')
				.addClass('row-selected');
			});
		}, 100);
	}

	SegmentMembers.init = function() {

		SegmentMembers.name = SegmentData.name;
		if(SegmentRecords.data.records.issues && SegmentRecords.data.records.issues.length){
			$popup.show({
	            header:"SOQL Error",
	            message:SegmentRecords.data.records.issues[0],
	            okText:'OK'
	        })
	        .onOk(function() {

	        })
	        .onCancel(function() {

	        })
		}

		
		SegmentMembers.wholeSetData = SegmentRecords.data.records.dataResult;
		

		if(SegmentMembers.wholeSetData.length){

			SegmentMembers.gridOptions = {
				enableSorting: true,
				enableColumnResizing: false,
				rowHeight:22,
				enableRowSelection: false,
				columnDefs:[],
				onRegisterApi: function(gridApi){
					setTimeout(function() {
						gridApi.grid.refresh();
					}, 500);
				}
			};
			
			var keySetSFDC = Object.keys(SegmentMembers.wholeSetData[0]),
				keySetCustom = SegmentColumns.data;

			var noOfCols = (keySetSFDC.length<keySetCustom.length)?keySetSFDC.length:keySetCustom.length;

			for (var i = 0; i < noOfCols; i++) {
				SegmentMembers.gridOptions.columnDefs[i] = {
					name:keySetCustom[i].sourceFieldName,
					field:keySetSFDC[i],
					minWidth:300,
					cellFilter:'conditionalDate'
				}
			}


			
			SegmentMembers.gridOptions.data = SegmentMembers.wholeSetData;
			setRowSelectionEvent();
		}
		
	}

	SegmentMembers.init();

    SegmentMembers.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
    var promise;
    SegmentMembers.search = function() {

    	if(promise)
    		$timeout.cancel(promise);

    	promise = $timeout(function() {
    		SegmentMembers.gridOptions.data = $filter('filter')(SegmentMembers.wholeSetData,SegmentMembers.searchItem);
    		setRowSelectionEvent();
    	}, 500);
    }
});
