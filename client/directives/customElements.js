segmentationApp
.directive("feederDropdown", function() {
    return {
        restrict : "A",
        scope:{
        	fdModel:"=",
            array:"=",
            filters:"="
        },
        controller:function($scope,$element,$timeout) {
            var fdModelClick = function() {
                var self = this;
                $timeout(function() {
                    $scope.fdModel = $(self).scope()[self.attributes.sel.value];
                }, 0);
            }

            var addEvents = function() {
                $element.find('ul>li').off('click',fdModelClick).click(fdModelClick);
            }

            $scope.$watch('array.length',function(newValue) {
                if(newValue){
                    addEvents();
                }
            });


        }
    };
})
.directive("selectable", function() {
    return {
        restrict : "C",
        controller:function($element) {
            $element.on('focus',function() {
                $(this).select();
            });
        }
    };
})
.directive("filterItem", function() {
    return {
        restrict : "C",
        controller:function($element) {
            $element.on('dblclick', ".title", function(e) {
                $(this).next('input').focus();
            });
        }
    };
})
.directive("closeCtrl", function($timeout) {
    return {
        restrict : "C",
        scope:{
            closeModel:"=",
            onClose:"&"
        },
        controller:function($element,$scope) {
            setTimeout(function() {
                var inputWidth = $element.outerWidth();
                var spanWidth  = $element.siblings('span').outerWidth();
                var totalWidth = inputWidth+spanWidth;

                if($element.siblings('span').hasClass('pull-left'))
                    totalWidth -= 3;
                if($element.attr('unit'))
                    $element.siblings('ul').css('width',totalWidth+$element.attr('unit'));
                else
                    $element.siblings('ul').css('width',totalWidth+'px');

            }, 100);
            $element.siblings('span').on('click', function(e) {
                $timeout(function() {
                    $scope.closeModel = null;
                    $scope.onClose();
                }, 0);
            });
        }
    };
})
.directive("queryDragger", function(queryRecSer) {
    var drake = dragula({
        copy:function(el,source) {
            return true;
        },
        accepts:function(el,target) {
            if($(el).hasClass('non-draggable')){
                return false;
            }
            if($(target).hasClass('query-dropped-grouped-area')){
                return false;
            }
            return true;
        }
    });

    var dropFunction,
        currentDraggedProp;

    drake
    /*.on('drag',function(el,target) {
        queryRecSer.onDragStart(parseInt($(el).attr('index')),el);
    })*/
    .on('over',function(el,target) {
        if($(target).hasClass('query-dropped-grouped-area')){
            $('.query-dropped-empty-area,.query-dropped-loaded-area')
                .removeClass('dropper-indicator');

            return;
        }

        if($(target).hasClass('query-dropped-empty-area')){

            $('.query-dropped-empty-area,.query-dropped-loaded-area')
                .removeClass('dropper-indicator');

            $(target)
                .addClass('dropper-indicator');
        }
        if($(target).hasClass('query-dropped-loaded-area')){

            $('.query-dropped-empty-area,.query-dropped-loaded-area')
                .removeClass('dropper-indicator');

            $(target)
                .addClass('dropper-indicator');
        }
    })
    .on('drop', function (el,target) {

        if($(target).hasClass('query-dropped-grouped-area')){
            return;
        }

        if($(target).hasClass('query-dropped-empty-area')){
            queryRecSer.onDropEnd(parseInt($(el).attr('index')));
        }

        if($(target).hasClass('query-dropped-loaded-area')){
            queryRecSer.onDropEnd(parseInt($(el).attr('index'))
                ,parseInt($(target).attr('query-index')));
        }

        $('.query-dropped-empty-area,.query-dropped-loaded-area')
            .removeClass('dropper-indicator');

        $(target)
            .find('li.grid-query-li')
            .remove();
    })

    return {
        restrict : "A",
        scope:{

        },
        controller:function($scope,$element) {
            // drake.containers.push($element[0]);
        }
    };
})


/***Tree Admin****/
.directive("userParentControl", function() {
    var commonProp = {};
    return {
        restrict : "A",
        controller:function($scope,$element,$timeout) {
            this.prop = commonProp;
        }
    };
})
.directive("userParentFormat", function($timeout) {
    return {
        require  : 'userParentControl',
        restrict : "C",
        scope:{
            modelValue:"=ngModel",
            settingData:"=",
            onSelectingNode:"=",
            initValue:'@initValue'
        },
        link:function($scope,$element,attr,userParentController) {
            $scope.modelValue = $scope.initValue;
            var validValue = $scope.modelValue;
            $element.on('keydown',function() {
                $timeout(function() {
                    $scope.modelValue=validValue;
                }, 0);
            });
            $element.on('focus blur',function(event) {
                var state;
                if(event.type === 'focus')
                    state = true;
                else if(event.type === 'blur')
                    state = false;
                userParentController.prop.onFocus(state,event);
            });
            userParentController.prop.onSelectNode = function(value,node) {
                $timeout(function() {
                    $scope.modelValue = validValue = $scope.initValue+value;
                    $scope.onSelectingNode(node);
                }, 0);
            }

            userParentController.prop.settingData = function(data) {
                $scope.settingData(data);
            }
        }
    };
})
.directive("userParentStruct", function($timeout,$document) {
    return {
        require  : 'userParentControl',
        restrict : "C",
        link:function($scope,$element,attr,userParentController) {
            $element.hide();
            var isOnDiv = false,
                currentJSTree;
            function parentStrMEnter(){
                isOnDiv=true;
                setTimeout(function() {
                    $document.on('click',dropDownBlurred)
                }, 0);
            }
            function parentStrMLeave(){
                isOnDiv=false;
            }
            function dropDownBlurred(event) {
                if(!isOnDiv){
                    var status = $(event.target).hasClass('user-parent-format');
                    if(!status){
                        $element.removeAttr('style');
                        $element.hide();
                        $document.off('click',dropDownBlurred)
                    }
                }
            }

            function jsTreeInit() {
                var data,callback;
                userParentController.prop.settingData({
                    input:null,
                    onSuccess:function(result) {
                        result.forEach(function(item) {
                            data.push({
                                id:"node-" + (Math.round(Math.random() * 10e12)),
                                parent:"#",
                                text:item.referenceTo.replace("__c", "__r") +" ("+item.name+") ",
                                prop:item,
                                data:item.referenceTo,
                                state:{ loaded:false }
                            });
                        });
                        callback.call(this, data);
                    }
                });

                function getParentData(id) {
                    var node = _.find(data,{id:id});
                    userParentController.prop.settingData({
                        input:{
                            instanceConfigId:node.prop.instanceConfigId,
                            objectName:node.prop.referenceTo
                        },
                        onSuccess:function(result) {
                            data.forEach(function(item) {
                                if(item.id == id){
                                    item.state = {
                                        opened:true
                                    }
                                }
                            });
                            if(!_.filter(data, {
                                            parent: id
                                        }).length){
                                var newData = [];
                                result.forEach(function(item) {
                                    item.level = (node.prop.level) ? node.prop.level + 1 : 2;
                                    newData.push({
                                        id:"node-" + (Math.round(Math.random() * 10e12)),
                                        parent:id,
                                        text:item.referenceTo.replace("__c", "__r") +" ("+item.name+") ",
                                        prop:item,
                                        data:item.referenceTo,
                                        state:{ loaded:(node.prop.level >= 3) }
                                    });
                                });
                                data = data.concat(newData);
                                callback.call(this, data);
                            }else{
                                callback.call(this, data);
                            }
                        }
                    });
                }

                var relationString="";
                function constructParentRelation(node){
                    if(node.parent != '#'){
                        var parentNode = _.find(data,{id:node.parent});
                        var currentNode = _.find(data,{id:node.id});
                        return constructParentRelation(parentNode) + "." + currentNode.prop.fieldToUse;
                    } else {
                        var currentNode = _.find(data,{id:node.id});
                        return relationString + "." + currentNode.prop.fieldToUse;
                    }
                }



                currentJSTree = $element.find('.admin-tree-body').jstree({
                    core: {
                        multiple: false,
                        animation: 0,
                        dblclick_toggle: false,
                        themes:{
                            icons:false
                        },
                        data : function(obj, cb) {
                            data = [];
                            callback = cb;
                            callback.call(this, data);
                        }
                    }
                });

                currentJSTree
                .on("changed.jstree", function(e, datum) {
                    var string = constructParentRelation(datum.node);
                    var selectedNode = _.find(data,{id:datum.node.id});
                    userParentController.prop.onSelectNode(string,selectedNode.prop);
                })
                .off("click.jstree", ".jstree-ocl")
                .on("click.jstree", ".jstree-ocl", $.proxy(function(e) {
                    var jstreeNode = $(this).closest('.jstree-node');

                    var expandedTreeItemID = jstreeNode.attr('id');
                    var state = _.filter(data, {
                        id: expandedTreeItemID
                    })[0].state;

                    if (jstreeNode.hasClass('jstree-closed')) {
                        jstreeNode.addClass('jstree-loading');
                        getParentData(expandedTreeItemID);
                    } else {
                        jstreeNode.addClass('jstree-closed').removeClass('jstree-open');
                        jstreeNode.find('.jstree-children').hide();
                        state.opened = false;
                    }
                }));
            }


            $element
            .off('mouseenter',parentStrMEnter)
            .off("mouseleave",parentStrMLeave)
            .on("mouseenter",parentStrMEnter)
            .on("mouseleave",parentStrMLeave);

            userParentController.prop.onFocus = function(state,event) {
                event.stopPropagation();
                var style = {};
                var selElem = $(event.currentTarget);


                style.position = 'absolute';
                style.top = (selElem.position().top + selElem.outerHeight() + 2) + 'px';
                style.left = selElem.position().left + 'px';
                style.width = selElem.outerWidth() + 'px';

                if(state){
                    setTimeout(function() {
                        $element.show();
                        $element.css(style);
                    }, 0);
                }
                else{
                    if(!isOnDiv){
                        $element.removeAttr('style');
                        $element.hide();
                    }
                }
            }



            jsTreeInit();

            $element.on('destroy',function() {
                $document.off('click',dropDownBlurred);
            });

        }
    };
})


/***End Tree Admin****/

/***SOQL Tree ***/

.directive("soqlParentControl", function() {
    var commonProp = {};
    return {
        restrict : "A",
        controller:function($scope,$element,$timeout) {
            this.prop = commonProp;
        }
    };
})
.directive("soqlParentFormat", function($timeout) {
    return {
        require  : 'soqlParentControl',
        restrict : "C",
        scope:{
            modelValue:"=ngModel",
            settingData:"=",
            onSelectingNode:"=",
            initValue:'@initValue'
        },
        link:function($scope,$element,attr,soqlParentController) {
            $scope.modelValue = $scope.initValue;
            var validValue = $scope.modelValue;
            $element.on('keydown',function() {
                $timeout(function() {
                    $scope.modelValue=validValue;
                }, 0);
            });
            /*$element.on('focus blur',function(event) {
                var state;
                if(event.type === 'focus')
                    state = true;
                else if(event.type === 'blur')
                    state = false;
                soqlParentController.prop.onFocus(state,event);
            });*/
            soqlParentController.prop.onSelectNode = function(value,node) {
                $timeout(function() {

                    if(node.objectType === 'parent'){
                        $scope.modelValue = validValue = /*$scope.initValue+*/value;
                    } else if(node.objectType === 'child'){
                        $scope.modelValue = validValue = value;
                    }

                    $scope.onSelectingNode(node);
                }, 0);
            }

            soqlParentController.prop.settingData = function(data) {
                $scope.settingData(data);
            }
        }
    };
})
.directive("soqlParentStruct", function($timeout,$document) {
    return {
        require  : 'soqlParentControl',
        restrict : "C",
        link:function($scope,$element,attr,soqlParentController) {
            // $element.hide();
            var isOnDiv = false,
                currentJSTree;
            /*function parentStrMEnter(){
                isOnDiv=true;
                setTimeout(function() {
                    $document.on('click',dropDownBlurred)
                }, 0);
            }
            function parentStrMLeave(){
                isOnDiv=false;
            }
            function dropDownBlurred(event) {
                if(!isOnDiv){
                    var status = $(event.target).hasClass('user-parent-format');
                    if(!status){
                        $element.removeAttr('style');
                        $element.hide();
                        $document.off('click',dropDownBlurred)
                    }
                }
            }*/

            function jsTreeInit() {
                var data = [],callback;
                soqlParentController.prop.settingData({
                    input:null,
                    onSuccess:function(result) {
                        result.forEach(function(item) {
                            if(item.referenceTo){
                                item.objectType = "parent";
                                data.push({
                                    id:"node-" + (Math.round(Math.random() * 10e12)),
                                    parent:"#",
                                    text:item.referenceTo.replace("__c", "__r") +" ("+item.name+") ",
                                    prop:item,
                                    data:item.referenceTo,
                                    state:{ loaded:false }
                                });
                            } else {
                                item.objectType = "child";
                                // console.log(item);
                                data.push({
                                    id:"node-" + (Math.round(Math.random() * 10e12)),
                                    parent:"#",
                                    text:item.childSObject,
                                    prop:item,
                                    data:item.childSObject,
                                    li_attr:{
                                        class:"tree-child"
                                    },
                                    state:{ loaded:false }
                                });
                            }
                        });
                        console.log(data);
                        if(callback)
                            callback.call(this, data);
                    }
                });

                function getParentData(id) {
                    var node = _.find(data,{id:id});
                    soqlParentController.prop.settingData({
                        input:{
                            instanceConfigId:node.prop.instanceConfigId,
                            objectName:(node.prop.referenceTo),
                            node:node.prop
                        },
                        onSuccess:function(result) {
                            data.forEach(function(item) {
                                if(item.id == id){
                                    item.state = {
                                        opened:true
                                    }
                                }
                            });
                            if(!_.filter(data, {
                                            parent: id
                                        }).length){
                                var newData = [];
                                result.forEach(function(item) {
                                    item.level = (node.prop.level) ? node.prop.level + 1 : 2;
                                    if(item.referenceTo){
                                        item.objectType = "parent";
                                        newData.push({
                                            id:"node-" + (Math.round(Math.random() * 10e12)),
                                            parent:id,
                                            text:item.referenceTo.replace("__c", "__r") +" ("+item.name+") ",
                                            prop:item,
                                            data:item.referenceTo,
                                            state:{ loaded:false }
                                        });
                                    } else {
                                        item.objectType = "child";
                                        newData.push({
                                            id:"node-" + (Math.round(Math.random() * 10e12)),
                                            parent:id,
                                            text:item.childSObject,
                                            prop:item,
                                            data:item.childSObject,
                                            li_attr:{
                                                class:"tree-child"
                                            },
                                            state:{ loaded:false }
                                        });
                                    }
                                });
                                data = data.concat(newData);
                                callback.call(this, data);
                            }else{
                                callback.call(this, data);
                            }
                        }
                    });
                }

                var relationString="";
                function constructParentRelation(node){
                    if(node.parent != '#'){
                        var parentNode = _.find(data,{id:node.parent});
                        var currentNode = _.find(data,{id:node.id});
                        return constructParentRelation(parentNode) + "." + currentNode.prop.fieldToUse;
                    } else {
                        var currentNode = _.find(data,{id:node.id});
                        return relationString + "" + currentNode.prop.fieldToUse;
                    }
                }



                currentJSTree = $element.find('.admin-tree-body').jstree({
                    core: {
                        multiple: false,
                        animation: 0,
                        dblclick_toggle: false,
                        themes:{
                            icons:false
                        },
                        data : function(obj, cb) {
                            if(!data.length)
                                data = [];
                            callback = cb;
                            callback.call(this, data);
                        }
                    }
                });

                currentJSTree
                .on("changed.jstree", function(e, datum) {
                    // console.log(datum.node.original.prop.objectType);
                    var string;
                    if(datum.node.original.prop.objectType === 'parent'){
                        string = constructParentRelation(datum.node);
                        if(string.indexOf('undefined')>-1){
                            string = datum.node.data;
                        }
                    }
                    else if(datum.node.original.prop.objectType === 'child')
                        string = datum.node.data;

                    


                    var selectedNode = _.find(data,{id:datum.node.id});
                    soqlParentController.prop.onSelectNode(string,selectedNode.prop);
                })
                .off("click.jstree", ".jstree-ocl")
                .on("click.jstree", ".jstree-ocl", $.proxy(function(e) {
                    var jstreeNode = $(this).closest('.jstree-node');

                    var expandedTreeItemID = jstreeNode.attr('id');
                    var state = _.filter(data, {
                        id: expandedTreeItemID
                    })[0].state;

                    if (jstreeNode.hasClass('jstree-closed')) {
                        jstreeNode.addClass('jstree-loading');
                        getParentData(expandedTreeItemID);
                    } else {
                        jstreeNode.addClass('jstree-closed').removeClass('jstree-open');
                        jstreeNode.find('.jstree-children').hide();
                        state.opened = false;
                    }
                }));
            }


            /*$element
            .off('mouseenter',parentStrMEnter)
            .off("mouseleave",parentStrMLeave)
            .on("mouseenter",parentStrMEnter)
            .on("mouseleave",parentStrMLeave);*/

            /*soqlParentController.prop.onFocus = function(state,event) {
                event.stopPropagation();
                var style = {};
                var selElem = $(event.currentTarget);


                style.position = 'absolute';
                style.top = (selElem.position().top + selElem.outerHeight() + 2) + 'px';
                style.left = selElem.position().left + 'px';
                style.width = selElem.outerWidth() + 'px';

                if(state){
                    setTimeout(function() {
                        $element.show();
                        $element.css(style);
                    }, 0);
                }
                else{
                    if(!isOnDiv){
                        $element.removeAttr('style');
                        $element.hide();
                    }
                }
            }*/



            jsTreeInit();

            /*$element.on('destroy',function() {
                $document.off('click',dropDownBlurred);
            });*/

        }
    };
});