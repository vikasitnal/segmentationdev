segmentationApp
    .service('queryRecSer', function($filter) {
        this.QueryDragDrop = {};
        this.poppedObject = {
            outside: true
        };
        this.selQueriesArray = [];
        this.selElemObjs = [];
        this.selGroup = {};

        this.removeItemsSelGroup = {};
        this.removeItemsSelIndex = null;


        this.maxElemsForGrp = 0;
        this.noElemsForGrp = 0;
        this.unGroupButtonStatus = false;

        this.unSelectAll = function() {
            this.selQueriesArray = [];
            this.selElemObjs.forEach(function(item) {
                item.isSelected = false;
            });
            this.selElemObjs = _.filter(this.selElemObjs, {
                isSelected: true
            });
            this.unGroupButtonStatus = false;
            /*Newly Added*/
            this.selGroup = {};
        }

        this.onDragStart = angular.noop;
        this.onDropEnd = angular.noop;
    })
    .config(function($provide) {
        $provide.decorator('uibTypeaheadPopupDirective', function($delegate, $parse) {
            var uibTypeaheadPopupDirective = $delegate[0];
            uibTypeaheadPopupDirective.controller = function($scope, queryRecSer, $timeout, $element, $q, uibTypeaheadParser,$attrs) {
                var populateMatches = function() {
                    setTimeout(function() {
                        $q.when(parserResult.source(toBeChangedScope, locals)).then(function(matches) {
                            $scope.matches = [];
                            if (matches) {
                                for (var i = 0; i < matches.length; i++) {
                                    locals[parserResult.itemName] = matches[i];
                                    $scope.matches.push({
                                        id: 'typeahead-' + $scope.$id + '-' + Math.floor(Math.random() * 10000) + '-option-' + i,
                                        label: parserResult.viewMapper(toBeChangedScope, locals),
                                        model: matches[i]
                                    });
                                }
                            }
                        });
                    }, 300);
                }

                var dropClicker = $element.closest('div').find('.drop');
                var dropFocusser = $element.prev("input.query-options");
                if(dropFocusser.length){
                    var focusOnAsync = function() {
                        if(!$scope.matches.length){
                            populateMatches();
                            dropFocusser.off('focus',focusOnAsync);
                        }
                    }

                    dropFocusser.on('focus',focusOnAsync);

                    $scope.positionInFeeder = function() {
                        var position = dropFocusser.position();
                        position.top = position.top + dropFocusser.height();
                        return position;
                    }

                    /*TODO*/
                    if(dropFocusser.attr('page') === 'soql-query'){
                        var toBeChangedScope = $element.closest('div').scope();
                    } else {
                        var toBeChangedScope = $element.closest('div').scope().$parent;
                        
                    }



                    var parserResult = uibTypeaheadParser.parse(dropFocusser.attr('uib-typeahead'));
                    var locals = {
                        $viewValue: ''
                    }

                    dropClicker.on('click', function() {


                        var isDisabled = dropFocusser.prop('disabled');
                        if (isDisabled)
                            return;


                        dropFocusser.focus();
                        dropFocusser.select();
                        populateMatches();
                    });
                }

                // dropFocusser.off('focusout',dropFocusserOut)
                // .on('focusout',dropFocusserOut)

            }



            return $delegate;
        });
    })
    .directive("queryRecursive", function() {
        var selQueryPop;
        return {
            restrict: "A",
            templateUrl: './directiveTemplates/queryRecursive.html',
            scope: {
                object: "=",
                groupId: '=',
                pageType: '@',
                selArrayObject: '=',
                parentObject: '='
            },
            controller: function($scope, queryRecSer, $element, $timeout, $filter, apiCalls) {
                var setToolTip = function() {
                    clearTimeout(st);
                    st = setTimeout(function() {
                        var toolTipText = $element.find('.query-item-defination:not(".ng-hide")');
                        var textArray = toolTipText.text().split("\n");
                        var text = '';
                        textArray.forEach(function(item) {
                            if (item.trim().length) {
                                text = text + item.trim() + ' ';
                            }
                        });
                        
                        toolTipText.attr('title', text);
                    }, 100);
                }

                function outsidePopupClicked(event) {
                    queryRecSer.poppedObject.show = false;
                    var elementToFocusOn = $element.find('.popover.query-popup');
                    elementToFocusOn.off('click',insidePopupClicked);
                    $(document).off('click',outsidePopupClicked);
                    $scope.$digest();
                }


                function insidePopupClicked(event) {
                    event.stopPropagation();
                }


                function initiateOnDocumentSelect() {
                    setTimeout(function() {
                        var elementToFocusOn = $element.find('.popover.query-popup');
                        $(document).on('click',outsidePopupClicked);
                        elementToFocusOn.on('click',insidePopupClicked);
                    }, 180);
                }


                /*var setOutside = function(status) {
                    $timeout(function() {
                        queryRecSer.poppedObject.outside = status;
                    }, 0);
                    setToolTip();
                }*/

                /*setTimeout(function() {

                    $element.find(document.querySelectorAll('.query-body>.query-item+.query-popup'))
                        .off('mouseenter')
                        .off('mouseleave')
                        .on("mouseenter", function() {
                            setOutside(false);
                        })
                        .on("mouseleave", function() {
                            setOutside(true);
                        });
                }, 15);*/

                /* All Watchers */
                $scope.$watchGroup(['QueryTpl.object.value', 'QueryTpl.object.start', 'QueryTpl.object.end', 'QueryTpl.object.selectedElqField','QueryTpl.object.isNot'], function() {
                    setToolTip();
                });

                $scope.$watch('QueryTpl.treeViewChanged', function() {
                    if (QueryTpl.treeViewInitialized)
                        initiateTreeView();
                });


                var fl      =   $filter('filter'),
                    orBy    =   $filter('orderBy');
                var st, rejectPredicates = {};
                var innerControls = {};


                var QueryTpl = $scope.QueryTpl = {
                    object: $scope.object,
                    toBeSanitized: $scope.object.toBeSanitized,
                    groupID: $scope.groupId,
                    selArrayObject: $scope.selArrayObject,
                    parentObject: $scope.parentObject,
                    pageType: $scope.pageType                  
                };


                if(QueryTpl.pageType === 'decision'){
                    

                    QueryTpl.cmpElqFields = {
                        object:QueryTpl.object,
                        pageType:QueryTpl.pageType,
                        eloquaFields: QueryTpl.toBeSanitized.eloquaFields,
                        eloquaComparingOperators:QueryTpl.toBeSanitized.eloquaComparingOperators
                    }


                    if(QueryTpl.object.type == 'elqcmp'){
                        QueryTpl.cmpElqFields.eloquaQueryStatus = true;
                    }
                }

                if(QueryTpl.toBeSanitized.objectName)
                    QueryTpl.toBeSanitized.objectName = QueryTpl.toBeSanitized.objectName.replace('__c','');

                QueryTpl.dateOptions = {
                    showWeeks       :false,
                    formatMonth     :'MMM',
                    formatDayTitle  :'MMM yyyy',
                    customClass     :function(data) {
                        return '';
                    }
                }


                QueryTpl.dynDays=['Today','Tomorrow','Yesterday'];
                QueryTpl.initDynamicDay = function(type){
                    if(type==1)
                        QueryTpl.object.value = (!QueryTpl.object.value)?'Today':QueryTpl.object.value;
                    if(type==2){
                        QueryTpl.object.start = (!QueryTpl.object.start)?'Today':QueryTpl.object.start;
                        QueryTpl.object.end = (!QueryTpl.object.end)?'Today':QueryTpl.object.end;
                    }
                }


                QueryTpl.durationTimes=['Hours','Days','Weeks','Months'];
                QueryTpl.durationCount = 1;
                QueryTpl.durationUnit = 'Days';

                QueryTpl.initDurationTime = function() {
                    QueryTpl.object.value = (!QueryTpl.object.value)?'1 Days':QueryTpl.object.value;
                    var durationSet = QueryTpl.object.value.split(' ');
                    QueryTpl.durationCount = durationSet[0];
                    QueryTpl.durationUnit = durationSet[1];
                }

                QueryTpl.changeDurationTime = function() {
                    QueryTpl.object.value = QueryTpl.durationCount + " " + QueryTpl.durationUnit;
                }




                if (QueryTpl.object.assignedOperator)
                    QueryTpl.valuesUIStatus = QueryTpl.object.assignedOperator.ui_id;

                if (QueryTpl.toBeSanitized.id == 1)
                    QueryTpl.fields = QueryTpl.toBeSanitized.fields;

                QueryTpl.fields = _.reject(QueryTpl.fields,{type:'textarea'});
                QueryTpl.fields = _.reject(QueryTpl.fields,{type:'address'});


                var fillPicklist = function() {
                    if(QueryTpl.object.value && (QueryTpl.object.assignedOperator.ui_id == 6 || QueryTpl.object.assignedOperator.ui_id == 12 )){
                        QueryTpl.pickedList = [];
                        QueryTpl.pickedList = _.filter(QueryTpl.toBeSanitized.pickListsData,{
                            name:QueryTpl.object.selectedField.name
                        })[0].picklist;
                        QueryTpl.pickedValue = _.filter(QueryTpl.pickedList,{
                            value:QueryTpl.object.value
                        })[0];
                    }
                }

                if(QueryTpl.toBeSanitized.id == 1){
                    fillPicklist();
                }

                QueryTpl.assignOperators = fl(QueryTpl.toBeSanitized.operators, {
                    Operator_Label: '!0',
                });


                QueryTpl.queryContext = [
                    ['Delete', function($itemScope, $event, model) {
                        queryRecSer.removeItemsSelGroup = QueryTpl.selArrayObject;
                        if(QueryTpl.parentObject){
                            queryRecSer.selQueriesArray[0] = {
                                index:queryRecSer.removeItemsSelGroup.queryIndex
                            }
                            queryRecSer.selGroup = QueryTpl.parentObject;
                        }
                        queryRecSer.deleteQuery(QueryTpl.object.queryIndex);
                    }]
                ];

                /* Tree view
                 * Initializations
                 */
                QueryTpl.treeViewData = [];
                QueryTpl.treeViewChanged = 0;
                var treeViewRender;

                var startRelationShipString = QueryTpl.toBeSanitized.objectName;

                var assignTreeViewEvents = function() {
                    $('#queryTree-' + QueryTpl.treeViewStruct.id)
                        .on("changed.jstree", function(e, data) {
                            var selParent = fl(QueryTpl.treeViewData, {
                                id: data.selected[0]
                            })[0];
                            QueryTpl.object.parentRelationShipString = createRelationShipString(selParent);
                            QueryTpl.toBeSanitized.treeViewData = QueryTpl.treeViewData;
                            QueryTpl.parRelHeader = QueryTpl.object.parentRelationShipString.replace(RegExp(".\\[.*?\\]", "g"), ".");
                            getFieldsAccordParent(selParent.item,true);

                        })
                        .off("click.jstree", ".jstree-ocl")
                        .on("click.jstree", ".jstree-ocl", $.proxy(function(e) {
                            var jstreeNode = $(this).closest('.jstree-node');

                            var expandedTreeItemID = jstreeNode.attr('id');
                            var state = fl(QueryTpl.treeViewData, {
                                id: expandedTreeItemID
                            })[0].state;



                            innerControls.getSelNodeEl = function() {

                                return $element.find('#'+expandedTreeItemID+"_anchor");
                            }

                            innerControls.afterClick = true;


                            if (jstreeNode.hasClass('jstree-closed') && !state.noChangesReq) {

                                jstreeNode.addClass('jstree-loading');

                                state.loaded = true;
                                state.opened = true;
                                state.noChangesReq = true;

                                renderTreeView(fl(QueryTpl.treeViewData, {
                                    id: expandedTreeItemID
                                })[0].item, expandedTreeItemID);

                            } else {
                                state.opened = state.loaded = !state.opened;
                                if (state.opened) {
                                    jstreeNode.addClass('jstree-open').removeClass('jstree-closed');
                                    jstreeNode.find('.jstree-children').show();
                                } else {
                                    jstreeNode.addClass('jstree-closed').removeClass('jstree-open');
                                    jstreeNode.find('.jstree-children').hide();
                                }
                                /*$('#queryTree-'+QueryTpl.treeViewStruct.id).jstree(true).settings.core.data = QueryTpl.treeViewData;
                                $('#queryTree-'+QueryTpl.treeViewStruct.id).jstree(true).refresh();*/
                            }

                            setInitStep();

                        }))
                }


                var scrollToSelObj = function() {
                    setTimeout(function() {
                        if(innerControls.getSelNodeEl){
                            var selNodeEl = innerControls.getSelNodeEl();
                            
                            var top = selNodeEl.offset().top - selNodeEl.closest('.jstree').offset().top;
                            $element.find('.jstree').animate({
                                scrollTop : top-40,
                                scrollLeft: 0
                            },0);
                            innerControls.getSelNodeEl = null;
                        }
                    }, 200);
                }

                var createRelationShipString = function(selParent) {
                    if (selParent.parent == '#') {
                        return startRelationShipString + ".[P]" + selParent.item.relationshipName;
                    } else {
                        var traversedParent = fl(QueryTpl.treeViewData, {
                            id: selParent.parent
                        })[0];
                        return createRelationShipString(traversedParent) + ".[P]" + selParent.item.relationshipName;
                    }
                }


                var getFieldsAccordParent = function(selParent,onSelStatus) {
                    QueryTpl.selParentStatus = false;
                    QueryTpl.fields = [];
                    QueryTpl.object.selectedField = null;
                    if(onSelStatus)
                        QueryTpl.object.assignedOperator = null;

                    setTimeout(function() {
                        apiCalls.getParentFieldsAssoc(selParent)
                            .then(function(result) {
                                if (result.data.objectsList) {
                                    QueryTpl.fields = result.data.objectsList[0].allFields;
                                    QueryTpl.fields = _.reject(QueryTpl.fields,{type:'textarea'});
                                    QueryTpl.fields = _.reject(QueryTpl.fields,{type:'address'});
                                    QueryTpl.toBeSanitized.pickListsData = result.data.objectsList[0].picklistValuesArray;

                                    QueryTpl.selParentStatus = true;

                                    if(QueryTpl.object.fieldName){
                                        QueryTpl.object.selectedField = _.filter(QueryTpl.fields,{name:QueryTpl.object.fieldName},true)[0];
                                        QueryTpl.object.fieldName = null;

                                        fillPicklist();
                                        setToolTip();
                                    }
                                }
                            });
                    }, 10);
                }

                var renderTreeView = function(obj, id) {
                    apiCalls.getParentsOfObject(obj)
                        .then(function(result) {

                            QueryTpl.treeViewData.forEach(function(item) {
                                item.state.noChangesReq = false;
                            });

                            if (result.status == 200) {
                                if (!_.filter(QueryTpl.treeViewData, {
                                        parent: id
                                    }).length) {

                                    result.data.forEach(function(item) {

                                        item.level = (id) ? obj.level + 1 : 1;

                                        QueryTpl.treeViewData.push({
                                            id: "node-" + (Math.round(Math.random() * 10e12)),
                                            text: item.referenceTo + " (" + item.Object_Label + ")",
                                            parent: id || '#',
                                            state: {
                                                loaded: (obj.level >= 4)
                                            },
                                            item: item
                                        });
                                    });

                                }

                                if(typeof treeViewRender == 'object')
                                    treeViewRender.cb.call(treeViewRender.self,QueryTpl.treeViewData);

                            } else {
                                QueryTpl.treeViewData.concat([]);
                            }
                        });
                }


                var initiateTreeView = function() {

                    QueryTpl.treeViewStruct = {
                        id: Math.round(Math.random() * 10e12),
                        treeData: {
                            core: {
                                multiple: false,
                                animation: 0,
                                dblclick_toggle: false,
                                data: function(obj, cb) {
                                    var data = JSON.parse(JSON.stringify(QueryTpl.treeViewData));
                                    treeViewRender = {
                                        self:this,
                                        cb:cb
                                    }
                                    cb.call(this, data);
                                }
                            }
                        }
                    }

                    setTimeout(function() {
                        if ($('#queryTree-' + QueryTpl.treeViewStruct.id).length) {
                            var height = $('#queryTree-' + QueryTpl.treeViewStruct.id).height();

                            if (height > 0) {
                                $('#queryTree-' + QueryTpl.treeViewStruct.id)
                                    .css({
                                        'height': height
                                    });
                            }

                            $('#queryTree-' + QueryTpl.treeViewStruct.id).empty().removeAttr('class role aria-activedescendant aria-busy');

                        }
                        $('#queryTree-' + QueryTpl.treeViewStruct.id).jstree(angular.copy(QueryTpl.treeViewStruct.treeData));
                        assignTreeViewEvents();

                    }, 0);

                }

                /* Start getting object treeView
                 * Initialized with the selected Object and
                 * if has parents is selected
                 */
                if (QueryTpl.toBeSanitized.id == 2 || QueryTpl.toBeSanitized.id == 3) {
                    var messedData = {};


                    messedData.Object_Name = QueryTpl.toBeSanitized.selObject.Object_Id;
                    messedData.Object_Label = QueryTpl.toBeSanitized.selObject.Object_Name;
                    messedData.Source_Instance_Config_Id = QueryTpl.toBeSanitized.selObject.Source_Instance_Config_Id;

                    if (QueryTpl.object.treeViewData) {
                        var traverseParentsArray = _.tail(QueryTpl.object.parentRelationShipString.split('.[P]'));

                        var parseThroughParents = function(obj) {
                            var commonParentSet = _.filter(QueryTpl.treeViewData,{item:{relationshipName:traverseParentsArray[0]}});
                            var selParent = commonParentSet[commonParentSet.length-1];
                            if (traverseParentsArray.length>1) {
                                traverseParentsArray = _.tail(traverseParentsArray);
                                parseThroughParents(selParent);
                            } else {
                                if(selParent){
                                    var selID = selParent.id;
                                    var selIDs = commonParentSet.map(function(item) {
                                        return "#"+item.id+"_anchor";
                                    });


                                    innerControls.getSelNodeEl = function() {
                                        var visibleSelectedParents = document.querySelectorAll(selIDs),
                                            vPLength = document.querySelectorAll(selIDs).length;
                                        var selectedParent = visibleSelectedParents[vPLength-1];

                                        return $element.find(selectedParent).addClass('jstree-clicked');
                                    }
                                    getFieldsAccordParent(selParent.item);
                                }
                            }
                        }

                        QueryTpl.treeViewData = angular.copy(QueryTpl.object.treeViewData);
                        QueryTpl.toBeSanitized.treeViewData = QueryTpl.treeViewData;
                        QueryTpl.treeViewInitialized = true;
                        QueryTpl.treeViewChanged++;
                        QueryTpl.object.treeViewData = null;

                        QueryTpl.parRelHeader = QueryTpl.object.parentRelationShipString.replace(RegExp(".\\[.*?\\]", "g"), ".");

                        parseThroughParents(messedData);

                    } else {

                        renderTreeView(messedData);
                    }
                }

                /* End Tree View*/




                QueryTpl.fieldOut = function() {



                    if (typeof(QueryTpl.object.selectedField) == 'string') {
                        QueryTpl.object.selectedField = fl(QueryTpl.fields, {
                            label: QueryTpl.object.selectedField.label
                        }, true)[0];

                    }

                }

                QueryTpl.operatorOut = function() {

                    if (typeof(QueryTpl.object.assignedOperator) == 'string') {
                        QueryTpl.object.assignedOperator = fl(QueryTpl.assignOperators, {
                            Field_Type_Name: QueryTpl.object.selectedField.type
                        }, true)[0];

                        if (QueryTpl.object.assignedOperator)
                            QueryTpl.valuesUIStatus = QueryTpl.object.assignedOperator.ui_id;
                    }



                }

                function setInitStep() {
                    QueryTpl.object.isNot = false;
                    QueryTpl.object.value = QueryTpl.object.start = QueryTpl.object.end = '';
                }


                QueryTpl.onSelField = function() {
                    QueryTpl.pickedValue = null;
                    QueryTpl.valuesUIStatus = 0;
                    QueryTpl.object.assignedOperator = fl(_.sortBy(QueryTpl.assignOperators,['Operator_Label']), {
                        Field_Type_Name: QueryTpl.object.selectedField.type
                    }, true)[0];


                    if (QueryTpl.object.assignedOperator){
                        QueryTpl.valuesUIStatus = QueryTpl.object.assignedOperator.ui_id;

                        if(QueryTpl.valuesUIStatus == 6){

                            QueryTpl.pickedList= [];

                            QueryTpl.pickedList = _.filter(QueryTpl.toBeSanitized.pickListsData,{
                                name:QueryTpl.object.selectedField.name
                            })[0].picklist;

                        }
                    }

                    setInitStep();
                    setToolTip();
                }



                QueryTpl.onAssignOperator = function() {
                    if(QueryTpl.valuesUIStatus == 9)
                        return;

                    QueryTpl.object.value = QueryTpl.object.start = QueryTpl.object.end = '';
                    QueryTpl.valuesUIStatus = QueryTpl.object.assignedOperator.ui_id;
                    var toolTipText = $element.find('.query-item-defination:not(".ng-hide")');
                    setInitStep();
                    setToolTip();
                    
                    
                }

                QueryTpl.switchOperator = function() {
                    QueryTpl.object.operatorSwitch = !QueryTpl.object.operatorSwitch;
                    QueryTpl.object.logicalOperator = (QueryTpl.object.operatorSwitch) ? 'AND' : 'OR';
                    queryRecSer.checkAmbiguity();
                }

                QueryTpl.disableIsNot = function() {
                    if(QueryTpl.object && QueryTpl.object.selectedField){

                        var cond = QueryTpl.valuesUIStatus==4||QueryTpl.valuesUIStatus==5  ;
                            cond = cond || (QueryTpl.object.assignedOperator && QueryTpl.object.assignedOperator.Operator_Id==42);
                            cond = cond || (QueryTpl.object.assignedOperator && QueryTpl.object.assignedOperator.Operator_Id==47);
                            cond = cond || QueryTpl.valuesUIStatus==7||QueryTpl.valuesUIStatus==8||QueryTpl.object.selectedField.type=='id';

                        if(cond){
                            cond = QueryTpl.object.assignedOperator.Operator_Id !== 30;
                        }


                        QueryTpl.isNotShouldDis = cond;
                    }

                    if(QueryTpl.object.assignedOperator){
                        var opName = QueryTpl.object.assignedOperator.Operator_Name;
                        if(opName)
                            var cond2 = !!opName.match(/greater|less|true|false/g);
                        if(!QueryTpl.isNotShouldDis)
                            QueryTpl.isNotShouldDis =  cond2;



                    }

                    return QueryTpl.isNotShouldDis;

                }






                QueryTpl.showHide = function(event) {
                    QueryTpl.doubleClicked = true;
                    if (QueryTpl.activateSelect) {
                        $timeout.cancel(QueryTpl.activateSelect);
                    }

                    initiateOnDocumentSelect();


                    if (QueryTpl.object.assignedOperator)
                        QueryTpl.valuesUIStatus = QueryTpl.object.assignedOperator.ui_id;

                    QueryTpl.toBeSanitized.show = !QueryTpl.toBeSanitized.show;
                    queryRecSer.poppedObject = QueryTpl.toBeSanitized;

                    if (QueryTpl.toBeSanitized.show && !QueryTpl.treeViewInitialized) {
                        initiateTreeView();
                        QueryTpl.treeViewInitialized = true;
                    }
                    scrollToSelObj();
                }

                QueryTpl.selectThisQueryElement = function(ev) {
                    QueryTpl.doubleClicked = false;
                    QueryTpl.activateSelect = $timeout(function() {
                        if (!QueryTpl.doubleClicked) {
                            if (!QueryTpl.object.toBeSanitized.isSelected) {

                                if (!ev.ctrlKey) {
                                    queryRecSer.unSelectAll();
                                }

                                QueryTpl.object.toBeSanitized.isSelected = true;

                                queryRecSer.selQueriesArray.push({
                                    index: QueryTpl.object.queryIndex,
                                    isSelected: QueryTpl.object.toBeSanitized.isSelected,
                                    groupID: QueryTpl.groupID || 0
                                });

                                queryRecSer.selElemObjs.push(QueryTpl.object.toBeSanitized);


                            } else {

                                if (!ev.ctrlKey) {

                                    queryRecSer.selElemObjs.forEach(function(item) {
                                        item.isSelected = false;
                                    });

                                    QueryTpl.object.toBeSanitized.isSelected = true;
                                    queryRecSer.selElemObjs = fl(queryRecSer.selElemObjs, {
                                        querySIndex: QueryTpl.object.queryIndex
                                    });
                                    queryRecSer.selQueriesArray = fl(queryRecSer.selQueriesArray, {
                                        index: QueryTpl.object.queryIndex
                                    });

                                } else {

                                    QueryTpl.object.toBeSanitized.isSelected = false;
                                    rejectPredicates = {
                                        elemObjs: {
                                            querySIndex: QueryTpl.object.queryIndex,
                                            isSelected: false
                                        },
                                        queriesArray: {
                                            index: QueryTpl.object.queryIndex,
                                            groupID: QueryTpl.groupID || 0
                                        }
                                    }


                                    queryRecSer.selElemObjs = _.reject(queryRecSer.selElemObjs, rejectPredicates.elemObjs);
                                    queryRecSer.selQueriesArray = _.reject(queryRecSer.selQueriesArray, rejectPredicates.queriesArray);

                                }
                            }

                            queryRecSer.selQueriesArray = orBy(queryRecSer.selQueriesArray, 'index');

                            queryRecSer.noElemsForGrp = _.filter(queryRecSer.selQueriesArray, {
                                groupID: (QueryTpl.groupID || 0)
                            }).length;


                            
                            // console.log(queryRecSer.noElemsForGrp,queryRecSer.maxElemsForGrp);



                            if (queryRecSer.noElemsForGrp >= queryRecSer.maxElemsForGrp) {
                                queryRecSer.maxElemsForGrp = queryRecSer.noElemsForGrp;
                                queryRecSer.selGroup = QueryTpl.selArrayObject;
                            }

                            if (queryRecSer.selElemObjs && queryRecSer.selElemObjs[0])
                                queryRecSer.unGroupButtonStatus = queryRecSer.selElemObjs[0].groupID && queryRecSer.selElemObjs.length == 1;


                            if(queryRecSer.unGroupButtonStatus)
                                queryRecSer.selGroup = QueryTpl.selArrayObject;
                                
                        }
                    }, 180);
                }

                /*Picklist*/
                QueryTpl.onSelPickList = function() {
                    if(QueryTpl.pickedValue){
                        QueryTpl.object.value = QueryTpl.pickedValue.value;
                    }
                }


            }
        };
    });
