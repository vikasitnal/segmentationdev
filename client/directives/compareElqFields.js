segmentationApp
    .directive("cmpElqFields", function() {
        return {
            restrict: "A",
            templateUrl: './directiveTemplates/compareElqFields.html',
            scope:{
                cmpElqFields:"="
            },
            controller: function($scope) {


                var cmpElqFields = $scope.cmpElqFields;


                $scope.$watch('cmpElqFields.object.selectedField.name',function(newValue,oldValue) {
                    if(oldValue !== newValue && oldValue !== undefined){
                        cmpElqFields.object.selElqCmpOperator = undefined;
                        cmpElqFields.object.selectedElqField = undefined;
                    }
                });
                


                cmpElqFields.changeStatus = function() {

                    if(cmpElqFields.object && cmpElqFields.object.selectedField && cmpElqFields.object.selectedField.name 
                        && cmpElqFields.eloquaQueryStatus){
                        cmpElqFields.object.value = undefined;
                        cmpElqFields.object.start = undefined;
                        cmpElqFields.object.end = undefined;
                    }
                    else{
                        cmpElqFields.object.selElqCmpOperator = undefined;
                    }

                    cmpElqFields.object.type = (cmpElqFields.eloquaQueryStatus)?'elqcmp':'element';
                }

            }
        };
    });