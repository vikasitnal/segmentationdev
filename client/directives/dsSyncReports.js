segmentationApp
.directive("dsSyncReportsModal", function() {
    return {
        restrict : "A",
        templateUrl : './directiveTemplates/dsSyncReports.html'
    };
})
.controller('dsyncReportsCtrl', function($scope, $uibModalInstance,$http,$timeout,$filter,apiCalls,SyncReports) {

    DSSyncReports = $scope.DSSyncReports = {};
    DSSyncReports.syncData = SyncReports.data.result;
    var weekStartEnd = {
        start:null,
        end:null
    }




    var getPrevDateBy = function(differenceFrom,differenceBy) {
        var diffTime = new Date(new Date(differenceFrom)-differenceBy*8.64e7).getTime();
        return new Date(diffTime);
    }


    DSSyncReports.updateThisWeek = function(next) {

        if(!weekStartEnd.end){
            weekStartEnd.end = new Date();
        } else {
            if(next)
                weekStartEnd.end = getPrevDateBy(weekStartEnd.end,-5);
            else
                weekStartEnd.end = getPrevDateBy(weekStartEnd.start,1);
        }

        weekStartEnd.start = getPrevDateBy(weekStartEnd.end,4);
        DSSyncReports.thisWeek = $filter('suffixedDate')(weekStartEnd.start)+" - "+$filter('suffixedDate')(weekStartEnd.end);
    }

    DSSyncReports.prevWeekExecution = function() {
        DSSyncReports.updateThisWeek(false);
        DSSyncReports.renderDateCategorizedExecutions(false);
    }

    DSSyncReports.nextWeekExecution = function() {
        DSSyncReports.updateThisWeek(true);
        DSSyncReports.renderDateCategorizedExecutions(false);
    }



    DSSyncReports.renderDateCategorizedExecutions = function() {
    	DSSyncReports.categoryDateData = [];
        var weekCategorizedExecutions = [];



        DSSyncReports.dateFormat = 'mediumDate';
        var dateCtgData = _.groupBy(DSSyncReports.syncData,function(item) {

            dateCtgKey = $filter('date')(parseInt(+item.endTime),DSSyncReports.dateFormat);
            return dateCtgKey;
        });


        
        for (var i = 0,j = 4; i < 5; i++,--j) {
            var dateAsKey = $filter('date')(getPrevDateBy(new Date(weekStartEnd.end),j),DSSyncReports.dateFormat);

            var newItem = {
                timestamp: Math.floor((new Date(dateAsKey)).getTime()/1000)+'',
                yesCount:0,
                noCount:0,
                erroredCount:0,
            }


            if(dateCtgData[dateAsKey]){
                dateCtgData[dateAsKey].forEach(function(item) {
                    newItem.yesCount = newItem.yesCount + item.yesCount;
                    newItem.noCount = newItem.noCount + item.noCount;
                    newItem.erroredCount = newItem.erroredCount + item.erroredCount;
                });
            }


            weekCategorizedExecutions[i] = newItem;

        }

        DSSyncReports.categoryDateData = weekCategorizedExecutions;

        
        if(DSSyncReports.barChartOptions)
            DSSyncReports.barChartOptions.render(DSSyncReports.categoryDateData,DSSyncReports.dateFormat);
    }

    DSSyncReports.updateThisWeek();
    DSSyncReports.renderDateCategorizedExecutions();


    DSSyncReports.close = function() {
    	$uibModalInstance.close();
    }
});
