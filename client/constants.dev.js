segmentationApp
.constant('APIBASEURL', 'http://'+window.location.host+'/')
.constant('URLLIST', {
	
	'ADMINDATA':'exampleJsons/User/getAdminData',
	'LOGGEDINDETAILS':'exampleJsons/User/getLoginDetails',
	'SFDCOBJECTS':'exampleJsons/User/getSfdcObjects',
	'GETEXTERNALSYSOBJS':'exampleJsons/User/instanceConfiguration',
	'OBJECTMAPPING':'exampleJsons/User/objectMapping',
	'SAVEUSER':'exampleJsons/User/addInstance',
	'SYNCSFDCDATA':'exampleJsons/User/syncSfdcData',
	'ADMINGETPARENTS':'exampleJsons/User/getParentObjects',
	'ADMINGETPARENTFIELDS':'exampleJsons/User/getParentObjectFields',
	
	'FEEDERSERVICEMAPPEDFIELDS':'exampleJsons/mappedFields.json',
	'GETFEEDEREXTERNALSYSTEMS':'exampleJsons/getExtSystem.json',
	'GETSELECTEDSFDCOBJECTS':'exampleJsons/selectedSFDCObjects.json',
	'GETEXISTINGFEEDER':'exampleJsons/savedJson.json',
	'GETPARENTOFOBJECT':'exampleJsons/treeView/',
	'GETPARENTFIELDSASSOC':'exampleJsons/parentFields/',
	'EXISTINGSEGMENTS':'exampleJsons/getExistingSegments.json',
	'GETEXISTINGSEGMENT':'exampleJsons/getExistingSegment.json',
	'CONTACTRECORDS':'exampleJsons/feeder/getContactsRecords',
	'UPDATECONTACTSCOUNT':'exampleJsons/feeder/getContactsCount',
	'CONTACTCOLUMNS':'exampleJsons/feeder/getContactsColumns.json'
})
