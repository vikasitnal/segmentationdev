segmentationApp
    .controller('fieldMappingCtrl', function($scope,$rootScope, $uibModalInstance, $http, $timeout, $filter, apiCalls, $alert, $popup, SelectedObject,status,sfCtrlStatus,saveSegment) {
        var fieldMapping = $scope.fieldMapping = {};
        fieldMapping.selectedObject = SelectedObject;

        fieldMapping.sfCtrlStatus =  sfCtrlStatus;


        fieldMapping.emptyPriority = function() {
            fieldMapping.selPriority = {
                name: ''
            }
        }
        
        fieldMapping.setPriority = function() {
            if (fieldMapping.selPriority && fieldMapping.selPriority.name.length)
                var priority = fieldMapping.selPriority;


            apiCalls.setDSImportPriorityData({
                    instanceid: fieldMapping.selectedObject.instanceid,
                    priority: priority
                })
                .then(function(response) {

                    apiCalls.getDSImportPriorityData({
                            instanceid: fieldMapping.selectedObject.instanceid
                        })
                        .then(function(response) {
                            var data = response.data;

                            $popup.show({
                                header: "Import Priority",
                                message: "Import Priority saved successfully",
                                okText: 'OK',
                                hideCancel:true
                            })
                            .onOk(function() {
                                

                            })
                            .onCancel(function() {

                            });
                        });
                })
        }

        fieldMapping.importPriority = function(index, selectedObject) {
            apiCalls.getImportPriorityData()
                .then(function(result) {
                    fieldMapping.importPriorityData = result.data;
                    apiCalls.getDSImportPriorityData({
                            instanceid: fieldMapping.selectedObject.instanceid
                        })
                        .then(function(response) {
                            var data = response.data;
                            if(data.import_priority){
                                fieldMapping.selPriority = JSON.parse(data.import_priority);
                            }

                            
                        });
                });
            /*var height = $('#importPriority').height();
            $('#importPriority').show().css('height', '0px').animate({
                height: height
            }, User.AnimationComplete);*/
        }
        fieldMapping.importPriority();


        fieldMapping.setParentData = function(data) {
            if (!data.input) {
                apiCalls.adminGetParents({
                        "instanceConfigId": SelectedObject.Source_Instance_Config_Id,
                        "objectName": SelectedObject.Object_Id
                    })
                    .then(function(result) {
                        data.onSuccess(result.data);
                    });
            } else {
                apiCalls.adminGetParents({
                        "instanceConfigId": data.input.instanceConfigId,
                        "objectName": data.input.objectName
                    })
                    .then(function(result) {
                        data.onSuccess(result.data);
                    });
            }
        }

        fieldMapping.getObjectParentFields = function(selNode) {

            fieldMapping.sdfcConFields = [];

            fieldMapping.selSdfcObj = null;

            apiCalls.getAdminParentFields({
                instanceConfigId: selNode.instanceConfigId,
                objectName: selNode.referenceTo
            }).then(function(result) {

                fieldMapping.sdfcConFields = _.sortBy(result.data, ['label']);
                fieldMapping.sdfcConFields.forEach(function(item) {
                    item.shownValue = item.label + " (" + item.name + ")";
                });

                fieldMapping.mappedContactFields.forEach(function(item) {
                    fieldMapping.sdfcConFields = _.reject(fieldMapping.sdfcConFields, {
                        name: item.Source_Field_Id
                    });
                });
                fieldMapping.emptyElqField();
                fieldMapping.emptySFDCField();
            });
        }
        fieldMapping.cancelFieldMapping = function() {
            fieldMapping.sdfcConFields = initialSFDCConFields;
            fieldMapping.emptyElqField();
            fieldMapping.emptySFDCField();
            fieldMapping.removeAlreadyMapped();
        }

        fieldMapping.removeAlreadyMapped = function() {
            status.fieldMappingCount = fieldMapping.mappedContactFields.length;
            fieldMapping.mappedContactFields.forEach(function(item) {
                fieldMapping.sdfcConFields = _.reject(fieldMapping.sdfcConFields, {
                    name: item.Source_Field_Id
                });
                fieldMapping.eloquaFields = _.reject(fieldMapping.eloquaFields, {
                    internalName: item.Target_Field_Id
                });
            });
        }


        fieldMapping.mapInstanceObject = function(selectedObjectID) {
            fieldMapping.mappedContactFields = null;
            fieldMapping.selSdfcObj = null;
            fieldMapping.selElqObj = null;


            apiCalls.getDecisionFieldMapping({
                    Source_Instance_Config_Id: selectedObjectID.Source_Instance_Config_Id,
                    Source_Object_Id: selectedObjectID.Object_Id,
                    sfCtrlStatus:sfCtrlStatus
                })
                .then(function(result) {
                    fieldMapping.eloquaFields = _.sortBy(result.data.EloquaObjects.elements, ['name']);
                    fieldMapping.sdfcConFields = _.sortBy(result.data.SFDCOjects, ['label']);
                    fieldMapping.history = {
                        sdfcConFields:fieldMapping.sdfcConFields,
                        eloquaFields:fieldMapping.eloquaFields,
                        
                    }

                    fieldMapping.sdfcConFields.forEach(function(item) {
                        item.shownValue = item.label + " (" + item.name + ")";
                    });

                    fieldMapping.eloquaFields.forEach(function(item) {
                        item.shownValue = item.name;
                    });

                    fieldMapping.emptyElqField();
                    fieldMapping.emptySFDCField();



                    fieldMapping.mappedContactFields = result.data.MappedFields;


                    fieldMapping.removeAlreadyMapped();

                    status.fieldMappingCount = fieldMapping.mappedContactFields.length;

                    initialSFDCConFields = fieldMapping.sdfcConFields;

                });

        }



        fieldMapping.addMapping = function() {
            var index = _.findIndex(fieldMapping.mappedContactFields, function(mapping) {
                return mapping.Target_Field_Id == fieldMapping.selElqObj.internalName;
            });
            if (index >= 0) {
                /*$alert.show({
                    messageType: 'danger',
                    strongMessage: 'Field mapping for the target field is already available',
                    message: ''
                });*/
            } else {

                apiCalls.postDecisionFieldMapping({
                    Source_Object_Name: SelectedObject.Object_Name,
                    Source_Object_Id: SelectedObject.Object_Id,
                    Source_Field_Name: fieldMapping.selSdfcObj.label,
                    Source_Field_Id: fieldMapping.selSdfcObj.name,
                    Source_Field_Type: fieldMapping.selSdfcObj.type,
                    Target_Field_Name: fieldMapping.selElqObj.name,
                    Target_Field_Id: fieldMapping.selElqObj.internalName,
                    Target_Field_Type: fieldMapping.selElqObj.dataType,
                    Query_Relationship: fieldMapping.selParentFormat
                }).then(function(result) {
                    fieldMapping.mappedContactFields = result.data;
                    if(fieldMapping.history){
                        fieldMapping.sdfcConFields = fieldMapping.history.sdfcConFields;
                    }
                    fieldMapping.mappedContactFields.forEach(function(item) {
                        fieldMapping.sdfcConFields = _.reject(fieldMapping.sdfcConFields, {
                            name: item.Source_Field_Id
                        });
                        fieldMapping.eloquaFields = _.reject(fieldMapping.eloquaFields, {
                            internalName: item.Target_Field_Id
                        });
                    });

                    status.fieldMappingCount = fieldMapping.mappedContactFields.length;



                    fieldMapping.selSdfcObj = null;


                    fieldMapping.emptyElqField();
                    fieldMapping.emptySFDCField();
                    saveSegment();
                    /*$alert.show({
                        messageType: 'success',
                        strongMessage: 'Fields mapped successfully',
                        message: ''
                    });*/
                    $(document).scrollTop(0);
                })
            }


        }


        fieldMapping.emptyElqField = function() {
            fieldMapping.selElqObj = {
                shownValue: ''
            }
        }
        fieldMapping.emptySFDCField = function() {
            fieldMapping.selSdfcObj = {
                shownValue: ''
            }
        }




        fieldMapping.onSelectingObject = function() {
            setTimeout(function() {
                $('#addObject').focus();
            }, 2);
        }

        fieldMapping.icon = function(param) {
            if (selectedParam.replace('-', '') == param) {
                if (selectedParam.indexOf('-') == -1) {
                    return ' glyphicon-triangle-bottom';
                } else {
                    return ' glyphicon-triangle-top';
                }
            } else {
                return '';
            }
        }


        fieldMapping.close = function() {
            $uibModalInstance.dismiss('cancel');
        };

                    
        fieldMapping.mapInstanceObject(SelectedObject);

        fieldMapping.proceedAnywayDeleteField = function() {
            fieldMapping.deleteContactField(fieldMapping.selectedFieldIndex, fieldMapping.selectedMappendContactField);
        }

        fieldMapping.deleteContactField = function($index, mappedContactField) {
            apiCalls.deleteDecisionFieldMapping({id:mappedContactField.id})
                .then(function(result) {
                    fieldMapping.mappedContactFields.splice($index, 1);
                    status.fieldMappingCount = fieldMapping.mappedContactFields.length;

                    if(fieldMapping.history){
                        fieldMapping.sdfcConFields = fieldMapping.history.sdfcConFields;
                        fieldMapping.eloquaFields = fieldMapping.history.eloquaFields;
                    }
                    
                    fieldMapping.mappedContactFields.forEach(function(item) {
                        fieldMapping.sdfcConFields = _.reject(fieldMapping.sdfcConFields, {
                            name: item.Source_Field_Id
                        });
                        fieldMapping.eloquaFields = _.reject(fieldMapping.eloquaFields, {
                            internalName: item.Target_Field_Id
                        });
                    });

                    saveSegment();
                })

        }

        fieldMapping.confirmDeleteField = function($index, mappedContactField) {
            if(($index == 0 && !fieldMapping.sfCtrlStatus) || !$rootScope.license){

            } else {

                $popup.show({
                        header: "Deleting Mapped Fields",
                        message: "Are you sure you want to delete this mapped Field ?",
                        okText: 'Yes'
                    })
                    .onOk(function() {
                        fieldMapping.selectedFieldIndex = $index;
                        fieldMapping.selectedMappendContactField = mappedContactField;

                        fieldMapping.proceedAnywayDeleteField();


                    })
                    .onCancel(function() {

                    });
            }

        }
    });