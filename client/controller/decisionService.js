segmentationApp
.controller('decisionService',function($scope,$timeout,$filter,apiCalls,$http,queryRecSer,$popup,$q,$uibModal, $document,$segmentationLoader,$location) {
	var Feeder = $scope.Feeder = {};
	var urlParams = $location.search();

	Feeder.fieldMappingStatus = {};

	Feeder.fdrGrndClass = 'col-md-9 col-sm-9 col-xs-8';

	/*Fixing Right Pane*/
	Feeder.fdrGrndClass = 'col-md-7 col-sm-6 col-xs-4';
	// Feeder.selectedFilter.rightPaneStatus = true;



	var fl=$filter('filter'),
		orBy=$filter('orderBy'),
		innerFunctions = {};


	Feeder.init = function() {
		Feeder.executionStatus = 'DRAFT';
		Feeder.filterCriterias = [];
		Feeder.queryItems = [];
		Feeder.extSystems = [];

		Feeder.frequency = ['Run Once','30 Minutes','1 Hour','1 Day','1 Week'];

		Feeder.invalidFieldsStat = false;



		Feeder.criteriaOptions = [{
			id:1,
			name:'Compare (#objectName) Fields',
			actualName:'',
			type: 'filter',
			imageSRC:'images/CLRF.png'
		},{
			id:2,
			name:'Has Related Object Fields',
			actualName:'',
			imageSRC:'images/HROF.png'
		}];

		/*For Testing saved json */
		/*$http.get('exampleJsons/savedJson.json')
			.then(function(result) {
				Feeder.getSegment(result.data);
			});*/

		/****************/


		$segmentationLoader.show();
		apiCalls.getFeederExtSystems(urlParams.key)
		.then(function(result){
			Feeder.extSystems = _.reject(result.data.getActiveInstance,{Status:'Inactive'});

			Feeder.executionStatus = result.data.segmentStatus;


			
			if(Feeder.executionStatus == 'INACTIVE'){
				Feeder.extSystems = Feeder.inActiveExtSystems.slice();
			}



			Feeder.inActiveExtSystems = _.filter(result.data.getActiveInstance,{
				Status:'Inactive'
			});


			return apiCalls.getExistingFeeder();
		})
		.then(function(result) {
			if(result.data.status == 1 && result.data.SegmentJSON.length){
				Feeder.segmentID = result.data.SegmentId;
				Feeder.executionStatus = result.data.segmentStatus;
				
				if(Feeder.executionStatus == 'INACTIVE'){
					Feeder.extSystems = Feeder.inActiveExtSystems.slice();
				}


				Feeder.getSegment(JSON.parse(result.data.SegmentJSON));


				/*$timeout(function() {
					apiCalls.updateContactsCount({
						instanceConfigId:Feeder.filterCriterias[0].selExtSys.Source_Instance_Config_Id,
						segmentID:Feeder.segmentID
					})
					.then(function(result) {
						Feeder.countProp = result.data;
						SOQLErrored();
						$segmentationLoader.hide();
					});

				}, 1000);*/
			}else{
				Feeder.draftName = 'Decision Service '+JSON.stringify(new Date()).replace(/"/g, '');
				$segmentationLoader.hide();
			}

		})
	}
	Feeder.init();



	Feeder.showFieldMapping = function() {


		var modalInstance = $uibModal.open({
			animation: true,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'templates/fieldMapping.html',
			size: 'view-segment',
			controller:'fieldMappingCtrl',
			resolve:{
				SelectedObject:Feeder.selObject,
				status:Feeder.fieldMappingStatus,
				sfCtrlStatus:Feeder.sfCtrlStatus,
				saveSegment:function() {
					if(Feeder.segmentID)
						return Feeder.saveSegment;
					else
						return angular.noop;
				}
			}
		});

		/*modalInstance.result.then(function() {
			Feeder.saveSegment();
		}, function(error) {

		});*/
	}


	


	Feeder.modalDsSyncReports = function() {

	    var modalInstance = $uibModal.open({

	        animation: true,
	        ariaLabelledBy: 'modal-title',
	        ariaDescribedBy: 'modal-body',
	        templateUrl: 'dsSyncReports.html',
	        size: 'view-segment',
	        controller:'dsyncReportsCtrl',
	        resolve: {
	            SyncReports: function() {
	            	$segmentationLoader.show();
	            	return apiCalls.dsGetSyncReports()
	            	.finally(function() {
	            		$segmentationLoader.hide();
	            	});
	            }
	        }
	    });
		modalInstance.result.then(function (selectedItem) {
			
		}, function(error) {

		});
	}


	Feeder.modalSyncReports = function() {

		/*For trial only Need to be deleted in server*/
		// Feeder.segmentID = 1;
		/**/
		$segmentationLoader.show();

	    var modalInstance = $uibModal.open({
	        animation: true,
	        ariaLabelledBy: 'modal-title',
	        ariaDescribedBy: 'modal-body',
	        templateUrl: 'viewSyncReports.html',
	        size: 'view-segment',
	        controller:'viewSyncReportsCtrl',
	        controllerAs:'SyncReports',
	        resolve: {
	        	info:function() {
					$segmentationLoader.hide();
	        	}
	            /*SegmentData: function() {
	            	if(Feeder.draftName && Feeder.segmentID){
	            		segmentData.name = Feeder.draftName;
	            		segmentData.id = Feeder.segmentID;
	                	return segmentData;
	            	}
	                else
	                	return {};
	            }*/
	        }
	    });
	};

	/*Update Contacts Count*/
	Feeder.updateContactCount = function() {
		/*$segmentationLoader.show();
		apiCalls.updateContactsCount({
			instanceConfigId:Feeder.filterCriterias[0].selExtSys.Source_Instance_Config_Id,
			segmentID:Feeder.segmentID
		})
		.then(function(result) {
			Feeder.countProp = result.data;
			SOQLErrored();
			$segmentationLoader.hide();
		});*/
	}

	Feeder.getObjects = function(getDataObj,extSys) {
		/*if(Feeder.selectedFilter)
			Feeder.selectedFilter.selExtSys = extSys;*/


		Feeder.selExtSys = extSys;
		Feeder.SFDCObjects = [];
		Feeder.selObject = null;
		Feeder.filterCriterias = [];
		Feeder.countProp = null;

        var defer = $q.defer();
		$timeout(function() {
			var selExtSys = (!getDataObj)?Feeder.selExtSys:getDataObj;


			Feeder.sfCtrlStatus = (selExtSys.sfCtrlStatus == 'true')?true:false;
			/*Backup*/
			// apiCalls.getSelectedObjects({
			apiCalls.dsGetSelectedObjects({
				Source_Instance_Config_Id:(selExtSys.Source_Instance_Config_Id),
				sfCtrlStatus:Feeder.sfCtrlStatus
			})
			.then(function(result){
				Feeder.SFDCObjects = result.data;

				Feeder.SFDCObjects = _.sortBy(Feeder.SFDCObjects,['Object_Name']);


				return apiCalls.dsGetEloquaRelatedData({
					Source_Instance_Config_Id:(selExtSys.Source_Instance_Config_Id)
				})
			})			
			.then(function(result) {
				Feeder.eloquaFields = result.data.eloquaFields;
				Feeder.eloquaComparingOperators = result.data.eloquaComparingOperators;
				if(getDataObj){
					defer.resolve();
				}

				if(getDataObj){
					defer.resolve();
					setTimeout(function() {
						Feeder.updateContactCount();
					}, 1000);
				}
			});

			/*******************************/
		}, 200);

		return defer.promise;
	}

	var prevSelObj;
	Feeder.onChangeObject = function(getDataObj,object) {
		if(!getDataObj){
			Feeder.selObject =  object;
		}

		if(prevSelObj){

			unchangedPrevObj = angular.copy(prevSelObj);
			apiCalls.getDecisionFieldMapping({
	                Source_Instance_Config_Id: unchangedPrevObj.Source_Instance_Config_Id,
	                Source_Primary_Object_Id: unchangedPrevObj.Source_Primary_Object_Id,
	                Source_Object_Id: unchangedPrevObj.Object_Id,
                    sfCtrlStatus:Feeder.sfCtrlStatus,
	                forDeleting:true
	            })
	            .then(function(result) {

	            	var MappedFields = Feeder.MappedFields = result.data.MappedFields;
					$segmentationLoader.hide();


	            	if(MappedFields && MappedFields.length){
	            		$popup.show({
				            header:"Customized Field Mapping",
				            message:'The object already is having customized field mapping. In case you change the object you will lose the field mapping changes.',
				            okText:'OK'
				        })
				        .onOk(function() {
				        	var deleted = 0;
							$segmentationLoader.show();

				        	MappedFields.forEach(function(mappedContactField) {
					            apiCalls.deleteDecisionFieldMapping(mappedContactField.id)
					                .then(function(result) {
					                	deleted++;
					                	if(deleted == MappedFields.length){
											setTimeout(function() {
												$segmentationLoader.hide();
						                		$popup.show({
										            header:"Customized Field Mapping",
										            message:'All customized fields deleted successfully.',
										            okText:'OK'
										        })
										        .onOk(function() {
										        	Feeder.getSFDCObjectRelatedData(getDataObj,object);
										        })
										        .onCancel(function() {
										        	
										        });
											}, 1000);
					                	};
					                });
				        	})

				        })
				        .onCancel(function() {
				        	prevSelObj = Feeder.selObject =  unchangedPrevObj;
				        });
	            	} else {
	            		Feeder.getSFDCObjectRelatedData(getDataObj,object);
	            	}
	            });

		} else {
			// Feeder.onChangeObject(getDataObj,object);
			if(Feeder.segmentID){

	            Feeder.getSFDCObjectRelatedData(getDataObj,object);
				/*apiCalls.getDecisionFieldMapping({
	                Source_Instance_Config_Id: unchangedPrevObj.Source_Instance_Config_Id,
	                Source_Primary_Object_Id: unchangedPrevObj.Source_Primary_Object_Id,
	                Source_Object_Id: unchangedPrevObj.Object_Id
	            })*/
			} else {
				apiCalls.deleteAllDecisionFieldMapping()
				.then(function(result) {
	            	Feeder.getSFDCObjectRelatedData(getDataObj,object);
				})
				.catch(function(error) {
					console.log(error);
				})
			}
		}




		prevSelObj = Feeder.selObject;
	}


	Feeder.getSFDCObjectRelatedData = function(getDataObj,object) {



		if(Feeder.selectedFilter){
			Feeder.selectedFilter.selExtSys = Feeder.selExtSys;
			Feeder.selectedFilter.selObject = Feeder.selObject;

			/*Data filtering*/
			Feeder.countProp = null;
		}

		if(Feeder.selObject){
			prevSelObj = Feeder.selObject;
		}

		var defer = $q.defer()
		Feeder.selFields = [];
		Feeder.operators = [];
		Feeder.fieldOperatorUI = [];
		Feeder.invalidFieldsStat = false;
		$timeout(function() {
			var selObject = (!getDataObj)?Feeder.selObject:getDataObj;
			console.log(getDataObj);


			selObject.sfCtrlStatus = Feeder.sfCtrlStatus;

			$segmentationLoader.show();


			apiCalls.dsObjectFields(selObject)
			.then(function(result){

				
				Feeder.selObject = result.data.selSFDCObject;
				Feeder.EmailFields =  result.data.EmailFields;

				// console.log(Feeder.selObject);
				// console.log(prevSelObj);
				if(getDataObj == 0){
					Feeder.MappedFields = null;
					Feeder.continueSaving = false;
				}



				if(!Feeder.MappedFields || (Feeder.MappedFields && Feeder.MappedFields.length == 0)){
					apiCalls.getDecisionFieldMapping({
		                Source_Instance_Config_Id: Feeder.selObject.Source_Instance_Config_Id,
		                Source_Primary_Object_Id: Feeder.selObject.Source_Primary_Object_Id,
		                Source_Object_Id: Feeder.selObject.Object_Id,
	                    sfCtrlStatus:Feeder.sfCtrlStatus,
		                forDeleting:true
		            })
		            .then(function(result) {
		            	Feeder.MappedFields = result.data.MappedFields;
						

						$segmentationLoader.hide();


		            	if(!Feeder.MappedFields.length && !Feeder.sfCtrlStatus && Feeder.EmailFields.length){

							apiCalls.deleteAllDecisionFieldMapping()
							.then(function(result) {
								console.log(result);
							})
							.catch(function(error) {
								console.log(error);
							})
                			$("#emailFields").modal({
                				backdrop:'static'
                			});
		            	}
		            });
				} else {
					$segmentationLoader.hide();
				}


				Feeder.relatedObjectData = result.data;
				Feeder.selFields = Feeder.relatedObjectData.objectsList[0].allFields;
				Feeder.pickListsData = Feeder.relatedObjectData.objectsList[0].picklistValuesArray;
				Feeder.pickListsData.forEach(function(metaPicklist) {
					if(metaPicklist.picklist && metaPicklist.picklist.length){
						metaPicklist.picklist.forEach(function(picklistItem) {
							if(!picklistItem.label){
								picklistItem.label = picklistItem.value;
							}
						});
					}
				});

				Feeder.operators = Feeder.relatedObjectData.operators;
				Feeder.fieldOperatorUI = Feeder.relatedObjectData.fieldOperatorUI;


				if(getDataObj){
					defer.resolve(Feeder.relatedObjectData);
				}
			});
		}, 0);
		return defer.promise;
	}


	Feeder.continueSaving = true;
	Feeder.saveEmailField = function(index) {

        apiCalls.postDecisionFieldMapping({
            Source_Field_Id: Feeder.EmailFields[index].emailFieldName,
            Target_Field_Id: 'C_EmailAddress',
            Source_Object_Id: Feeder.selObject.Object_Id,
            Source_Object_Name: Feeder.selObject.Object_Name,

            Source_Field_Name: Feeder.EmailFields[index].emailFieldLabel,
            Source_Field_Type: 'email',
            Target_Field_Name: 'Email Address',
            Target_Field_Type: 'text',
            Query_Relationship: Feeder.selObject.Object_Id
        }).then(function(result) {
        	Feeder.MappedFields = result.data;
        	if(Feeder.segmentID && Feeder.continueSaving){
        		Feeder.saveSegment();
        		Feeder.continueSaving = true;
        	}
        });

	}

	var filterNaming = function(filterName,type,index) {
		this.index = (this.index)?this.index+1:1;

		var name = (filterName)?filterName: ' ' + type + ' Criteria '+ this.index;
		var existingFilter = _.find(Feeder.filterCriterias,{name:name});
		if(existingFilter){
			name = name + "a";
		} else {

		}
		return name;
	}

	Feeder.setFilter = function(filterName, type) {
		Feeder.selFilInd = -1;
		var index = (Feeder.filterCriterias.length)?Feeder.filterCriterias.length+1:1;


		this.filterIndex = (this.filterIndex)?this.filterIndex+1:1;



		var filterCritObj = {
			filterIndex:index,
			type: type,
			name: filterNaming(filterName,type,index)//(filterName)?filterName: ' ' + type + ' Criteria '+ this.filterIndex
		}

		if(type == 'SOQL'){
           filterCritObj.valid = false;
           filterCritObj.validated = false;
		}
		Feeder.filterCriterias.push(filterCritObj);





		if(Feeder.countProp){
			if(Feeder.countProp.counts)
				Feeder.countProp.counts.push('--');
			else{

			}
		}
		return filterCritObj;

	}

	Feeder.filterContext = [
		['Delete',function unSetFilter($itemScope, $event, model) {


			_.remove(Feeder.filterCriterias,{filterIndex:model.filterIndex});
			Feeder.filterCriterias.forEach(function(item,index) {
				item.filterIndex = index+1;
			});

			if(Feeder.selectedFilter && Feeder.selectedFilter.filterIndex === model.filterIndex){
				Feeder.queryItems = [];
				Feeder.selectedFilter.type = null;
				Feeder.selectedFilter.selObject = null;
			}

			Feeder.countProp = null;


			var lastIndex = Feeder.filterCriterias.length-1;
			if(lastIndex>=0)
				Feeder.selectFilter(Feeder.filterCriterias[lastIndex],lastIndex);
		}]
	];


	Feeder.selectFilter = function(seldFilter,index) {
		Feeder.selectedFilter = seldFilter;
		Feeder.selectedFilter.selExtSys = Feeder.selExtSys;
		Feeder.selectedFilter.selObject = Feeder.selObject;
		Feeder.selFilInd = index;
		if(seldFilter.type != 'SOQL'){
			Feeder.queryItems = (Feeder.selectedFilter.elements)?Feeder.selectedFilter.elements:[];
		} else {
			Feeder.queryItems = null;
		}

	}

	Feeder.setQuery = function(crit, getStatus) {
	    var index = (Feeder.queryItems.length) ? Feeder.queryItems.length + 1 : 1;
	    var queryItems;

	    crit.selObject = Feeder.selectedFilter.selObject;
	    crit.objectName = Feeder.selectedFilter.selObject.Object_Name;
	    crit.fields = Feeder.selFields;
	    crit.pickListsData = Feeder.pickListsData;
	    crit.operators = Feeder.operators;
	    crit.fieldOperatorUI = Feeder.fieldOperatorUI;
	    crit.eloquaFields = Feeder.eloquaFields;
	    crit.eloquaComparingOperators = Feeder.eloquaComparingOperators;


	    var toBeSanitized = angular.copy(crit);
	    toBeSanitized.querySIndex = index;


	    if (index == 1) {
	        queryElementObj = {
	            queryIndex: index,
	            type: 'element',
	            toBeSanitized: toBeSanitized,
	            isNot: false,
	            value: ''
	        }
	        Feeder.queryItems.push(queryElementObj);
	    } else {
	        queryElementObj = {
	            queryIndex: index,
	            type: 'element',
	            toBeSanitized: toBeSanitized,
	            logicalOperator: 'AND',
	            isNot: false,
	            value: '',
	            operatorSwitch: 1 //1 refers to AND 0 refers to OR
	        }
	        Feeder.queryItems.push(queryElementObj);
	    }

	    Feeder.queryItems = orBy(Feeder.queryItems, 'queryIndex');
	    queryRecSer.checkAmbiguity();

	    if (Feeder.selectedFilter)
	        Feeder.selectedFilter.elements = Feeder.queryItems;
	    if (getStatus) {
	        Feeder.queryItems = [];
	        return queryElementObj;
	    }
	}


	Feeder.clicked = function(ev) {

		/*if(queryRecSer.poppedObject.outside){
			queryRecSer.poppedObject.show = false;
		}*/

		if(ev.target!=document.querySelector('#frequency')){
			$('#frequency').find('.dropdown-menu').hide();
		}
	}


	$('#frequency').on('click',function(event) {
		$(this).find('.dropdown-menu').toggle();
	});




	Feeder.freezeObject = function() {
		if(!Feeder.filterCriterias.length)
			return false;
		else{
			var totalQueries = 0;
			Feeder.filterCriterias.forEach(function(item) {
				if(item.elements)
					totalQueries = totalQueries+item.elements.length;
			});
			return totalQueries;

		}
	}

	Feeder.groupButtonEnable = function() {
		if(queryRecSer.selQueriesArray && queryRecSer.selQueriesArray.length>1){
			var arr = fl(queryRecSer.selQueriesArray,{groupID:queryRecSer.selQueriesArray[0].groupID},true);
			if(queryRecSer.selQueriesArray[0].groupID == 0){
				return (arr.length == queryRecSer.selQueriesArray.length);
			}else{
				if(arr.length == queryRecSer.selQueriesArray.length){
					return queryRecSer.selGroup.elements.length>2 && arr.length<queryRecSer.selGroup.elements.length;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}

	Feeder.getUngroupEnable = function() {
		return queryRecSer.unGroupButtonStatus;
	}


	var groupElements = function(arrToBeFoundFrom) {
		/*To be filled in group*/
		var groupedArray = [];
		arrToBeFoundFrom.forEach(function(qItem) {

			var pr = fl(queryRecSer.selQueriesArray,{
				index:qItem.queryIndex
			})[0];

			qItem.toBeSanitized.isSelected = false;

			if(pr){
				groupedArray.push(angular.copy(qItem));
			}
		});

		/*Indexing grouped array*/
		groupedArray.forEach(function(item,index) {
			item.queryIndex = index+1;
		})

		/*Creation of Group*/
		var indexGrouped = queryRecSer.selQueriesArray[0].index;
		var item = fl(arrToBeFoundFrom,{queryIndex:indexGrouped})[0];
		item.value = undefined;
		item.type = 'group';
		item.toBeSanitized = {
			groupID : Date.now(),
			querySIndex:item.queryIndex
		}

		/*Assigned to Group*/
		groupedArray[0].operatorSwitch = groupedArray[0].logicalOperator = undefined;
		item.elements = groupedArray;
		queryRecSer.checkAmbiguity();

	}



	Feeder.groupQueries = function() {

		groupElements(queryRecSer.selGroup.elements);

		queryRecSer.selQueriesArray = _.drop(queryRecSer.selQueriesArray);
		_.pullAllWith(queryRecSer.selGroup.elements, queryRecSer.selQueriesArray, function(a,b) {
			return a.queryIndex==b.index;
		});


		queryRecSer.selGroup.elements.forEach(function(item,index) {
			item.queryIndex = index+1;
			item.toBeSanitized.querySIndex = index+1;
			item.toBeSanitized.isSelected = false;
		});



		queryRecSer.maxElemsForGrp = 0;

		queryRecSer.unSelectAll();
		queryRecSer.checkAmbiguity();

	}

	Feeder.unGroup = function() {
		if(!queryRecSer.selQueriesArray.length || !queryRecSer.selGroup)
			return;


		var unGroupIndex = queryRecSer.selQueriesArray[0].index;

		var firstSliceIndex = _.findIndex(queryRecSer.selGroup.elements,{queryIndex:unGroupIndex});

		var firstSlicedArray  = _.slice(queryRecSer.selGroup.elements,0,firstSliceIndex);

		var groupItem = fl(queryRecSer.selGroup.elements,{queryIndex:unGroupIndex})[0];

		if(groupItem.elements)
			var secondSlicedArray = _.concat(groupItem.elements,
								_.slice(queryRecSer.selGroup.elements,unGroupIndex));

		secondSlicedArray.forEach(function(item,index) {
			if(groupItem.logicalOperator && index == 0){
				item.logicalOperator = groupItem.logicalOperator;
				item.operatorSwitch = groupItem.operatorSwitch;
			}

			item.queryIndex = index + unGroupIndex;
			item.toBeSanitized.querySIndex = index+unGroupIndex;
			item.toBeSanitized.isSelected = false;
		});

		if(queryRecSer.selGroup.filterIndex){
			queryRecSer.selGroup.elements = Feeder.queryItems =_.concat(firstSlicedArray,secondSlicedArray);
		}else{
			queryRecSer.selGroup.elements = _.concat(firstSlicedArray,secondSlicedArray);
		}

		queryRecSer.unSelectAll();
		queryRecSer.checkAmbiguity();
	}

	/*Checking recursive Ambiguity*/
	var recursiveAmbiguity = function(arr,statusProp) {
		var tailArr = _.tail(arr);
		if(tailArr.length){
			var firstOpSwitch = tailArr[0].operatorSwitch;
			var count = 0;
			tailArr.forEach(function(item) {
				if(firstOpSwitch == item.operatorSwitch){
					count++;
				}
				if(item.type == 'group'){
					recursiveAmbiguity(item.elements,statusProp);
				}
			});
			statusProp.ambStatusArr.push({
				status:count!==tailArr.length
			});
		}
	}

	queryRecSer.checkAmbiguity = function() {
		this.checkAmbiguity.ambStatusArr = [];
		recursiveAmbiguity(Feeder.queryItems,this.checkAmbiguity);
		var ambiguityExists = _.filter(this.checkAmbiguity.ambStatusArr,{status:true})[0];
		if(ambiguityExists){
			Feeder.selectedFilter.ambiguityExists = true;
		} else {

			Feeder.selectedFilter.ambiguityExists = false;
		}
	}

	queryRecSer.deleteQuery = function(queryIndex) {
		_.remove(queryRecSer.removeItemsSelGroup.elements,{queryIndex:queryIndex});
		$timeout(function() {
			queryRecSer.removeItemsSelGroup.elements.forEach(function(item,index) {
	            item.queryIndex = index+1;
	            item.toBeSanitized.querySIndex = index+1;
	            item.toBeSanitized.isSelected = false;
	            if(item.queryIndex == 1){
	            	item.logicalOperator = null;
	            	item.operatorSwitch = null;
	            }
	        });
	        $timeout(function() {
	        	if(queryRecSer.removeItemsSelGroup.elements.length == 1)
	        		Feeder.unGroup();
	        }, 0);
		}, 0);
	}


	var changeToValidValue = function(value,type) {
		var returnVal=value;
		if(type.indexOf('date')>-1){
			returnVal = new Date(value);
			if(returnVal.toString() === 'Invalid Date'){
				returnVal = value;
			}
		}
		return returnVal;
	}
	/* Get Segment */
	var jsonToQueryElementsBuilder = function(sourceArr,destinationArr,global) {
		sourceArr.forEach(function(item,index) {

			/* Differentiate Compare and Has related */
			var crit;

			if(item.elementType == 1 || item.type == 'elqcmp'){
				if(item.relationship)
					crit = angular.copy(Feeder.criteriaOptions[1]);
				else
					crit = angular.copy(Feeder.criteriaOptions[0]);
			}
			else
				crit = angular.copy(Feeder.criteriaOptions[1]);



			crit.actualName      = crit.name.replace("(#objectName)",global.selObject.Object_Name);
			crit.objectName 	 = global.selObject.Object_Name;
			destinationArr[index] = {};

			if( item.type == 'element' || item.type == 'elqcmp' ){
				destinationArr[index].toBeSanitized = crit;
				Feeder.getSFDCObjectRelatedData(global.selObject)
				.then(function(data) {
					var flds = destinationArr[index].toBeSanitized.fields = data.objectsList[0].allFields;
					destinationArr[index].toBeSanitized.pickListsData = data.objectsList[0].picklistValuesArray;
					destinationArr[index].toBeSanitized.operators 		  = data.operators;
					destinationArr[index].toBeSanitized.fieldOperatorUI   = data.fieldOperatorUI;
					destinationArr[index].toBeSanitized.eloquaFields   = Feeder.eloquaFields;
					destinationArr[index].toBeSanitized.eloquaComparingOperators   = Feeder.eloquaComparingOperators;
					
					/*TODO*/
					if(item.elementType == 1 || item.type == 'elqcmp')
						destinationArr[index].selectedField = _.filter(flds,{name:item.fieldName},true)[0];
					else{
						var copyItem = angular.copy(item);
						innerFunctions.getSavedHasRel = function() {
							return copyItem;
						};

						destinationArr[index].parentRelationShipString = item.relationship;
						destinationArr[index].toBeSanitized.selObject = global.selObject;
						destinationArr[index].fieldName = item.fieldName;
					}

					/*Indexes*/
					destinationArr[index].queryIndex = index+1;
					destinationArr[index].toBeSanitized.querySIndex = index+1;

					/*property*/
					destinationArr[index].type = item.type;
					destinationArr[index].logicalOperator = item.operator;

					destinationArr[index].operatorSwitch = (item.operator=='AND')?true:false;
					if(item.condition.value){
						destinationArr[index].value = changeToValidValue(item.condition.value,item.condition.type);
					} else if(item.condition.start){
						destinationArr[index].start = changeToValidValue(item.condition.start,item.condition.type);
						destinationArr[index].end = changeToValidValue(item.condition.end,item.condition.type);
					}

					var condition = item.condition;

					destinationArr[index].assignedOperator = _.filter(data.operators,{
						Field_Type_Name:condition.type,
						Operator_Name:condition.operator.replace('not','')
					})[0];

					if(condition.operator.indexOf('not')>-1){
						destinationArr[index].isNot = true;
					}


					/*treeview added for has related*/
					if(item.elementType == 2){
						destinationArr[index].treeViewData = item.treeViewData;
					}

					if(item.type == 'elqcmp'){

						if(item.condition.isNot){
							destinationArr[index].isNot = true;
						}

						destinationArr[index].selectedElqField = _.find(Feeder.eloquaFields,{internalName:item.condition.elqField});
						destinationArr[index].selElqCmpOperator = _.find(Feeder.eloquaComparingOperators,{operatorName:item.condition.operator});
						destinationArr[index].type = 'elqcmp';

						if(item.relationship){
							destinationArr[index].treeViewData = item.treeViewData;
							destinationArr[index].toBeSanitized.id = 3;
							var copyItem = angular.copy(item);
							innerFunctions.getSavedHasRel = function() {
								return copyItem;
							};

							destinationArr[index].parentRelationShipString = item.relationship;
							destinationArr[index].toBeSanitized.selObject = global.selObject;
							destinationArr[index].fieldName = item.fieldName;
						}

					}
				});
			} else {
				destinationArr[index].value = undefined;
				destinationArr[index].queryIndex = index+1;
				destinationArr[index].toBeSanitized = {
					groupID : Date.now(),
					querySIndex:index+1
				}

				destinationArr[index].logicalOperator = item.operator;
				destinationArr[index].operatorSwitch = (item.operator=='AND')?true:false;

				destinationArr[index].type = item.type;
				destinationArr[index].elements = [];
				jsonToQueryElementsBuilder(item.elements,destinationArr[index].elements,global);
			}


		})
	}
	Feeder.getSegment = function(formattedJSON,tempStat) {

		$timeout(function() {

			var selExtSys = fl(Feeder.extSystems,{
				Source_Instance_Config_Id:formattedJSON.sourceInstanceConfigId
			})[0];

			Feeder.getObjects(selExtSys)
			.then(function() {


				if(tempStat){
					Feeder.draftName = 'Untitled Segment';
					Feeder.setFrequency = 'Run Once';
				}else{
					Feeder.draftName = formattedJSON.name;
					Feeder.setFrequency = formattedJSON.setFrequency;
				}

				formattedJSON.elements.forEach(function(filterItem) {


					var filterCritObj = Feeder.setFilter(filterItem.filter.name, filterItem.filter.type);
					Feeder.selExtSys  = filterCritObj.selExtSys = selExtSys;


					/*----------------------------*/
					// filterCritObj.eloquaFields = Feeder.eloquaFields;




					Feeder.selObject  = filterCritObj.selObject = fl(Feeder.SFDCObjects,{
						Object_Id:filterItem.MasterObject
					},true)[0];


					filterCritObj.retrievalID = filterItem.filter.id;
					if(filterItem.filter.type == 'SOQL'){
						filterCritObj.query = filterItem.filter.query;
						filterCritObj.valid = true;
						filterCritObj.validated = true;
					} else {
						filterCritObj.elements = [];
						jsonToQueryElementsBuilder(filterItem.filter.elements,filterCritObj.elements,filterCritObj,0)
					}
				});

				$segmentationLoader.hide();


			});

		}, 0);

	}



	/* Saving Feeder
	/* Accept recursive Arrays
	/* queryElementsJsonBuild()*/

	var valueFilter = function(value,type) {
		if(type=='date'/*type.indexOf('date')>-1*/){
			value = $filter('date')(value, 'yyyy-MM-dd');
		}
		return value;
	}

	var emptyValidation = function(item) {
		var startEndStatus;
		var value = (item.value)?item.value.toString():'';
		if(!value.length && item.type == 'element'){
			startEndStatus = (item.start && item.end);


			if(typeof(item.start) !== 'object' &&typeof(item.end) !== 'object' ){
				startEndStatus = startEndStatus && (item.start.length>0) && (item.end.length > 0);
			}

			if(!startEndStatus)
				throw new Error("Values not available");
		}


	}

	var queryElementsToJsonBuilder = function(sourceArr,destinationArr) {
		if(!sourceArr.length)
			throw new Error('No query Present');
		sourceArr.forEach(function(item,index) {
			if(item.type=='element'||item.type=='elqcmp'){
				if(!item.selectedField && !(Feeder.selFilInd >- 1) && innerFunctions.getSavedHasRel && (item.toBeSanitized.id == 2 || item.toBeSanitized.id == 3)){
					destinationArr[index] = innerFunctions.getSavedHasRel();
					return;
				}

				destinationArr[index] = {
					type:item.type,
					elementType:(item.type == 'element')?item.toBeSanitized.id:null,
					fieldName:item.selectedField.name,
					operator:item.logicalOperator||null,
					relationship:item.parentRelationShipString,
					treeViewData:item.toBeSanitized.treeViewData
				}

				if(item.type === 'element'){



					destinationArr[index].condition={
						type: item.assignedOperator.Field_Type_Name,
						operator: item.assignedOperator.Operator_Name
					}


					if(item.assignedOperator.ui_id !== 1)
						emptyValidation(item);


					if(item.value){
						destinationArr[index].condition.value = valueFilter(item.value,item.assignedOperator.Field_Type_Name);
					}




					if(item.start && item.end){
						destinationArr[index].condition.start = valueFilter(item.start,item.assignedOperator.Field_Type_Name);
						destinationArr[index].condition.end = valueFilter(item.end,item.assignedOperator.Field_Type_Name);
					}

					if(item.isNot){
						// if(destinationArr[index].condition.operator.indexOf('not')!=-1)
							destinationArr[index].condition.operator = "not"+destinationArr[index].condition.operator;
					}
				} else if(item.type === 'elqcmp') {
					destinationArr[index].condition={
						type: item.selectedField.type,
						operator: item.selElqCmpOperator.operatorName,
						elqField:item.selectedElqField.internalName,
						isNot:item.isNot
					}
					
				}

			}else{
				destinationArr[index] = {};
				destinationArr[index].type = item.type;
				destinationArr[index].elements = [];
				destinationArr[index].operator = item.logicalOperator;
				queryElementsToJsonBuilder(item.elements,destinationArr[index].elements);
			}
		});
	}

	Feeder.getDisableSave = function() {



		var elqCmpStatus = (_.find(Feeder.queryItems,{type:'elqcmp'}))?true:false;
		
		if(Feeder.sfCtrlStatus){
			if(!elqCmpStatus/*|| !Feeder.fieldMappingStatus.fieldMappingCount*/){
				return true;
			}
		} else {

			if(Feeder.MappedFields && !Feeder.MappedFields.length)
				return true;
		}

		if(Feeder.selectedFilter && Feeder.selectedFilter.ambiguityExists)
			return true;

		if(Feeder.executionStatus == 'INACTIVE')
			return true;


		var index = _.findIndex(Feeder.filterCriterias, function(obj){
			if(obj.type === 'SOQL'){
            	return obj.valid == false || obj.validated == false;
			}
            else{
            	return false;
            }
		});
		return !(Feeder.filterCriterias.length && !Feeder.disableSave) || index >= 0;
	}

	Feeder.viewContactsDisable = function() {
		if(Feeder.segmentID){
			return false;
		}else{
			return true;
		}
		/*if(!Feeder.draftName.length || Feeder.draftName == 'Untitled Segment'){
			return true;
		}
		return false;*/
	}


	var draftNameUnsaved = function() {
		$('.feeder-pane-header input').addClass('error');
		var heavyCounts = 0;
		var heavyTimeouts = function() {
			setTimeout(function() {
				if($('.feeder-pane-header input').hasClass('heavy')){
					$('.feeder-pane-header input').removeClass('heavy');
				}
				else{
					$('.feeder-pane-header input').addClass('heavy');
				}

				if(heavyCounts<5){
					heavyTimeouts();
					heavyCounts++;
				}
			}, 500);
		}
		heavyTimeouts();
	}

	Feeder.saveSegment = function() {

		/*Clean UI at Saving*/
		queryRecSer.unSelectAll();


		if((!Feeder.draftName.length || Feeder.draftName == 'Untitled Segment') && !Feeder.segmentID){
			draftNameUnsaved();

			$popup.show({
	            header:"Save Segment",
	            message:'The Segment "Untitled Segment" can not be saved. Please select a different name.',
	            okText:'OK',
	            hideCancel:true
	        })
	        .onOk(function() {
				$('.feeder-pane-header input')
					.removeClass('error heavy')
					.select();
	        })
	        .onCancel(function() {

	        });

			return;
		}

		if(Feeder.executionStatus == 'ACTIVATED' && !Feeder.activatedConfirm){

			$popup.show({
	            header:"Save Segment",
	            message:'This asset is currently in an active Campaign. Any saved changes will also be live. Do you still want to save?',
	            okText:'OK',
	            hideCancel:true
	        })
	        .onOk(function() {
	        	Feeder.activatedConfirm = true;
	        	Feeder.saveSegment();
	        	Feeder.activatedConfirm = false;

	        })
	        .onCancel(function() {

	        });

			return;
		}

		try{
			var savingJson = {},queryElement;
			savingJson.name = Feeder.draftName;
			savingJson.setFrequency = Feeder.setFrequency;
			savingJson.sourceInstanceConfigId = Feeder.filterCriterias[0].selExtSys.Source_Instance_Config_Id;


			savingJson.createdAt = Math.floor(Date.now()/1000);
			savingJson.updatedAt = Math.floor(Date.now()/1000);
			// savingJson.selObject = filterItem.selObject.Object_Id;


			savingJson.elements = [];

			Feeder.filterCriterias.forEach(function(filterItem) {
				var filterElement = {
					MasterObject : filterItem.selObject.Object_Id,
					filter:{
						name: filterItem.name,
						id:filterItem.retrievalID,
						type: filterItem.type
					}
				}
				if(filterItem.type == 'Filter'){
                    filterElement.filter.elements = [];
                    queryElementsToJsonBuilder(filterItem.elements,filterElement.filter.elements);
				} else {
                    filterElement.filter.query =   filterItem.query;
				}
				savingJson.elements.push(filterElement);


			});

			// console.log(savingJson);
			// console.log("-------");
			// console.log(JSON.stringify(savingJson));
			// Feeder.getSegment(JSON.stringify(savingJson));


			/*
			$segmentationLoader.show();
			$http.post('/setjson',savingJson)
			.then(function(result) {
				console.log(result);
				Feeder.disableSave = false;
				apiCalls.updateContactsCount({
					instanceConfigId:Feeder.filterCriterias[0].selExtSys.Source_Instance_Config_Id,
					segmentID:Feeder.segmentID
				})
				.then(function(result) {
					Feeder.countProp = result.data;
        			$segmentationLoader.hide();
				})
			});
			return;*/

			Feeder.disableSave = true;

			savingJson.id = (Feeder.segmentID||null);


        	$segmentationLoader.show();
			apiCalls.saveDecisionSegment({
				json:savingJson
			},urlParams.key)
			.then(function(result) {
				Feeder.disableSave = false;
				Feeder.segmentID = result.data.segmentId;
				Feeder.activatedConfirm = false;
				$segmentationLoader.hide();

				/*if(Feeder.segmentID){
					return apiCalls.updateContactsCount({
						instanceConfigId:Feeder.filterCriterias[0].selExtSys.Source_Instance_Config_Id,
						segmentID:Feeder.segmentID
					})
				}*/
			})
			.then(function(result) {

				/*if(result){
					Feeder.countProp = result.data;
					SOQLErrored();
				} else {
					Feeder.countProp = null;
				}
        		$segmentationLoader.hide();*/
			})

		}catch(error){

			
			$popup.show({
	            header:"Save Segment",
	            message:"Can not save segment. Segment has filters with incomplete criteria.",
	            okText:'Ok'
	        })
	        .onOk(function() {

	        })
	        .onCancel(function() {

	        })

		}
	}


	var SOQLErrored = function() {
		if(Feeder.countProp.queryIssues && Feeder.countProp.queryIssues.length){
			if(Feeder.countProp.queryIssues.length == 1){
				var errMsg = Feeder.countProp.queryIssues[0];
				if(errMsg.indexOf('INVALID_LOGIN')>-1){
					$popup.show({
			            header:"SOQL Error",
			            message:"Please see the default credentials exist in your Instance or contact your Admin.",
			            okText:'Ok'
			        })
			        .onOk(function() {

			        })
			        .onCancel(function() {

			        })
				} else {
					$popup.show({
			            header:"SOQL Error",
			            message:Feeder.countProp.queryIssues[0],
			            messageList:[],
			            okText:'OK'
			        })
			        .onOk(function() {

			        })
			        .onCancel(function() {

			        })
				}
			} else {
				$popup.show({
		            header:"SOQL Error",
		            message:"Error in filters",
		            messageList:Feeder.countProp.queryIssues,
		            okText:'OK'
		        })
		        .onOk(function() {

		        })
		        .onCancel(function() {

		        })
			}
		}
	}









	/*Drag and Drop operations*/
	var managePositioningOnDrag = function(rawArray,targetIndex,positioneeItem) {
		var firstSlicedArray  = _.slice(rawArray,0,targetIndex);
		var lastSlicedArray  = _.initial(_.slice(rawArray,targetIndex));
		var middleSlicedArray = [positioneeItem];
		rawArray = _.concat(firstSlicedArray,middleSlicedArray);
		rawArray = _.concat(rawArray,lastSlicedArray);
		rawArray.forEach(function(item,index) {
			if(index){
				item.logicalOperator = 'AND';
				item.operatorSwitch = 1;
			}

			item.queryIndex = index + 1;
			item.toBeSanitized.querySIndex = index+1;
		});
		return rawArray;
	}
	queryRecSer.onDropEnd = function(sourceIndex,targetIndex) {
		var crit = Feeder.criteriaOptions[sourceIndex];
		$timeout(function() {
			if(targetIndex){
				Feeder.setQuery(crit);
				var positioneeItem = Feeder.queryItems[Feeder.queryItems.length-1];
				Feeder.queryItems = managePositioningOnDrag(Feeder.queryItems,targetIndex,positioneeItem);

				if(Feeder.selectedFilter)
					Feeder.selectedFilter.elements = Feeder.queryItems;

			}else{
				Feeder.setQuery(crit);
			}
		}, 100);
	}
});
