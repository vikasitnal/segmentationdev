segmentationApp
.controller('Errors',function($scope,$timeout,$filter,apiCalls) {
	var Errors = $scope.Errors = {};
	var start = 0;
	var end = 15;
	Errors.currentNum = 1;
	Errors.pagesStart = 0;
	Errors.pageOffset = 10;

	var firstLimit = end*Errors.pageOffset;


	Errors.init = function(offset) {
        apiCalls.getAllErrors({
        	limit:firstLimit,
        	offset:(offset)?(offset):0
        })
        .then(function(result) {

        	if(Errors.totalData && Errors.totalData.length){
        		
        		/*if(Errors.totalData.length<Errors.pageOffset*end){
            		Errors.totalData = Errors.totalData.concat(Array(start-Errors.totalData.length).fill({}));
        		}*/

            	Errors.totalData = Errors.totalData.concat(result.data.data);
            	firstLimit = 2*firstLimit;
				Errors.data = Errors.totalData.slice(start,end);
        	}
            else{
            	Errors.totalData = result.data.data;

	            Errors.totalErrorsCount = result.data.totalErrorsCount;
	            Errors.setDataView();
            }


        });
	}

	Errors.init();

	Errors.setDataView = function() {
		Errors.data = Errors.totalData.slice(start,end);
		Errors.setPaginationArray();
	}
	Errors.setPaginationArray = function() {
		var totalPages = Math.ceil(Errors.totalErrorsCount/15);

		Errors.paginationNumberSet = Array.apply(null, {length: totalPages}).map(function(item,i) {
			return i+1;
		});
	}

	Errors.setPage = function(num) {
		Errors.currentNum = num;
		start = (num-1)*15;
		end = num*15;
		if(end<=firstLimit)
			Errors.data = Errors.totalData.slice(start,end);
		else {
			Errors.init(firstLimit+1);
		}


		if(Errors.currentNum<=Errors.pagesStart+1){
			Errors.pagesStart=(Errors.pagesStart-Errors.pageOffset>=0)?(Errors.pagesStart-Errors.pageOffset):Errors.pagesStart;
		}

		if(Errors.currentNum>=Errors.pagesStart+Errors.pageOffset+1){
			Errors.pagesStart=(Errors.pagesStart+Errors.pageOffset<Errors.paginationNumberSet.length)?(Errors.pagesStart+Errors.pageOffset):Errors.pagesStart;
		}

	}

	Errors.numPrev = function() {
		if(Errors.currentNum>1)
			Errors.setPage(Errors.currentNum-1);
	}

	Errors.numNext = function() {
		if(Errors.currentNum<Errors.paginationNumberSet.length)
			Errors.setPage(Errors.currentNum+1);
	}

	Errors.pageShiftPrev = function() {
		Errors.pagesStart=(Errors.pagesStart-Errors.pageOffset>=0)?(Errors.pagesStart-Errors.pageOffset):Errors.pagesStart;
		Errors.setPage(Errors.pagesStart+1);
	}

	Errors.pageShiftNext = function() {
		Errors.pagesStart=(Errors.pagesStart+Errors.pageOffset<Errors.paginationNumberSet.length)?(Errors.pagesStart+Errors.pageOffset):Errors.pagesStart;
		Errors.setPage(Errors.pagesStart+1);
	}


});