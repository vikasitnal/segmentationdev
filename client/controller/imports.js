segmentationApp
.controller('Imports',function($scope,$timeout,$filter,apiCalls) {
	var Imports = $scope.Imports = {};
	var start = 0;
	var end = 15;
	Imports.currentNum = 1;
	Imports.pagesStart = 0;
	Imports.pageOffset = 10;

	var firstLimit = end*Imports.pageOffset;


	Imports.init = function(offset) {
        apiCalls.getAllImports({
        	limit:firstLimit,
        	offset:(offset)?(offset):0
        })
        .then(function(result) {

        	if(Imports.totalData && Imports.totalData.length){
        		
        		/*if(Imports.totalData.length<Imports.pageOffset*end){
            		Imports.totalData = Imports.totalData.concat(Array(start-Imports.totalData.length).fill({}));
        		}*/

            	Imports.totalData = Imports.totalData.concat(result.data.data);
            	firstLimit = 2*firstLimit;
				Imports.data = Imports.totalData.slice(start,end);
        	}
            else{
            	Imports.totalData = result.data.data;

	            Imports.totalImportsCount = result.data.totalImportsCount;
	            Imports.setDataView();
            }


        });
	}

	Imports.init();

	Imports.setDataView = function() {
		Imports.data = Imports.totalData.slice(start,end);
		Imports.setPaginationArray();
	}
	Imports.setPaginationArray = function() {
		var totalPages = Math.ceil(Imports.totalImportsCount/15);

		Imports.paginationNumberSet = Array.apply(null, {length: totalPages}).map(function(item,i) {
			return i+1;
		});
	}

	Imports.setPage = function(num) {
		Imports.currentNum = num;
		start = (num-1)*15;
		end = num*15;
		if(end<=firstLimit)
			Imports.data = Imports.totalData.slice(start,end);
		else {
			Imports.init(firstLimit+1);
		}


		if(Imports.currentNum<=Imports.pagesStart+1){
			Imports.pagesStart=(Imports.pagesStart-Imports.pageOffset>=0)?(Imports.pagesStart-Imports.pageOffset):Imports.pagesStart;
		}

		if(Imports.currentNum>=Imports.pagesStart+Imports.pageOffset+1){
			Imports.pagesStart=(Imports.pagesStart+Imports.pageOffset<Imports.paginationNumberSet.length)?(Imports.pagesStart+Imports.pageOffset):Imports.pagesStart;
		}

	}

	Imports.numPrev = function() {
		if(Imports.currentNum>1)
			Imports.setPage(Imports.currentNum-1);
	}

	Imports.numNext = function() {
		if(Imports.currentNum<Imports.paginationNumberSet.length)
			Imports.setPage(Imports.currentNum+1);
	}

	Imports.pageShiftPrev = function() {
		Imports.pagesStart=(Imports.pagesStart-Imports.pageOffset>=0)?(Imports.pagesStart-Imports.pageOffset):Imports.pagesStart;
		Imports.setPage(Imports.pagesStart+1);
	}

	Imports.pageShiftNext = function() {
		Imports.pagesStart=(Imports.pagesStart+Imports.pageOffset<Imports.paginationNumberSet.length)?(Imports.pagesStart+Imports.pageOffset):Imports.pagesStart;
		Imports.setPage(Imports.pagesStart+1);
	}


});