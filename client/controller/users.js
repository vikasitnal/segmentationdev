segmentationApp.controller('User', function($scope, $timeout, apiCalls, $filter, $popup, $alert) {
    /*var User = this;
    this.name = "Share"*/
    var User = $scope.User = {};
    var initialSFDCConFields = [];
    // new MaskedPassword(document.getElementById("token"), '\u25CF');
    User.managecredentials = true;

    User.init = function() {
        $('#fieldMapping').hide();
        $('#instanceConfiguration').hide();
        $('#parentObjects').hide();
        $('#importPriority').hide();
        $('#decisionSettings').hide();
        $('#addExternalSystemForm').hide();

        /*$(window).scroll(function(){
            if(isScrolledIntoView($('#instanceConfiguration'))){
                $(this).children('span').text('visible');
            }
            else{
                $(this).children('span').text('invisible');
            }
        });

        function isScrolledIntoView(elem){
            var $elem = $(elem);
            var $window = $(window);

            var docViewTop = $window.scrollTop();
            var docViewBottom = docViewTop + $window.height();

            var elemTop = $elem.offset().top;
            var elemBottom = elemTop + $elem.height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }*/

        User.btnAddVis = true;
        User.newUser = {
            status: "New"
        }

        User.stsArr = ['Active', 'Inactive'];

        apiCalls.getLoggedInDetails()
            .then(function(result) {
                if (result.data.appInfo) {
                    User.newUser.siteid = User.permanentSiteID = result.data.appInfo.siteid;
                }
                return apiCalls.getAdminData();
            }).then(function(result) {

                User.extSystems = _.sortBy(result.data.InstanceInfo, ['sourceSystemName']);

                var securityToken = '';
                User.extSystems.forEach(function(item, index) {
                    if (item.Data_Sync_Status == 'Sync On Progress') {
                        User.checkSyncStatus(item, index);
                    }
                    var srcSysName = $filter('filter')(result.data.ExternalSourceSystem, {
                        Source_System_Id: item.Source_System_Id
                    })[0];
                    if (srcSysName)
                        item.sourceSystemName = srcSysName.Source_System_Name;

                    var srcSysInsObj = $filter('filter')(result.data.SourceSystemInstance, {
                        Source_System_Instance_id: item.Source_System_Instance_Id
                    })[0];
                    if (srcSysInsObj)
                        item.sourceSystemInstanceName = srcSysInsObj.Source_System_Instance_Name;

                    var sToken = item.Security_token;

                    var securityToken = '';
                    for (var i = 0; i < sToken.length; i++) {
                        securityToken += '\u25CF';
                    }

                    item.securityToken = securityToken;


                });

                User.srcSystems = result.data.ExternalSourceSystem;
                User.newUser.selSourceSystem = User.srcSystems[0];

                User.sysInstances = result.data.SourceSystemInstance;


            });
    }
    User.init();


    User.acluserids = [];
    User.getAclUserIds = function() {
        apiCalls.getacluserids().then(function(result) {
            User.acluserids = _.map(result.data[0], 'UserId');
            User.acltotal = User.acluserids.length;
        });
    }

    User.getAclUserIds();


    User.acls = [];
    User.aclpage = 1;
    User.getAcl = function(page) {
        apiCalls.getacl(page).then(function(result) {
            User.acls = result.data[0];
        });
    }
    User.getAcl(1);


    User.checkAcl = function(id) {
        var index = User.acluserids.indexOf(parseInt(id));
        if (index > -1) {
            return true;
        } else {
            return false;
        }
    }

    User.postAcl = function(user, remove, index) {
        if (remove) {
            var acl = {
                UserId: user.id,
                Name: user.name,
                Email: user.emailAddress
            }
        } else {
            var acl = {
                UserId: user.originalObject.id,
                Name: user.originalObject.name,
                Email: user.originalObject.emailAddress
            };
        }
        apiCalls.postacl(acl).then(function(result) {
            User.acls = result.data[0];
            User.acluserids = User.acls.map(function(item) {
                return item.UserId;
            });


            if (remove) {
                User.eloquausers.splice(index, 1);
                if (!User.eloquausers.length) {
                    User.eloquausers = null;
                    User.searchingusers = null;
                    User.eloquaQuery = '';
                }
            }
        }, function(err) {
            User.eloquausers = null;
            User.searchingusers = null;
            User.aclFailedProp = {
                status:true,
                messageType: 'danger',
                strongMessage: err.data.message,
                message: ''
            };
        });
    }

    User.deleteAcl = function(id, index) {
        apiCalls.deleteacl(id).then(function(result) {
            User.acls.splice(index, 1);
            User.acluserids.splice(index, 1);
        });
    }

    User.eloquausers = null;
    User.eloquaQuery = "";

    User.searchuser = function(text, page) {
        User.eloquausers = [];
        User.searchingusers = true;
        apiCalls.searchuser(text, page).then(function(result) {
            User.eloquausers = result.data.elements;
            User.eloquapage = result.data.page;
            User.totaleloquapages = result.data.total;
            User.searchingusers = false;
        });
    }


    User.getExtSysObjs = function(selectedInstance, selectedObjLabel) {
        User.extSystemObjects = null;
        User.existingObjects = null;
        apiCalls.getSfdcObjects({
                siteId: User.permanentSiteID,
                Source_Instance_Config_Id: selectedInstance.Source_Instance_Config_Id
            })
            .then(function(result) {
                User.existingObjects = _.sortBy(result.data, ['Object_Label']);
                User.emptySelExtObj();
                /*User.selExtObj = {
                    shownValue:''
                };*/
            });

        apiCalls.getExtSysObjs({
                Source_Instance_Config_Id: selectedInstance.Source_Instance_Config_Id
            })
            .then(function(result) {
                User.extSystemObjects = _.sortBy(result.data.getObjects, ['Object_Name']);

                if (selectedObjLabel) {
                    var selectedObjectID = _.find(result.data.getObjects, ['Object_Name', selectedObjLabel]);
                    apiCalls.getObjectMapping({
                            Source_Instance_Config_Id: selectedObjectID.Source_Instance_Config_Id,
                            Source_Primary_Object_Id: selectedObjectID.Source_Primary_Object_Id,
                            Source_Object_Id: selectedObjectID.Object_Id
                        })
                        .then(function(result) {

                        })
                }
            })
    }

    User.checkSyncStatus = function(user, index) {
        apiCalls.checkSyncStatus(user.Source_Instance_Config_Id).then(function(res) {
            User.extSystems[index].Data_Sync_Status = res.data.status;
            if (res.data.status != 'Sync Complete') {
                setTimeout(function() {
                    User.checkSyncStatus(user, index);
                }, 60 * 1000);
            }
        }, function(err) {
            User.extSystems[index].Data_Sync_Status = "Failed";
        });
    }


    User.confirmdeactivate = function(instance, index) {
        User.selectedInstanceIndex = index;
        apiCalls.checksourceinstanceconfigdependencies(instance.Source_Instance_Config_Id).then(function(result) {
            User.fielddependencies = result.data.feederinstances;
            User.selectedFieldSegments = _.keyBy(result.data.segments, 'Segment_ID');
            if (User.fielddependencies.length > 0) {
                $("#instancedependencies").modal();
            } else {
                User.toggleStatus(instance);
            }
        });
    }

    User.canceldeactivate = function() {
        User.extSystems[User.selectedInstanceIndex].StatusBool = !User.extSystems[User.selectedInstanceIndex].StatusBool;
    }

    User.confirmDeleteField = function($index, mappedContactField) {
        $popup.show({
                header: "Deleting Mapped Fields",
                message: "Are you sure you want to delete this mapped field ?",
                okText: 'Yes'
            })
            .onOk(function() {
                User.selectedFieldIndex = $index;
                User.selectedMappendContactField = mappedContactField;

                apiCalls.checkMappedField({
                    Source_Field_Mapping_Id: mappedContactField.Source_Field_Mapping_Id,
                    Source_Object_Id: User.selectedObject.Source_Primary_Object_Id
                }).then(function(result) {
                    User.fielddependencies = result.data.feederinstances;
                    User.selectedFieldSegments = _.keyBy(result.data.segments, 'Segment_ID');

                    if (User.fielddependencies.length > 0) {
                        $("#fielddependencies").modal();
                    } else {
                        User.proceedAnywayDeleteField();
                    }
                });

            })
            .onCancel(function() {

            });

    }

    User.deleteContactField = function($index, mappedContactField) {
        apiCalls.deleteMappedField({
                Source_Field_Mapping_Id: mappedContactField.Source_Field_Mapping_Id,
                Source_Object_Id: User.selectedObject.Source_Primary_Object_Id
            })
            .then(function(result) {
                if (result.data.status == 1) {
                    User.mappedContactFields.splice($index, 1);
                    User.removeAlreadyMapped();
                    $("#fielddependencies").modal("hide");
                }
            })

    }

    User.testUserConnection = function() {
        apiCalls.testUserConnection(User.newUser)
            .then(function(result) {
                if (result.data.status) {
                    $alert.show({
                        messageType: 'success',
                        strongMessage: 'Connection successful',
                        message: ''
                    });
                    User.newUser.status = 'Active';
                } else {
                    $alert.show({
                        messageType: 'danger',
                        strongMessage: 'Unable to connect to Salesforce. Please verify credentials and try again.',
                        message: ''
                    });
                    User.newUser.status = 'Failed';
                }
                $(document).scrollTop(0);
            });
    }

    User.syncDisStat = function() {
        if (User.extSystems) {
            var condition = _.find(User.extSystems, {
                User_name: User.newUser.username
            });
            return !(User.newUser.username && User.newUser.token && condition);
        } else {
            return true;
        }
    }



    User.syncSfdcData = function() {
        User.newUser.categoryID = _.find(User.extSystems, {
            User_name: User.newUser.username
        }).Source_Instance_Config_Id;
        apiCalls.syncSfdcData(User.newUser)
            .then(function(result) {
                if (result.data.status) {
                    $alert.show({
                        messageType: 'warning',
                        strongMessage: 'Process to sync the data structure has started.  It will process in the background.',
                        message: ''
                    });

                    var index = _.findIndex(User.extSystems, function(user) {
                        return user.Source_Instance_Config_Id == User.newUser.categoryID;
                    });
                    User.checkSyncStatus(User.extSystems[index], index);
                } else {
                    $alert.show({
                        messageType: 'danger',
                        strongMessage: 'Unable to connect to Salesforce. Please verify credentials and try again.',
                        message: ''
                    });
                }
                $(document).scrollTop(0);
            });
    }
    User.presentAddExternalSystem = function() {
        var height = $('#addExternalSystemForm').height();
        $('#addExternalSystemForm').show().css('height', '0px').animate({
            height: height
        }, User.AnimationComplete);
    }

    User.editExtSource = function(index, extSystem) {
        // console.log("index ", index);
        User.formType = 'Edit';
        User.btnAddVis = false;
        User.managecredentials = true;
        User.newUser = {
            username: extSystem.User_name,
            pwd: extSystem.Password,
            token: extSystem.Security_token,
            desc: extSystem.Description,
            status: extSystem.Status,
            syncStatus: extSystem.Data_Sync_Status,
            siteid: extSystem.Site_Id,
            categoryID: extSystem.Source_Instance_Config_Id,
            email: extSystem.Email,
            Source_Instance_Config_Id: extSystem.Source_Instance_Config_Id
        }
        var fltr = $filter('filter');

        User.newUser.selSourceSystem = fltr(User.srcSystems, {
            Source_System_Id: extSystem.Source_System_Id
        })[0];

        User.newUser.selSysIns = fltr(User.sysInstances, {
            Source_System_Instance_id: extSystem.Source_System_Instance_Id
        })[0];

        User.presentAddExternalSystem();
    }


    User.newUserCancel = function() {
        $('#addExternalSystemForm').animate({
            height: '0px'
        }, User.AnimationComplete).fadeOut(0);
        User.newUser = {
            status: "Active",
            siteid: User.permanentSiteID,
            selSourceSystem: User.srcSystems[0]
        }
        User.btnAddVis = true;
    }

    User.disableSave = function(newUser) {
        if (newUser) {
            if (!newUser.Source_Instance_Config_Id) {
                var cond = newUser.username && newUser.pwd;
                cond = cond && newUser.desc;
                cond = cond && newUser.status;
                cond = cond && newUser.email;
                cond = cond && newUser.selSysIns;
                return !cond;
            } else {
                var cond = newUser.username;
                cond = cond && newUser.desc;
                cond = cond && newUser.status;
                cond = cond && newUser.email;
                cond = cond && newUser.selSysIns;
                return !cond;
            }
        } else {
            return true;
        }
    }

    /*External System Objects*/
    User.instanceConfiguration = function(index, selectedInstance) {
        User.btnAddVis = false;
        User.selectedextSysIndex = index;
        User.selectedInstance = selectedInstance;

        User.getExtSysObjs(selectedInstance);

        var height = $('#instanceConfiguration').height();
        $('#instanceConfiguration').show().css('height', '0px').animate({
            height: height + 'px'
        }, User.AnimationComplete);


    }

    /*Decision Settings*/
    User.decisionSettings = {
        applyControllerStatus:true,
        openConfig : function(index, selectedInstance) {
            User.btnAddVis = false;
            User.selectedextSysIndex = index;
            User.selectedInstance = selectedInstance;

            this.getStatus();

            var height = $('#decisionSettings').height();
            $('#decisionSettings').show().css('height', '0px').animate({
                height: height + 'px'
            }, User.AnimationComplete);

        },
        getStatus:function() {
            apiCalls.sfCtrlStatusGet({
                instanceId:User.selectedInstance.Source_Instance_Config_Id
            })
            .then(function(result) {
                User.decisionSettings.applyControllerStatus = result.data.status;
            });
        },
        changeStatus: function() {
            apiCalls.checksourceinstanceconfigdependencies(User.selectedInstance.Source_Instance_Config_Id)
            .then(function(result) {
                var segments = result.data.segments;
                if(segments.length){
                    var decisions = _.filter(segments,{segment_type:'decisionservice'});


                    if(decisions.length){

                        $popup.show({
                            header: "Decision dependencies exists",
                            message: "Are you sure you want to delete all the Decisions related to this instance to use the controller?",
                            okText: 'Yes'
                        })
                        .onOk(function() {

                        })
                        .onCancel(function() {
                            User.decisionSettings.applyControllerStatus = !User.decisionSettings.applyControllerStatus;
                        });

                    } else {
                        apiCalls.sfCtrlStatusUpdate({
                            instanceId:User.selectedInstance.Source_Instance_Config_Id,
                            sfdcCtrlStatus:this.applyControllerStatus
                        })
                        .then(function(result) {
                            if(!result.data.status){
                                User.decisionSettings.applyControllerStatus = !User.decisionSettings.applyControllerStatus;

                                $alert.show({
                                    messageType: 'danger',
                                    strongMessage: 'Server Error',
                                    message: ''
                                });
                            }
                        });
                    }


                } else {
                    
                }
            });
        }
    }

    User.importPriority = function(index, selectedObject) {
        User.disablePriority = true;
        apiCalls.getImportPriorityData()
            .then(function(result) {
                User.importPriorityData = result.data;
                User.fieldMappingOn = true;
                User.selectedObjIndex = index;
                User.selectedObject = selectedObject;
                User.setPriority();
            });
        var height = $('#importPriority').height();
        $('#importPriority').show().css('height', '0px').animate({
            height: height
        }, User.AnimationComplete);
    }

    User.emptyPriority = function() {
        User.selPriority = {
            name: ''
        }
    }


    User.setPriority = function(event) {
        if (event) {
            event.target.blur();
        }

        if (User.selPriority && User.selPriority.name.length)
            var priority = User.selPriority;

        apiCalls.setImportPriorityData({
                primaryObjectId: User.selectedObject.Source_Primary_Object_Id,
                priority: priority
            })
            .then(function(data) {
                if (data.status == 1 || data.status == 2) {
                    $alert.show({
                        messageType: 'success',
                        strongMessage: 'Priority has been updated successfully',
                        message: ''
                    });
                }
                if (data.result && data.result.priority) {
                    User.selPriority = data.result.priority;
                }
                User.disablePriority = false;
            })
    }



    User.deleteUser = function(user, $index) {

        $popup.show({
                header: "Deleting Instance",
                message: "Are you sure you want to delete instance " + user.Description + " ?",
                okText: 'Yes'
            })
            .onOk(function() {
                apiCalls.deleteUser(user)
                    .then(function(result) {
                        User.extSystems.splice($index, 1);
                        $alert.show({
                            messageType: 'success',
                            strongMessage: 'Instance deleted successfully',
                            message: ''
                        });
                    }, function(err) {
                        $alert.show({
                            messageType: 'danger',
                            strongMessage: err.data.message,
                            message: ''
                        });
                    });
            })
            .onCancel(function() {
                // console.log("success cancel");
            });
    }


    User.deleteInstanceObject = function($index, selInsObj) {

        $popup.show({
                header: "Deleting Instance Object",
                message: "Are you sure you want to delete Instance ?",
                okText: 'Yes'
            })
            .onOk(function() {
                apiCalls.deleteInstanceObject({
                        Source_Primary_Object_Id: selInsObj.Source_Primary_Object_Id
                    })
                    .then(function(result) {
                        if (result.data.status == 1) {
                            User.extSystemObjects.splice($index, 1);
                            User.emptySelExtObj();
                            User.emptyPriority();
                            User.emptyElqField();
                        } else {

                            User.dependencies = result.data.dependencies;
                            $("#dependencies").modal();
                        }
                    });
            })
            .onCancel(function() {
                // console.log("success cancel");
            });
    }

    User.emptyElqField = function() {
        User.selElqObj = {
            shownValue: ''
        }
    }
    User.emptySFDCField = function() {
        User.selSdfcObj = {
            shownValue: ''
        }
    }
    User.emptySelExtObj = function() {
        User.selExtObj = {
            'Object_Label': ''
        }
    }

    User.setParentData = function(data) {
        if (!data.input) {
            apiCalls.adminGetParents({
                    "instanceConfigId": User.selectedObject.Source_Instance_Config_Id,
                    "objectName": User.selectedObject.Object_Id
                })
                .then(function(result) {
                    data.onSuccess(result.data);
                });
        } else {
            apiCalls.adminGetParents({
                    "instanceConfigId": data.input.instanceConfigId,
                    "objectName": data.input.objectName
                })
                .then(function(result) {
                    data.onSuccess(result.data);
                });
        }
    }

    User.getObjectParentFields = function(selNode) {

        User.sdfcConFields = [];

        User.selSdfcObj = null;

        apiCalls.getAdminParentFields({
            instanceConfigId: selNode.instanceConfigId,
            objectName: selNode.referenceTo
        }).then(function(result) {

            User.sdfcConFields = _.sortBy(result.data, ['label']);
            User.sdfcConFields.forEach(function(item) {
                item.shownValue = item.label + " (" + item.name + ")";
            });

            User.mappedContactFields.forEach(function(item) {
                User.sdfcConFields = _.reject(User.sdfcConFields, {
                    name: item.Source_Field_Id
                });
            });
            User.emptyElqField();
            User.emptySFDCField();
        });
    }
    User.cancelFieldMapping = function() {
        User.sdfcConFields = initialSFDCConFields;
        User.emptyElqField();
        User.emptySFDCField();
        User.removeAlreadyMapped();
    }

    User.removeAlreadyMapped = function() {
        if(User.history){
            User.sdfcConFields = User.history.sdfcConFields;
        }
        User.mappedContactFields.forEach(function(item) {
            User.sdfcConFields = _.reject(User.sdfcConFields, {
                name: item.Source_Field_Id
            });
            User.eloquaFields = _.reject(User.eloquaFields, {
                internalName: item.Target_Field_Id
            });
        });
    }

    User.addMapping = function() {
        var index = _.findIndex(User.mappedContactFields, function(mapping) {
            return mapping.Target_Field_Id == User.selElqObj.internalName;
        });
        if (index >= 0) {
            $alert.show({
                messageType: 'danger',
                strongMessage: 'Field mapping for the target field is already available',
                message: ''
            });
        } else {
            apiCalls.addMapping({
                Source_Object_Name: User.selectedObject.Object_Name,
                Source_Object_Id: User.selectedObject.Source_Primary_Object_Id,
                Source_Field_Name: User.selSdfcObj.label,
                Source_Field_Id: User.selSdfcObj.name,
                Source_Field_Type: User.selSdfcObj.type,
                Target_Field_Name: User.selElqObj.name,
                Target_Field_Id: User.selElqObj.internalName,
                Target_Field_Type: User.selElqObj.dataType,
                objectParentRelation: User.selParentFormat
            }).then(function(result) {
                User.mappedContactFields = result.data;
                User.removeAlreadyMapped();



                User.selSdfcObj = null;


                User.emptyElqField();
                User.emptySFDCField();

                $alert.show({
                    messageType: 'success',
                    strongMessage: 'Fields mapped successfully',
                    message: ''
                });
                $(document).scrollTop(0);
            })
        }


    }

    User.mapInstanceObject = function(index, selectedObjectID) {
        User.fieldMappingOn = true;
        User.selectedObjIndex = index;
        User.selectedObject = selectedObjectID;
        User.mappedContactFields = null;
        User.selSdfcObj = null;
        User.selElqObj = null;


        apiCalls.getObjectMapping({
                Source_Instance_Config_Id: selectedObjectID.Source_Instance_Config_Id,
                Source_Primary_Object_Id: selectedObjectID.Source_Primary_Object_Id,
                Source_Object_Id: selectedObjectID.Object_Id
            })
            .then(function(result) {
                User.eloquaFields = _.sortBy(result.data.EloquaObjects.elements, ['name']);
                User.sdfcConFields = _.sortBy(result.data.SFDCOjects, ['label']);
                User.history = {
                    sdfcConFields:User.sdfcConFields
                }

                User.sdfcConFields.forEach(function(item) {
                    item.shownValue = item.label + " (" + item.name + ")";
                });

                User.eloquaFields.forEach(function(item) {
                    item.shownValue = item.name;
                });

                User.emptyElqField();
                User.emptySFDCField();

                User.mappedContactFields = result.data.MappedFields;
                User.removeAlreadyMapped();

                initialSFDCConFields = User.sdfcConFields;

            });


        var height = $('#fieldMapping').height();
        $('#fieldMapping').show().css('height', '0px').animate({
            height: height + 'px'
        }, User.AnimationComplete);
    }
    /*User.findParents = function(selectedObject) {

        apiCalls.adminGetParents({
            Source_Instance_Config_Id:selectedObject.Source_Instance_Config_Id,
            Source_Primary_Object_Id:selectedObject.Source_Primary_Object_Id,
            Source_Object_Id:selectedObject.Object_Id
        }).then(function(result) {
            User.parentOfObjects = result.data;
        });


        var height = $('#parentObjects').height();
        $('#parentObjects').show().css('height', '0px').animate({
            height: height + 'px'
        }, User.AnimationComplete);
    }*/

    User.sysObjDrop = function(index, selectedObject, cond) {
        User.toggleNewExtObj = false;
        User.sysObjsDisable = true;
        User.selExtObj = null;
        switch (cond) {
            case 'field_mapping':
                User.mapInstanceObject(index, selectedObject);
                break;
            case 'set_import_priority':
                User.importPriority(index, selectedObject);
                break;
            case 'delete':
                User.sysObjsDisable = false;
                User.deleteInstanceObject(index, selectedObject);
                break;
            default:
                break;
        }
    }



    User.addMapping = function() {
        var index = _.findIndex(User.mappedContactFields, function(mapping) {
            return mapping.Target_Field_Id == User.selElqObj.internalName;
        });
        if (index >= 0) {
            $alert.show({
                messageType: 'danger',
                strongMessage: 'Field mapping for the target field is already available',
                message: ''
            });
        } else {
            apiCalls.addMapping({
                Source_Object_Name: User.selectedObject.Object_Name,
                Source_Object_Id: User.selectedObject.Source_Primary_Object_Id,
                Source_Field_Name: User.selSdfcObj.label,
                Source_Field_Id: User.selSdfcObj.name,
                Source_Field_Type: User.selSdfcObj.type,
                Target_Field_Name: User.selElqObj.name,
                Target_Field_Id: User.selElqObj.internalName,
                Target_Field_Type: User.selElqObj.dataType,
                objectParentRelation: User.selParentFormat
            }).then(function(result) {
                User.mappedContactFields = result.data;
                if(User.history){
                    User.sdfcConFields = User.history.sdfcConFields;
                }
                User.mappedContactFields.forEach(function(item) {
                    User.sdfcConFields = _.reject(User.sdfcConFields, {
                        name: item.Source_Field_Id
                    });
                    User.eloquaFields = _.reject(User.eloquaFields, {
                        internalName: item.Target_Field_Id
                    });
                });



                User.selSdfcObj = null;


                User.emptyElqField();
                User.emptySFDCField();

                $alert.show({
                    messageType: 'success',
                    strongMessage: 'Fields mapped successfully',
                    message: ''
                });
                $(document).scrollTop(0);
            })
        }


    }



    User.AnimationComplete = function() {
        $(this).removeAttr('style');
    }

    User.closePanel = function(ev) {
        var selectedPanel = $(ev.target).closest('.panel');
        var height = selectedPanel.height();
        var animationEnd = function() {
            selectedPanel.hide();
            selectedPanel.css('height', height + 'px');
            if (!User.fieldMappingOn) {
                User.btnAddVis = true;
                User.selectedextSysIndex = -1;
            } else {
                User.toggleFieldMaps = false;
            }
            User.toggleNewExtObj = false;
            User.selectedObjIndex = -1;
            User.fieldMappingOn = false;
            User.sysObjsDisable = false;
            User.selExtObj = {
                Object_Label: ''
            };
            User.emptyPriority();

            $scope.$digest();
        }
        selectedPanel.animate({
            height: '0px'
        }, animationEnd);
    }


    User.onSelectingObject = function() {
        setTimeout(function() {
            $('#addObject').focus();
        }, 2);
    }

    User.addExtObject = function() {
        if (User.selExtObj) {
            var existedItem = $filter('filter')(User.extSystemObjects, {
                Object_Name: User.selExtObj.Object_Label
            }, true);
            if (!existedItem.length) {
                apiCalls.addExternalObject({
                    Source_Instance_Config_Id: User.selectedInstance.Source_Instance_Config_Id,
                    Object_Name: User.selExtObj.Object_Label,
                    Object_Id: User.selExtObj.Object_Name
                }).then(function(result) {

                    $alert.show({
                        messageType: 'success',
                        strongMessage: 'Object added successfully',
                        message: ''
                    });
                    User.getExtSysObjs(User.selectedInstance, User.selExtObj.Object_Label);

                    User.emptySelExtObj();
                    User.emptyPriority();
                    User.emptyElqField();
                });
            } else {
                $alert.show({
                    messageType: 'danger',
                    strongMessage: 'Object already exists',
                    message: ''
                });
            }
        }
    }


    User.deleteExtSource = function($index) {

        $popup.show({
                header: "Deleting External Source",
                message: "Are you sure you want to delete External source?",
                okText: 'Yes'
            })
            .onOk(function() {
                apiCalls.deleteExtSource(User.extSystems[$index].Source_Instance_Config_Id)
                    .then(function(result) {
                        if (result.data.status == 1) {

                            $alert.show({
                                messageType: 'danger',
                                strongMessage: 'Deletion succeeded',
                                message: ''
                            });

                            User.extSystems.splice($index, 1);
                        } else {
                            $alert.show({
                                messageType: 'danger',
                                strongMessage: 'Deletion Failed',
                                message: 'Waiting for server to respond...'
                            })
                        }
                    })
            })
            .onCancel(function() {

            });
    }
    User.proceedAnyway = function() {
        User.saveUserProceed(true);
        $("#dependencies").modal();
        setTimeout(function() {
            $("#dependencies").modal("hide");
        }, 10);
    }


    User.proceedAnywayDeleteField = function() {
        User.deleteContactField(User.selectedFieldIndex, User.selectedMappendContactField);
    }

    User.toggleStatus = function(instance) {
        instance.Status = instance.StatusBool == true ? 'Active' : 'Inactive';
        if (instance.StatusBool == true) {
            apiCalls.activateUser(instance);
        } else {
            apiCalls.deactivateUser(instance);
        }
    }


    User.saveUserProceed = function(proceedConfirmed) {
        if (proceedConfirmed) {
            User.proceedConfirmation = false;
            User.newUser.proceed = true;
        }

        /*initialize*/
        User.dependencies = [];
        apiCalls.testUserConnection(User.newUser)
            .then(function(testresult) {
                if (testresult.data.status) {
                    User.newUser.status = 'Active';
                } else {
                    User.newUser.status = 'Failed';
                }
                apiCalls.saveUser(User.newUser)
                    .then(function(result) {
                        if (typeof(result.data) == 'object') {

                            if (result.data.status == 100 || result.data.status == 200) {

                                User.extSystems = _.sortBy(result.data.externalSystems, ['sourceSystemName']);
                                User.extSystems.forEach(function(item) {

                                    var srcSysName = $filter('filter')(User.srcSystems, {
                                        Source_System_Id: item.Source_System_Id
                                    })[0];
                                    if (srcSysName)
                                        item.sourceSystemName = srcSysName.Source_System_Name;

                                    var srcSysInsObj = $filter('filter')(User.sysInstances, {
                                        Source_System_Instance_id: item.Source_System_Instance_Id
                                    })[0];

                                    if (srcSysInsObj)
                                        item.sourceSystemInstanceName = srcSysInsObj.Source_System_Instance_Name;

                                });

                            } else if (result.data.status == 0) {
                                User.dependencies = result.data.dependencies;
                                User.proceedConfirmation = true;
                                $("#dependencies").modal();
                            } else {
                                $alert.show({
                                    messageType: 'danger',
                                    strongMessage: 'Getting some admin issues please wait until we fix this..',
                                    message: ''
                                });
                            }
                            User.newUser.proceed = false;

                            var strongMessage;
                            if (User.formType == 'New') {
                                strongMessage = 'Instance created successfully';
                            } else {
                                strongMessage = 'Changes saved successfully';
                            }
                            if (testresult.data.status) {
                                $alert.show({
                                    messageType: 'success',
                                    strongMessage: strongMessage + ' , Connection to salesforce is successful',
                                    message: ''
                                });
                            } else {
                                $alert.show({
                                    messageType: 'success',
                                    strongMessage: strongMessage,
                                    errorMessage: 'Unable to connect to Salesforce, Connection to salesforce is unsuccessful please verify credentials and try again.',
                                    message: ''
                                });
                            }
                            var addedindex = _.findIndex(User.extSystems, function(user) {
                                return user.User_name == User.newUser.username;
                            });
                            User.editExtSource(addedindex, User.extSystems[addedindex]);
                            $(document).scrollTop(0);
                        }


                        $(document).scrollTop(0);
                    }, function(err) {
                        $alert.show({
                            messageType: 'danger',
                            strongMessage: err.data.message,
                            message: ''
                        });
                    });
            });
    }


    User.newUserSave = function(newUser, event) {
        if (event) {
            event.target.blur();
        }
        var existingUser = _.find(User.extSystems, {
            User_name: newUser.username
        });
        if (existingUser && newUser.categoryID === undefined) {
            $timeout(function() {
                $alert.show({
                    messageType: 'danger',
                    strongMessage: 'User is already existing',
                    message: ''
                });
            }, 100);
            return;
        }

        User.saveUserProceed();
    }

    User.blockDefaultShow = function() {
        $("#panelDefault").hide();
    }


});