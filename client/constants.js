segmentationApp
.constant('APIBASEURL', window.location.origin+'/')
.constant('URLLIST', {



	/*********** Admin ***************/
	'ADMINDATA':'instance/getAdminData',
	'SAVEUSER':'instance/addInstance',
    'ACTIVATEUSER':'instance/activateInstance',
    'DEACTIVATEUSER':'instance/deactivateInstance',
	'DELETEUSER':'instance/deleteInstance',
	'TESTCONNECTION':'instance/checkConnSfdc',
	'SYNCSFDCDATA':'instance/syncSfdcData',
	'LOGGEDINDETAILS':'instance/getLoginDetails',
	'DELETEEXTSOURCE':'instance/deleteOrg',
	'SFDCOBJECTS':'instance/getSfdcObjects',
	'IMPORTPRIORITY':'instance/importPriority',
	'ADDEXTERNALOBJECT':'instance/postObject',
	'GETEXTERNALSYSOBJS':'instance/instance_configuration',
	'DELETEINSTANCEOBJECT':'instance/deleteSfdcOrg',
	'OBJECTMAPPING':'instance/objectMapping',
	'ADDMAPPING':'instance/addMapping',
	'CHECKMAPPEDFIELD':'instance/checkfeederinstances',
	'CHECKSOURCEINSTANCECONFIGDEPENDENCIES':'instance/checkfeederinstancesbysourceconfig',
	'DELETEMAPPEDFIELD':'instance/deleteSfdcMapping',
	'ADMINGETPARENTS':'instance/getParentObjects',
	'ADMINGETCHILDS':'instance/getChildObjects',
	'ADMINGETPARENTFIELDS':'instance/getParentObjectFields',
	'SETIMPORTPRIORITYDATA':'instance/setGetImportPriority',
	'SFCTRLSTATUSGET':'instance/sfCtrlStatusGet',
	'SFCTRLSTATUSUPDATE':'instance/sfCtrlStatusUpdate',

	/*********** Feeder ***************/

	'GETFEEDEREXTERNALSYSTEMS':'feederService/getExternalSys',
	'GETSELECTEDSFDCOBJECTS':'feederService/selectedSFDCObjects',
	'SAVESEGMENT':'feederService/saveSegmentJson',
	'FEEDERSERVICEMAPPEDFIELDS':'feederService/mappedFields',
	'GETEXISTINGFEEDER':'feederService/getSegment',
	'GETPARENTOFOBJECT':'feederService/getParentObjects',
	'GETPARENTFIELDSASSOC':'feederService/parentFields',
	'EXISTINGSEGMENTS':'feederService/getExistingSegments',
	'GETEXISTINGSEGMENT':'feederService/getExistingSegment',
	'CONTACTRECORDS':'feederService/getContactsRecords',
	'UPDATECONTACTSCOUNT':'feederService/getContactsCount',
	'CONTACTCOLUMNS':'feederService/getContactsColumns',
	'VALIDATESOQL':'feederService/validateSOQL',
	'CHECKSYNCSTATUS':'feederService/checkSync',
	'GETSOQLOBJECTFIELDS':'feederService/getSOQLObjectFields',




	/***************** Sync Reports **********************/
	'EXECUTIONNUMBERS':'syncReports/executionNumbers',
	'EXECUTIONS':'syncReports/executions',
	'RECENTIMPORTS':'syncReports/recentImports',
	'RECENTACTIONS':'syncReports/recentActions',
	'RECENTERRORS':'syncReports/recentErrors',

	/********************** Imports **************************/
	'IMPORTS':'syncReports/paginateImports',

	/********************** Errors **************************/
	'ERRORS':'syncReports/paginateErrors',

	//acl endpoints
	'GETACL': 'acl',
	'POSTACL': 'acl',
	'DELETEACL': 'acl',
	'GETACLUSERIDS': 'acl/userids',

	// search eloqua users

	'SEARCHUSER': 'instance/users',

	/***************** Decision Service ******************/
	'SAVEDECISIONSEG':'decisionService/onDecisionSave',
	'DSGETSFDCOBJECTS':'decisionService/getSFDCObjects',
	'DSGETELOQUARELATEDDATA':'decisionService/getEloquaRelatedData',
	'DSOBJECTFIELDS':'decisionService/objectFields',
	'DSSYNCDETAILS':'decisionService/getSyncDetails',
	'DSSAVEIMPORTPRIORITY':'decisionService/saveImportPriority',
	'DSGETIMPORTPRIORITY':'decisionService/getImportPriority',

	// decision service field mapping
	'GETDSFIELDMAPPING': 'decision/getdecisionFieldMapping',
	'POSTDSFIELDMAPPING': 'decision/decisionFieldMapping',
	'DELETEALLFIELDMAPPING': 'decision/deleteAlldecisionFieldMapping',
	'DELETEDSFIELDMAPPING': 'decision/decisionFieldMapping'

})
