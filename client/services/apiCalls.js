segmentationApp.service('apiCalls', function($http, $q, URLLIST, APIBASEURL,$popup) {
    
    /*Private Functions*/
    function Errored(error) {
        $popup.show({
            header: "Server Error - Please restart the application",
            message: error,
            okText: 'Yes'
        })
        .onOk(function() {
            // console.log(window);
            // window.close();
            // location.reload();
        });
    }

    /*Admin*/
    this.getLoggedInDetails = function() {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.LOGGEDINDETAILS)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                Errored(error);
                defer.reject(error);
            });

        return defer.promise;
    }
    this.getAdminData = function() {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.ADMINDATA)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                Errored(error);
                defer.reject(error);
            });

        return defer.promise;
    }
    this.getSfdcObjects = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.SFDCOBJECTS, {
                params: obj
            })
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                Errored(error);
                defer.reject(error);
            });

        return defer.promise;
    }

    this.adminGetParents = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.ADMINGETPARENTS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                Errored(error);
                defer.reject(error);
            });

        return defer.promise;
    }

    this.adminGetChilds = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.ADMINGETCHILDS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                Errored(error);
                defer.reject(error);
            });

        return defer.promise;
    }


    this.checkMappedField = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.CHECKMAPPEDFIELD, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.checksourceinstanceconfigdependencies = function(id) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.CHECKSOURCEINSTANCECONFIGDEPENDENCIES + '?id=' + id)
            .then(function(response) {
                defer.resolve(response);
            });

        return defer.promise;
    }

    this.deleteMappedField = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DELETEMAPPEDFIELD, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }


    this.addMapping = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.ADDMAPPING, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getObjectMapping = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.OBJECTMAPPING, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getExtSysObjs = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETEXTERNALSYSOBJS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.deleteInstanceObject = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DELETEINSTANCEOBJECT, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }



    this.sfCtrlStatusGet = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.SFCTRLSTATUSGET, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.sfCtrlStatusUpdate = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.SFCTRLSTATUSUPDATE, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }


    this.addExternalObject = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.ADDEXTERNALOBJECT, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }
    this.testUserConnection = function(User) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.TESTCONNECTION, {
                userName: User.username,
                password: User.pwd,
                securityToken: User.token,
                Source_Instance_Config_Id: User.Source_Instance_Config_Id || User.categoryID || undefined
            })
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.syncSfdcData = function(User) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.SYNCSFDCDATA, {
                Source_Instance_Config_Id: User.categoryID,
                siteId: User.siteid
            })
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.activateUser = function(User) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.ACTIVATEUSER, {
                Source_Instance_Config_Id: User.Source_Instance_Config_Id
            }, function(err) {
                defer.reject(err);
            })
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }


    this.deactivateUser = function(User) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DEACTIVATEUSER, {
                Source_Instance_Config_Id: User.Source_Instance_Config_Id
            }, function(err) {
                defer.reject(err);
            })
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

    this.saveUser = function(User) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.SAVEUSER, {
                User_name: User.username,
                Password: User.pwd,
                Token: User.token,
                Instance_type: User.selSysIns.Source_System_Instance_id,
                Description: User.desc,
                Source_System_Id: User.selSourceSystem.Source_System_Id,
                Status: User.status,
                Site_Id: User.siteid,
                Email: User.email,
                proceed: User.proceed,
                CategoryId: (User.categoryID) ? User.categoryID : ""
            }, function(err) {
                defer.reject(err);
            })
            .then(function(response) {
                console.log("inside front->saveUser ", response.data);
                var insts = response.data;
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }




    this.checkSyncStatus = function(id) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.CHECKSYNCSTATUS + "?id=" + id, function(err) {
                defer.reject(err);
            })
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }




    this.deleteUser = function(user) {
        var defer = $q.defer();
        $http.delete(APIBASEURL + URLLIST.DELETEUSER + '?id=' + user.Source_Instance_Config_Id + '&Site_Id=' + user.Site_Id, function(err) {
                defer.reject(err);
            })
            .then(function(response) {
                var insts = response.data;
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }

    this.deleteExtSource = function(id) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DELETEEXTSOURCE, {
                Source_Instance_Config_Id: id
            })
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getImportPriorityData = function() {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.IMPORTPRIORITY)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getAdminParentFields = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.ADMINGETPARENTFIELDS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    this.setImportPriorityData = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.SETIMPORTPRIORITYDATA, obj)
            .then(function(response) {
                defer.resolve(response.data);
            })
            .catch(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }




    /*Feeder*/

    this.getExistingFeeder = function() {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETEXISTINGFEEDER)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getFeederExtSystems = function(key) {
        var defer = $q.defer();

        $http.get(APIBASEURL + URLLIST.GETFEEDEREXTERNALSYSTEMS + '?key='+key)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getSelectedObjects = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETSELECTEDSFDCOBJECTS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.feederServiceMappedFields = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.FEEDERSERVICEMAPPEDFIELDS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.saveSegment = function(obj,key) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.SAVESEGMENT + '?key='+key, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }

    this.getParentsOfObject = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETPARENTOFOBJECT, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;

    }

    this.getParentFieldsAssoc = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETPARENTFIELDSASSOC, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;

    }

    this.getExistingSegments = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.EXISTINGSEGMENTS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    this.getExistingSegment = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETEXISTINGSEGMENT, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    this.updateContactsCount = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.UPDATECONTACTSCOUNT, obj)
            .then(function(response) {
                defer.resolve(response);
            });
        return defer.promise;
    }


    this.getContactRecords = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.CONTACTRECORDS, obj)
            .then(function(response) {
                defer.resolve(response);
            });
        return defer.promise;
    }




    this.getContactColumns = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.CONTACTCOLUMNS, obj)
            .then(function(response) {
                defer.resolve(response);
            });
        return defer.promise;
    }

    this.validateSOQL = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.VALIDATESOQL, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }



    this.saveDecisionSegment = function(obj,key) {
        var defer = $q.defer();

        
        console.log(APIBASEURL + URLLIST.SAVEDECISIONSEG + '?key='+key);



        $http.post(APIBASEURL + URLLIST.SAVEDECISIONSEG + '?key='+key, obj)
            .then(function(response) {
                defer.resolve(response);
            });

        return defer.promise;
    }

    this.getSOQLObjectFields = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETSOQLOBJECTFIELDS, obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(reject);
            });

        return defer.promise;
    }

    /***Sync Reports***/
    this.getExecutionNumbers = function(page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.EXECUTIONNUMBERS)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }
    this.getExecutions = function(page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.EXECUTIONS)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }
    this.getRecentImports = function(page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.RECENTIMPORTS)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }
    this.getRecentActions = function(page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.RECENTACTIONS)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

    this.getRecentErrors = function(page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.RECENTERRORS)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

    this.getAllImports = function(params) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.IMPORTS, {
                params: params
            })
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

    this.getAllErrors = function(params) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.ERRORS, {
                params: params
            })
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

    // acl services
    this.getacl = function(page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETACL + '?page=' + page)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }



    // acl services
    this.getacluserids = function() {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETACLUSERIDS)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }



    this.postacl = function(acl) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.POSTACL, acl)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }



    this.deleteacl = function(id) {
        var defer = $q.defer();
        $http.delete(APIBASEURL + URLLIST.DELETEACL + '/' + id)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }




    this.searchuser = function(text, page) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.SEARCHUSER + '?query=' + text + '&page=' + page + '&paginate=true')
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }



    /*Used for client side development*/
    /*this.getSelectedObjects = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETSELECTEDSFDCOBJECTS,obj)
            .then(function(response) {
                defer.resolve(response);
            });

        return defer.promise;
    }

    this.feederServiceMappedFields = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.FEEDERSERVICEMAPPEDFIELDS,obj)
            .then(function(response) {
                defer.resolve(response);
            });

        return defer.promise;
    }

    this.getExistingFeeder = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETEXISTINGFEEDER,obj)
            .then(function(response) {
                defer.resolve(response);
            });

        return defer.promise;
    }

    this.getParentsOfObject = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETPARENTOFOBJECT+obj.Object_Name,obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(response) {
                defer.resolve(response);
            });

        return defer.promise;

    }

    this.getParentFieldsAssoc = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.GETPARENTFIELDSASSOC+obj.Object_Name,obj)
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(response) {
                defer.resolve(response);
            });

        return defer.promise;

    }*/



    // decision service field mapping api

    this.dsObjectFields = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DSOBJECTFIELDS, obj)
            .then(function(response) {
                defer.resolve(response);
            });

        return defer.promise;
    }

    this.dsGetSelectedObjects = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DSGETSFDCOBJECTS, obj)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }
    
    this.setDSImportPriorityData = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DSSAVEIMPORTPRIORITY, obj)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }

    this.getDSImportPriorityData = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DSGETIMPORTPRIORITY, obj)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }

    this.dsGetEloquaRelatedData = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DSGETELOQUARELATEDDATA, obj)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });

        return defer.promise;
    }


    this.dsGetSyncReports = function(obj) {
        var defer = $q.defer();
        $http.get(APIBASEURL + URLLIST.DSSYNCDETAILS, {
                params: obj
            })
            .then(function(response) {
                defer.resolve(response);
            })
            .catch(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    }





    this.getDecisionFieldMapping = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.GETDSFIELDMAPPING, obj)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }




    this.postDecisionFieldMapping = function(payload) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.POSTDSFIELDMAPPING, payload)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }



    this.deleteDecisionFieldMapping = function(obj) {
        var defer = $q.defer();
        $http.delete(APIBASEURL + URLLIST.DELETEDSFIELDMAPPING ,{
                params:obj
            })
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

    this.deleteAllDecisionFieldMapping = function(obj) {
        var defer = $q.defer();
        $http.post(APIBASEURL + URLLIST.DELETEALLFIELDMAPPING)
            .then(function(response) {
                defer.resolve(response);
            }, function(err) {
                defer.reject(err);
            });
        return defer.promise;
    }

});