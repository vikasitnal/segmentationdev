segmentationApp

.service('$segmentationLoader',function($rootScope) {
    $('.segmentation-loader').hide();
    this.show = function() {
        $('.segmentation-loader').show();
    }

    this.hide = function() {
        $('.segmentation-loader').hide();
    }

})
.service('$alert',function($rootScope) {
	this.show = function(info) {
		$rootScope.Alert = {
			messageStatus:true
		}

		angular.extend($rootScope.Alert,info);
	}
})
.service('$popup', function($rootScope,$timeout) {
	var self = this,
		info;
    $(document).ready(function(){
    	/*$("#informationModal").on('show.bs.modal', function () {
    		angular.extend($rootScope.POPUP,info);
            $timeout(angular.noop,100);
        });*/
    });

    self.getInfo = function() {
        return info;
    }

    self.show = function(infoParam) {
    	info = infoParam;
    	$("#informationModal").modal({backdrop: false});
        $timeout(angular.noop,100);
    	return $rootScope.POPUP;
    }

    self.hide = function() {
        $rootScope.POPUP.messageList = [];
    	$("#informationModal").modal("hide");
    }


    $rootScope.POPUP = {
    	header:"",
    	message:"",
    	hide:self.hide,
    	onOk:function(callBack) {
    		if(typeof(callBack) == 'function'){
	    		this.ok = callBack;
    		}
    		return this;
    	},
    	onCancel:function(callBack) {
    		if(typeof(callBack) == 'function'){
    			this.cancel = callBack;
    		}
    		return this;
    	}
    };


});