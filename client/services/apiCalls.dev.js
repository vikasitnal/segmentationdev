segmentationApp.service('apiCalls', function($http, $q, URLLIST, APIBASEURL) {

    /*Used for client side development*/
    
    /*User*/
        this.getLoggedInDetails = function() {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.LOGGEDINDETAILS)
                .then(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;
        }

        this.getAdminData = function() {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.ADMINDATA)
                .then(function(response) {
                    setTimeout(function() {
                        defer.resolve(response);
                    }, 0);
                });

            return defer.promise;
        }
        
        this.getSfdcObjects = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.SFDCOBJECTS,{
                params:obj
            })
            .then(function(response) {
                setTimeout(function() {
                    defer.resolve(response);
                }, 0);
            });

            return defer.promise;
        }


        this.adminGetParents = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.ADMINGETPARENTS,obj)
                .then(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;
        }
        
        this.getAdminParentFields  = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.ADMINGETPARENTFIELDS,obj)
                .then(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;
        }
        
        this.getExtSysObjs = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETEXTERNALSYSOBJS,obj)
                .then(function(response) {
                    setTimeout(function() {
                        defer.resolve(response);
                    }, 0);
                });

            return defer.promise;
        }
        this.getObjectMapping = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.OBJECTMAPPING,obj)
                .then(function(response) {
                    setTimeout(function() {
                        defer.resolve(response);
                    }, 0);
                });

            return defer.promise;
        }

        this.saveUser = function(User) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.SAVEUSER)
            .then(function(response) {
                defer.resolve(response);
            });

            return defer.promise;
        } 

        this.syncSfdcData = function(User) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.SYNCSFDCDATA)
            .then(function(response) {
                defer.resolve(response);
            });

            return defer.promise;
        }

    /*Feeder*/
        this.getFeederExtSystems = function() {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETFEEDEREXTERNALSYSTEMS)
                .then(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;
        }

        this.getSelectedObjects = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETSELECTEDSFDCOBJECTS,obj)
                .then(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;
        }

        this.feederServiceMappedFields = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.FEEDERSERVICEMAPPEDFIELDS,obj)
                .then(function(response) {
                    setTimeout(function() {
                        defer.resolve(response);
                    }, 5000);
                });

            return defer.promise;
        }
        
        this.getExistingFeeder = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETEXISTINGFEEDER,obj)
                .then(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;
        }

        this.getParentsOfObject = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETPARENTOFOBJECT+obj.Object_Name,obj)
                .then(function(response) {
                    defer.resolve(response);
                })
                .catch(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;

        }

        this.getParentFieldsAssoc = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETPARENTFIELDSASSOC+obj.Object_Name,obj)
                .then(function(response) {
                    defer.resolve(response);
                })
                .catch(function(response) {
                    defer.resolve(response);
                });

            return defer.promise;

        }

        this.getExistingSegments = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.EXISTINGSEGMENTS)
                .then(function(response) {
                    defer.resolve(response);
                })
                .catch(function(response) {
                    defer.resolve(response);
                });
            return defer.promise;
        }



        this.getExistingSegment = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.GETEXISTINGSEGMENT)
                .then(function(response) {
                    defer.resolve(response);
                })
                .catch(function(response) {
                    defer.resolve(response);
                });
            return defer.promise;
        }
    	

        this.updateContactsCount = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.UPDATECONTACTSCOUNT,obj)
                .then(function(response) {
                    defer.resolve(response);
                });
            return defer.promise;
        }

        this.getContactRecords = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.CONTACTRECORDS,obj)
                .then(function(response) {
                    defer.resolve(response);
                });
            return defer.promise;
        }


        this.getContactColumns = function(obj) {
            var defer = $q.defer();
            $http.get(APIBASEURL + URLLIST.CONTACTCOLUMNS,obj)
                .then(function(response) {
                    defer.resolve(response);
                });
            return defer.promise;
        }

});
