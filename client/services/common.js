segmentationApp
.service('$common',function($rootScope) {

	this.insertStringAtPosition = function(string,replaceAbleString,pos) {
		return [string.slice(0, pos), replaceAbleString, string.slice(pos)].join('');
	}

})
