segmentationApp
.filter('typeDate', function() {
    return function(value) {
        if(typeof(value)=='object')
            return true;
        else
            return false;
    };
})
.filter('suffixedDate', function($filter) {
    function putSuffix(num) {
        if(num>9 && num < 14){
            return 'th';
        }

        switch(num%10){
            case 1:
                return 'st';
            case 2:
                return 'nd';
            case 3:
                return 'rd';
            default:
                return 'th';
        }
    }
    function convertDate(value) {
        var date = new Date(value).getDate();
        var suffixedDate = $filter('date')(value,'MMMM')+' '+date+putSuffix(date);
        return suffixedDate;
    }
    return function(value) {
        if(typeof(value)=='object'){
            return convertDate(value.getTime());
        } else {
            return convertDate(value);
        }
    };
})
.filter('conditionalDate', function($filter) {
    return function(value) {
        if(value && value.match)
            var dateStatus = value.match(/\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2]\d|3[0-1])T(?:[0-1]\d|2[0-3]):[0-5]\d:[0-5]\d\.\d{3}\+\d{4}/);
        if(dateStatus)
            return $filter('date')(value,'M/d/yyyy h:mm a');

        return value;
    };
})
.filter('sfdcDate', function() {
    return function(value,type) {
    	if(type=='date')
    		return value;
    	else
    		return '"'+value+'"';
    };
});