// app.js
var segmentationApp = angular.module('SegmentationApp',
    [
        'ui.router',
        'ui.bootstrap',
        'contextMenuModule',
        'd3Module',
        'ui.grid',
        'ui.grid.resizeColumns',
        // 'ui.grid.moveColumns',
        'ui.grid.autoResize',
        'uiSwitch',
        'angucomplete',
        'localytics.directives'
    ]);



segmentationApp
.config(function($stateProvider, $urlRouterProvider) {


    $urlRouterProvider.otherwise('/home');
    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html',
            controller:'User',
            controllerAs: 'User'
        })
        .state('feeder', {
            url: '/feeder',
            templateUrl: 'templates/feeder.html',
            controller:'Feeder',
            controllerAs: 'Feeder',
            onEnter: function(){
                angular.element('.segmentation-header').hide();
            },
            onExit: function(){
                angular.element('.segmentation-header').show();
            }
        })
        .state('decision', {
            url: '/decisionService',
            templateUrl: 'templates/decisionService.html',
            controller:'decisionService',
            controllerAs: 'decisionService',
            onEnter: function(){
                angular.element('.segmentation-header').hide();
            },
            onExit: function(){
                angular.element('.segmentation-header').show();
            }
        })
        .state('allImports', {
            url: '/Imports',
            templateUrl: 'templates/allImports.html',
            controller:'Imports',
            controllerAs: 'Imports',
        })
        .state('allErrors', {
            url: '/Errors',
            templateUrl: 'templates/allErrors.html',
            controller:'Errors',
            controllerAs: 'Errors',
        })
        .state('licenseExpired', {
            url: '/licenseExpired',
            templateUrl: 'templates/licenseExpired.html'
        })
        .state('accessDenied', {
            url: '/accessDenied',
            templateUrl: 'templates/accessDenied.html'
        });


})
.run(function(onload, $rootScope, apiCalls, $state, $timeout) {
    
    $rootScope.license = true;
    if(window.location.hash !== '#/accessDenied')
        return;

    
    apiCalls.getLoggedInDetails().then(function(result){
        $rootScope.license = result.data.license;
    });
});
